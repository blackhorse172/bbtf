<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "Create Sponsors";
$action = base_url().'Sponsors/save';
if($isEdit == true){
$action = base_url().'Sponsors/saveEdit';
}
?>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
							<?php if($isEdit == true) : ?><input type="hidden" name="id" value="<?= $data->id ?>"/><?php endif ?>

								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Title</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" id="title" name="title" placeholder="Enter your caption here">
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Image</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<?php if($isEdit == true) : ?>
												<?php if($data->image != '' || $data->image != null): ?>
												<img width="600px" id="image" class="img-responsive" src="<?= base_url() ?>files/sponsor/<?= $data->image ?>">
												<?php endif ?>
											<?php endif ?>
										<input type="file" class="form-control m-input" id="image" name="image">
										</div>
									</div>
								</div>
								<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">link</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" id="link" name="link" class="form-control m-input" required />
										</div>
									</div>
								
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Position</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<select class="form-control m-input" name="position" id="position" required>
												<option></option>
												<option value="1">Platinum</option>
												<option value="2">Gold</option>

											</select>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Publish</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<select class="form-control m-input" name="status" id="status" required>
												<option></option>
												<option value="1">Publish</option>
												<option value="0">UnPublish</option>

											</select>
										</div>
									</div>
									
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Submit</button>
												<button type="button" class="btn btn-secondary" onclick="window.location.href = '<?= site_url() ?>Suported'">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
						<?php if($isEdit == true) : ?>
<script>
$(document).ready(function(){
	$("#title").val("<?= $data->title; ?>");
	$("#status").val("<?= $data->status; ?>");
	$("#link").val("<?= $data->link; ?>");
	$("#position").val("<?= $data->position; ?>");




});
</script>
<?php endif ?>
