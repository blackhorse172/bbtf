<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid  m-error-1" style="background-image: url(<?= base_url() ?>assets/metronic/app/media/img/error/bg1.jpg);">
				<div class="m-error_container">
					<span class="m-error_number">
						<h1> <?= $title ?></h1>
					</span>
					<p class="m-error_desc">
						<?= $message ?>
					</p>
				</div>
			</div>
		</div>
