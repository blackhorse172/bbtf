<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo  CNF_APPNAME ;?></title>

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: { "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"] },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!--end::Web font -->
	<?php $this->load->view('layouts/admin/metronic/base'); ?>  
	
</head>
<body>
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">

        <!-- begin::Body -->
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		<div class="m-content">
						<div class="row">
							<div class="col-lg-12">
								<div class="m-portlet">
									<div class="m-portlet__body m-portlet__body--no-padding">
										<div class="m-invoice-2">
											<div class="m-invoice__wrapper">
												<div class="m-invoice__head" style="background-image: url(<?= base_url() ?>assets/app/media/img//logos/bg-6.jpg);">
													<div class="m-invoice__container m-invoice__container--centered">
														<div class="m-invoice__logo">
															<a href="#">
																<h1>RECEIPT</h1>
															</a>
															<a href="#">
																<img src="<?= base_url() ?>assets/logo/<?= $company->company_logo?>">
															</a>
														</div>
														<span class="m-invoice__desc">
															<span><?= $company->company_address ?></span>
															<!-- <span>Mississippi 96522</span> -->
														</span>
														<div class="m-invoice__items">
															<div class="m-invoice__item">
																<span class="m-invoice__subtitle">Transaction Date</span>
																<span class="m-invoice__text"><?= $trxHeader->trx_date ?></span>
															</div>
															<div class="m-invoice__item">
																<span class="m-invoice__subtitle">INVOICE NO.</span>
																<span class="m-invoice__text"><?= $invoiceNo ?></span>
															</div>
															<div class="m-invoice__item">
																<span class="m-invoice__subtitle">INVOICE TO.</span>
																<span class="m-invoice__text"><b><?= $user ?></b></span>
															</div>
														</div>
													</div>
												</div>
												<div class="m-invoice__body m-invoice__body--centered">
													<div class="table-responsive">
														<table class="table">
															<thead>
																<tr>
																	<th>DESCRIPTION</th>
																	<th>AMOUNT</th>
																	<th>PRICE</th>
																	<th>SUB TOTAL</th>
																</tr>
															</thead>
															<tbody>
																<?php foreach ($trxDetail as $key => $value) : ?>
																<tr>
																	<td><?= $value->product_name ?></td>
																	<td><?= $value->product_amount ?></td>
																	<td><?= number_format($value->product_price,2) ?></td>
																	<td class="m--font-danger">IDR <?= number_format($value->product_price,2) ?></td>
																</tr>
																<?php endforeach; ?>
																<!-- <tr>
																	<td>Tax</td>
																	<td><?= $company->tax ?>%</td>
																	<td><?= $trxHeader->trx_tax ?></td>
																	<td class="m--font-danger">IDR <?= number_format($trxHeader->trx_tax,2) ?></td>
																</tr>
																<tr>
																	<td>Service Tax</td>
																	<td><?= $company->service_tax ?>%</td>
																	<td><?= $trxHeader->trx_service_tax ?></td>
																	<td class="m--font-danger">IDR <?= number_format($trxHeader->trx_service_tax,2) ?></td>
																</tr> -->
															</tbody>
														</table>
													</div>
												</div>
												<div class="m-invoice__footer">
													<div class="m-invoice__table  m-invoice__table--centered table-responsive">
														<table class="table">
															<thead>
																<tr>
																	<th>BANK</th>
																	<th>ACC.NO.</th>
																	<th>ACC.NAME.</th>
																	<th>PAYMENT DATE</th>
																	<th>TOTAL AMOUNT</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td><?= $payment->bank_name ?></td>
																	<td><?= $payment->bank_acc_number ?></td>
																	<td><?= $payment->bank_acc_name ?></td>

																	<td><?= $payment->payment_date ?></td>
																	<td class="m--font-danger">IDR <?= number_format($trxHeader->trx_total,2) ?></td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

            </div>
        </div>
</body>	
		
<script type="text/javascript">
	jQuery(document).ready(function ($) {
		

	});
</script>
    
</body>
</html>


