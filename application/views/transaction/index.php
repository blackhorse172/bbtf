
<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "List Transaction";

?>

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= $title ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <!-- <li class="m-portlet__nav-item">
                    <a href="<?= site_url()?>Transaction/Create" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Create</span>
                        </span>
                    </a>
                </li> -->
                <li class="m-portlet__nav-item"></li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <div id="gridContainer"></div>
    </div>
</div>
<script>
    (function ($) {
        var dataSource = {
            load: function () {
                var items = $.Deferred();
                var data = <?= $data; ?>;
                items.resolve(data);
                return items.promise();
            }
        };
        $("#gridContainer").dxDataGrid({
            dataSource: dataSource,
            showBorders: true,
            showRowLines: true,
            columnAutoWidth: true,
            allowColumnResizing: true,
            allowColumnReordering: true,
            filterRow: {
                visible: true,
                applyFilter: "auto"
            },
            headerFilter: {
                visible: true
            },
            paging: {
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20],
                showInfo: true
            },
            columns: [
                {
                    caption: "Transaction Code",
                    dataField: "trx_code",
				},
				{
                    caption: "Transaction Status",
					dataField: "trx_status",
					cellTemplate: function (container, options) {
						//console.log(options);
                        var id = options.key.id;
                        var status = options.key.trx_status;
                        if (status == 0) {
                            $("<div>")
                                .append($("<button type='button'  class='btn m-btn--pill btn-danger'>WAITING PAYMENT</button>"))
                                .appendTo(container);
                        } else if(status == 1) {
                            
                            $("<div>")
                                .append($("<button type='button'  class='btn m-btn--pill btn-warning'>WAITING CONFIRMED</button>"))
                                .appendTo(container);
						}
						else if(status == 2) {
                            
                            $("<div>")
                                .append($("<button type='button'  class='btn m-btn--pill btn-success'>PAID</button>"))
                                .appendTo(container);
                        }

						else if(status == 3) {
                            
                            $("<div>")
                                .append($("<button type='button'  class='btn m-btn--pill btn-success'>VOID</button>"))
                                .appendTo(container);
                        }

                    }
				},
				{
                    caption: "LOC",
					dataField: "category_id",
					cellTemplate: function (container, options) {
						//console.log(options);
						var id = options.key.id;
                        var trx_code = options.key.trx_code;
						
						var category = options.key.category_id;
						
                        if (category == 2) {
							$("<div>")
                                .append($("<span onclick='flinkDownloadLoc(\""+trx_code+"\")' class='btn m-btn--pill btn-focus'>Download LOC</button>"))
                                .appendTo(container);
						}
						//  else if(status == 1) {
                            
                        //     $("<div>")
                        //         .append($("<button type='button'onclick='flinkEdit(\""+trx_code+"\")' class='btn m-btn--pill btn-warning'>CONFIRM PAYMENT</button>"))
                        //         .appendTo(container);
						// }
						// else if(status == 2) {
                            
                        //     $("<div>")
                        //         .append($("<button type='button'  class='btn m-btn--pill btn-success'>PAID</button>"))
                        //         .appendTo(container);
                        // }

                    }
				},
				{
                    caption: "Transaction Date",
					dataField: "trx_date",
					dataType: "date"
				},
				{
                    caption: "Transaction Type",
					dataField: "trx_type",
					
				},

				{
                    caption: "Transaction SubTotal",
					dataField: "trx_subtotal",
					dataType: "number",
                	format: "#,##0.##"
				},
				{
                    caption: "Tax",
					dataField: "trx_tax",
					dataType: "number",
                	format: "#,##0.##"
				},
				{
                    caption: "Service Tax",
					dataField: "trx_service_tax",
					dataType: "number",
                	format: "#,##0.##"
				},

				{
                    caption: "TOTAL",
					dataField: "trx_total",
					dataType: "number",
                	format: "#,##0.##"
				},

				{
                    caption: "ACTION",
					dataField: "trx_status",
					cellTemplate: function (container, options) {
						//console.log(options);
						var id = options.key.id;
                        var trx_code = options.key.trx_code;
						
                        var status = options.key.trx_status;
                        if (status == 0) {
                            $("<div>")
                                .append($("<button type='button' onclick='flinkEdit(\""+trx_code+"\")'  class='btn m-btn--pill btn-danger'>SEND PAYMENT</button>"))
                                .appendTo(container);
                        } else if(status == 1) {
                            
                            $("<div>")
                                .append($("<button type='button'  class='btn m-btn--pill btn-warning'>WAITING CONFIRMED</button>"))
                                .appendTo(container);
						}
						else if(status == 2) {
                            
                            $("<div>")
                                .append($("<button type='button' onclick='flinkInvoice(\""+trx_code+"\")'  class='btn m-btn--pill btn-success'>Print Receipt</button>"))
                                .appendTo(container);
                        }

						

                    }
				},
                

			],
			masterDetail: {
                                enabled: true,
                                template: function(container, options) { 
                                    var masterData = options.data;
                                    $("<div>")
                                        .addClass("show-detail")
                                        .text("Loading....")
                                        .appendTo(container);
                                    var trx_detail = "<?php echo base_url() ?>transaction/json_getDetail"
                                    $.ajax({
                                      url: trx_detail,
                                      type: 'post',
                                      dataType: 'json',
                                      data: {trx_code: masterData.trx_code,trx_type: masterData.trx_type },
                                    })
                                    .done(function(result) {
                                      data_detail = result;
                                      $("<div>")
                                        .addClass("master-detail-caption")
                                        .text("Transaction Detail")
                                        .appendTo(container);
                                    $("<div>")
                                        .dxDataGrid({
                                            columnAutoWidth: true,
                                            showBorders: true,
                                            columns: [
                                            {
                                                caption: "Product Name",
                                                dataField: "product_name"
                                                
                                            },{
                                                caption: "Product Amount",
                                                dataField: "product_amount",
                                                
                                            },
                                            {
                                                caption: "Product Price",
												dataField: "product_price",
												dataType: "number",
                                                 format: "Rp #,##0.##"
                                            }],
                                            dataSource: result
                                        }).appendTo(container);

                                        $('.show-detail').css("display","none");
                                    })
                                    .fail(function() {
                                      console.log("error");
                                    })
                                    .always(function() {
                                      
                                    });
                                }
                            }
        });
    })(jQuery);

    function fNonActive(id,title,msg) {
        swal({
            title: title,
            text: msg,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            //console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Transaction/nonActive/" + id,"_self");
            }
        })
    }
    function flinkEdit(id) {
                window.open("<?= base_url() ?>Payment/Create/" + id,"_self");

	}
	function flinkInvoice(id) {
                window.open("<?= base_url() ?>Transaction/invoice/" + id,"_blank");

	}

	function flinkDownloadLoc(id) {
                window.open("<?= base_url() ?>Seller/downloadLoc/" + id,"_blank");

	}
	
	
</script>

