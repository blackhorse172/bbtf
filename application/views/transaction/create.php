<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "Create Memberships";
$action = base_url().'Memberships/save';
if($isEdit == true){
$action = base_url().'Memberships/saveEdit';
}
?>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
							<?php if($isEdit == true) : ?><input type="hidden" name="id" value="<?= $data->membership_id ?>"/><?php endif ?>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Membership Name</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="name" id="name" aria-describedby="emailHelp" placeholder="Enter Name">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Membership Price</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input mask_number" name="fee" id="fee" aria-describedby="emailHelp" placeholder="Enter Price" required>
										</div>
									</div>
									
								<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Category</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<select class="form-control m-input" name="category_id" id="category_id" required>
												<option></option>
												<?php foreach($group as $item) : ?>
												<option value="<?= $item->category_id ?>"><?= $item->category_name ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
									
									
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Submit</button>
												<button type="button" onclick="window.location.href = '<?= base_url() ?>Memberships'" class="btn btn-secondary">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>

<?php if($isEdit == true) : ?>
<script>
$(document).ready(function(){
	$("#name").val("<?= $data->membership_name; ?>");
	$("#fee").val("<?= $data->membership_fee; ?>");
	$("#category_id").val("<?= $data->category_id; ?>");
	



});
</script>
<?php endif ?>
<script>

$(document).ready(function () {
	$(".mask_number").inputmask({
                      'alias': 'numeric',
                      'groupSeparator': '.',
                    'radixPoint': ',',
                      'digits': 0,
                      'autoGroup': true
				});
				
});
</script>
