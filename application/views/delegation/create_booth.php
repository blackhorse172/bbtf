<?php

//$this->load->view('layouts/admin/metronic/devextreme');
$title = "Update Seller Detail on ".$trxCode;
$action = base_url().'Delegations/updateBoothName';

?>
<style>
	.length25 {
		text-transform: uppercase;
	}
</style>
<div class="row">
<div class="col-xl-12">
<div class="m-portlet " id="m_portlet">
<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-file"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Seller Profile
												</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<!-- <li class="m-portlet__nav-item">
													<a href="#" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air">
														<span>
															<i class="la la-plus"></i>
															<span>Add Event</span>
														</span>
													</a>
												</li> -->
											</ul>
										</div>
									</div>

<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= base_url()?>Delegations/updateData">
<input type="hidden" name="id" value="<?= $profile['id'] ?>">
<?php 
		$seller_nameDisabled = "";
if($profile['is_packed'] == 0){
	if($profile['delegation_name'] != ""){
		$seller_nameDisabled = "disabled";
	}
} else {
	if($profile['delegation_name'] != ""){
		$seller_nameDisabled =  "disabled";
	}
	
}
											
											
?>
<div class="m-form__content">
										<div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg">
											<div class="m-alert__icon">
												<i class="la la-warning"></i>
											</div>
											<div class="m-alert__text">
												You can only submit once
											</div>
											<div class="m-alert__close">
												<button type="button" class="close" data-close="alert" aria-label="Close">
												</button>
											</div>
										</div>
									</div>
									
								<div class="m-portlet__body">
								<?php if($profile['is_packed'] == 0) : ?>
										<?php 
											$delegateDisabled =  $profile['delegation_name'] != "" ? "disabled" : "";
											$co_delegateDisabled =  $profile['co_delegate'] != "" ? "disabled" : "";
											$booth_nameDisabled =  $profile['booth_name'] != "" ? "disabled" : "";



										?>
										<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Full Delegate Name *</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" <?= $delegateDisabled ?> class="form-control m-input length25" maxlength="15" required  value="<?= $profile['delegation_name'] ?>" name="delegation_name" id="delegation_name" aria-describedby="delegation_name" placeholder="Delegate Name">
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Co Delegate Name *</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" <?= $co_delegateDisabled ?> class="form-control m-input length25" maxlength="15" required value="<?= $profile['co_delegate'] ?>" name="co_delegate" id="co_delegate" aria-describedby="co_delegation_name" placeholder="Co Delegate Name">
										</div>
									</div>

									<?php else: ?>
										<?php 
											$fullDisabled =  $profile['delegate_name'] != "" ? "disabled" : "";
											$booth_nameDisabled =  $profile['booth_name'] != "" ? "disabled" : "";

										?>

										<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Full Delegate Name *</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" <?= $seller_nameDisabled ?> class="form-control m-input length25" maxlength="15" required  value="<?= $profile['delegation_name'] ?>" name="delegation_name" id="delegation_name" aria-describedby="delegation_name" placeholder="Delegate Name">
										</div>
									</div>
										<?php if($profile['product_id']== 7 || $profile['product_id']== 8) :?>
											<div class="form-group m-form__group row">
												<label class="col-form-label col-lg-3 col-sm-12">Co Delegate Name *</label>
												<div class="col-lg-7 col-md-7 col-sm-12">
													<input type="text" <?= $seller_nameDisabled ?> class="form-control m-input length25" maxlength="15" required value="<?= $profile['co_delegate'] ?>" name="co_delegate" id="co_delegate" aria-describedby="co_delegation_name" placeholder="Co Delegate Name">
												</div>
											</div>
										<?php endif; ?>
									<?php endif; ?>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Booth Name *</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" <?= $seller_nameDisabled ?> class="form-control m-input length25" maxlength="25" required  value="<?= $profile['booth_name'] ?>" name="booth_name" id="booth_name" aria-describedby="booth_name" placeholder="booth_name">
										</div>
									</div>
									<hr>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" maxlength="25" <?= $seller_nameDisabled ?> class="form-control m-input length25" value="<?= $profile['seller_name'] ?>" name="seller_name" id="seller_name" aria-describedby="seller_name" placeholder="Company Name">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Address*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" <?= $seller_nameDisabled ?> value="<?= $profile['seller_address'] ?>" name="seller_address" id="seller_address" aria-describedby="bank_name" placeholder="Address">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">City*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" <?= $seller_nameDisabled ?> value="<?= $profile['seller_city'] ?>" name="seller_city" id="seller_city" aria-describedby="bank_name" placeholder="City">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Province*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" <?= $seller_nameDisabled ?> class="form-control m-input" value="<?= $profile['seller_province'] ?>" name="seller_province" id="seller_province" aria-describedby="notes" placeholder="Province">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Country*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select  class="form-control m-select2" id="seller_country_code" name="seller_country_code">
										<option value="">Select Country</option>
										
										<?php foreach ($country as $key => $value) : ?>
											<option <?= $seller_nameDisabled ?> value="<?= $value['country_code'] ?>"><?= $value['country_name'] ?></option>
										<?php endforeach;?>
									</select>	
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="email" class="form-control m-input" readonly value="<?= $profile['email_login'] ?>" name="email" id="email" aria-describedby="email" placeholder="email" disabled>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Phone*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input <?= $seller_nameDisabled ?> type="text" class="form-control m-input" value="<?= $profile['seller_phone'] ?>" name="seller_phone" id="seller_phone" aria-describedby="seller_phone" placeholder="Phone" required>
										</div>
									</div><div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Fax</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input <?= $seller_nameDisabled ?> type="text" class="form-control m-input" value="<?= $profile['seller_fax'] ?>" name="seller_fax" id="seller_fax" aria-describedby="seller_fax" placeholder="Fax">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Target Market*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<div class="m-checkbox-list">
											<?php 
											
											foreach ($target_market as $key => $value) : ?>
														<label class="m-checkbox">
															
															<input <?= $seller_nameDisabled ?> type="checkbox" id="<?=  str_replace(' ','_',$value['gs_value']) ?>" <?php ?> value="<?=  $value['gs_value'] ?>" name="seller_target_market[]"> <?=  $value['gs_value'] ?>
															<span></span>
														</label>
											<?php endforeach; ?>
														
										</div>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Category*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control m-select2" id="category_id" name="category_id">
										<option value="">Select Category</option>
										<?php foreach ($category as $key => $value) : ?>
											<option <?= $seller_nameDisabled ?> value="<?= $value['category_id'] ?>"><?= $value['category_name'] ?></option>
										<?php endforeach;?>
									</select>	
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Profile*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<textarea <?= $seller_nameDisabled ?> name="seller_company_profile" value="<?= $profile['seller_company_profile'] ?>" class="form-control" id="" rows="10"><?= $profile['seller_company_profile'] ?></textarea>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Website</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" <?= $seller_nameDisabled ?> class="form-control m-input" value="<?= $profile['seller_website'] ?>" name="seller_website" id="seller_website" aria-describedby="seller_website" placeholder="Website">
										</div>
									</div>
									
									
									
									<?php if($seller_nameDisabled == "") : ?>
										<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button  type="submit" class="btn btn-brand">Update</button>
											</div>
										</div>
									</div>
								</div>
									<?php endif; ?>
								
							</form>

</div>
</div>
</div>

<script>
	
$ (function ($) {
	'use strict';
	$(".m-select2").select2();
	$(".length25").maxlength({alwaysShow:!0,threshold:5,warningClass:"m-badge m-badge--primary m-badge--rounded m-badge--wide",limitReachedClass:"m-badge m-badge--brand m-badge--rounded m-badge--wide"});
	
	$("#seller_country_code").val("<?= $profile['seller_country_code'] ?>");
	$("#seller_country_code").select2().trigger('change');

	$("#category_id").val("<?= $profile['category_id'] ?>");
	$("#category_id").select2().trigger('change');

	var target = <?= $profile['seller_target_market'] ?>;

	$.each(target, function (i, e) { 
		 $("#"+e.replace(" ","_")).attr("checked",true);
	});

	$("#email").change(function (e) { 
		e.preventDefault();
		$.ajax({
			type: "get",
			url: "<?= base_url() ?>Seller/check_email_exist",
			data: {email: $(this).val()},
			dataType: "json",
			success: function (response) {
				if(response.status){
					$("#email").val("");
					$("#email").focus();
					swal({
                                title: "Warning",
                                text: "EMAIL IS EXIST, PLEASE LOG IN OR RESET PASSWORD",
                                type: "warning"
							});
					
					
				}
			}
		});
	});
});
</script>
