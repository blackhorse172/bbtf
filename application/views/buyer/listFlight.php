
<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "List Flight Buyer";

?>

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= $title ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <!-- <li class="m-portlet__nav-item">
                    <a href="<?= site_url()?>Memberships/Create" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Create</span>
                        </span>
                    </a>
                </li> -->
                <li class="m-portlet__nav-item"></li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <div id="gridContainer"></div>
    </div>
</div>
<script>
    (function ($) {
        var dataSource = {
            load: function () {
                var items = $.Deferred();
                var data = <?= $data; ?>;
                items.resolve(data);
                return items.promise();
            }
        };
        $("#gridContainer").dxDataGrid({
            dataSource: dataSource,
            showBorders: true,
            showRowLines: true,
            columnAutoWidth: true,
            allowColumnResizing: true,
            allowColumnReordering: true,
            filterRow: {
                visible: true,
                applyFilter: "auto"
            },
            headerFilter: {
                visible: true
            },
            paging: {
                pageSize: 10
			},
			groupPanel: {
                        visible: true
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20],
                showInfo: true
			},
			export: {
                enabled: true
            },
            columns: [

				

				{
                    caption: "Buyer Name",
                    dataField: "first_name",
				},

				{
                    caption: "Arrival Date",
                    dataField: "arrival_date",
					format: "date"
				},


				{
                    caption: "Arrival Time",
                    dataField: "arrival_time",
				},

				{
                    caption: "Arrival Flight Number",
                    dataField: "arrival_flight_number",
				},
				{
                    caption: "Arrival Terminal",
                    dataField: "arrival_terminal",
				},

				{
                    caption: "Arrival Transport",
                    dataField: "arrival_transport",
					cellTemplate: function (container, options) {
						var arrival_transport = options.key.arrival_transport;
						if(arrival_transport == 1) {
							$("<div>")
							.append("<span>Yes</span")
							.appendTo(container);
						} else {
							$("<div>")
							.append("<span>No</span")
							.appendTo(container);
						}
                   
                	}
                },

				{
                    caption: "depart Date",
                    dataField: "depart_date",
					format: "date"
				},


				{
                    caption: "depart Time",
                    dataField: "depart_time",
				},

				{
                    caption: "depart Flight Number",
                    dataField: "depart_flight_number",
				},
				{
                    caption: "depart Terminal",
                    dataField: "depart_terminal",
				},

				{
                    caption: "depart Transport",
                    dataField: "depart_transport",
					cellTemplate: function (container, options) {
						var arrival_transport = options.key.depart_transport;
						if(arrival_transport == 1) {
							$("<div>")
							.append("<span>Yes</span")
							.appendTo(container);
						} else {
							$("<div>")
							.append("<span>No</span")
							.appendTo(container);
						}
                   
                	}
                },
			

				
            

				
              

			],
			summary: {
                        totalItems: [
                            {
                                column: "company_name",
                                summaryType: "count",
                                //valueFormat:"#,##0.##"
                            },
                            
                        ]
                    }
        });
	})(jQuery);
	
	function flinkDelete(id,title,msg) {
        swal({
			title: "Delete",
            text: "Are You Sure ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>TradeBuyer/delete/" + id,"_self");
            }
        })
	}

	function fsetIgnore(id) {
        swal({
			title: "Ignore",
            text: "Do you want Set Ignore ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            //console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>TradeBuyer/setIgnoreStatus/" + id,"_self");
            }
        })
	}

	function fsetInv(id) {
        swal({
			title: "Resend Invoice",
            text: "Do you want Resend Invoice ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            //console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>TradeBuyer/resendInv/" + id,"_self");
            }
        })
	}
	

    function fNonActive(id,title,msg) {
        swal({
            title: title,
            text: msg,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>TradeBuyer/confirm/" + id,"_self");
            }
        })
	}
	
	function fNonActiveMembership(id,title,msg) {
        swal({
            title: title,
            text: msg,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Memberships/nonActiveSellerMembership/" + id,"_self");
            }
        })
	}
	
    function flinkEdit(id) {
                window.open("<?= base_url() ?>Memberships/TradeBuyer/" + id,"_self");

    }
</script>

