<div class="row">
<div class="col-xl-12">
<div class="m-portlet " id="m_portlet">


<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= base_url()?>Buyer/saveFlight">
<input type="hidden" name="buyer_id" value="<?= $profile['buyer_id'] ?>">
								
<div class="m-portlet__body">
<div class="m-form__content">
										<div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg">
											<div class="m-alert__icon">
												<i class="la la-warning"></i>
											</div>
											<div class="m-alert__text">
												You can only submit once
											</div>
											<div class="m-alert__close">
												<button type="button" class="close" data-close="alert" aria-label="Close">
												</button>
											</div>
										</div>
									</div>

<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-file"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Arrival Details
												</h3>
											</div>
										</div>
									</div>
									

								<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Arrival Date*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="arrival_date" name="arrival_date" required>
										<option value=""></option>
										<?php foreach ($arrivalDate as $key => $value) : ?>
											<option value="<?= $value['gs_value'] ?>"><?= date_format(date_create($value['gs_value']),"d-M-Y"); ?></option>
										<?php endforeach;?>
									</select>	
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Flight Number*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $flight['arrival_flight_number'] ?>" class="form-control m-input" name="arrival_flight_number" id="arrival_flight_number" aria-describedby="seller_name" placeholder="Flight Number" required>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Arrival Time*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="hidden" value="<?= $flight['arrival_time'] ?>" class="form-control m-input" name="arrival_time" id="arrival_time" aria-describedby="seller_name" placeholder="Arrival Time" required>
											<div id="arrivals_time"></div>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Terminal</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="arrival_terminal" name="arrival_terminal" required>
										<option value=""></option>
										<option value="DOMESIC">DOMESIC</option>
										<option value="INTERNATIONAL">INTERNATIONAL</option>
									</select>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Arrange for arrival transport</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<div class="m-radio-inline">
																	<label class="m-radio">
																		<input type="radio" id="arrival_transport_1" name="arrival_transport" value="1"> Yes
																		<span></span>
																	</label>
																	<label class="m-radio">
																		<input type="radio" checked id="arrival_transport_2" name="arrival_transport" value="0"> No
																		<span></span>
																	</label>
																</div>
										</div>
									</div>

									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-file"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Departure Details
												</h3>
											</div>
										</div>
									</div>

								<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Departure Date*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="depart_date" name="depart_date" required>
										<?php foreach ($departDate as $key => $value) : ?>
											<option value="<?= $value['gs_value'] ?>"><?=  date_format(date_create($value['gs_value']),"d-M-Y");  ?></option>
										<?php endforeach;?>
									</select>	
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Flight Number*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $flight['depart_flight_number'] ?>" class="form-control m-input" name="depart_flight_number" id="depart_flight_number" aria-describedby="seller_name" placeholder="Flight Number" required>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Departure Time*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="hidden" value="<?= $flight['depart_time'] ?>" class="form-control m-input" name="depart_time" id="depart_time" aria-describedby="seller_name" placeholder="Arrival Time" required>
										<div id="departure_time"></div>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Terminal</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="depart_terminal" name="depart_terminal" required>
										<option value=""></option>
										<option value="DOMESIC">DOMESIC</option>
										<option value="INTERNATIONAL">INTERNATIONAL</option>
									</select>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Arrange for Departure transport</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<div class="m-radio-inline">
										
																	<label class="m-radio">
																		<input type="radio" id="depart_transport_1"  name="depart_transport" value="1"> Yes
																		<span></span>
																	</label>
																	<label class="m-radio">
																		<input type="radio" checked id="depart_transport_2" name="depart_transport" value="0"> No
																		<span></span>
																	</label>
																</div>
										</div>
									</div>

									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-file"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Passport
												</h3>
											</div>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Passport</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
												<?php if($flight['buyer_passport'] != '' || $flight['buyer_passport'] != null): ?>
												<img width="600px" id="image_exits" class="img-responsive" src="<?= base_url() ?>files/passport/<?= $flight['buyer_passport'] ?>">
												<?php else: ?>
													<input type="file" class="form-control m-input" name="image" id="image">
												<?php endif ?>

										
										</div>
									</div>
									<?php if($flight == null) : ?>
										<div class="m-portlet__foot m-portlet__foot--fit">
											<div class="m-form__actions m-form__actions">
												<div class="row">
													<div class="col-lg-9 ml-lg-auto">
														<button type="submit" class="btn btn-brand">Update</button>

													</div>
												</div>
											</div>
										</div>
										<?php endif ?>

								
							</form>

</div>
</div>
</div>
<?php if($flight != null) : ?>

	<script>
	
	$ (function ($) {
		'use strict';
		$(".m-select2").select2();
	
		$("#arrival_date").val("<?= $flight['arrival_date'] ?>");
		$("#arrival_terminal").val("<?= $flight['arrival_terminal'] ?>");
	
		$("#depart_date").val("<?= $flight['depart_date'] ?>");
		$("#depart_terminal").val("<?= $flight['depart_terminal'] ?>");

		$("input[name=arrival_transport]").removeAttr("checked");
		$("input[name=depart_transport]").removeAttr("checked");

		if(<?= $flight['depart_transport'] ?> == 1){
			
			$("#depart_transport_1").attr("checked",true);
		} else {
			$("#depart_transport_2").attr("checked",true);
		}

		if(<?= $flight['arrival_transport'] ?> == 1){
			
			$("#arrival_transport_1").attr("checked",true);
		} else {
			$("#arrival_transport_2").attr("checked",true);
		}
		//depart_transport
			
		$("input").attr("disabled",true);
		$("select").attr("disabled",true);

		$("#departure_time").dxDateBox({
			displayFormat: "HH:mm",
			type: "time",
			width: '250',
			disabled: true,
			value: "<?= $flight['depart_time'] ?>"
		});

		$("#arrivals_time").dxDateBox({
			displayFormat: "HH:mm",
			type: "time",
			width: '250',
			disabled: true,
			value:"<?= $flight['arrival_time'] ?>"
		});

	
		
	});
	</script>
<?php else : ?>
<script>
	var date = new Date();
	var time = date.getHours()+":"+date.getMinutes();

	$("#depart_time").val(time);
	$("#arrival_time").val(time);


	$("#departure_time").dxDateBox({
    displayFormat: "HH:mm",
    type: "time",
	width: '250',
	value: new Date(),
	onValueChanged: function (e) {
            var previousValue = e.previousValue;
            var newValue = e.value;
			console.log(e);
			$("#depart_time").val($("#departure_time :hidden").val());
            // Event handling commands go here
        }
});

$("#arrivals_time").dxDateBox({
    displayFormat: "HH:mm",
    type: "time",
	width: '250',
	value: new Date(),
	onValueChanged: function (e) {
            var previousValue = e.previousValue;
            var newValue = e.value;
			console.log(e);
			$("#arrival_time").val($("#arrivals_time :hidden").val());
            // Event handling commands go here
        }
});

function formatHour(date) { 
	var time = date.getHours()+":"+date.getMinutes();

	return time;
 }
</script>
<?php endif; ?>
