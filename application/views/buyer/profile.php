<div class="row">
<div class="col-xl-12">
<div class="m-portlet " id="m_portlet">
<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-file"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Buyer Profile
												</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
											</ul>
										</div>
									</div>

<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= base_url()?>Memberships/saveBuyer">
								
<div class="m-portlet__body">
<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">mr/mrs*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<input type="text" class="form-control m-input" value="<?= $profile['gender'] ?>" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">First Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['first_name'] ?>" class="form-control m-input" name="first_name" id="first_name" aria-describedby="seller_name" placeholder="first_name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Last Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['last_name'] ?>" class="form-control m-input" name="last_name" id="last_name" aria-describedby="last_name" placeholder="last_name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['company_name'] ?>" class="form-control m-input" name="company_name" id="company_name" aria-describedby="bank_name" placeholder="company_name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Job Title</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['job_title'] ?>" class="form-control m-input" name="job_title" id="job_title" aria-describedby="job_title" placeholder="Job Title" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Profile*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<textarea name="buyer_company_profile" value="<?= $profile['buyer_company_profile'] ?>" class="form-control" id="" rows="10" required><?= $profile['buyer_company_profile'] ?></textarea>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Nature Of Business*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<input type="text" class="form-control m-input" value="<?= $profile['nature_of_business'] ?>" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">City*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<input type="text" class="form-control m-input" value="<?= $profile['buyer_city'] ?>" name="buyer_city" id="buyer_city" aria-describedby="buyer_city" placeholder="Job Title" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Country*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['country_name'] ?>" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<input type="text" class="form-control m-input" value="<?= $profile['buyer_email'] ?>" >
										
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Phone*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['buyer_phone'] ?>" name="buyer_phone" id="buyer_phone" aria-describedby="buyer_phone" placeholder="Phone" required>
										</div>
									</div>
									
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Website</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['buyer_website'] ?>" name="buyer_website" id="buyer_website" aria-describedby="buyer_website" placeholder="Website">
										</div>
									</div>
<!-- info -->
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Do you selling Bali and Beyond product?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<input type="text" class="form-control m-input" value="<?= $profile['do_you_selling_bali_and_beyond_product'] ?>" >
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Please state your local partner in Bali/ Indonesia (preferable name and email address)?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['please_state_you_local_partner_in_bali_indonesia'] ?>" name="please_state_you_local_partner_in_bali_indonesia" id="please_state_you_local_partner_in_bali_indonesia" aria-describedby="please_state_you_local_partner_in_bali_indonesia" placeholder="" required>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Your ID member of ASITA, IATA, PATA, ASEANTA or others member organization</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['member_id'] ?>" name="member_id" id="member_id" aria-describedby="member_id" placeholder="" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Since when you promoted Indonesia destination?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input"  value="<?= $profile['since_when_you_promoted_indonesia_destination'] ?>" name="since_when_you_promoted_indonesia_destination" id="since_when_you_promoted_indonesia_destination" aria-describedby="since_when_you_promoted_indonesia_destination" placeholder="" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Is this your first time attending on BBTF?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['is_this_your_first_time_attending_on_bbtf'] ?>"  name="is_this_your_first_time_attending_on_bbtf" id="is_this_your_first_time_attending_on_bbtf" aria-describedby="buyer_website" placeholder="" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">How did you know about BBTF?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['how_did_you_know_about_bbtf'] ?>"  name="how_did_you_know_about_bbtf" id="how_did_you_know_about_bbtf" aria-describedby="" placeholder="" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Indicated 3 main destinations in Indonesia you want to meet in BBTF?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['indicated_main_destination'] ?>" name="indicated_main_destination" id="indicated_main_destination" aria-describedby="how_did_you_know_about_bbtf" placeholder="" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Which products are you interested in?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<input type="text" class="form-control m-input" name="which_products_are_you_interested_in" value="<?= $profile['which_products_are_you_interested_in'] ?>" >
											
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Do you sell any package in South East Asia?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<input type="text" class="form-control m-input" name="do_you_sell_any_package_in_south_east_asia" value="<?= $profile['do_you_sell_any_package_in_south_east_asia'] ?>" >
											
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Please state your biggest partner in South East Asia (Company and Contact name)?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['biggest_partner'] ?>" class="form-control m-input" name="biggest_partner" id="biggest_partner" aria-describedby="biggest_partner" placeholder="" required>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Please state your potential group to Bali and Beyond in 2020-2021</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['potential_group'] ?>" class="form-control m-input" name="potential_group" id="potential_group" aria-describedby="potential_group" placeholder="" required>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Do you intending to explore beyond Bali after BBTF?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['explore_bbtf'] ?>" class="form-control m-input" name="explore_bbtf" id="explore_bbtf" aria-describedby="buyer_website" placeholder="" required>
										</div>
									</div>

									


								
							</form>

</div>
</div>
</div>
