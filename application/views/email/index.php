<div class="row">
<div class="col-xl-12">
<div class="m-portlet " id="m_portlet">
<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-file"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Testing Email
												</h3>
											</div>
										</div>
									</div>

<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= base_url()?>Email/send">
								<div class="m-portlet__body">
								
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Email*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="email" id="email" aria-describedby="bank_name" placeholder="email" required>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Message</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="message" id="message" aria-describedby="" placeholder="message" required>
										</div>
									</div>
									
									
									
									
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Submit Registration</button>
											</div>
										</div>
									</div>
								</div>
							</form>

</div>
</div>
</div>

<script>
	
$ (function ($) {
	'use strict';
	$(".m-select2").select2();
$("#terms").click(function (e) { 
	e.preventDefault();
	$("#termsModal").modal({ backdrop: "static", keyboard: false });
	$("#termsModal").modal("show");

});

	$("#email__").change(function (e) { 
		e.preventDefault();
		$.ajax({
			type: "get",
			url: "<?= base_url() ?>Media/check_email_exist",
			data: {email: $(this).val()},
			dataType: "json",
			success: function (response) {
				if(response.status){
					$("#email").val("");
					$("#email").focus();
					swal({
                                title: "Warning",
                                text: "EMAIL IS EXIST, PLEASE LOG IN OR RESET PASSWORD",
                                type: "warning"
							});
					
					
				}
			}
		});
	});
});
</script>
