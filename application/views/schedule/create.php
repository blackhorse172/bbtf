<?php
$title = "Detail Schedule";
$action = base_url().'Admin/Schedule/Save';
if($isEdit == true){
$action = base_url().'Admin/Schedule/save/'.$data->id;
}
?>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
							<?php if($isEdit == true) : ?><input type="hidden" name="id" value="<?= $data->id ?>"/><?php endif ?>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Name</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="meeting_name" value="<?php if($isEdit == true) echo $data->meeting_name; ?>" class="form-control m-input" id="meeting_name" placeholder="Enter Name" required>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Desc</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<textarea class="summernote" name="meeting_desc" id="content"><?php if($isEdit == true) echo $data->meeting_desc; ?></textarea>
										</div>
									</div>
								</div>
								<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Date</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="date" name="meeting_date" value="<?php if($isEdit == true) echo $data->meeting_date; ?>" class="form-control m-input" id="meeting_date" placeholder="Enter date" required>
										</div>
									</div>
									<?php if($isEdit == true) : ?>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">file</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<?php if($isEdit == true) : ?>
												<?php if($data->file != '' || $data->file != null): ?>
												<a href="<?= base_url() ?>files/schedule/<?= $data->file ?>" ><?= $data->file ?></a>
												<!-- <img width="600px" id="image_exits" class="img-responsive" src="<?= base_url() ?>files/product/<?= $data->image ?>"> -->
												<?php endif ?>
											<?php endif ?>
										<input type="file" class="form-control m-input" name="image" id="exampleInputEmail1">
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Note</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<textarea name="note" id="" cols="30" rows="10"><?php if($isEdit == true) echo $data->note; ?></textarea>
											<!-- <input type="text" name="meeting_name" value="" class="form-control m-input" id="note" placeholder="note" required> -->
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Status</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<select name="status" id="status" class="form-control" required>
												<option value=""></option>
												<option value="1">DONE</option>
												<option value="2">RESCEDULE</option>
											</select>
										</div>
									</div>

									<?php endif; ?>
								

								<!-- <div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Category</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<select class="form-control m-input" name="category_id" id="category_id" required>
												<option></option>
												<?php foreach($category as $item) : ?>
												<option value="<?= $item->category_id ?>"><?= $item->category_name ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div> -->
									<!-- <div class="form-group m-form__group row" id="parentForm" style="display:none;">
										<label class="col-form-label col-lg-3 col-sm-12">Parent</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<select class="form-control m-input" id="parent">
												<option></option>
												<?php foreach($parent as $item) : ?>
												<option value="<?= $item->id ?>"><?= $item->product_name ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div> -->
									<!-- <div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Product Price</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="price" class="form-control m-input mask_number" id="price" placeholder="Enter Price" required>
										</div>
									</div> -->
									
									<!-- <div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Product Discount</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="discount" class="form-control m-input mask_number" id="discount" placeholder="Enter discount">
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Discount End Date</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="date" name="discount_date" class="form-control m-input" id="discount_date" placeholder="Enter discount_date">
										</div>
									</div> -->
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="button" class="btn btn-secondary" onclick="window.location.href = '<?= site_url() ?>Schedule'">Back</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
<?php if($isEdit == true) : ?>
<script>
$(document).ready(function(){
$('input,select').attr('disabled',true);
$('#status').val(<?= $data->status ?>)
});
</script>
<?php endif ?>

<script type="text/javascript">
        $(document).ready(function(){
            $('.summernote').summernote({
                height: "300px",
                callbacks: {
                    onImageUpload: function(image) {
                        uploadImage(image[0]);
                    },
                    onMediaDelete : function(target) {
                        deleteImage(target[0].src);
                    }
                }
            });
 
            function uploadImage(image) {
                var data = new FormData();
                data.append("image", image);
                $.ajax({
                    url: "<?php echo site_url('Products/uploadImage')?>",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    type: "post",
                    success: function(url) {
                        $('.summernote').summernote("insertImage", url);
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            }
 
            function deleteImage(src) {
                $.ajax({
                    data: {src : src},
                    type: "POST",
                    url: "<?php echo site_url('Products/deleteImage')?>",
                    cache: false,
                    success: function(response) {
                        console.log(response);
                    }
                });
            }
 
        });
         
    </script>
<script>


$(document).ready(function () {

	$("#category_id").change(function (e) { 
		e.preventDefault();
		if($(this).val() == 2){
			$("#parent").attr("name","parent");
			$("#parent").attr("required",true);

			$("#parentForm").show();
		} else {
			$("#parentForm").hide();
			$("#parent").removeAttr("name");
			$("#parent").removeAttr("required");


		}
	});

	$(".mask_number").inputmask({
                      'alias': 'numeric',
                      'groupSeparator': '.',
                    'radixPoint': ',',
                      'digits': 0,
                      'autoGroup': true
				});
				
});
</script>
