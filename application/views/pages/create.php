<?php
$title = "Create Pages";
$action = base_url().'Pages/Save';
if($isEdit == true){
$action = base_url().'Pages/SaveEdit';
}
?>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
							<?php if($isEdit == true) : ?><input type="hidden" name="id" value="<?= $data->pageID ?>"/><?php endif ?>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Title</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="title" class="form-control m-input" id="title" placeholder="Enter post title" require>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Content</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<textarea class="summernote" name="content" id="content"><?php if($isEdit == true) echo htmlentities($data->content); ?></textarea>
										</div>
									</div>
								</div>

								<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Meta Keyword</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="meta_keyword" class="form-control m-input" id="meta_keyword" placeholder="Enter post title">
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Meta Desc</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="meta_desc" class="form-control m-input" id="meta_desc" placeholder="Enter post title">
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Sub Header Image</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<?php if($isEdit == true) : ?>
												<?php if($data->sub_image != '' || $data->sub_image != null): ?>
												<img width="600px" id="image_exits" class="img-responsive" src="<?= base_url() ?>files/sub_header/<?= $data->sub_image ?>">
												<?php endif ?>
											<?php endif ?>
										<input type="file" class="form-control m-input" name="image" id="exampleInputEmail1">
										</div>
									</div>
								
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Submit</button>
												<button type="button" class="btn btn-secondary" onclick="window.location.href = '<?= site_url() ?>Pages'">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
<?php if($isEdit == true) : ?>
<script>
$(document).ready(function(){
	$("#title").val("<?= $data->title; ?>");
	$("#meta_keyword").val("<?= $data->meta_keyword; ?>");
	$("#meta_desc").val("<?= $data->meta_desc; ?>");

	




});
</script>
<?php endif ?>

<script type="text/javascript">
        $(document).ready(function(){
            $('.summernote').summernote({
                height: "300px",
				codemirror: { // codemirror options
						theme: "monokai"
					},
                callbacks: {
                    onImageUpload: function(image) {
                        uploadImage(image[0]);
                    },
                    onMediaDelete : function(target) {
                        deleteImage(target[0].src);
                    },
					onInit : function(){
						//$(".btn-codeview").click()
						$('.summernote').summernote('codeview.toggle');
					},
					onBlurCodeview : function(){
						$('.summernote').summernote('codeview.deactivate');
						
					},
					// onFocus : function(){
					// 	$('.summernote').summernote('codeview.active');
						
					// } 
                }
            });
 
            function uploadImage(image) {
                var data = new FormData();
                data.append("image", image);
                $.ajax({
                    url: "<?php echo site_url('Pages/uploadImage')?>",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    type: "post",
                    success: function(url) {
                        $('.summernote').summernote("insertImage", url);
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            }
 
            function deleteImage(src) {
                $.ajax({
                    data: {src : src},
                    type: "POST",
                    url: "<?php echo site_url('Pages/deleteImage')?>",
                    cache: false,
                    success: function(response) {
                        console.log(response);
                    }
                });
            }
 
        });
         
    </script>
