<div class="row">
<div class="col-xl-12">
<div class="m-portlet " id="m_portlet">
<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-file"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Seller Profile
												</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<!-- <li class="m-portlet__nav-item">
													<a href="#" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air">
														<span>
															<i class="la la-plus"></i>
															<span>Add Event</span>
														</span>
													</a>
												</li> -->
											</ul>
										</div>
									</div>

<form class="m-form m-form--fit m-form--label-align-right">

								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['seller_name'] ?>" name="seller_name" id="seller_name" aria-describedby="seller_name" placeholder="Company Name">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Address*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['seller_address'] ?>" name="seller_address" id="seller_address" aria-describedby="bank_name" placeholder="Address">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">City*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['seller_city'] ?>" name="seller_city" id="seller_city" aria-describedby="bank_name" placeholder="City">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Province*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['seller_province'] ?>" name="seller_province" id="seller_province" aria-describedby="notes" placeholder="Province">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Country*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="seller_country_code" name="seller_country_code">
										
											<option value="<?= $value['country_code'] ?>"><?= $profile['country_name'] ?></option>
									
									</select>	
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="email" class="form-control m-input" value="<?= $profile['seller_email'] ?>" name="email" id="email" aria-describedby="email" placeholder="email" readonly>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Registration Person</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['registration_person'] ?>" name="registration_person" id="registration_person" aria-describedby="registration_person" placeholder="Registration Person">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Job Title</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['job_title'] ?>" name="job_title" id="job_title" aria-describedby="job_title" placeholder="Job Title" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Phone*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['seller_phone'] ?>" name="seller_phone" id="seller_phone" aria-describedby="seller_phone" placeholder="Phone">
										</div>
									</div><div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Fax</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['seller_fax'] ?>" name="seller_fax" id="seller_fax" aria-describedby="seller_fax" placeholder="Fax">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Target Market*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<input type="text" class="form-control m-input" value="<?= $profile['seller_target_market'] ?>" name="seller_fax" id="seller_fax" aria-describedby="seller_fax" placeholder="Fax">

										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Category*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="category_id" name="category_id" disabled>
										<?php foreach ($category as $key => $value) : ?>
											<option value="<?= $value['category_id'] ?>"><?= $value['category_name'] ?></option>
										<?php endforeach;?>
									</select>	
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Profile*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<textarea name="seller_company_profile" value="<?= $profile['seller_company_profile'] ?>" class="form-control" id="" rows="10"><?= $profile['seller_company_profile'] ?></textarea>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Website</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['seller_website'] ?>" name="seller_website" id="seller_website" aria-describedby="seller_website" placeholder="Website">
										</div>
									</div>
									
									
									
									
								
							</form>

</div>
</div>
</div>

<script>
	
$ (function ($) {


	$("#category_id").val("<?= $profile['category_id'] ?>");

	
});
</script>
