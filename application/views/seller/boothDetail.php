
<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "List Seller Booth Detail ".$profile['seller_name'];

?>

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= $title ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <!-- <li class="m-portlet__nav-item">
                    <a href="<?= site_url()?>Memberships/Create" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Create</span>
                        </span>
                    </a>
                </li> -->
                <li class="m-portlet__nav-item"></li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <div id="gridContainer"></div>
    </div>
</div>
<div id="resetPasswordModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<form class="form-inline" method="post" action="<?= base_url() ?>Seller/ResetPassword">
						<input type="hidden" id="buyerId" name="buyerId" value="">
						<input type="hidden" id="trxCode" name="trxCode" value="">

						<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">New Password</label>
										<div class="col-lg-12 col-md-7 col-sm-12">
											<input type="text" readonly name="newPassword" id="newPassword" class="form-control">	
										</div>
						</div>
						<br>
						<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										<button type="submit" class="btn btn-brand">Submit Password</button>
									</div>
						
				</form>
			</div>
		</div>
	</div>
</div>

<div id="boothNumberModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<form class="form-inline" enctype="multipart/form-data" method="post" action="<?= base_url() ?>Seller/updateBoothNumber">
						<input type="hidden" id="id" name="id" value="">
						<input type="hidden" id="trxCodeBooth" name="trxCode" value="">

						<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Booth Number</label>
										<div class="col-lg-12 col-md-7 col-sm-12">
											<input type="text" name="boothNumber" required id="boothNumber" class="form-control">	
										</div>
										<!-- <label class="col-form-label col-lg-3 col-sm-12">Layouts</label>
										<div class="col-lg-12 col-md-7 col-sm-12">
											<input type="file" name="image" id="image" class="form-control" required>	
										</div> -->
						</div>
						<br>
						<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										<button type="submit" class="btn btn-brand">Update Booth Number</button>
									</div>
						
				</form>
			</div>
		</div>
	</div>
</div>

<script>
    (function ($) {
        var dataSource = {
            load: function () {
                var items = $.Deferred();
                var data = <?= $data; ?>;
                items.resolve(data);
                return items.promise();
            }
        };
        $("#gridContainer").dxDataGrid({
            dataSource: dataSource,
            showBorders: true,
            showRowLines: true,
            columnAutoWidth: true,
            allowColumnResizing: true,
            allowColumnReordering: true,
            filterRow: {
                visible: true,
                applyFilter: "auto"
            },
            headerFilter: {
                visible: true
            },
            paging: {
                pageSize: 10
			},
			groupPanel: {
                        visible: true
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20],
                showInfo: true
			},
			export: {
                enabled: true
            },
            columns: [
                {
                    caption: "Company Name",
                    dataField: "seller_name",
				},

				// {
                //     caption: "Seller Category",
                //     dataField: "category_name",
				// },

				{
                    caption: "Seller email",
                    dataField: "email_login",
				},

				{
                    caption: "Seller phone",
                    dataField: "seller_phone",
				},

				{
                    caption: "Booth Name ",
                    dataField: "booth_name",
				},
                {
                    caption: "Booth Number ",
                    dataField: "booth_number",
				},

				{
                    caption: "Full Delegate",
                    dataField: "delegation_name",
				},

				{
                    caption: "Co Delegate",
                    dataField: "co_delegate",
				},
				{
                    caption: "Reset Password",
					cellTemplate: function (container, options) {
						//console.log(options);
                        var id = options.key.id;
                        var trxCode = options.key.ref_trans;

                       
						var password = options.key.password_login;

						if(password != null)
						{
							$("<div>")
                                .append($("<span onclick='fResetPassword(\""+id+"\",\""+trxCode+"\")' class='m-nav__link-badge'><span class='m-badge m-badge--focus m-badge--wide m-badge--rounded'>Reset Password </span></span>"))
                                .appendTo(container);
						}
                       
                            
                        }
				},

				{
                    caption: "Set Booth Number",
					cellTemplate: function (container, options) {
						//console.log(options);
                        var id = options.key.id;
                        var trxCode = options.key.ref_trans;

                       
						var password = options.key.password_login;

							$("<div>")
                                .append($("<span onclick='fboothNumber(\""+id+"\",\""+trxCode+"\")' class='m-nav__link-badge'><span class='m-badge m-badge--warning m-badge--wide m-badge--rounded'>Update Booth </span></span>"))
                                .appendTo(container);
						
                       
                            
                        }
				},

                {
                    caption: "password view",
                    dataField: "password_view",
				},
				// {
				// 	caption: "Seller Listing",
				// 	dataField: "is_listing",
				// 	cellTemplate: function (container, options) {
				// 		//console.log(options);
                //         var id = options.key.seller_id;
                        
				// 		var listing = options.key.is_listing;
				// 		var title = "";
				// 		if(listing == "" || listing == null || listing == 0)
				// 		{
				// 			$("<div>")
                //                 .append($("<span onclick='fSendListing(" + id + ")' class='m-nav__link-badge'><span class='m-badge m-badge--danger m-badge--wide m-badge--rounded'>Un Listing</span></span>"))
				// 				.appendTo(container);
								
				// 		} else {
				// 			$("<div>")
                //                 .append($("<span onclick='fSendListing(" + id + ")' class='m-nav__link-badge'><span class='m-badge m-badge--focus m-badge--wide m-badge--rounded'>Listing</span></span>"))
				// 				.appendTo(container);
				// 		}
                            
                //         }
				// },


                // {
                //     caption: "Active",
                //     dataField: "is_active",
                //     cellTemplate: function (container, options) {
				// 		console.log(options);
                //         var id = options.key.seller_id;
                //         var IsActive = options.key.is_active;
                //          var message = "";
                //         var title = "";
                //         if (IsActive != "1") {
                //              message = "Active ?";
                //             title = options.key.seller_name;
                //             $("<div>")
                //                 .append($("<button type='button' id='btnApprove_'" + id + "  onclick='fNonActive(" + id + ",\""+title+"\",\"" + message + "\")' class='btn m-btn--pill btn-danger'>NON ACTIVE</button>"))
                //                 .appendTo(container);
                //         } else {
                //              message = "Non Active ?";
                //             title = options.key.seller_name;
                //             $("<div>")
                //                 .append($("<button type='button' id='btnApprove_'" + id + " onclick='fNonActive(" + id + ",\""+title+"\",\"" + message + "\")'  class='btn m-btn--pill btn-success'>ACTIVE</button>"))
                //                 .appendTo(container);
                //         }

                //     }
				// },
				
				// {
                //     caption: "Membership",
                //     dataField: "is_membership",
                //     cellTemplate: function (container, options) {
				// 		console.log(options);
                //         var id = options.key.seller_id;
                //         var IsActive = options.key.is_membership;
                //          var message = "";
                //         var title = "";
                //         if (IsActive != "1") {
                //              message = "Verify Membership ?";
                //             title = options.key.seller_name;
                //             $("<div>")
                //                 .append($("<button type='button' id='btnApprove_'" + id + "  onclick='fNonActiveMembership(" + id + ",\""+title+"\",\"" + message + "\")' class='btn m-btn--pill btn-danger'>NONE</button>"))
                //                 .appendTo(container);
                //         } else {
                //              message = "Cancel Membership ?";
                //             title = options.key.seller_name;
                //             $("<div>")
                //                 .append($("<button type='button' id='btnApprove_'" + id + " onclick='fNonActiveMembership(" + id + ",\""+title+"\",\"" + message + "\")'  class='btn m-btn--pill btn-success'>ACTIVE</button>"))
                //                 .appendTo(container);
                //         }

                //     }
				// },
				// {
                // alignment: "center",
                // pinned: true,
                // cellTemplate: function (container, options) {
                //  var id = options.key.seller_id;
                //     $("<div>")
                //     //  .append($("<button type='button' onclick='flinkEdit(" + id + ")'  class='btn m-btn--pill btn-success'><i class='flaticon-edit'></i></button>"))
                //     .append($("<a onclick='flinkEdit(" + id + ")'  class='btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only'><i class='flaticon-edit'></i></a>"))

                //  .appendTo(container);
                // }
                // },
              

			],
			summary: {
                        totalItems: [
                            {
                                column: "seller_name",
                                summaryType: "count",
                                //valueFormat:"#,##0.##"
                            },
                            
                        ]
                    }
        });
    })(jQuery);

    function fNonActive(id,title,msg) {
        swal({
            title: title,
            text: msg,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
           
            if (e.value) {
                window.open("<?= base_url() ?>Memberships/nonActiveSeller/" + id,"_self");
            }
        })
	}
	
	function fNonActiveMembership(id,title,msg) {
        swal({
            title: title,
            text: msg,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
          
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Memberships/nonActiveSellerMembership/" + id,"_self");
            }
        })
	}
	
    function flinkEdit(id) {
                window.open("<?= base_url() ?>Memberships/Seller/" + id,"_self");

    }

	function fSendListing(id) {
        swal({
			title: "Listing",
            text: "Do you want listing / unlisting ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
           
            if (e.value) {
                window.open("<?= base_url() ?>Seller/listing/" + id,"_self");
            }
        })
    }

	function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
}
	
	function fResetPassword(id,trxCode) {
        swal({
			title: "Reset",
            text: "Do you want Reset Password ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
           
            if (e.value) {
				$("#resetPasswordModal").modal("show");
				var newPass = randomString(6,"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
				$("#newPassword").val(newPass);
				$("#buyerId").val(id);
				$("#trxCode").val(trxCode);


            }
        })
	}

	function fboothNumber(id,trxCode) {
   
		$("#id").val(id);
		$("#trxCodeBooth").val(trxCode);
		$("#boothNumberModal").modal("show");
	}

	

</script>

