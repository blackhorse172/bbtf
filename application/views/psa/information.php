
<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "PSA INFORMATION";

?>

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= $title ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <!-- <li class="m-portlet__nav-item">
                    <a href="<?= site_url()?>Memberships/Create" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Create</span>
                        </span>
                    </a>
                </li> -->
                <li class="m-portlet__nav-item"></li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
		<p><strong>GENERAL INFORMATION</strong></p>
<ol>
<li>PSA will be started on 11 May 2020.</li>
<li>Login password will be sent to registered email to our registration form and use that email for your login access.</li>
<li>All Buyers and Sellers have a total of 32 appointments per account.</li>
<li>We will open Two session
<ol>
<li>Session I : 11 - 16 May 2020</li>
<li>Session II : 18 &ndash; 23 May 2020</li>
</ol>
</li>
<li>The PSA system will only allow you to have maximum 25 confirmed appointments, the remaining 7 appointments will be matched during Buyer meet Seller session on 10 June 2020 at BNDCC</li>
<li>In PSA, you can only submit once time per session. Please make sure that you set all your request appointments accordingly within.</li>
<li>During the PSA sessions, any Buyers or Sellers that are highlighted in red means they are FULL</li>
</ol>
<p><strong>HOW TO SET UP YOUR APPOINTMENTS</strong></p>
<ol>
<li>Please go to the official website of BBTF 2020 and click the Login menu</li>
<li>Choose PSA menu then click APPOINTMENT menu.</li>
<li>You can see the Seller/Buyer's profile by clicking the "detail".</li>
<li>Remember that you can only submit once time per session</li>
</ol>
<p><strong>FIRST PSA SESSION</strong></p>
<ol>
<li>The first session is scheduled to take place from May 11<sup>th</sup> - 16<sup>th</sup> 2020 (local Bali time).</li>
<li>Each delegate can request and get maximum of 25 appointments confirmed</li>
<li>You can only submit your request once with maximum 32 request, so please make sure that you fill up all your requests accordingly.</li>
<li>The system will process your requests and match the appointment requests, a confirmed appointment will be locked in if both delegates requests the same slot.</li>
<li>A notification will be sent to you for any appointment requests received and you can confirm the GREEN bottom request on your login page.</li>
</ol>
<p><strong>SECOND PSA SESSION</strong></p>
<ol>
<li>The second PSA session will be held from May 18<sup>th</sup> &ndash; 23<sup>rd</sup> 2020 (local Bali time).</li>
<li>During the second session you can fill up your remaining free appointment slots (if any).</li>
<li>You can only submit your request once, so please make sure that you fill up all yours requests accordingly.</li>
<li>The system will process the requests for all matching appointment and lock it</li>
<li>A notification will be sent to you for any appointment requests received and you can confirm the GREEN bottom request on your login page.</li>
<li>The Comittee has the RIGHT to match Seller/Buyer based on BBTF 2020 interest.</li>
<li>The final PSA result can be viewed on your login page on May 26<sup>th</sup>, 2020.</li>
</ol>
    </div>
</div>

