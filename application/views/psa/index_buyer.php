<?php
//$title = "Schedule Table Color Settings";
//$action = base_url().'Settings/ScheduleTableColorSave';

?>
<style>
	.modal-lg {
		max-width: 100%;
	}
	.selected-data {
    margin-bottom: 20px;
    padding: 20px;
    background-color: rgba(191, 191, 191, 0.15);
}
</style>
<!--begin::Portlet-->
<div class="m-portlet ">
							<div class="m-portlet__body  m-portlet__body--no-padding">
								<div class="row m-row--no-padding m-row--col-separator-xl">
									<div class="col-md-12 col-lg-6 col-xl-3">

										<!--begin::Total Profit-->
										<div class="m-widget24">
											<div class="m-widget24__item">
												<h4 class="m-widget24__title">
													Slot Appointment
												</h4><br>
												<span class="m-widget24__desc">
													Default slot appointment
												</span>
												<span class="m-widget24__stats m--font-brand">
												<?= $defaultSlot ?>
												</span>
												<div class="m--space-10"></div>
												
											</div>
										</div>

										<!--end::Total Profit-->
									</div>
									<div class="col-md-12 col-lg-6 col-xl-3">

										<!--begin::New Feedbacks-->
										<div class="m-widget24">
											<div class="m-widget24__item">
												<h4 class="m-widget24__title">
													Request From Seller
												</h4><br>
												<span class="m-widget24__desc">
													Your Have Request
												</span>
												<span class="m-widget24__stats m--font-info">
													<?= $countConfirm ?>
												</span>
												<div class="m--space-10"></div>
											
											</div>
										</div>

										<!--end::New Feedbacks-->
									</div>

									<div class="col-md-12 col-lg-6 col-xl-3">

										<!--begin::New Feedbacks-->
										<div class="m-widget24">
											<div class="m-widget24__item">
												<h4 class="m-widget24__title">
													Request To Seller
												</h4><br>
												<span class="m-widget24__desc">
													Your Request
												</span>
												<span class="m-widget24__stats m--font-warning">
													<?= $countRequest ?>
												</span>
												<div class="m--space-10"></div>
											
											</div>
										</div>

										<!--end::New Feedbacks-->
									</div>
									
									<div class="col-md-12 col-lg-6 col-xl-3">

										<!--begin::New Users-->
										<div class="m-widget24">
											<div class="m-widget24__item">
												<h4 class="m-widget24__title">
													Remaining Slot
												</h4><br>
												<span class="m-widget24__desc">
													Your Remaining Slot
												</span>
												<span class="m-widget24__stats m--font-danger">
												<?= $remainingSlot ?>
												</span>
												<div class="m--space-10"></div>
												
											</div>
										</div>

										<!--end::New Users-->
									</div>
								</div>
							</div>
						</div>
						<div class="m-portlet m-portlet--tabs">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text">
													PSA Tabs
												</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="nav nav-tabs m-tabs-line m-tabs-line--right" role="tablist">
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link active show" data-toggle="tab" href="#m_portlet_base_demo_1_tab_content" role="tab" aria-selected="true">
														<i class="flaticon-multimedia"></i> Seller
													</a>
												</li>
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_portlet_base_demo_2_tab_content" role="tab" aria-selected="false">
														<i class="flaticon-reply"></i> Request From Seller
													</a>
												</li>
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_portlet_base_demo_3_tab_content" role="tab" aria-selected="false">
														<i class="flaticon-calendar"></i> My Schedule
													</a>
												</li>
											</ul>
										</div>
									</div>
									<div class="m-portlet__body">
										<div class="tab-content">
											<div class="tab-pane active show" id="m_portlet_base_demo_1_tab_content" role="tabpanel">
											<div class="m-portlet m-portlet--mobile">
												<div class="m-portlet__head">
													<div class="m-portlet__head-caption">
														<div class="m-portlet__head-title">
															<h3 class="m-portlet__head-text">
																<?= $title ?>
															</h3>
														</div>
													</div>
													<div class="m-portlet__head-tools">
														<ul class="m-portlet__nav">
															<li class="m-portlet__nav-item">
																<?php if($countRequest == 0) : ?>
																<a onclick="fSubmit()" class="btn m-btn--square  btn-accent" id="btnSaveReq">
																	<span>
																		<i class="la la-plus"></i>
																		<span>Submit Request</span>
																	</span>
																</a>
																<?php endif ?>
															</li>
															<li class="m-portlet__nav-item"></li>
														</ul>
													</div>
												</div>
												<div class="m-portlet__body">
													<!--begin: Datatable -->
													<div class="selected-data">
															<span class="caption">Selected Records:</span>
															<span id="selected-items-container"></span>
													</div>
													<div id="gridContainer"></div>

												</div>
											</div>
											</div>
											<div class="tab-pane" id="m_portlet_base_demo_2_tab_content" role="tabpanel">
											<div class="m-portlet m-portlet--mobile">
												<div class="m-portlet__head">
													<div class="m-portlet__head-caption">
														<div class="m-portlet__head-title">
															<h3 class="m-portlet__head-text">
																<?= $title ?>
															</h3>
														</div>
													</div>
													
												
												</div>
												
												<div class="m-portlet__body">
													<!--begin: Datatable -->
													<!-- <div class="selected-data">
															<span class="caption">Selected Records:</span>
															<span id="selected-items-container"></span>
													</div> -->
													<div id="gridContainerConfirm"></div>

												</div>
											</div>
											</div>
											<div class="tab-pane" id="m_portlet_base_demo_3_tab_content" role="tabpanel">
											<div id="gridContainerSchedule"></div>
											</div>
										</div>
									</div>
								</div>


<div id="userDetail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="userDetail-title" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="userDetail-title">Detail</h5>
				<button class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="detailBody"></div>
			</div>
			<div class="modal-footer">
				
			</div>
		</div>
	</div>
</div>
							<!--end::Form-->
						</div>
						<script>
							var grid,
								gridConfirm,
								gridSchedule,
								selectedItemsDataGrid,
								changedBySelectBox,
								justDeselected;
    (function ($) {
        var dataSource = {
            load: function () {
                var items = $.Deferred();
                var data = <?= $data; ?>;
                items.resolve(data);
                return items.promise();
            }
		};
		
		var dataSourceConfirmList = {
            load: function () {
                var items = $.Deferred();
                var data = <?= $dataConfirmList; ?>;
                items.resolve(data);
                return items.promise();
            }
		};

		var dataSourceSchedule = {
            load: function () {
                var items = $.Deferred();
                var data = <?= $scheduleData; ?>;
                items.resolve(data);
                return items.promise();
            }
		};
		
        grid = $("#gridContainer").dxDataGrid({
            dataSource: dataSource,
            keyExpr: "id",
            showBorders: true,
            showRowLines: true,
            columnAutoWidth: true,
            allowColumnResizing: true,
            allowColumnReordering: true,
            filterRow: {
                visible: true,
                applyFilter: "auto"
            },
            headerFilter: {
                visible: true
            },
            paging: {
                pageSize: 20
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [10, 20,40, 60],
                showInfo: true
            },
            selection: {
                mode: "multiple"
            },
            onCellPrepared: function (e) {
                
              
                if (e.rowType == "data") {
                   
					if (e.data.status == 0 || e.data.status_from_seller == 0 ){
						e.cellElement.css('background', 'yellow');
						// e.cellElement.find('.dx-select-checkbox').dxCheckBox("instance").option("disabled", true);
                        // e.cellElement.off();
						
					 }else if (e.data.status == 1 || e.data.status_from_seller == 1 ) 
                        e.cellElement.css('background', 'green');
                     else if (e.data.status == 2 || e.data.status_from_seller == 2 ) 
                         e.cellElement.css('background', 'red');

					if(e.rowType === "data" & e.column.command === 'select' && (e.data.status == 0 || e.data.status == 1 || e.data.status == 2)) {
						e.cellElement.find('.dx-select-checkbox').dxCheckBox("instance").option("selected", true);
                        e.cellElement.find('.dx-select-checkbox').dxCheckBox("instance").option("disabled", true);
						e.cellElement.find('.dx-select-checkbox').dxCheckBox("instance").option("value", true);

                        e.cellElement.off();
                    }

					if(e.rowType === "data" & e.column.command === 'select' && (e.data.status_from_seller == 0 || e.data.status_from_seller == 1 || e.data.status_from_seller == 2)) {
						
						e.cellElement.find('.dx-select-checkbox').dxCheckBox("instance").option("disabled", true);
						e.cellElement.find('.dx-select-checkbox').dxCheckBox("instance").option("selected", true);
						e.cellElement.find('.dx-select-checkbox').dxCheckBox("instance").option("value", true);
						e.cellElement.off();
                       
                    }
                }
            },
			onSelectionChanged: function(e) {
				var data = e.selectedRowsData;
				var select = 0;
				if(data.length > 0) {
					$.each(data, function (i, v) { 
						select = select + 1;
						$("#selected-items-container").text(select);
						
					});
					if(select > <?= $defaultSlot ?>)
						{
							showAlert("ALERT","Max Request is <?= $defaultSlot ?>");
							$('#btnSaveReq').hide();
						} else {
							$('#btnSaveReq').show();
						}
				}else {
					$("#selected-items-container").text("Nobody has been selected");
				} 
					
			},
			onRowPrepared: function(e) {  
					e.rowElement.css({ height: 10});  
			},
            columns: [
                {
                    caption: "Company Name",
                    dataField: "company_name",
				},
				{
                    caption: "Country",
                    dataField: "country_name",
				},
				
				{
                alignment: "center",
                pinned: true,
                cellTemplate: function (container, options) {
                 var id = options.key.id;
				
						$("<div>")
                    .append($("<a style='height:20px;' onclick='fUserDetail(" + id + ")'  class='btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only'><i class='flaticon-eye'></i></a>"))					
                 	.appendTo(container);
					
                   
                }
                },
				
                

			],
			
		}).dxDataGrid("instance");
		//grid confirm
		gridConfirm = $("#gridContainerConfirm").dxDataGrid({
            dataSource: dataSourceConfirmList,
            keyExpr: "id",
            showBorders: true,
            showRowLines: true,
            columnAutoWidth: true,
            allowColumnResizing: true,
            allowColumnReordering: true,
            filterRow: {
                visible: true,
                applyFilter: "auto"
            },
            headerFilter: {
                visible: true
            },
            paging: {
                pageSize: 20
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [10, 20,40, 60],
                showInfo: true
            },
            // selection: {
            //     mode: "multiple"
            // },
            onCellPrepared: function (e) {
                
              
                if (e.rowType == "data") {
                    var status = e.data.status;
                    
                     if (e.data.status == 0 || e.data.status_from_seller == 0 ){
						e.cellElement.css('background', 'yellow');
						// e.cellElement.find('.dx-select-checkbox').dxCheckBox("instance").option("disabled", true);
                        // e.cellElement.off();
						
					 }else if (e.data.status == 1 || e.data.status_from_seller == 1 ) 
                        e.cellElement.css('background', 'green');
                     else if (e.data.status == 2 || e.data.status_from_seller == 2 ) 
                         e.cellElement.css('background', 'red');

					if(e.rowType === "data" & e.column.command === 'select' && (e.data.status == 0 || e.data.status == 1 || e.data.status == 2)) {
						e.cellElement.find('.dx-select-checkbox').dxCheckBox("instance").option("selected", true);
                        e.cellElement.find('.dx-select-checkbox').dxCheckBox("instance").option("disabled", true);
						e.cellElement.find('.dx-select-checkbox').dxCheckBox("instance").option("value", true);

                        e.cellElement.off();
                    }

					if(e.rowType === "data" & e.column.command === 'select' && (e.data.status_from_seller == 0 || e.data.status_from_seller == 1 || e.data.status_from_seller == 2)) {
						
						e.cellElement.find('.dx-select-checkbox').dxCheckBox("instance").option("disabled", true);
						e.cellElement.find('.dx-select-checkbox').dxCheckBox("instance").option("selected", true);
						e.cellElement.find('.dx-select-checkbox').dxCheckBox("instance").option("value", true);
						e.cellElement.off();
                       
                    }
                }
            },
			onSelectionChanged: function(e) {
				var data = e.selectedRowsData;
				var select = 0;
				if(data.length > 0) {
					$.each(data, function (i, v) { 
						select = select + 1;
						$("#selected-items-container").text(select);
						
					});
					if(select > <?= $defaultSlot ?>)
						{
							showAlert("ALERT","Max Request is <?= $defaultSlot ?>");
							$('#btnSaveReq').hide();
						} else {
							$('#btnSaveReq').show();
						}
				}else {
					$("#selected-items-container").text("Nobody has been selected");
				} 
					
			},
			onRowPrepared: function(e) {  
					e.rowElement.css({ height: 10});  
			},
            columns: [
                {
                    caption: "Company Name",
                    dataField: "company_name",
				},
				{
                    caption: "Country",
                    dataField: "country_name",
				},
				
				{
                alignment: "center",
                pinned: true,
                cellTemplate: function (container, options) {
                 var id = options.key.id;
				
						$("<div>")
					.append($("<a style='height:20px;' onclick='fUserDetail(" + id + ")'  class='btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only'><i class='flaticon-eye'></i></a>"))					
					.append($("<span> </span>"))					

					.append($("<a style='height:20px;' onclick='fConfirmPsa(" + id + ")'  class='btn btn-default m-btn m-btn--icon btn-sm m-btn--icon-only'><i class='la la-check'></i></a>"))					

                 	.appendTo(container);
					
                   
                }
                },
				
                

			],
			
		}).dxDataGrid("instance");

		gridSchedule = $("#gridContainerSchedule").dxDataGrid({
            dataSource: dataSourceSchedule,
            keyExpr: "id",
            showBorders: true,
            showRowLines: true,
            columnAutoWidth: true,
            allowColumnResizing: true,
            allowColumnReordering: true,
            filterRow: {
                visible: true,
                applyFilter: "auto"
            },
            headerFilter: {
                visible: true
            },
            paging: {
                pageSize: 20
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [10, 20,40, 60],
                showInfo: true
            },
            selection: {
                mode: "multiple"
            },
            
            columns: [
                {
                    caption: "Company Name",
                    dataField: "seller_name",
				},
				{
                    caption: "Time Start",
                    dataField: "time_start",
				},
				{
                    caption: "Time End",
                    dataField: "time_end",
				},

				{
                    caption: "Year",
                    dataField: "year",
				},
				{
                    caption: "Periode",
                    dataField: "periode",
				},
			],
			
		}).dxDataGrid("instance");
		
    })(jQuery);

	function fUserDetail(id) {
		$.ajax({
			type: "GET",
			url: "<?= site_url() ?>"+ "Seller/getProfilePsa/"+id,
			data: id,
			dataType: "html",
			success: function (response) {
				$('#detailBody').html(response);
			}
		});
		$('#userDetail').modal('show');
		
	  }

    function fNonActive(id,title,msg) {
        swal({
            title: title,
            text: msg,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Transaction/nonActive/" + id,"_self");
            }
        })
    }

	function fSubmit()
	{
		//console.log(grid.getSelectedRowsData());
		data =  grid.getSelectedRowsData();
		if(data.length > 0)
		{
			swal({
				title: "Submit Request",
				text: "Are You Sure For Submit ? hou have submit for once!",
				type: "warning",
				showCancelButton: !0,
				confirmButtonText: "Yes!"
			}).then(function (e) {
				
				if (e.value) {
				$.ajax({
					type: "POST",
					url: "<?= site_url() ?>"+"Psa/"+"saveReqPsa",
					data: {user : data},
					dataType: "json",
					success: function (response) {
						if(response.status == 1)
						{
							showAlert("SUBMIT","Save Succesfull !");
							location.reload();

						}else {
							showAlert("SUBMIT","UPS, PLEASE CONTACT US FOR INFO !");
						}
					}
				});
				}
			})
		} else {
			showAlert("ALERT","Please Select Data");
		}
		
	}

	function fConfirmPsa(id)
	{
		//console.log(grid.getSelectedRowsData());
		//data =  gridConfirm.getSelectedRowsData();
		// if(data.length > 0)
		// {
			swal({
				title: "Confirm",
				text: "Are You Sure Confirm This User ? ",
				type: "warning",
				showCancelButton: !0,
				confirmButtonText: "Yes!"
			}).then(function (e) {
				
				if (e.value) {
				$.ajax({
					type: "POST",
					url: "<?= site_url() ?>"+"Psa/"+"ConfirmPsa",
					data: {user : id},
					dataType: "json",
					success: function (response) {
						if(response.status == 1)
						{
							showAlert("SUBMIT","Save Succesfull !");
							location.reload();

						}else if(response.status == 0){
							showAlert("SUBMIT","Schedule Seller is FULL !");
						}else {
							showAlert("SUBMIT","UPS, PLEASE CONTACT US FOR INFO !");
						}
					}
				});
				}
			})
		// } else {
		// 	showAlert("ALERT","Please Select Data");
		// }
		
	}

    function flinkEdit(id) {
                window.open("<?= base_url() ?>Schedule/create/" + id,"_self");

    }
</script>


