<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "Create Slider";
$action = base_url().'Slider/save';
if($isEdit == true){
$action = base_url().'Slider/saveEdit';
}
?>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
							<?php if($isEdit == true) : ?><input type="hidden" name="slider_id" value="<?= $data->slider_id ?>"/><?php endif ?>

								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Title</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" id="title" name="title" placeholder="Enter your caption here">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Description</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" id="desc" name="desc" placeholder="Enter your description here">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Image</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<?php if($isEdit == true) : ?>
												<?php if($data->image != '' || $data->image != null): ?>
												<img width="600px" id="image" class="img-responsive" src="<?= base_url() ?>files/slider/<?= $data->image ?>">
												<?php endif ?>
											<?php endif ?>
										<input type="file" class="form-control m-input" id="image" name="image">
										</div>
									</div>
								</div>
								<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Category</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control m-input" name="category_id" id="category_id" required>
												<option></option>
												<?php foreach($category as $item) : ?>
												<option value="<?= $item->category_id ?>"><?= $item->category_name ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Publish</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<select class="form-control m-input" name="status" id="status" required>
												<option></option>
												<option value="1">Publish</option>
												<option value="0">UnPublish</option>

											</select>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">End Show</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="date" id="end_show" name="end_show" class="form-control m-input" />
										</div>
									</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Submit</button>
												<button type="button" class="btn btn-secondary" onclick="window.location.href = '<?= site_url() ?>Slider'">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
						<?php if($isEdit == true) : ?>
<script>
$(document).ready(function(){
	$("#title").val("<?= $data->title; ?>");
	$("#status").val("<?= $data->status; ?>");
	$("#category_id").val("<?= $data->category_id; ?>");
	$("#end_show").val("<?= $data->end_show; ?>");
	$("#desc").val("<?= $data->descr; ?>");
	

});
</script>
<?php endif ?>
