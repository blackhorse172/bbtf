<div class="col-md-12 px-3 p-md-0">
                <div class="filter-content overflow-hidden">
				<ul class="portfolio-grid work-5col hover-option4 gutter-medium">
						<li class="grid-sizer"></li>
						
						<?php if(count($gallery) > 0) : ?>
						<?php foreach ($gallery as $key => $v) : ?>
<!-- start portfolio-item item -->

<li class="grid-item <?= $v->category_id ?> wow fadeInUp">
                            <a href="<?= base_url().'files/gallery/'.$v->image ?>" title="<?= $v->title ?>">
                                <figure>
								<div class="portfolio-img bg-gold"><img src="<?= base_url().'files/gallery/'.$v->image ?>" class="project-img-gallery" /></div>
                                    <figcaption>
                                        <div class="portfolio-hover-main text-center">
                                            <div class="portfolio-hover-box vertical-align-middle">
                                                <div class="portfolio-hover-content position-relative">
                                                    <span class="font-weight-600 letter-spacing-1 alt-font text-uppercase margin-5px-bottom display-block"><?= $v->descr ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </li>
                        <!-- end portfolio item -->
						<?php endforeach;?>
					
						<?php endif; ?>
                        
                        
                    </ul>
                </div>
            </div>
