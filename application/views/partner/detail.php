
<!--Content-->				
<section class="no-padding wow fadeIn bg-light-gray" style="visibility: visible; animation-name: fadeIn; height: auto;">
    <div class="container-fluid padding-30px-lr">
        <div class="row equalize sm-equalize-auto">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 cover-background sm-height-500px xs-height-350px wow fadeInLeft" style="background-image: url(<?= base_url() ?>assets/frontend/images/sp.jpg); visibility: visible; animation-name: fadeInLeft; height: 535px;">
              <div class="xs-height-400px"></div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 wow fadeInRight" style="visibility: visible; animation-name: fadeInRight; height: 535px;">
                <div class="padding-ten-lr padding-three-tb pull-left md-padding-ten-all sm-no-padding-lr xs-padding-50px-tb xs-no-padding-lr">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sm-margin-40px-bottom xs-margin-30px-bottom xs-no-padding-lr">
                        <h2 class="alt-font text-extra-dark-gray sm-width-100 sm-margin-lr-auto xs-width-100 text-uppercase font-weight-700 sm-no-margin-bottom"><?= $detail['title'] ?></h2>
                    </div>
                    <div class="col-2-nth">
                        <!-- start description item-->
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-six-bottom padding-20px-right md-no-padding-right md-margin-30px-bottom xs-margin-30px-bottom xs-no-padding last-paragraph-no-margin">
                          <a href="#" class="alt-font post-title text-large text-link-gold w-100 d-block lg-width-100 margin-15px-bottom" style="font-weight: 700;"><h3 style="font-weight: 600;">IDR <?= number_format($detail['price'],2) ?></h3></a>
                            <h6>Provided by our official partner</h6>
                            <h4><?= $detail['tour_name'] ?></h4>
                            <p class="width-90 xs-width-100"><?= $detail['desc'] ?></p>
                        </div>
                        <!-- end description item-->
                        <!-- <div class="author sm-margin-30px-bottom">
                            <a class="btn btn-medium btn-orange lg-margin-15px-bottom margin-10px-top d-table d-lg-inline-block md-margin-lr-auto xs-margin-30px-bottom" href="#">GET SPECIAL PRICE</a>
                        </div> -->
                    </div>
                </div>
            </div> 
        </div>
    </div>
</section>
<!--Content End-->
<?php
$limit = 4;
$query = "SELECT * FROM official_partner where id != ".$detail['id']." AND is_active=1
ORDER BY RAND()
LIMIT ".$limit." ";

$q = $this->db->query($query);
$otherPartner = $q->result_array();
?>
<!--Other Offer -->
<section>
    <div class="padding-25px-lr margin-20px-top">
        <div class="row"> 
          <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 text-LEFT" style="margin-bottom: 3px;">
                    <h4>
                    OTHER SPECIAL OFFER FROM OUR OFFICIAL PARTNERS</h4>
				</div>
				<?php if(count($otherPartner) > 0) : ?>
				<?php foreach ($otherPartner as $key => $value) : ?>

                <!-- start tour item -->
                <div class="col-md-3 col-sm-6 col-xs-12 margin-50px-bottom last-paragraph-no-margin xs-margin-30px-bottom wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                    <div class="blog-post bg-white">
                        <div class="blog-post-images overflow-hidden position-relative">
                            <a href="<?= base_url() ?>Partner/detail/<?= $detail['id'] ?>">
                                <img src="<?= base_url() ?>assets/frontend/images/sp.jpg" alt="" data-no-retina="">
                            </a>
                        </div>
                        <div class="post-details padding-15px-all md-padding-20px-all card">
                            <a href="<?= base_url() ?>Partner/detail/<?= $detail['id'] ?>" class="alt-font post-title text-large text-link-gold w-100 d-block lg-width-100 margin-15px-bottom" style="font-weight: 700;"><?= $value['title'] ?></a>
                            <span class="d-block margin-20px-top"><span class="glyphicon glyphicon-map-marker"></span> <?= $value['location'] ?></span>
                            <div class="separator-line-horrizontal-full bg-medium-gray margin-20px-tb"></div>
                            <a href="<?= base_url() ?>Partner/detail/<?= $detail['id'] ?>" class="alt-font post-title text-large text-link-gold w-100 d-block lg-width-100 margin-15px-bottom" style="font-weight: 700;">IDR <?= number_format($value['price'],2) ?></a>
                            <!-- <div class="author">
                                <a class="btn btn-medium btn-orange lg-margin-15px-bottom margin-10px-top d-table d-lg-inline-block md-margin-lr-auto" href="#">Details</a>
                            </div> -->
                        </div>
                    </div>
                </div>
                <!-- end tour item -->
					<?php endforeach;?>
			
			<?php else: ?>
					<h3> No Data....</h3>
			<?php endif; ?>
                       
                
                        
              </div>
            </div>

        </div>
      </div>
</section>
<!--Other Tour End-->
