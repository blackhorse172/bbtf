<?php
$title = "Company";
$action = base_url().'Company/Save';

?>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
							<?php if($isEdit == true) : ?><input type="hidden" name="id" value="<?= $data->id ?>"/><?php endif ?>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Name</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $data->company_name ?>" name="company_name" class="form-control m-input" id="company_name" placeholder="Enter post company_name">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Logo</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<?php if($isEdit == true) : ?>
												<?php if($data->company_logo != '' || $data->company_logo != null): ?>
												<img width="600px" id="image_exits" class="img-responsive" src="<?= base_url() ?>files/company/<?= $data->company_logo ?>">
												<?php endif ?>
											<?php endif ?>
										<input type="file" class="form-control m-input" name="image" id="exampleInputEmail1">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Addres</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text"  value="<?= $data->company_address ?>" name="company_address" class="form-control m-input" id="company_address" placeholder="Enter post company_name">
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Email</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text"  value="<?= $data->company_email ?>" name="company_email" class="form-control m-input" id="company_email" placeholder="Enter post company_name">
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Telp</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text"  value="<?= $data->company_telp ?>" name="company_telp" class="form-control m-input" id="company_telp" placeholder="Enter post company_name">
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Telp 2</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text"  value="<?= $data->company_telp2 ?>" name="company_telp2" class="form-control m-input" id="company_telp2" placeholder="Enter post company_name">
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Tax</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text"  value="<?= $data->tax ?>" name="tax" class="form-control m-input" id="tax" placeholder="Enter post company_name">
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Tax Service</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text"  value="<?= $data->service_tax ?>" name="service_tax" class="form-control m-input" id="service_tax" placeholder="Enter post company_name">
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Image</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<?php if($isEdit == true) : ?>
												<?php if($data->caption != '' || $data->caption != null): ?>
												<img width="600px" id="caption" class="img-responsive" src="<?= base_url() ?>files/company/<?= $data->caption ?>">
												<?php endif ?>
											<?php endif ?>
										<input type="file" class="form-control m-input" id="image" name="caption">
										</div>
									</div>

									<!-- <div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Caption</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text"  value="<?= $data->caption ?>" name="caption" class="form-control m-input" id="caption" placeholder="Enter post company_name">
										</div>
									</div> -->

								</div>
								
									
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Submit</button>
												<button type="button" class="btn btn-secondary" onclick="window.location.href = '<?= site_url() ?>Article'">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>



