<div style='border:1px solid #99bdfc; background-color: #f2f6fe; padding: 30px; width:700px;'>
      <p>&nbsp;</p>
      <p>Dear, <?= $buyer_name ?></p>
      <p>&nbsp;</p>
      <p><b>Greeting from Bali & Beyond Travel Fair  <?= $year ?></b></p>

      <p>&nbsp;</p><p>The Committee of BBTF  <?= $year ?> thank you for your interest to participate in the upcoming Bali and Beyond Travel Fair  <?= $year ?>.<br />
	  <p>&nbsp;</p>

	  We have been receiving your request,however we deeply regret to inform that we are not able to confirm your request as our buyer participants has been over quota of our capacity at this time. As much as we would like to welcome your as our buyer at BBTF  <?= $year ?>,however our space are very much limited. Hence we do apologize for not being able to accommodate you.</p>
	  
	  <p>&nbsp;</p>
	  <p>We thank you once again for your kind understanding and we hope that we can collaborate in BBTF 2020.</p>
      <p>&nbsp;</p>
	
	
	  <p>With our best regards,<br>
	  Admin for Buyer Division<br />
	  <b>BBTF Secretariat Office</b></p>
	  <b>Jl. Raya Puputan No.41 Renon, Denpasar 80235, Bali, Indonesia</b></p>
          <b>T : +62 361 243 225 , F. +62 361 244 263</b></p>
          <b>E.: buyer@bbtf.or.id</b></p>
          <b>W.: www.bbtf.or.id</b></p>
	  <p><img src='<?= site_url() ?>assets/logo/logo_1.png'></p>
      </div>
