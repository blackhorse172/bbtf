<div style='border:1px solid #99bdfc; background-color: #f2f6fe; padding: 30px; width:700px;'>
      <table border='0'>
        <tr>
          <td><img src='<?= site_url() ?>assets/logo/logo_1.png'></td>
        </tr>
      </table>

     <p>Date: <?= $invoice_date ?><br />
	Due date:  <?= $invoice_due_date ?></b></p>
      <p>&nbsp;</p>
      <p>Dear, <?= $buyer_name ?></p>
      <p>&nbsp;</p>
      <p>Warmest greetings from BALI & BEYOND TRAVEL FAIR (BBTF) <?= $year ?>..!</p>

      <p>&nbsp;</p><p>We sincerely thank you very much indeed for your participation to be part as a BUYER in the upcoming event BALI & BEYOND TRAVEL FAIR (BBTF) <?= $year ?> for which will be scheduled to take place in Bali from the date of <?= $date ?> <?= $year ?>. After reviewing and internal discussion within the Committee of BBTF <?= $year ?>, we are delighted to inform you that your BUYER classification at BBTF <?= $year ?> as the following conditions:</p>

      <p>&nbsp;</p><p align='center'><b><u>BUYER Classification :</u></b></p>
      <p align='center' style='border:1px solid #f00;padding:15px 30px;border-radius:9px;background:#f00;color:#fff;font-size:180%;font-weight:bold;clear:both;width:230px;margin:5px auto 15px;'><?= $buyer_type ?></p>

      <p><b>As a BUYER at BBTF <?= $year ?>, you are eligible to receive:</b></p>
      <!-- !bbtf_buyer_eligible_n_arrangement -->
	 
		<style>
			a{
				text-decoration: none;
			}
			table.detil{
				border-top:1px solid #ccc;
				border-left:1px solid #ccc;
			}
			table.detil td{
				border-bottom:1px solid #ccc;
				border-right:1px solid #ccc;
			}
			input.bt_submit{
				padding: 5px 25px;
				background: #549BC8;
				color: #fff;	
				border:1px solid #2566A7;
				font-weight: bold;
				cursor: pointer;
			}
			
			p.ad a{
				border: 1px solid #000;
				color:#fff;
				background-color: #87B941;
				padding:8px 65px;
				text-decoration: none;
				font-weight: bold;
				border-radius: 5px;
			}
			p.ad a.d{
				background-color: #f00;
				white-space: nowrap;
			}
		</style>
		
		<p>&nbsp;</p>
        <table border='0' cellpadding='10' cellspacing='0' class='detil' style='margin-bottom:5px;' width='700'>
			<tr bgcolor='#FFff99'>
              <td width='30' align='center bgcolor='#FFff99''>NO.</td> 
              <td width='400' bgcolor='#FFff99'>ITEM</td>  
              <td width='120' align='right' bgcolor='#FFff99'>PRICE / IDR</td>
            </tr>
			      <tr bgcolor='#FFffee'>
              <td align='center' bgcolor='#FFffee'>1.</td>
              <td bgcolor='#FFffee'>NON Refundable Registration Fee</td> 
				      <td align='right' bgcolor='#FFffee'><?= $fee ?></td>
            </tr>
            <tr bgcolor='#FFffcc'><td colspan='2' align='right' bgcolor='#FFffcc'>Total</td> <td align='right' bgcolor='#FFffcc'><b> <?= $fee ?></b></td></tr>
		</table>
          
	  <p>&nbsp;</p>
	  
	  <p></p>
	  
	  
	  <table border='0' cellpadding='0' cellspacing='0'>
	  		<tr>
				<td width='50%'><strong>CREDIT CARD PAYMENT</strong></td>
				
				<td></td>
							
			</tr>
	  		<tr valign='top'>
				<td width='80%' style='padding-top:10px'>
				
					<p style='margin-top: 10px;'><a href='http://bbtf.or.id/dokupay/!bbtf_tkid/!bbtf_token' style='background-color:#55AF55; color: #fff; border-radius: 5px; border: 1px solid #57C852; padding: 9px 20px;'><b>REGISTRATION FEE</b></a></p>
					<p style='font-size: 90%; margin-top: 10px;'>note: Using this credit card will charge you 4% of total amount</p>
												
				</td>
				
				<td style='vertical-align:top!important;padding-top:22px;'>
				
					<p align='center' class='ad'><a href='http://bbtf.or.id/decline-by-buyer/!bbtf_tkid/!bbtf_token' class='d'>DECLINE</a></p>
					
				</td>
													
			</tr>			
	  </table>
	  
	  <p>&nbsp;</p>
	  
      <p>&nbsp;</p><p>Should you require any further information about BALI & BEYOND TRAVEL FAIR (BBTF) <?= $year ?>, please feel free to reach out to the Committee at your convenience Or simply visit our website at www.bbtf.or.id</p>

 

      <p>&nbsp;</p><p>&nbsp;</p><p>Warmest Regards, <p>
 
      <p>&nbsp;</p><p>&nbsp;</p>Sekretariat of BBTF <?= $year ?></p><p>&nbsp;</p><p>&nbsp;</p>

 

      <p style='border:solid #ff8d00; border-width:1px 0; background:#fce5d5; padding:15px;'><b>BALI & BEYOND TRAVEL FAIR (BBTF) <?= $year ?> </b><br />
      <u>Secretariat Office:</u><br />
      Jl. Raya Puputan No. 41, Renon, Denpasar 80235, Bali<br />
      P. 62 (+361) 445 7111 F. (+62 361) 244 263<br />
      E-mail: buyer@bbtf.or.id<br />
      Website: <u><a href='http://bbtf.or.id'>www.bbtf.or.id</a></u></p>
      </div>
