<ul>
          <li>Refundable Economy Airfare</li>
          <li>Four (04) nights stay (accommodation) during 26 - 30 June 2018.</li>
          <li>Inter transfers: airport - official hotel - airport and official hotel - venue - official hotel during main event</li>
          <li>NON Refundable Registration Fee - IDR. <?= $fee ?> (Indonesian Currency)</li>
          <li>Meals during BBTF !bbtf_year program</li>
      </ul>

      <p>&nbsp;</p>
      <p><b>In association with the above BUYER classification, please find the following arrangement related to the Pre Appointment Scheduled as a BUYER :</b></p>
      <ul>
          <li>To participate all activities during BBTF !bbtf_year event</li>
          <li>To complete all Pre Appointment Scheduled at least 34 appointments for Platinum Domestic buyer during two (2) full days program.</li>
      </ul>
