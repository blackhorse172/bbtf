
<div style='border:1px solid #99bdfc; background-color: #f2f6fe; padding: 30px; width:700px;'>
     	<img src='<?= site_url() ?>assets/logo/logo_1.png'>
		 <p><strong>Dear <?= $gender ?>.<?= $buyer_name ?></strong></p>
<p><strong>Greeting from BBTF <?= $year ?></strong></p>
<p>&nbsp;</p>
<p>Since your confirmation is a matter for us, we do apologize to release your registration from our system.</p>
<p>This consideration is objectively decided by the committee of BBTF <?= $year ?> because we did not receive any confirmation within the due date required.</p>
<p>Once again, thank you very much for your enthusiasm to registered our BBTF <?= $year ?> event and we do hope to see you on our next BBTF event.</p>
<p>&nbsp;</p>
<p>Sincerely yours</p>
<p>&nbsp;</p>
<p>Committee of BBTF <?= $year ?></p>
<p>&nbsp;</p>
<p style='border:solid #ff8d00; border-width:1px 0; background:#fce5d5; padding:15px;'><b style='font-size:18px;'>BBTF Secretariat Office</b><br />
      Jl. Raya Puputan No. 41 Renon, Denpasar 80235, Bali Indonesia<br />
      P : +62 361 4457111 ; 4457700<br />
	  F: +62 361 244 263<br />
	  E: info@bbtf.or.id<br />
      W: www.bbtf.or.id</p>
      </div>
