<style>
	#company_name {
		text-transform:uppercase;
	}
</style>
<div class="row">
<div class="col-xl-12">
<div class="m-portlet " id="m_portlet">
<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-file"></i>
												</span>
												<h3 class="m-portlet__head-text">
													NEW Trade Buyer DATA
												</h3>
											</div>
										</div>
									</div>

<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= base_url()?>TradeBuyer/create">
								<div class="m-portlet__body">
								
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['company_name'] ?>" maxlength="25" name="company_name" id="company_name" aria-describedby="bank_name" placeholder="company_name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Address*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['company_address'] ?>" name="company_address" id="company_address" aria-describedby="bank_name" placeholder="company_address" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">City*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<input type="text" class="form-control m-input" value="<?= $profile['city'] ?>"  name="city" id="city" aria-describedby="city" placeholder="city" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Country*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<input type="text" value="<?= $profile['country_name'] ?>">
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Office Phone*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['company_phone'] ?>" class="form-control m-input" name="company_phone" id="company_phone" aria-describedby="company_phone" placeholder="company_phone" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<label class="col-form-label col-lg-3 col-sm-12"><?= $profile['company_email'] ?></label>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Full Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['full_name'] ?>" class="form-control m-input" maxlength="15" name="full_name" id="full_name" aria-describedby="seller_name" placeholder="full_name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Position</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['position'] ?>" class="form-control m-input" name="position" id="position" aria-describedby="postion" placeholder="postion">
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Mobile</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['mobile'] ?>" class="form-control m-input" name="mobile" id="mobile" aria-describedby="mobile" placeholder="mobile" >
										</div>
									</div>
									
							</form>

</div>
</div>
</div>

<script>
	$(document).ready(function () {
		$("#country_code").val("<?= $profile['country_code'] ?>")
	});
</script>
