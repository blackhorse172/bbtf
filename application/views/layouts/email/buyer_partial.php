<div style='border:1px solid #99bdfc; background-color: #f2f6fe; padding: 30px; width:700px;'>
      <table border='0'>
        <tr>
          <td><img src='<?= site_url() ?>assets/logo/logo_1.png'></td>
        </tr>
      </table>

      <p>&nbsp;</p>
      <p>Dear, <?= $buyer_name ?></p>
      <p>&nbsp;</p>
      <p>Warmest greetings from BALI & BEYOND TRAVEL FAIR (BBTF) <?= $year ?>..!</p>

      <p>&nbsp;</p><p>We sincerely thank you very much indeed for your participation to be part as a BUYER in the upcoming event BALI & BEYOND TRAVEL FAIR (BBTF) <?= $year ?> for which will be scheduled to take place in Bali from the date of 9 - 13 Juni <?= $year ?>. After reviewing and internal discussion within the Committee of BBTF <?= $year ?>, we are delighted to inform you that your BUYER classification at BBTF <?= $year ?> as the following conditions:</p>

      <p>&nbsp;</p><p align='center'><b><u>BUYER Classification :</u></b></p>
      <p align='center' style='border:1px solid #f00;padding:15px 30px;border-radius:9px;background:#f00;color:#fff;font-size:180%;font-weight:bold;clear:both;width:230px;margin:5px auto 15px;'><?= $buyer_type ?></p>

      <style>
			a{
				text-decoration: none;
			}
			table.detil{
				border-top:1px solid #ccc;
				border-left:1px solid #ccc;
			}
			table.detil td{
				border-bottom:1px solid #ccc;
				border-right:1px solid #ccc;
			}
			input.bt_submit{
				padding: 5px 25px;
				background: #549BC8;
				color: #fff;	
				border:1px solid #2566A7;
				font-weight: bold;
				cursor: pointer;
			}
			
			p.ad a{
				border: 1px solid #000;
				color:#fff;
				background-color: #87B941;
				padding:8px 65px;
				text-decoration: none;
				font-weight: bold;
				border-radius: 5px;
			}
			p.ad a.d{
				background-color: #f00;
				white-space: nowrap;
			}
		</style>
		
		<p>&nbsp;</p>
        <table border='0' cellpadding='10' cellspacing='0' class='detil' style='margin-bottom:5px;' width='700'>
			<tr bgcolor='#FFff99'>
              <td width='30' align='center bgcolor='#FFff99''>NO.</td> 
              <td width='400' bgcolor='#FFff99'>ITEM</td>  
              <td width='120' align='right' bgcolor='#FFff99'>PRICE / IDR</td>
            </tr>
			      <tr bgcolor='#FFffee'>
              <td align='center' bgcolor='#FFffee'>1.</td>
              <td bgcolor='#FFffee'>NON Refundable Registration Fee</td> 
				      <td align='right' bgcolor='#FFffee'><?= $fee ?></td>
            </tr>
            <tr bgcolor='#FFffcc'><td colspan='2' align='right' bgcolor='#FFffcc'>Total</td> <td align='right' bgcolor='#FFffcc'><b> <?= $fee ?></b></td></tr>
		</table>
          
	  <p>&nbsp;</p>
	  
	  <p></p>
      
	  <p><b>please <a href="<?= base_url() ?>user/login" target="_blank">login</a> to your member area to confirm and select payment</b></p>
	  
	  <ul>
            <li>Four (04) nights stay (accommodation) during <?= $date ?> 2020</li>
            <li>Inter transfers: airport - official hotel - airport and official hotel - venue - official hotel during main event</li>
            <li>NON Refundable Registration Fee - IDR. <?= $fee ?></li>
            <li>Meals during BBTF <?= $year ?> program</li>
        </ul>
        <p>&nbsp;</p>
        <p><b>In association with the above BUYER classification, please find the following arrangement related to the Pre Appointment Scheduled as a BUYER :</b></p>
        <ul>
            <li>To participate all activities during BBTF <?= $year ?></li>
            <li>To complete all Pre Appointment Scheduled at least 26 appointments for Gold buyer during two (2) full days program.</li>
        </ul>
	  
	  
	  
	  <p>&nbsp;</p>
	  
      <p>&nbsp;</p><p>Should you require any further information about BALI & BEYOND TRAVEL FAIR (BBTF) <?= $year ?>, please feel free to reach out to the Committee at your convenience Or simply visit our website at www.bbtf.or.id</p>

 

      <p>&nbsp;</p><p>&nbsp;</p><p>Warmest Regards, <p>
 
      <p>&nbsp;</p><p>&nbsp;</p>Sekretariat of BBTF <?= $year ?></p><p>&nbsp;</p><p>&nbsp;</p>

 

      <p style='border:solid #ff8d00; border-width:1px 0; background:#fce5d5; padding:15px;'><b>BALI & BEYOND TRAVEL FAIR (BBTF) <?= $year ?> </b><br />
      <u>Secretariat Office:</u><br />
      Jl. Raya Puputan No. 41, Renon, Denpasar 80235, Bali<br />
      P. 62 (+361) 445 7111 F. (+62 361) 244 263<br />
      E-mail: buyer@bbtf.or.id<br />
      Website: <u><a href='http://bbtf.or.id'>www.bbtf.or.id</a></u></p>
      </div>
