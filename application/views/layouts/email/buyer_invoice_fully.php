<div style='border:1px solid #99bdfc; background-color: #f2f6fe; padding: 30px; width:700px;'>
     	<img src='<?= site_url() ?>assets/logo/logo_1.png'>
		 <p></p>
<p>
    <b>Dear </b>
    <b><?= $gender ?>.<?= $first_name ?>&nbsp;<?= $last_name ?></b>
    <b></b>
    <b></b>
</p>
<p>
    Congratulation your company <b><?= $company_name ?></b> are classified as:
</p>
<h1 style="text-align: center; "><span style="background-color: rgb(255, 0, 0); color: rgb(247, 247, 247);"><b>Fully Hosted</b></span></h1>
<p>
    <b>Fully Hosted Buyer Benefit:</b></p>
<ul>
    <li>The refund airfare will be done and based on the airfare showing on the air ticket with maximum cap of the following&nbsp;quota</li>
</ul>
<table border="0" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td>
                <p>1</p>
            </td>
            <td width="280">
                <p>
                    Singapore, Malaysia, Brunei
                </p>
            </td>
            <td>
                <p>
                    USD. 350
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    2.
                </p>
            </td>
            <td>
                <p>
                    Philippine, Thailand
                </p>
            </td>
            <td>
                <p>
                    USD. 400
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    3.
                </p>
            </td>
            <td>
                <p>
                    Vietnam, Myanmar, Cambodia, Laos
                </p>
            </td>
            <td>
                <p>
                    USD. 450
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    4.
                </p>
            </td>
            <td>
                <p>
                    Perth, Darwin
                </p>
            </td>
            <td>
                <p>
                    USD. 500
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    5.
                </p>
            </td>
            <td>
                <p>
                    Melbourne, Sydney, Gold Coast (Brisbane)
                </p>
            </td>
            <td>
                <p>
                    USD. 700
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    6.
                </p>
            </td>
            <td>
                <p>
                    Japan, South Korea
                </p>
            </td>
            <td>
                <p>
                    USD. 650
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    6.
                </p>
            </td>
            <td>
                <p>
                    Middle east &amp; India
                </p>
            </td>
            <td>
                <p>
                    USD. 700
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    7.
                </p>
            </td>
            <td>
                <p>
                    China, Hong Kong, Taiwan &amp; Macau
                </p>
            </td>
            <td>
                <p>
                    USD. 600
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    6.
                </p>
            </td>
            <td>
                <p>
                    West &amp; Eastern Europe
                </p>
            </td>
            <td>
                <p>
                    USD. 1,100
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    8.
                </p>
            </td>
            <td>
                <p>
                    North &amp; South Africa, America
                </p>
            </td>
            <td>
                <p>
                    USD. 1,100
                </p>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </tbody>
</table>
<ul>
    <li>Four (04) Nights stay (accommodation) during <?= $date ?>
        <?= $year ?>.<br></li>
    <li>Inter transfers: airport - official hotel - airport and official hotel - venue - official hotel during main event.
    </li>
</ul>
<ul>
    <li>
        Meals during BBTF
        <?= $year ?> program.
    </li>
</ul>
<p>
    <b>Fully Hosted Buyer Mandatory:</b>
    <b></b>
</p>
<ul type="disc">
    <li>
        An administrative fee of IDR.
        <?= $fee ?> for Fully Hosted Buyer (Indonesian Currency) <b>NON REFUNDABLE</b><b> </b> and the payment by credit card will added a surcharge 4 % (master card &amp; visa logo only)<b></b>
    </li>
    <li>
        To participate all activities during BBTF
        <?= $year ?> event
    </li>
    <li>
        To complete all Pre Appointment Scheduled (PSA) at least 32 appointments for Fully Hosted buyer during two (2) full days program.
    </li>
    <li>
        Refund ticket required :
    </li>
</ul>
<ol start="1" type="1">
    <li>
        Colour Scan Passport
    </li>
    <li>
        Original Arrival Boarding Pass
    </li>
    <li>
        Original printed ticket with showing airfare
    </li>
    <li>
        Colour Scan Ngurah Rai Immigration Stamp
    </li>
</ol>
<ul>
    <li>
        <b>Cancellation</b>
    </li>
</ul>
<ul type="disc">
    <li>
        <b>NON REFUNDABLE administrative fee IDR.<?= $fee ?> for Fully Hosted Buyer &amp; applied before 3 months prior to events
        </b>
    </li>
    <li>
        <b>NON REFUNDABLE administrative fee IDR.<?= $fee ?> for Fully Hosted Buyer &amp; applied for NO SHOW BUYER
        </b>
    </li>
</ul>
<table width="100%" cellspacing="0" cellpadding="0">
  <tr>
      <td>
          <table cellspacing="0" cellpadding="0">
              <tr>
                  <td style="border-radius: 2px;" bgcolor="#6135FF">
                      <a href="<?= $approve_url ?>" target="_blank" style="padding: 8px 12px; border: 1px solid #ED2939;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;">
                          Accept        
                      </a>
				  </td>
				  <td style="border-radius: 2px;" bgcolor="#ED2939">
                      <a href="<?= $decline_url ?>" target="_blank" style="padding: 8px 12px; border: 1px solid #ED2939;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;">
                          Decline         
                      </a>
                  </td>
              </tr>
          </table>
      </td>
  </tr>
</table>
<p>
    If we are not received any confirmation within <b>10 working days</b> your registration would be automatically release from our system.
</p>
<p>
Should you require any further information about BBTF <?= $year ?>, please feel free to reach out to the Committee at your convenience or simply visit our website at <a href="www.bbtf.or.id">www.bbtf.or.id</a> 
</p>
<p>
Sincerely yours,</p>
<p><br></p>
<p>Committee of BBTF <?= $year ?></p>
<p><br></p>
<p style='border:solid #ff8d00; border-width:1px 0; background:#fce5d5; padding:15px;'><b style='font-size:18px;'>BBTF Secretariat Office</b><br />
      Jl. Raya Puputan No. 41 Renon, Denpasar 80235, Bali Indonesia<br />
      P : +62 361 4457111 ; 4457700<br />
	  F: +62 361 244 263<br />
	  E: info@bbtf.or.id<br />
      W: www.bbtf.or.id</p>
      </div>
