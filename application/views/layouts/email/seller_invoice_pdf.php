<html>
    <head>

    </head>
    <body>
    <div class='email_wrapper'>
			  <table border='0' width='800'>
				<tr>
				  <td width='350'><img src='<?= site_url() ?>assets/logo/asita-logo-2019.gif'></td>
				  <td align='center'><img src='<?= site_url() ?>assets/logo/logo_1.png' width='250'></td>
				</tr>		
				<tr>
				  <td align='right'><h1>INVOICE &nbsp; &nbsp; </h1></td>
				  <td><h2><?= $invoiceNo ?></h2></td>
				</tr>		
				<tr>
				  <td valign='top' valign='top'>Attn : <b><?= $seller_name ?></b><br/>
				  										<b><?= $seller_phone ?>				  
				  	<p>&nbsp;</p>				  
				  	<p>Date: <b><?= $date ?><br />
				  	Due date:  <b><?= $dueDate ?></b></p>
				  </td>
				  <td>
						BBTF<br>
						DPD ASITA BALI<br>
						Jl. Raya Puputan No 41<br>
						Renon - Denpasar<br>
						T. 4457111, 4457700 F. 0361 244		  
				  </td>
				</tr>
			  </table>		
			  <p>&nbsp;</p>		
				<style>
					.email_wrapper{
						border:1px solid #ddd; background-color: #fafafa; padding: 30px; width:700px;
						font-family: garamond, times, arial, tahoma, verdana;
						font-size: 14px!important;
					}
					.email_wrapper table,
					.email_wrapper tbody{
						border: none;	
					}
					.email_wrapper td{
						vertical-align: top!important;
					}
					.email_wrapper h1{
						font-size: 30px;
						margin-top: 0;	
					}
					.email_wrapper h2 {
						display: table;
						margin-top: 9px;	
						font-size: 20px;
					}
					table.detil{
						border-top:1px solid #ccc;
						border-left:1px solid #ccc;
					}
					table.detil td{
						border-bottom:1px solid #ccc;
						border-right:1px solid #ccc;
					}
					input.bt_submit{
						padding: 5px 25px;
						background: #549BC8;
						color: #fff;	
						border:1px solid #2566A7;
						font-weight: bold;
						cursor: pointer;
					}
				</style>
				<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-0lax{text-align:left;vertical-align:top}
</style>
<table class="tg" border="1px;" width="100%" >
  <tr>
    <th class="tg-0lax">No</th>
    <th class="tg-0lax">Product</th>
    <th class="tg-0lax">Amount</th>
    <th class="tg-0lax">Sub Total</th>
  </tr>
  <?php foreach ($res as $key => $value) :?>
	<tr>
    <td class="tg-0lax"><?= $key + 1 ?></td>
    <td class="tg-0lax"><?= $value['product_name'] ?></td>
    <td class="tg-0lax"><?= $value['product_amount'] ?></td>
    <td class="tg-0lax"><?= number_format($value['product_price'],2) ?></td>
  </tr>
	<?php endforeach; ?>
  <tr>
    <td class="tg-0lax" colspan="3">total</td>
    <td class="tg-0lax"><?= number_format($total,2) ?></td>
  </tr>
</table>
<p>
	Note : all the above prices are excluded goverment tax and service.
</p>

				<!-- <table class="table text-left">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Company</thstyle="width: 1%;>
                        <th>Booth Number</th>
						<th>Fascia Name</th>
						<th>Country</th>
                    </tr>
                </thead>
                <tbody>
					<?php foreach ($res as $key => $value) :?>
					<tr>
                        <td><?= $key + 1 ?></td>
                        <td><?= $value['seller_name'] ?></td>
                        <td><?= $value['booth_number'] ?></td>
                        <td><?= $value['booth_name'] ?></td>						
						<td><?= $value['country_name'] ?></td>

                    </tr>
					<?php endforeach; ?>
                </tbody>
            </table>	   -->
			  <?php //number_format($total,2) ?>			  
			  <p>&nbsp;</p>
				<ul style='list-style: outside decimal;'>
					<li>
						Please settle payment before the due date above otherwise system will release automatically and re-send update invoice.
					</li>					
					<li>
						Our Bank Detail :<br>
						Bank Mandiri<br>
						KCP Denpasar Pasar Kumbasari<br>
						Jl. Gajah Mada No. 105, Denpasar - Bali. Phone: 0361 - 434812<br>
						A/C 1450010680763<br>
						a/n Bali And Beyond Travel Fair<br>
						Swift code : bmriidja			
					</li>
				</ul>		
			  <p>&nbsp;</p>
			  <p>&nbsp;</p>
			<table>
				<tr>
					<td width='500'>
						Prepared by :<br>
						<img width="100px" height="100px" src='./assets/logo/TTD_Novi_Accounting.png'><br> Novi yanti<br> Accounting<br>
					</td>
					<td>
						<p><b>Note : -</b></p>
					</td>
				</tr>
			</table>		
      </div>
    </body>
</html>

