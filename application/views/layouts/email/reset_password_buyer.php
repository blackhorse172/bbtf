<div style='border:1px solid #99bdfc; background-color: #f2f6fe; padding: 30px; width:700px;'>
     	<img src='<?= site_url() ?>assets/logo/logo_1.png'>
      <p>&nbsp;</p>
	  
	  	<p>&nbsp;</p>
		<p>Dear, <b><?= $gender ?>&nbsp;<?= $buyer_name ?></b></p>
		<p>&nbsp;</p>
		<p>Greetings from Bali &amp; Beyond Travel Fair <?= $year ?></p>
		<p>&nbsp;</p>
		<p>Your password has been reset.</p>
		<p>Here below your New Password :</p>
		<p>&nbsp;</p>
		<p>Password : <?= $password ?></p>
		<p><a href="<?= site_url() ?>user/login"><?= site_url() ?>user/login</a></p>
		<p>&nbsp;</p>
		<p>Please kindly contact us for any further information required</p>
		<p>&nbsp;</p>
		<p>Regards,</p>
		<p>Committee of  Bali &amp; Beyond Travel Fair </p>
		<p></p>
		<p>&nbsp;</p>
      <p style='border:solid #ff8d00; border-width:1px 0; background:#fce5d5; padding:15px;'><b style='font-size:18px;'>BBTF Secretariat Office</b><br />
      Jl. Raya Puputan No. 41 Renon, Denpasar 80235, Bali Indonesia<br />
      P : +62 361 4457111 ; 4457700<br />
	  F: +62 361 244 263<br />
	  E: info@bbtf.or.id<br />
      W: www.bbtf.or.id</p>
      </div>
