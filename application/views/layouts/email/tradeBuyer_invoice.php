<style>
      a{
        text-decoration: none;
      }
    </style>
        
      <div style='border:1px solid #99bdfc; background-color: #f2f6fe; padding: 30px; width:700px;'>
      <table border='0'>
        <tr>
          <td><img src='<?= site_url() ?>assets/logo/logo_1.png'></td>
        </tr>
      </table>

      <p>&nbsp;</p>
      <p>Dear, <?= strtoupper($full_name) ?></p>
      <p>&nbsp;</p>
      <p>Warmest greetings from the Committee of BBTF <?= $year ?>..!</p>

      <p>&nbsp;</p><p>Congratulation your company <?= strtoupper($company_name) ?> are successfully registered as :</p>

      <p align='center' style='border:1px solid #f00;padding:15px 30px;border-radius:9px;background:#f00;color:#fff;font-size:180%;font-weight:bold;clear:both;width:230px;margin:5px auto 15px;'>TRADE BUYER</p>

      <p><b>TRADE BUYER BENEFIT:</b></p>
      <ul>
          <li>Admission to Travex</li>
          <li>Access to onsite appointments</li>
         
      </ul>   
    
			<p>&nbsp;</p>
			<p>An administration fee for Trade Buyer is IDR. 1,450,000/person (Indonesian Currency) NON REFUNDABLE and the payment by credit card will added a surcharge 4 % (master card & visa logo only)</p>
			<p><b>Dress Code</b></p>
			<ul>
          <li>Must wear business attire during BBTF events at Nusa Dua,Bali</li>         
      </ul> 			
			<table width="100%" cellspacing="0" cellpadding="0">
  <tr>
      <td>
          <table cellspacing="0" cellpadding="0">
              <tr>
                  <td style="border-radius: 2px;" bgcolor="#6135FF">
                      <a href="<?= $approve_url ?>" target="_blank" style="padding: 8px 12px; border: 1px solid #ED2939;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;">
                          Accept        
                      </a>
				  </td>
				  <td style="border-radius: 2px;" bgcolor="#ED2939">
                      <a href="<?= $decline_url ?>" target="_blank" style="padding: 8px 12px; border: 1px solid #ED2939;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;">
                          Decline         
                      </a>
                  </td>
              </tr>
          </table>
      </td>
  </tr>
</table>
    <p>&nbsp;</p>
    <p>&nbsp;</p><p>Should you require any further information about BBTF <?= $year ?>, please feel free to reach out to the Committee at your convenience Or simply visit our website at www.bbtf.or.id</p>

      <p>&nbsp;</p><p>&nbsp;</p><p>Sincerely yours,<p>
 
      <p></p>

      <p>&nbsp;</p><p>&nbsp;</p>Committee of BBTF <?= $year ?> </p><p>&nbsp;</p><p>&nbsp;</p>

      <p style='border:solid #ff8d00; border-width:1px 0; background:#fce5d5; padding:15px;'><b style='font-size:18px;'>BBTF Secretariat Office</b><br />
      Jl. Raya Puputan No. 41 Renon, Denpasar 80235, Bali Indonesia<br />
      P : +62 361 4457111 ; 4457700<br />
	  F: +62 361 244 263<br />
	  E: info@bbtf.or.id<br />
      W: www.bbtf.or.id</p>
      </div>
