
<div style='border:1px solid #99bdfc; background-color: #f2f6fe; padding: 30px; width:700px;'>
     	<img src='<?= site_url() ?>assets/logo/logo_1.png'>
      <p>&nbsp;</p>
	  
	  <p>
    <b>Dear <?= $gender ?>.<?= $first_name ?>&nbsp;<?= $last_name ?></b>
    <b></b>
    <b> </b>
</p>
<p>
    Congratulation your company <b><?= $company_name ?></b> are classified as:<b></b>
</p>
<div>
    <h1 style="text-align: center; "><span style="color: rgb(247, 247, 247); background-color: rgb(255, 0, 0);"><b>Partially Hosted</b></span></h1>
</div>
<p><b>Partially Hosted Buyer Benefit :</b></p>
<ul>
    
    <li>
        Four (04) Nights stay (accommodation) during <?= $date ?>
        <?= $year; ?>
    </li>
    <li>
        Inter transfers: airport - official hotel - airport and official hotel - venue - official hotel during main event
    </li>
    <li>
        Meals during BBTF
        <?= $year; ?> program
    </li>
</ul>
<ul>
</ul>
<ul type="disc">
</ul>
<ul type="disc">
</ul>
<p>
    <b>Partially</b>
    <b> </b>
    <b>Hosted Buyer Mandatory:</b>
    <b></b>
</p>
<ul>
    <li>An administrative fee of IDR.
        <?= $fee ?> (Indonesian Currency) for Partially Hosted Payment by credit card will added a surcharge 4 % (master card &amp; visa logo only) <b>NON REFUNDABLE</b><b></b></li>
    <li>Participate all activities during BBTF
        <?= $year; ?> event</li>
    <li>
        To complete all Pre Appointment Schedule at least 26 appointments for partially buyer during two (2) full days program.
    </li>
</ul>
<p>
</p>
<p> <b>Cancellation</b></p>
<ul type="disc">
    <li>
        <b>
            NON REFUNDABLE administrative fee IDR. <?= $fee ?> for Partially Hosted Buyer applied
            before 3 months prior to events
        </b>
    </li>
    <li>
        <b>NON REFUNDABLE</b>
        <b>
            administrative fee IDR. <?= $fee ?> for Partially Hosted Buyer applied for NO SHOW BUYER
        </b>
    </li>
</ul>
<table width="100%" cellspacing="0" cellpadding="0">
  <tr>
      <td>
          <table cellspacing="0" cellpadding="0">
              <tr>
                  <td style="border-radius: 2px;" bgcolor="#6135FF">
                      <a href="<?= $approve_url ?>" target="_blank" style="padding: 8px 12px; border: 1px solid #ED2939;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;">
                          Accept        
                      </a>
				  </td>
				  <td style="border-radius: 2px;" bgcolor="#ED2939">
                      <a href="<?= $decline_url ?>" target="_blank" style="padding: 8px 12px; border: 1px solid #ED2939;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;">
                          Decline         
                      </a>
                  </td>
              </tr>
          </table>
      </td>
  </tr>
</table>
<p>
    If we are not received any confirmation within <b>10 working days</b> your registration would be automatically release from our system.
</p>
<p>
    Should you require any further information about BBTF
    <?= $year; ?>, please feel free to reach out to the Committee at your convenience or simply visit our website at <a href="http://www.bbtf.or.id/">www.bbtf.or.id</a>
</p>
<p>
Sincerely yours,</p>
<p><br></p>
<p>
</p>
<p>
    Committee of BBTF
    <?= $year; ?>
</p>
      <p style='border:solid #ff8d00; border-width:1px 0; background:#fce5d5; padding:15px;'><b style='font-size:18px;'>BBTF Secretariat Office</b><br />
      Jl. Raya Puputan No. 41 Renon, Denpasar 80235, Bali Indonesia<br />
      P : +62 361 4457111 ; 4457700<br />
	  F: +62 361 244 263<br />
	  E: info@bbtf.or.id<br />
      W: www.bbtf.or.id</p>
      </div>

