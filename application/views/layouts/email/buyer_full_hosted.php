			<p></p><p>
    <strong>Dear </strong>
    <strong>Mrs.</strong>
    <strong> </strong>
    <strong>Helen Mather</strong>
    <strong></strong>
</p>
<p>
    Congratulation your company <strong>Inspired By Asia</strong> are
    classified as:
</p>
<h1 style="text-align: center; "><span style="background-color: rgb(255, 0, 0); color: rgb(247, 247, 247);"><strong>Fully Hosted</strong></span></h1>
<p>
    <strong>Fully Hosted Buyer Benefit:</strong></p><ul><li>The refund airfare will be done and based on the airfare showing on the
    air ticket with maximum cap of the following&nbsp;</li></ul><table border="0" cellspacing="0" cellpadding="0"><tbody>
        <tr>
            <td>
                <p>1</p>
            </td>
            <td width="280">
                <p>
                    Singapore, Malaysia, Brunei
                </p>
            </td>
            <td>
                <p>
                    USD. 350
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    2.
                </p>
            </td>
            <td>
                <p>
                    Philippine, Thailand
                </p>
            </td>
            <td>
                <p>
                    USD. 400
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    3.
                </p>
            </td>
            <td>
                <p>
                    Vietnam, Myanmar, Cambodia, Laos
                </p>
            </td>
            <td>
                <p>
                    USD. 450
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    4.
                </p>
            </td>
            <td>
                <p>
                    Perth, Darwin
                </p>
            </td>
            <td>
                <p>
                    USD. 500
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    5.
                </p>
            </td>
            <td>
                <p>
                    Melbourne, Sydney, Gold Coast (Brisbane)
                </p>
            </td>
            <td>
                <p>
                    USD. 700
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    6.
                </p>
            </td>
            <td>
                <p>
                    Japan, South Korea
                </p>
            </td>
            <td>
                <p>
                    USD. 650
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    6.
                </p>
            </td>
            <td>
                <p>
                    Middle east &amp; India
                </p>
            </td>
            <td>
                <p>
                    USD. 700
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    7.
                </p>
            </td>
            <td>
                <p>
                    China, Hong Kong, Taiwan &amp; Macau
                </p>
            </td>
            <td>
                <p>
                    USD. 600
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    6.
                </p>
            </td>
            <td>
                <p>
                    West &amp; Eastern Europe
                </p>
            </td>
            <td>
                <p>
                    USD. 1,100
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p>
                    8.
                </p>
            </td>
            <td>
                <p>
                    North &amp; South Africa, America
                </p>
            </td>
            <td>
                <p>
                    USD. 1,100
                </p>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </tbody>
</table><ul><li>Four (04) Nights stay (accommodation) during <?= $date ?> 2020.<br></li><li>Inter transfers: airport - official hotel - airport and official hotel -
    venue - official hotel during main event.
</li></ul>
<ul>
    <li>
        Meals during BBTF 2020 program.
    </li>
</ul>
<p>
    <strong>Fully Hosted Buyer Mandatory:</strong>
    <strong></strong>
</p>
<ul type="disc">
    <li>
        An administrative fee of IDR.2,900,000 for Fully Hosted Buyer
        (Indonesian Currency) <strong>NON REFUNDABLE</strong><strong> </strong>
        and the payment by credit card will added a surcharge 4 % (master card
        &amp; visa logo only)<strong></strong>
    </li>
    <li>
        To participate all activities during BBTF 2020 event
    </li>
    <li>
        To complete all Pre Appointment Scheduled (PSA) at least 32
        appointments for Fully Hosted buyer during two (2) full days program.
    </li>
    <li>
        Refund ticket required :
    </li>
</ul>
<ol start="1" type="1">
    <li>
        Colour Scan Passport
    </li>
    <li>
        Original Arrival Boarding Pass
    </li>
    <li>
        Original printed ticket with showing airfare
    </li>
    <li>
        Colour Scan Ngurah Rai Immigration Stamp
    </li>
</ol>
<ul>
    <li>
        <strong>Cancellation</strong>
    </li>
</ul>
<ul type="disc">
    <li>
        <strong>
            ​NON REFUNDABLE administrative fee IDR.2,900,000 for Fully Hosted
            Buyer &amp; IDR. 2,175,000 for Partially Hosted Buyer applied
            before 3 months prior to events
        </strong>
    </li>
    <li>
        <strong>
            NON REFUNDABLE administrative fee IDR.2,900,000 for Fully Hosted
            Buyer &amp; IDR. 2,175,000 for Partially Hosted Buyer applied for
            NO SHOW BUYER
        </strong>
    </li>
</ul>
<p>
If we are not received any confirmation within    <strong>10 working days</strong> your registration would be automatically
    release from our system.
</p>
<p>
    We are looking forward to hear from you soon.
</p>
<p>
    Warmest Regards,</p><p><br></p><p>Committee of Bali &amp; Beyond Travel Fair&nbsp;</p><p><br></p><p>
</p>
<p>
    Secretariat of BBTF 2020
</p><p></p>
