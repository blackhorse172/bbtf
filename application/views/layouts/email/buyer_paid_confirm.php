<div style='border:1px solid #99bdfc; background-color: #f2f6fe; padding: 30px; width:700px;'>
      <img src='<?= site_url() ?>assets/logo/logo_1.png'>
      <p>&nbsp;</p>
      <p>Dear <?= $gender ?>.<?= $buyer_name ?></p>
      <p>&nbsp;</p>
      <p><b>Greetings from BBTF <?= $year ?></b></p>

      <p>&nbsp;</p><p>We have received your payment successfully and thank you for your participation at BBTF <?= $year ?>.</p>    
    	<p>Please be inform that we will send you buyer login access immediately</p>
      <p>If you have not received within 3 working days, please contact us.</p>

      <p>&nbsp;</p>
      <p>Sincerely yours</p>
      <p>Committee of BBTF <?= $year ?></p>

      <p style='border:solid #ff8d00; border-width:1px 0; background:#fce5d5; padding:15px;'><b style='font-size:18px;'>BBTF Secretariat Office</b><br />
      Jl. Raya Puputan No. 41 Renon, Denpasar 80235, Bali Indonesia<br />
      P : +62 361 445 7111<br />
    F: +62 361 244 263<br />
    E: buyer@bbtf.or.id<br />
      W: www.bbtf.or.id</p>
      </div>
