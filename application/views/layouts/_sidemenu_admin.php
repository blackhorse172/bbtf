

		<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-file"></i><span
									 class="m-menu__link-text">Pages</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item" aria-haspopup="true"><a href="<?= site_url() ?>Pages" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List Page</span></a></li>
										<!-- <li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Menus" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Page Menu</span></a></li> -->
									</ul>
								</div>
		</li>

		<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-file-1"></i><span
									 class="m-menu__link-text">News</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item" aria-haspopup="true"><a href="<?= site_url() ?>Article" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List News</span></a></li>
										<li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Article/Category" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Category News</span></a></li>
									</ul>
								</div>
							</li>

		
		

		
		<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-tabs"></i><span
									 class="m-menu__link-text">Slider</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item" aria-haspopup="true"><a href="<?= site_url() ?>Slider" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List Slider</span></a></li>
										<li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Slider/Category" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Category Slider</span></a></li>
									</ul>
								</div>
		</li>

		<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-music"></i><span
									 class="m-menu__link-text">Banner</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item" aria-haspopup="true"><a href="<?= site_url() ?>Banner" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List Banner</span></a></li>
										<li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Banner/Category" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Category Banner</span></a></li>
									</ul>
								</div>
		</li>


		<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-medal"></i><span
									 class="m-menu__link-text">Suported</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item" aria-haspopup="true"><a href="<?= site_url() ?>Suported" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Lists</span></a></li>
									</ul>
								</div>
		</li>
		<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-users-1"></i><span
									 class="m-menu__link-text">Buyer</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item" aria-haspopup="true"><a href="<?= site_url() ?>Admin/Hotel" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List Hotel</span></a></li>
										<li class="m-menu__item  m-menu__item" aria-haspopup="true"><a href="<?= site_url() ?>Buyer/listFlight" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Flights</span></a></li>
									</ul>
								</div>
		</li>

		<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-medal"></i><span
									 class="m-menu__link-text">Sponsor</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item" aria-haspopup="true"><a href="<?= site_url() ?>Sponsors" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Lists</span></a></li>
									</ul>
								</div>
		</li>
		
		<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-users"></i><span
									 class="m-menu__link-text">Users</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item" aria-haspopup="true"><a href="<?= site_url() ?>Users" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List Users</span></a></li>
										<li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Users/Category" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Category Users</span></a></li>
									</ul>
								</div>
							</li>

							<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-squares-1"></i><span
									 class="m-menu__link-text">Gallery</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--active" aria-haspopup="true"><a href="<?= site_url() ?>Gallery" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List Gallery</span></a></li>
										<li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Gallery/Category" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Category Gallery</span></a></li>
									</ul>
								</div>
							</li>

							<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-user-ok"></i><span
									 class="m-menu__link-text">Membership</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item" aria-haspopup="true"><a href="<?= site_url() ?>Memberships/Seller" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List Seller</span></a></li>
										<li class="m-menu__item aria-haspopup="true"><a href="<?= site_url() ?>Memberships/Buyer" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List Buyer</span></a></li>
										<li class="m-menu__item aria-haspopup="true"><a href="<?= site_url() ?>Memberships/Media" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List Media</span></a></li>
										<li class="m-menu__item aria-haspopup="true"><a href="<?= site_url() ?>Memberships/TradeBuyer" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Trade Buyer</span></a></li>
										
										<!-- <li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Memberships/Category" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Category Membership</span></a></li> -->
									</ul>
								</div>
							</li>

							<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-box-1"></i><span
									 class="m-menu__link-text">Product</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--active" aria-haspopup="true"><a href="<?= site_url() ?>Products" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List Products</span></a></li>
										<li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Products/Category" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Category Products</span></a></li>
										<li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Stock" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Stock</span></a></li>

									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-settings"></i><span
									 class="m-menu__link-text">Settings</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Company" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Company</span></a></li>
										<li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Settings/MenuColor" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Home Menu Color</span></a></li>
										<li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Settings/Psa" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">PSA Settings</span></a></li>

										<li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Settings/ScheduleTableColor" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Schedule Table Color</span></a></li>
										<!-- <li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Invoice" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Invoice</span></a></li>
										<li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>EmailTemplate" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Email Template</span></a></li> -->

									</ul>
								</div>
							</li>

							<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-price-tag"></i><span
									 class="m-menu__link-text">Transaction</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--active" aria-haspopup="true"><a href="<?= site_url() ?>Admin/Transactions" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Seller</span></a></li>
										<li class="m-menu__item  m-menu__item--active" aria-haspopup="true"><a href="<?= site_url() ?>Admin/Transactions/Doku" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Buyer & TradeBuyer</span></a></li>
										<!-- <li class="m-menu__item  m-menu__item--active" aria-haspopup="true"><a href="<?= site_url() ?>Admin/Transactions/TradeBuyer" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Trade Buyer</span></a></li> -->
										<li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Admin/Payments" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Payment</span></a></li>

									</ul>
								</div>
							</li>

							<li class="m-menu__item " aria-haspopup="true">
								<a href="<?= base_url() ?>Admin/Schedule" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-event-calendar-symbol"></i><span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">schedule</span>
											<!-- @*<span class="m-menu__link-badge"><span class="m-badge m-badge--danger">2</span></span>*@ -->
										</span>
									</span>
								</a>
							</li>

							<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-price-tag"></i><span
									 class="m-menu__link-text">Hosted Tour</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--active" aria-haspopup="true"><a href="<?= site_url() ?>Admin/HostedTours" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Request</span></a></li>
										<!-- <li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Admin/Payments" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Payment</span></a></li> -->

									</ul>
								</div>
							</li>

							<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-price-tag"></i><span
									 class="m-menu__link-text">Official Partner</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--active" aria-haspopup="true"><a href="<?= site_url() ?>Admin/OfficialPartner" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List</span></a></li>
										<!-- <li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Admin/Payments" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Payment</span></a></li> -->

									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-price-tag"></i><span
									 class="m-menu__link-text">Testimonies</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--active" aria-haspopup="true"><a href="<?= site_url() ?>Testimonies" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List</span></a></li>
										<!-- <li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Admin/Payments" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Payment</span></a></li> -->

									</ul>
								</div>
							</li>
