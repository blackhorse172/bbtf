<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Terms And Condition</title>
</head>
<body>
    <p style="text-align: center;"><strong>EXHIBITOR TERMS &amp; CONDITIONS</strong></p>
    <p style="text-align: center;"><strong>7<sup>th </sup>BBTF, 9-13<sup>th</sup> June 2020</strong></p>
    <p style="text-align: center;">&nbsp;</p>
    <p style="margin-left: auto; margin-right: auto;">BBTF events&nbsp;is organized by ASITA BALI Chapter which the event will be held in&nbsp;Nusa Dua, Bali&nbsp;for B2B session</p>
    <table style="margin-left: auto; margin-right: auto;" width="680">
        <tbody>
            <tr>
                <td width="293">
                    <p>Dates and Times&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                </td>
                <td width="387">
                    <p>: 9- 13&nbsp;June 2020</p>
                </td>
            </tr>
            <tr>
                <td width="293">
                    <p>Deadline for application</p>
                </td>
                <td width="387">
                    <p>: 13&nbsp;May&nbsp;2020</p>
                </td>
            </tr>
            <tr>
                <td width="293">
                    <p>Deadline for submission of building plans</p>
                </td>
                <td width="387">
                    <p>: 25&nbsp;May&nbsp;2020.&nbsp;Any building plan after the dateline, using&nbsp; Official Contractor is mandatory</p>
                </td>
            </tr>
            <tr>
                <td width="293">
                    <p>Commencement of construction</p>
                </td>
                <td width="387">
                    <p>: 8&nbsp;June 2020</p>
                </td>
            </tr>
            <tr>
                <td width="293">
                    <p>Commencement of dismantling</p>
                </td>
                <td width="387">
                    <p>: No dismantling booth before 06.30 pm on 12th&nbsp;June 2020</p>
                </td>
            </tr>
            <tr>
                <td width="293">
                    <p>Conclusion of dismantling</p>
                </td>
                <td width="387">
                    <p>: Dismantling booth MUST&nbsp;be done after 06.30pm and FINISH by 10:00pm on 12th&nbsp;June 2020</p>
                </td>
            </tr>
        </tbody>
    </table>
    <ul>
        <li><strong>Registration</strong>
            <ul>
                <li>Registration to <a href="http://www.bbtf.or.id">bbtf.or.id</a></li>
                <li>All the prices are EXCLUDED to Govertment Tax</li>
            </ul>
        </li>
    </ul>
    <ul>
        <ul>
            <li>Row and&nbsp;Corner Booth include standard booth panel (3x3 meters), carpet, standard lighting, one standard table, 3&nbsp;standard chairs, one fascia name, electricity 2 ampere (440 watt)</li>
            <li>Every booth with size of 3 X 3 will receive 1 X Full delegate badge &amp; 1 X Co Delegate Badge and 2 X access on Welcome dine</li>
            <li>1 Entity listing on BBTF 2020&nbsp;directory (Print &amp; Online version)</li>
            <li>Floor space only include 6 X 3 meters Carpet, electricity 4 ampere (880 watt) and 2 X Full Delegate &amp; 2 X Co Delegate and 4 X access on Welcome diner</li>
            <li>Deluxe Island space only include 15 X 6 meters on Gold Area, Carpet, electricity 20 ampere (4400 watt) and 10 X Full Delegate &amp; 10 X Co Delegate and 20 X access on Welcome diner</li>
            <li>Premium Island space only include 15 X 6 meters on Platinum area, Carpet, electricity 20 ampere (4400 watt) and 10 X Full Delegate &amp; 10 X Co Delegate and 20 X access on Welcome diner</li>
            <li><strong>Terms of payment</strong>
                <ul>
                    <li>The period of payment is specified in the stand rental invoice,&nbsp;please quote invoice number and customer number. All payment should be made to one of the accounts indicated in the invoice as follow:</li>
                </ul>
            </li>
        </ul>
    </ul>
    <p style="padding-left: 90px;">&nbsp; &nbsp; &nbsp; &nbsp; Bank Mandiri<br />&nbsp; &nbsp; &nbsp; &nbsp; KCP Denpasar Pasar Kumbasari<br />&nbsp; &nbsp; &nbsp; &nbsp; Jl. Gajah Mada No. 105, Denpasar - Bali<br />&nbsp; &nbsp; &nbsp; &nbsp; Phone: 62 361 - 434812<br />&nbsp; &nbsp;
        &nbsp; &nbsp; A/C 145.0010.680763<br />&nbsp; &nbsp; &nbsp; &nbsp; A/N Bali and Beyond Travel Fair<br />&nbsp; &nbsp; &nbsp; &nbsp; Swift code: bmriidja</p>
    <ul>
        <li><strong>Cancellation</strong>
            <ul>
                <li>50% cancellation fee will be applied for any written cancellation before 31&nbsp;December&nbsp;2019</li>
                <li>For any registration is made after 1 January 2020 and cancellation is made after 21 days from the booking date will be charge 100% cancellation fee</li>
            </ul>
        </li>
    </ul>
    <ul>
        <li><strong>Halls and indoor places regulation</strong>
            <ul>
                <li>No outside food is permitted unless has clearance from committee.</li>
                <li>No Selling food or drink on the booth unless permitted.</li>
                <li>Musical and folklore performances are requested to apply for permitted to committee. The same rules apply to all events/performances which take place at the stand.</li>
                <li>Advertising is only permitted within the exhibitors official stand area. Promotion teams may operate only with special permission.</li>
                <li>Handouts of a political nature may not be distributed in any form. Moreover, the design and decoration of the stands must be free from any kind of political statement.</li>
                <li>It is forbidden to attach posters or any other materials on any walls or floor surfaces outside the hired stand.</li>
                <li>When possible, requests by exhibitors for parking space on the exhibition grounds will be taken into consideration if possible; rights to a parking space proper or a certain parking space do not exist.</li>
                <li>During the construction-and dismantling period, as well as during the fair, the regulations of the traffic guide will be applied for authorized traffic on the fairground</li>
                <li>Exhibitors and accompanying persons are required to leave the halls no later than one hour after the fair closes. Everyone leaving the exhibition grounds with a parcel is required to show the parcel's origin to the exit guards.</li>
                <li>Animals are not allowed into the exhibition grounds.</li>
                <li>All Sellers must not dismantle any booth decoration before 12th&nbsp;June 2020&nbsp;at 06.30 pm. Penalty will apply</li>
            </ul>
        </li>
    </ul>
    <p>&nbsp;</p>
    <ul>
        <li><strong>Electricity and partition walls</strong></li>
    </ul>
    <p style="padding-left: 30px;">&nbsp; &nbsp;Additional Electricity and partition walls is on exhibitor personal expenses.</p>
    <ul>
        <li><strong>Exhibitor passes</strong>
            <ul>
                <li>Additional Co Delegate Badge&nbsp;is IDR 2.500.000 / pass (apply maximum 1&nbsp;/ standard booth)</li>
                <li><strong>Additional Co Delegate Badge with PSA &nbsp;is IDR. 15,000,000/person</strong></li>
                <li><strong>Upgrade from Co Delegate to Full Delegate with PSA &nbsp;: IDR. 12,500,000/person</strong></li>
                <li>Re-printing lost pass is IDR 500.000 / badge (apply maximum 1 pass / person)</li>
                <li>Entry to Tourism Seminar&nbsp;</li>
                <li>Only Fully Delegate Badge has access to Seller Meet Buyer Session</li>
                <li>Full Delegate &amp; Co Delegate inclusive : 1x Welcome dinner</li>
            </ul>
        </li>
    </ul>
    <ul>
        <li><strong>Dress code</strong></li>
    </ul>
    <p style="padding-left: 30px;">&nbsp; &nbsp; All participants must wear business attire during BBTF events at Nusa Dua, Bali</p>
    <p align="center">
        <strong><?= $seller_city ?>, <?= date('d-M-Y') ?></strong>
    </p>
    <p align="center">
        <strong></strong>
    </p>
    <p>
        <strong>Seller Terms &amp; Conditions agreed by:</strong>
    </p>
    <p>
        Contact Person :
        <?= $registration_person ?>
    </p>
    <p>
        Company Name :
        <?= $seller_name ?>
    </p>
    <p>
        Phone :
        <?= $seller_phone ?>
    </p>
</body>

</html>
