<html>

<head>

</head>

<body>
    <div>
        <img width="150px" height="100px" src='<?= site_url() ?>assets/logo/logo_1.png'>
        <p>&nbsp;</p>
    </div>
<p style="text-align: center;"><strong>TERMS &amp; CONDITIONS</strong></p>
<p style="text-align: center;"><strong>TRADE BUYER</strong></p>
<p style="text-align: center;"><strong>7<sup>th</sup> BBTF, 9-13<sup>th</sup> June 2020</strong></p>
<p>&nbsp;</p>
<ul>
<li><strong>Event and Organizer</strong></li>
</ul>
<p>BBTF event&nbsp;is organized by ASITA BALI Chapter which the event will be held in Nusa Dua, Bali&nbsp;for B2B session</p>
<ul>
<li><strong>BBTF 2020&nbsp;Trade Buyer&nbsp;Profile:</strong></li>
<ul>
<li>Travel wholesalers</li>
<li>Retailers and agents</li>
<li>Convention and incentive organizers</li>
</ul>
<li>An administration fee for Trade Buyer&nbsp;is IDR. 1,450,000/person WITHOUT Lunch,&nbsp;&nbsp;inclusive of:</li>
<ul>
<li>Admission to BtoB Travex</li>
<li>Access to onsite appointments</li>
</ul>
<li><strong>Dress Code</strong></li>
</ul>
<p>Must wear business attire during BBTF events at Nusa Dua,Bali</p>
<p align="center">
        <strong><?= $city ?>, <?= date('d-M-Y') ?></strong>
    </p>
    <p>
        <strong>Trade Buyer Terms &amp; Conditions agreed by</strong>
    </p>
    <p>
        Full Name :
        	&nbsp;
            <?= $full_name ?>
    </p>
    <p>
        Company :
        <?= $company_name ?>
    </p>
</body>
</html>
