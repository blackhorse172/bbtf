<p align="center">
    <strong>TERMS &amp; CONDITIONS</strong>
</p>
<p align="center">
    <strong>Bali &amp; Beyond Travel Fair</strong>
</p>
<p align="center">
    <strong>9-13<sup>th</sup> June 2020</strong>
</p>
<p>
    BBTF events is organized by ASITA BALI Chapter which the event will be held in Nusa Dua, Bali for B2B session
</p>
<ul type="disc">
    <li>
        <strong>BBTF 2020 Buyer Profile:</strong>
    </li>
</ul>
<p>
    All registrants must undergo accreditation before confirmation as a Buyer Participants
</p>
<ul type="disc">
    <ul type="circle">
        <li>
            Travel wholesalers
        </li>
        <li>
            Retailers and agents
        </li>
        <li>
            Convention and incentive organizers
        </li>
        <li>
            Other Tourism related
        </li>
    </ul>
</ul>
<ul type="disc">
    <li>
        <strong>Buyer Mandatory:</strong>
    </li>
    <ul type="circle">
        <li>
            Partially Hosted Buyers must fulfilling 80% appointments Travex (26 appointments)
        </li>
        <li>
            Fully Hosted Buyers must fulfilling 100% appointments Travex (32 appointments)
        </li>
        <li>
            An administrative fee of IDR. 2.900.000 for Fully Hosted Buyer &amp; IDR. 2.175.000 for Partially Hosted Buyer and NON REFUNDABLE
        </li>
        <li>
            Payment by credit card will added a surcharge 4 % (master card &amp; visa logo only)
        </li>
    </ul>
    <li>
        <strong>Hosted Buyer Privileges</strong>
    </li>
    <ul type="circle">
        <li>
            Fully Hosted Buyers receive:
        </li>
        <ul type="square">
            <li>
                Reimbursement for economy class flight tickets (subject to a cap amount which will be advised)*
            </li>
            <li>
                Free Admission to Travex
            </li>
            <li>
                Entry to Welcome Diner Party, Luncheon, Farewell Cocktail Party and Tourism Seminar
            </li>
            <li>
                Complimentary return airport transfer to official hotels at stipulated timing
            </li>
            <li>
                Complimentary daily shuttles to/from official hotels, venue and official functions
            </li>
            <li>
                Complimentary accommodation for 4 nights/5 days including daily breakfast
            </li>
            <li>
                Special rates for post tours
            </li>
        </ul>
        <li>
            Partial Hosted Buyers receive:
        </li>
        <ul type="square">
            <li>
                Free Admission to Travex
            </li>
            <li>
                Entry to Welcome Diner Party, Luncheon, Farewell Cocktail Party and Tourism Seminar
            </li>
            <li>
                Complimentary return airport transfer to official hotels at stipulated timing
            </li>
            <li>
                Complimentary daily shuttles to/from official hotels, venue and official functions
            </li>
            <li>
                Complimentary accommodation for 4 nights/5 days including daily breakfast
            </li>
            <li>
                Special rates for post tours
            </li>
        </ul>
        <li>
            <strong>Cancellation</strong>
        </li>
        <ul type="square">
            <li>
                <strong>
                    NON REFUNDABLE administrative fee IDR. 2.900.000 for Fully
                    Hosted Buyer &amp; IDR. 2.175.000 for Partially Hosted
                    Buyer applied before 3 months prior to events
                </strong>
            </li>
            <li>
                <strong>
                    NON REFUNDABLE administrative fee IDR. 2.900.000 for Fully
                    Hosted Buyer &amp; IDR. 2.175.000 for Partially Hosted
                    Buyer applied for NO SHOW BUYER
                </strong>
            </li>
        </ul>
    </ul>
</ul>
<p>
    <strong> Dress Code</strong>
</p>
<ul type="disc">
    <li>
        All Buyers must wear business attire during BBTF events at Nusa Dua, Bali
    </li>
</ul>
<p align="center">
    <strong><?= $buyer_city ?>, <?= date('d-M-Y') ?></strong>
</p>
<p>
    <strong>Buyer Terms &amp; Conditions agreed by</strong>
</p>
<p>
    Full Name : <?= $gender ?>&nbsp;<?= $first_name ?>&nbsp;<?= $last_name ?>
</p>
<p>
    Company : <?= $company_name ?>
</p>
