<html>

<head>

</head>

<body>
    <div>
        <img width="150px" height="100px" src='<?= site_url() ?>assets/logo/logo_1.png'>
        <p>&nbsp;</p>
    </div>
<p style="text-align: center;"><strong>TERMS &amp; CONDITIONS</strong></p>
<p style="text-align: center;"><strong>PARTIALLY HOSTED</strong></p>
<p style="text-align: center;"><strong>7<sup>th </sup>BBTF, 9-13<sup>th</sup> June 2020</strong></p>
<p><strong>&nbsp;</strong></p>
<ul>
<li><strong>Event and Organizer</strong></li>
</ul>
<p>BBTF event&nbsp;is organized by ASITA BALI Chapter which the event will be held in Nusa Dua, Bali&nbsp;for B2B session</p>
<ul>
<li><strong>BBTF 2020 Buyer Profile:</strong></li>
</ul>
<p>All registrants must undergo accreditation before confirmation as a&nbsp;Buyer Participants</p>
<ul>
<ul>
<li>Travel wholesalers</li>
<li>Retailers and agents</li>
<li>Convention and incentive organizers</li>
<li>Other Tourism related</li>
</ul>
</ul>
<ul>
<li><strong>Buyer Mandatory:</strong></li>
<li>&nbsp;</li>
<ul>
<li>Partially Hosted Buyers must fulfilling 80% appointments Travex ( 26 appointments)</li>
<li>An administrative fee IDR. 2,175,000&nbsp;for Partially Hosted Buyer and NON REFUNDABLE</li>
<li>Payment by credit card will added a surcharge 4 % (master card &amp; visa logo only)</li>
</ul>
</ul>
<p>&nbsp;</p>
<ul>
<li><strong>Hosted Buyer Privileges</strong></li>
<ul>
<li>Partial Hosted Buyers&nbsp;receive:</li>
<ul>
<li>Free Admission to Travex</li>
<li>Entry to Welcome Diner Party, Luncheon, Farewell Cocktail Party and Tourism Seminar</li>
<li>Complimentary return airport transfer to official hotels at stipulated timing</li>
<li>Complimentary daily shuttles to/from official hotels, venue and official functions</li>
<li>Complimentary accommodation for 4 nights/5 days including&nbsp;daily&nbsp;breakfast</li>
<li>Special rates for post tours</li>
</ul>
<li><strong>Cancellation</strong></li>
<ul>
<li><strong>NON REFUNDABLE administrative fee IDR. 2,175,000&nbsp;for Partially Hosted Buyer applied before 3 months prior to events</strong></li>
<li><strong>NON REFUNDABLE administrative fee IDR.&nbsp;2,175,000&nbsp;for Partially Hosted Buyer applied for NO SHOW BUYER</strong></li>
</ul>
</ul>
</ul>
<p><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;Dress Code</strong></p>
<ul>
<li>All Buyers must wear business attire during BBTF events at Nusa Dua, Bali.</li>
</ul>
<p align="center">
        <strong><?= $buyer_city ?>, <?= date('d-M-Y') ?></strong>
    </p>
    <p>
        <strong>Buyer Terms &amp; Conditions agreed by</strong>
    </p>
    <p>
        Full Name :
        <?= $gender ?>&nbsp;
            <?= $first_name ?>&nbsp;
                <?= $last_name ?>
    </p>
    <p>
        Company :
        <?= $company_name ?>
    </p>
</body>
</html>
