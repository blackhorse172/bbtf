<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo  CNF_APPNAME ;?></title>

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: { "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"] },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!--end::Web font -->
	<link href="<?= base_url() ?>assets/registration/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
	<link href="<?= base_url() ?>assets/registration/base/style.bundle.css" rel="stylesheet" type="text/css" />
	<!--begin::Global Theme Bundle -->
	<script src="<?= base_url() ?>assets/registration/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<?= base_url() ?>assets/registration/base/scripts.bundle.js" type="text/javascript"></script>

			<!--end::Global Theme Bundle -->
	<link rel="shortcut icon" href="<?= base_url() ?>favicon.ico" />
	<style>
		@media screen and (max-width: 968px) {
		.logomain {
			width: 25%;
		}
	}

	@media only screen and (max-width: 600px) {
		.logomain {
			width: 50%;
			margin-left: 0%;
		}
	}

	@media (min-width: 1200px) {
		.logomain {
			width: 100%;
			margin-left: 40%;
		}
	}
	</style>
    <?php $this->load->view('layouts/admin/metronic/devextreme'); ?>
	
</head>
<<body class="m-page--wide m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default">
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
	<?php $this->load->view('layouts/registration/_header'); ?>
        <!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
		    <div class="m-grid__item m-grid__item--fluid m-wrapper">
				<!-- BEGIN: Subheader -->
				<!-- <div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title "><?= $pageTitle ?></h3>
							</div>
							<div>
								<span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
									<span class="m-subheader__daterange-label">
										<span class="m-subheader__daterange-title"></span>
										<span class="m-subheader__daterange-date m--font-brand"></span>
									</span>
									<a href="#" class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
										<i class="la la-angle-down"></i>
									</a>
								</span>
							</div>
						</div>
					</div> -->

					<!-- END: Subheader -->
                <div class="m-content">
				<?php echo $content ;?>
				</div>
				
            </div>
        </div>
    </div>
	<footer class="m-grid__item m-footer ">
				<div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
					<div class="m-footer__wrapper">
						<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
							<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
								<span class="m-footer__copyright">
									<?= date('Y') ?> &copy; BBTF - BALI & BEYOND TRAVEL FAIR
								</span>
							</div>
							<div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
								<ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
									<!-- <li class="m-nav__item">
										<a href="#" class="m-nav__link">
											<span class="m-nav__link-text">About</span>
										</a>
									</li>
									<li class="m-nav__item">
										<a href="#" class="m-nav__link">
											<span class="m-nav__link-text">Privacy</span>
										</a>
									</li>
									<li class="m-nav__item">
										<a href="#" class="m-nav__link">
											<span class="m-nav__link-text">T&C</span>
										</a>
									</li>
									<li class="m-nav__item">
										<a href="#" class="m-nav__link">
											<span class="m-nav__link-text">Purchase</span>
										</a>
									</li>
									<li class="m-nav__item m-nav__item--last">
										<a href="#" class="m-nav__link" data-toggle="m-tooltip" title="Support Center" data-placement="left">
											<i class="m-nav__link-icon flaticon-info m--icon-font-size-lg3"></i>
										</a>
									</li> -->
								</ul>
							</div>
						</div>
					</div>
				</div>
			</footer>
<script type="text/javascript">
	jQuery(document).ready(function ($) {
		

	});		
  
<?php 
if($msg = $this->session->flashdata("message")){
?>  
  toastr["<?php echo $msg['type'] ?>"]("<?php echo $msg['caption'] ?>","<?php echo $msg['title'] ?>"); 
<?php 
}
?>
  
</script>
    
</body>
</html>
