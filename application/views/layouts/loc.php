
<?php 

@header("Content-type: application/vnd.ms-word");
@header("Expires: 0");
@header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
@header("Content-Disposition: attachment; filename=Loc.doc");
 ?>
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
</head>
<body>
<p><strong>BALI &amp; BEYOND TRAVEL FAIR (BBTF) 2020</strong></p>
<p><strong>Letter of Confirmation</strong></p>
<p>Organization :</p>
<p>Full Name :</p>
<p>Designation :</p>
<p>Address :</p>
<p>Email :</p>
<p>Phone :</p>
<p>Fax :</p>
<p>Mobile :</p>
<p>Confirm participation as a Seller at the BBTF 2020 with detail as follow:</p>
<ol>
<?php foreach ($data as $key => $value)  : ?>
<li>Booth type : <strong>(<?= $value['product_name'] ?>)</strong></li>

<li>Price : <strong>(<?= number_format($value['product_price'],2) ?>) </strong></li>
<?php endforeach; ?>
</ol>

<p><strong>Note: The Price is Exclude Government Tax</strong></p>
<p>This is to confirm that <strong>we will not withdraw</strong> our participation and payment will be paid for the latest &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;... 2020.</p>
<p>Thank you.</p>
<p>(_______________________)</p>
<p>&nbsp;</p>
<p>Date : _________________</p>
<body>
</html>
