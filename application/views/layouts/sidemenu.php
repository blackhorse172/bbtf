<?php if($this->session->userdata('gid') != 0) : ?>
<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
        <li class="m-menu__item " aria-haspopup="true">
            <a href="<?= base_url() ?>dashboard" class="m-menu__link ">
                <i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
					<?php if($this->session->userdata('gid') == 4) : ?>
						<span class="m-menu__link-text">Buyer Profile</span>
					<?php else: ?>
						<span class="m-menu__link-text">Dashboard</span>
					<?php endif; ?>
                        <!-- @*<span class="m-menu__link-badge"><span class="m-badge m-badge--danger">2</span></span>*@ -->
                    </span>
                </span>
            </a>
		</li>
<?php else: ?>
	<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
      
<?php endif; ?>

		<?php if((int)$this->session->userdata('gid') == 1 || (int)$this->session->userdata('gid') == 2)
		{
			$this->load->view('layouts/_sidemenu_admin');
		}
		?>

		<?php if((int)$this->session->userdata('gid') == 5)
		{
			$this->load->view('layouts/_sidemenu_seller');
		}
		?>

		<?php if((int)$this->session->userdata('gid') == 4)
		{
			$this->load->view('layouts/_sidemenu_buyer');
		}
		?>

		<?php if((int)$this->session->userdata('bid') != 0)
		{
			$this->load->view('layouts/_sidemenu_sellersub');
		}
		?>
							
        
    </ul>
</div>
