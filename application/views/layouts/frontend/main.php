<!doctype html>
<html class="no-js" lang="en">
    <head>
        <!-- title -->
        <title><?= $company['company_name'] ?></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
        <meta name="author" content="ThemeZaa">
        <!-- description -->
        <meta name="description" content="">
        <!-- keywords -->
        <meta name="keywords" content="">
        <!-- favicon -->
        <link rel="shortcut icon" href="<?= base_url() ?>assets/frontend/images/favicon.png">
        <link rel="apple-touch-icon" href="<?= base_url() ?>assets/frontend/images/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>assets/frontend/images/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>assets/frontend/images/apple-touch-icon-114x114.png">
       <!-- CSS -->
		<link rel="stylesheet" type="text/css" media="all" href="<?= base_url() ?>assets/frontend/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="all" href="<?= base_url() ?>assets/frontend/css/main.css">
		<link rel="stylesheet" type="text/css" media="all" href="<?= base_url() ?>assets/frontend/css/animation.css">
		<link rel="stylesheet" type="text/css" media="all" href="<?= base_url() ?>assets/frontend/css/magnific-popup.css">
		<link rel="stylesheet" type="text/css" media="all" href="<?= base_url() ?>assets/frontend/css/responsive.css">
		<link rel="stylesheet" type="text/css" media="all" href="<?= base_url() ?>assets/frontend/css/slick.css">


		<meta name="title" content="BBTF">
		<meta name="keywords" content="BBTF">
		<script src="<?= base_url() ?>assets/frontend/js/jquery.min.js"></script>
		<script src="<?= base_url() ?>assets/frontend/js/jquery.validate.min.js"></script>
		<script src="<?= base_url() ?>assets/frontend/js/isotope.pkgd.min.js"></script>
		<script src="<?= base_url() ?>assets/frontend/js/jquery.magnific-popup.min.js"></script>
		<script src="<?= base_url() ?>assets/frontend/js/imagesloaded.pkgd.min.js"></script>
		<script src="<?= base_url() ?>assets/frontend/js/wow.min.js"></script>

		


    </head>
    <body>
		
		<!-- start header -->
		<?php $this->load->view('layouts/frontend/_header'); ?>
        <!-- end header -->
		<!-- start content -->
		<?php echo $content;?>
		<!-- end content -->
        <!--start footer-->

		<?php $this->load->view('layouts/frontend/_footer'); ?>

		<div class="alert alert-danger alert-dismissible fade in" role="alert">
		<h4>Pleases tilt your device view in portrait mode for better view</h4>
		<button type="button" class="btn btn-danger" data-dismiss="alert" aria-label="Close">Its ok. Lets continue in this mode.</button>
		</div>

		<script src="<?= base_url() ?>assets/frontend/js/bootstrap.min.js"></script> 
		<script src="<?= base_url() ?>assets/frontend/js/jquery.easing.1.3.js"></script>
		<script src="<?= base_url() ?>assets/frontend/js/slick.js"></script>
		<script src="<?= base_url() ?>assets/frontend/js/bootstrap-hover-dropdown.js"></script> 
		<script src="<?= base_url() ?>assets/frontend/js/jquery.inview.js"></script> 
		<script src="<?= base_url() ?>assets/frontend/js/main.js"></script> 
		<?php //if($commonJs) :?>
			<script src="<?= base_url() ?>assets/frontend/js/common.js"></script> 
		<?php //endif; ?>
		<!-- Contact --> 
		<?php if($this->session->flashdata("success_register") == true){ ?>
<script>
$(document).ready(function () {
	$("#modalDetail").modal("show")
});
</script>  
<?php } ?>

<?php 
									if($this->session->flashdata("message") == true){ ?>
									<script>
$(document).ready(function () {
	$("#modalDetail").modal("show")
});
</script>  
									<?php 
									}
								?>
								
<div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg" role="document" > <!-- style="max-width:100%; min-height:100%;"> -->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 id="empName" class="m--font-bold"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
					<?php echo $this->session->flashdata("message") ?><?php echo $this->session->flashdata("success_register") ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
	
				
     </body>
 </html>
