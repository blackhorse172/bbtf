<header id="top">
  <nav class="navbar navbar-default affix-top" role="navigation" data-spy="affix" data-offset-top="165">
    
    <div class="container-fluid"> 
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-content">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar line1"></span> <span class="icon-bar line2"></span> <span class="icon-bar line3"></span> </button>
          <a class="navbar-brand" href="<?= base_url() ?>"><img src="<?= base_url() ?>assets/frontend/images/logo.jpg" class="img-responsive"></a> </div>
        <!-- Edit: Banner -->
        <div class="banner-caption">
          <div class="img-full" style="font-size: 15px">
					<img height="150px" src="<?= base_url() ?>files/company/<?= $company['caption'] ?>" class="img-responsive">
					</div>
        </div>
        <!-- Edit: Top Button Link -->
        <div class="top-side-btn">
          
          <div> 
            <!-- Edit: Social Media Link -->
            <ul class="soc">
              <li><a class="soc-facebook" href="https://www.facebook.com/BaliBeyondTravelFair/" target="_blank"></a></li>
              <li><a class="soc-twitter" href="https://twitter.com/BBTFIndonesia" target="_blank"></a></li>
              <li><a class="soc-youtube" href="https://www.youtube.com/channel/UC5r6RGkUYRCgw9BjmkvDzaA?view_as=subscriber" target="_blank"></a></li>
              <li><a class="soc-instagram" href="https://www.instagram.com/bbtfindonesia/" target="_blank"></a></li>
            </ul>
            <!-- Edit: Email Subscribe --> 
             </div>
        </div>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="navbar-collapse collapse" id="navbar-collapse-1" style="height: 1px;">
        <ul class="nav navbar-nav">
          <li class=""><a href="<?= base_url() ?>"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
          <!-- <li topli="true" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" tid="1" href="#">Information</a> 
            <ul class="dropdown-menu" role="menu"> 
              <li><a href="<?= base_url() ?>floor-layout.html" title="Floor Layout">Floor Layout</a></li> 
              <li><a href="<?= base_url() ?>buyer-information.html" title="Buyer Information">Buyer Information</a></li> 
              <li><a href="<?= base_url() ?>seller-information.html" title="Buyer Information">Seller Information</a></li> 
              <li><a href="<?= base_url() ?>trade-buyer-information.html" title="Trade Buyer Information">Trade Buyer Information</a></li> 
              <li><a href="<?= base_url() ?>media-information.html" title="Floor Layout">Media Information</a></li> 
              <li><a href="<?= base_url() ?>seller-list.html" title="Floor Layout">Seller List</a></li> 
              <li><a href="<?= base_url() ?>buyer-list.html" title="Buyer List">Buyer List</a></li> 
            </ul>
          </li> -->
          <li><a href="schedule.html">Schedule</a> </li>
          <li topli="true" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" tid="23" href="#">Registration</a> 
            <ul class="dropdown-menu   " role="menu"> 	
              <li><a href="<?= base_url() ?>seller-registration.html" title="Seller Registration">Seller Registration</a></li> 
              <li><a href="<?= base_url() ?>buyer-registration.html" title="Buyer Registration">Buyer Registration</a></li> 
              <li><a href="<?= base_url() ?>media-registration.html" title="Media Registration">Media Registration</a></li> 
              <li><a href="<?= base_url() ?>trade-buyer-registration.html" title="Trade Buyer Registration">Trade Buyer Registration</a></li> 
            </ul>
          </li>
          <!-- <li><a tid="38" href="hosted-tour.html">Hosted Tour</a> </li> -->
          <li topli="true" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" tid="41" href="#">Media</a> 
            <ul class="dropdown-menu" role="menu">	
              <li><a href="<?= base_url() ?>photo-gallery.html" title="Photo Gallery">Photo Gallery</a></li> 
              <li><a href="<?= base_url() ?>news.html" title="News">News</a></li> 
            </ul>
          </li>
          <!-- <li topli="true" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" tid="24" href="#">Register Sponsor</a> 
            <ul class="dropdown-menu" role="menu">	
              <li><a href="#" title="Be Sponsor">Be Sponsor</a></li> 
            </ul>
          </li> -->

          <!-- Edit: Login Link
          <li class="dropdown login"><a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" href="#">LOGIN</a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Seller Login</a></li>
              <li><a href="#">Buyer Login</a></li>
              <li><a href="#">Media Login</a></li>
              <li><a href="#">Trade Buyer Login</a></li>
            </ul>
          </li> -->
          <li></li>
          <li>            
          </li>
        </ul>
      </div>
      <!-- /.navbar-collapse --> 
    </div>
    <!-- /.container-fluid --> 
  </nav>
</header>
