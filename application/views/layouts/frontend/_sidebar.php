<img class="display-none sm-display-block" src="<?= base_url() ?>assets/frontend/images/wi.png" style="position: absolute; z-index: 5; height: 105px; right: 0; top:78px"/>

            <!-- start sidebar section -->
            <section class="no-padding main-slider height-100 sm-display-none wow fadeInRight " style="position: relative; top: -100vh; margin-bottom: -100vh; z-index: 3;">
                    <div class="height-100 width-100 black-move position-relative">
                            <div class="" style="background-image:url('<?= base_url() ?>assets/frontend/images/sidebar.png'); background-repeat: no-repeat; background-position: right center; background-position-x: 100%; background-size: contain;">
                                <div class="container-fluid position-relative full-screen">
                                        <div class="col-md-12 slider-typography text-center text-md-left">
                                            <div class="slider-text-middle-main padding-four-lr">
                                                <div class="slider-text-middle text-right">
                                                    <div class="width-20 float-right" style="margin-top: 10%;">
                                                        <img src="<?= base_url() ?>assets/frontend/images/wi.png" style="position: absolute; z-index: 5; height: 185px; right: 0; top:127px"/>
                                                        <div class="row" style="position:relative; z-index:6">
                                                                
                                                                <div class="col-12 col-md-12 feature-box-1 sm-margin-30px-bottom wow fadeIn last-paragraph-no-margin" style="visibility: visible; animation-name: fadeIn; margin-bottom: 10px">
                                                                        <div class="margin-15px-bottom alt-font sd">
                                                                           <a href="#" class="text-link-white-2"> 
                                                                            <span class="h5 line-height-22 padding-20px-left reg md-padding-15px w-100 d-table-cell align-right" style="text-transform: uppercase; padding-right: 12px;">Seller<br>Registration</span>
                                                                            <h6 class="char-value text-white letter-spacing-minus-1 font-weight-600" style="border: solid 2px #fff; border-radius: 50%; padding: 0 9px; font-size: 18px;"><i class="fas fa-store-alt"></i></h6>
                                                                        </a>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                    <div class="col-12 col-md-12 feature-box-1 sm-margin-30px-bottom wow fadeIn last-paragraph-no-margin" style="visibility: visible; animation-name: fadeIn; margin-bottom: 10px">
                                                                            <div class="margin-15px-bottom alt-font sd">
                                                                                    <a href="#" class="text-link-white-2"> 
                                                                                <span class="h5 line-height-22 padding-20px-left reg md-padding-15px w-100 d-table-cell align-right" style="text-transform: uppercase; padding-right: 12px;">Buyer<br>Registration</span>
                                                                                <h6 class="char-value text-white letter-spacing-minus-1 font-weight-600" style="border: solid 2px #fff; border-radius: 50%; padding: 0 13px; font-size: 18px;"><i class="fas fa-shopping-bag"></i></h6>
                                                                                </a>
                                                                            </div>
                                                                            
                                                                    </div>
                                                                    <div class="col-12 col-md-12 feature-box-1 sm-margin-30px-bottom wow fadeIn last-paragraph-no-margin" style="visibility: visible; animation-name: fadeIn; margin-bottom: 10px">
                                                                                <div class="margin-15px-bottom alt-font sd">
                                                                                        <a href="#" class="text-link-white-2"> 
                                                                                    <span class="h5 line-height-22 padding-20px-left reg md-padding-15px w-100 d-table-cell align-right" style="text-transform: uppercase; padding-right: 12px;">Media<br>Registration</span>
                                                                                    <h6 class="char-value text-white letter-spacing-minus-1 font-weight-600" style="border: solid 2px #fff; border-radius: 50%; padding: 0 12px; font-size: 18px;"><i class="fas fa-pencil-alt"></i></h6>
                                                                                    </a>
                                                                                </div>
                                                                                
                                                                    </div>
                                                                    <div class="col-12 col-md-12 feature-box-1 sm-margin-30px-bottom wow fadeIn last-paragraph-no-margin" style="visibility: visible; animation-name: fadeIn; margin-bottom: 10px">
                                                                        <div class="margin-15px-bottom alt-font sd">
                                                                                            <a href="#" class="text-link-white-2"> 
                                                                                        <span class="h5 line-height-22 padding-20px-left reg md-padding-15px w-100 d-table-cell align-right" style="text-transform: uppercase; padding-right: 12px;">Trade Buyer<br>Registration</span>
                                                                                        <h6 class="char-value text-white letter-spacing-minus-1 font-weight-600" style="border: solid 2px #fff; border-radius: 50%; padding: 0 9px; font-size: 18px;"><i class="fas fa-user-friends"></i></h6>
                                                                                        </a>
                                                                        </div>
                                                                                    
                                                                    </div>
                                                        </div>
                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                            </div>
                            <!-- end slider item -->
                    </div>
            </section>
            <!--END sidebar section-->


            <!--Separator 1-->
            <section class="no-padding separator1" style="background-image: url('<?= base_url() ?>assets/frontend/images/wave1.png');"></section>
            <!--End Separator 1-->

            <!--Register mobile-->
            <section class="no-padding display-none sm-display-block separator1m" style="background-image: url('<?= base_url() ?>assets/frontend/images/wave2.png');"></section>
            <section class="no-padding display-none sm-display-block" style="background-color: #A38346;">
                <div class="row padding-40px-tb">
                    <div class="container-fluid">
                        <div class="col-12 col-md-12 feature-box-1 sm-margin-30px-bottom wow fadeIn last-paragraph-no-margin" style="visibility: visible; animation-name: fadeIn; margin-bottom: 10px">
                                <div class="alt-font padding-fifteen-left">
                                        <a href="#" class="text-link-white-2"> 
                                                    <span class="h5 line-height-22 padding-20px-left md-padding-15px w-50 d-table-cell text-right" style="text-transform: uppercase; padding-right: 12px;">Seller<br>Registration</span>
                                                    <h6 class="char-value text-white letter-spacing-minus-1 font-weight-600"><i class="fas fa-store-alt" style="border: solid 2px #fff; border-radius: 50%; padding: 10px 8px; font-size: 18px;"></i></h6>
                                        </a>
                                </div>
                        </div>                
                        
                        <div class="col-12 col-md-12 feature-box-1 sm-margin-30px-bottom wow fadeIn last-paragraph-no-margin" style="visibility: visible; animation-name: fadeIn; margin-bottom: 10px">
                            <div class="alt-font padding-fifteen-left">
                                <a href="#" class="text-link-white-2"> 
                                    <span class="h5 line-height-22 padding-20px-left md-padding-15px w-50 d-table-cell text-right" style="text-transform: uppercase; padding-right: 12px;">Buyer<br>Registration</span>
                                    <h6 class="char-value text-white letter-spacing-minus-1 font-weight-600"><i class="fas fa-shopping-bag" style="border: solid 2px #fff; border-radius: 50%; padding: 10px 11px; font-size: 18px;"></i></h6>
                                </a>
                            </div>                          
                        </div>
                        
                        <div class="col-12 col-md-12 feature-box-1 sm-margin-30px-bottom wow fadeIn last-paragraph-no-margin" style="visibility: visible; animation-name: fadeIn; margin-bottom: 10px">
                            <div class="alt-font padding-fifteen-left">
                                <a href="#" class="text-link-white-2"> 
                                    <span class="h5 line-height-22 padding-20px-left md-padding-15px w-50 d-table-cell text-right" style="text-transform: uppercase; padding-right: 12px;">Media<br>Registration</span>
                                        <h6 class="char-value text-white letter-spacing-minus-1 font-weight-600"><i class="fas fa-pencil-alt" style="border: solid 2px #fff; border-radius: 50%; padding: 10px 10px; font-size: 18px;"></i></h6>
                                </a>
                            </div>
                        </div>
                                                
                        <div class="col-12 col-md-12 feature-box-1 sm-margin-30px-bottom wow fadeIn last-paragraph-no-margin" style="visibility: visible; animation-name: fadeIn; margin-bottom: 10px">
                            <div class="alt-font padding-fifteen-left">
                                <a href="#" class="text-link-white-2"> 
                                    <span class="h5 line-height-22 padding-20px-left md-padding-15px w-50 d-table-cell text-right" style="text-transform: uppercase; padding-right: 12px;">Trade Buyer<br>Registration</span>
                                        <h6 class="char-value text-white letter-spacing-minus-1 font-weight-600"><i class="fas fa-user-friends" style="border: solid 2px #fff; border-radius: 50%; padding: 10px 8px; font-size: 18px;"></i></h6>
                                </a>
                            </div>            
                        </div>
                    </div>
                </div>
            </section>
            <section class="no-padding display-none sm-display-block separator1m" style="background-image: url('<?= base_url() ?>assets/frontend/images/wave1.png');"></section>
            <!--Register 1 mobile-->
