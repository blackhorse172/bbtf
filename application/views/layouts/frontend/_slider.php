<!-- start slider section -->
<section class="no-padding main-slider height-100 width-100 mobile-height wow fadeIn " data-wow-delay=".6s">
            <div class="swiper-full-screen swiper-container height-100 width-100 black-move">
                <div class="swiper-wrapper">
                    <!-- start slider item -->
                    <div class="swiper-slide cover-background" style="background-image:url('<?= base_url() ?>assets/frontend/images/image1.jpg');">
                        <div class="container-fluid position-relative full-screen">
                            <div class="col-md-12 slider-typography text-center text-md-left">
                                <div class="slider-text-middle-main padding-five-lr">
                                    <div class="slider-text-bottom">
                                        <h6 class="alt-font text-white font-weight-700 letter-spacing-minus-1 line-height-none width-55 lg-width-60 md-width-70 lg-line-height-auto sm-width-100 sm-margin-15px-bottom">Wonderful Sumba</h6>
                                        <p class="text-white text-medium margin-four-bottom width-40 lg-width-50 md-width-60 sm-width-100 sm-margin-15px-bottom">See the world. It's more fantastic than any dream made or paid for in factories. Ask for no guarantees, ask for no security.</p>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end slider item -->
                    <!-- start slider item -->
                    <div class="swiper-slide cover-background" style="background-image:url('<?= base_url() ?>assets/frontend/images/image2.jpg');">
                        <div class="container-fluid position-relative full-screen">
                            <div class="col-md-12 slider-typography text-center text-md-left">
                                <div class="slider-text-middle-main padding-five-lr">
                                    <div class="slider-text-bottom">
                                        <h6 class="alt-font text-white font-weight-700 letter-spacing-minus-1 line-height-none width-55 lg-width-60 md-width-70 lg-line-height-auto sm-width-100 sm-margin-15px-bottom">Wonderful Sumba</h1>
                                        <p class="text-white text-medium margin-four-bottom width-40 lg-width-50 md-width-60 sm-width-100 sm-margin-15px-bottom">See the world. It's more fantastic than any dream made or paid for in factories. Ask for no guarantees, ask for no security.</p>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end slider item -->
                    <!-- start slider item -->
                    <div class="swiper-slide cover-background" style="background-image:url('images/image3.jpg');">
                        <div class="container-fluid position-relative full-screen">
                            <div class="col-md-12 slider-typography text-center text-md-left">
                                <div class="slider-text-middle-main padding-five-lr">
                                    <div class="slider-text-bottom">
                                        <h6 class="alt-font text-white font-weight-700 letter-spacing-minus-1 line-height-none width-55 lg-width-60 md-width-70 lg-line-height-auto sm-width-100 sm-margin-15px-bottom">Wonderful Sumba</h6>
                                        <p class="text-white text-medium margin-four-bottom width-40 lg-width-50 md-width-60 sm-width-100 sm-margin-15px-bottom">See the world. It's more fantastic than any dream made or paid for in factories. Ask for no guarantees, ask for no security.</p>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end slider item -->
                </div>
                <!-- start slider pagination -->
                <div class="swiper-pagination swiper-full-screen-pagination"></div>
                <div class="swiper-button-next swiper-button-black-highlight d-none"></div>
                <div class="swiper-button-prev swiper-button-black-highlight d-none"></div>
                <!-- end slider pagination -->
            </div>
        </section>
        <!-- end slider section --> 
