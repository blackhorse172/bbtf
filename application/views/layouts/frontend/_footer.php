<footer class="padding-40px-bottom">
  <div class="container-fluid"> <a class="scroll-more" href="index.html/#top"> <span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span></a> 
    <!-- Edit: Footer Link-->
    <div class="row padding-20px-lr padding-30px-top">
      <div class="col-lg-3 col-md-6 col-sm-6 widget border-right border-color-medium-dark-gray md-no-border-right md-margin-30px-bottom sm-text-center sm-margin-100px-bottom">
          <span class="text-white text-large">BBTF SECRETARIAT OFFICE</span>
          <p class="margin-20px-top width-95 sm-width-100 text-white">JL. RAYA PUPUTAN NO. 41,<br>RENON DENPASAR, BALI 80235, INDONESIA
              </p>
			  <p class="text-white"><span class="glyphicon glyphicon-earphone"></span> +62361 4457111<br>
	     	  <span class="glyphicon glyphicon-earphone"></span> +62361 4457700<br>
                  <span class="glyphicon glyphicon-print"></span> +62361 244263<br>
                  <span class="glyphicon glyphicon-envelope"></span> INFO@BBTF.OR.ID <br>     
                  <span class="glyphicon glyphicon-globe"></span> WWW.BBTF.OR.ID</p>
 
          <!-- start social media -->
          <div class="social-icon-style-8 d-inline-block vertical-align-middle f-left sm-margin-30px-bottom">
            <ul class="soc">
              <li><a class="soc-facebook" href="https://www.facebook.com/" target="_blank"></a></li>
              <li><a class="soc-twitter" href="https://twitter.com/" target="_blank"></a></li>
              <li><a class="soc-linkedin" href="https://www.linkedin.com/" target="_blank"></a></li>
              <li><a class="soc-instagram" href="https://www.instagram.com/" target="_blank"></a></li>
            </ul>
          </div>
          <!-- end social media -->
      </div>
      <!-- start additional links -->
      <!-- <div class="col-lg-3 col-md-6 col-sm-6 widget border-right border-color-medium-dark-gray padding-45px-left md-padding-15px-left md-no-border-right md-margin-30px-bottom text-left text-md-left">
          <span class="text-white text-large alt-font margin-20px-bottom d-block">INFORMATIONS</span>
          <ul style="">
              <li><a class="text-link-white-2" href="<?= base_url() ?>buyer-information.html">BUYER INFORMATION</a></li>
              <li><a class="text-link-white-2" href="<?= base_url() ?>seller-information.html">SELLER INFORMATION</a></li>
              <li><a class="text-link-white-2" href="<?= base_url() ?>">TRADE BUYER INFORMATION</a></li>
              <li><a class="text-link-white-2" href="<?= base_url() ?>">MEDIA INFORMATION</a></li>
              <li><a class="text-link-white-2" href="<?= base_url() ?>seller-list">SELLER LIST</a></li>
              <li><a class="text-link-white-2" href="<?= base_url() ?>buyer-list">BUYER LIST</a></li>
          </ul>
      </div> -->
      <!-- end additional links -->
      <!-- start contact information -->
      <div class="col-lg-3 col-md-6 col-sm-6 widget border-right border-color-medium-dark-gray padding-45px-left md-padding-15px-left md-clear-both md-no-border-right sm-margin-30px-bottom text-left text-md-left">
          <span class="text-white text-large alt-font d-block margin-20px-bottom">MEDIA</span>
          <ul style="" class="margin-15px-bottom">
              <li><a class="text-link-white-2" href="photo-gallery.html">PHOTO GALLERY</a></li>
              <li><a class="text-link-white-2" href="news.html">NEWS</a></li>
          </ul>
          <span class="text-white text-large alt-font d-block margin-20px-bottom">MEMBER</span>
          <ul style="">
              <li><a class="text-link-white-2" href="login.html">LOGIN</a></li>
              <!-- <li><a class="text-link-white-2" href="#">REGISTER</a></li> -->
          </ul>
      </div>
      <!-- end contact information -->
      <!-- start instagram gallery -->
      <div class="col-lg-3 col-md-6 widget col-sm-6 padding-45px-left md-padding-15px-left text-left text-md-left">
          <span class="text-white text-large alt-font">GALLERY</span>
          <div class="instagram-follow-api margin-20px-top">
              <ul id="">
				  <!-- SnapWidget -->
<!-- SnapWidget -->
<!-- SnapWidget -->
<script src="https://snapwidget.com/js/snapwidget.js"></script>
<iframe src="https://snapwidget.com/embed/794175" class="snapwidget-widget" allowtransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden;  width:100%; "></iframe>                  <!-- <li><figure><a href="#" target="_blank">
                      <img src="http://placehold.it/149x149"></a>
                      </figure>
                  </li>
                  <li><figure><a href="#" target="_blank">
                      <img src="http://placehold.it/149x149"></a>
                      </figure>
                  </li>
                  <li><figure><a href="#" target="_blank">
                      <img src="http://placehold.it/149x149"></a>
                      </figure>
                  </li>
                  <li><figure><a href="#" target="_blank">
                      <img src="http://placehold.it/149x149"></a>
                      </figure>
                  </li>
                  <li><figure><a href="#" target="_blank">
                      <img src="http://placehold.it/149x149"></a>
                      </figure>
                  </li>
                  <li><figure><a href="#" target="_blank">
                      <img src="http://placehold.it/149x149"></a>
                      </figure>
                  </li> -->
              </ul>
          </div>
      </div>
      <!-- end instagram gallery-->
  </div>
  </div>
</footer>
