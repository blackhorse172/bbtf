<!-- <li class="m-menu__item " aria-haspopup="true">
            <a onclick="#" class="m-menu__link ">
                <i class="m-menu__link-icon flaticon-user"></i><span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">Profile</span>
                        @*<span class="m-menu__link-badge"><span class="m-badge m-badge--danger">2</span></span>*@
                    </span>
                </span>
            </a>
		</li> -->

<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-file"></i><span
									 class="m-menu__link-text">Payment</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item" aria-haspopup="true"><a href="<?= site_url() ?>Payment" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Payment History</span></a></li>
										<!-- <li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Menus" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Page Menu</span></a></li> -->
									</ul>
								</div>
		</li>

		<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-file"></i><span
									 class="m-menu__link-text">Order</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item" aria-haspopup="true"><a href="<?= site_url() ?>Transaction" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List</span></a></li>
										<li class="m-menu__item  m-menu__item" aria-haspopup="true"><a href="<?= site_url() ?>Seller/Order" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Order Booth</span></a></li>

										<!-- <li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Menus" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Page Menu</span></a></li> -->
									</ul>
								</div>
		</li>

		<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-layer"></i><span
									 class="m-menu__link-text">Booth Detail</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item" aria-haspopup="true"><a href="<?= site_url() ?>booths/delegations" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Seller Login</span></a></li>
										<!-- <li class="m-menu__item  m-menu__item" aria-haspopup="true"><a href="<?= site_url() ?>booths/name" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Name</span></a></li> -->

										<!-- <li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Menus" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Page Menu</span></a></li> -->
									</ul>
								</div>
		</li>

		<li class="m-menu__item " aria-haspopup="true">
								<a href="<?= base_url() ?>Schedule" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-event-calendar-symbol"></i><span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">schedule</span>
											<!-- @*<span class="m-menu__link-badge"><span class="m-badge m-badge--danger">2</span></span>*@ -->
										</span>
									</span>
								</a>
							</li>
<!-- 
		<li class="m-menu__item  m-menu__item--submenu m-menu__item m-menu__item" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-file-1"></i><span
									 class="m-menu__link-text">Article</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
								<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item" aria-haspopup="true"><a href="<?= site_url() ?>Article" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">List Article</span></a></li>
										<li class="m-menu__item " aria-haspopup="true"><a href="<?= site_url() ?>Article/Category" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Category Article</span></a></li>
									</ul>
								</div>
							</li> -->
