<?php
$title = "Create Article";
$action = base_url().'Article/Save';
if($isEdit == true){
$action = base_url().'Article/SaveEdit';
}
?>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
							<?php if($isEdit == true) : ?><input type="hidden" name="id" value="<?= $data->blogID ?>"/><?php endif ?>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Title</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="title" class="form-control m-input" id="title" placeholder="Enter post title">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Image thumbnail</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<?php if($isEdit == true) : ?>
												<?php if($data->image != '' || $data->image != null): ?>
												<img width="600px" id="image_exits" class="img-responsive" src="<?= base_url() ?>files/blog/title_image/<?= $data->image ?>">
												<?php endif ?>
											<?php endif ?>
										<input type="file" class="form-control m-input" name="image" id="exampleInputEmail1">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Content</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<textarea class="summernote" name="content" id="content"><?php if($isEdit == true) echo $data->content; ?></textarea>
										</div>
									</div>
								</div>
								<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Category</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<select class="form-control m-input" name="CatID" id="CatID">
												<option></option>
												<?php foreach($category as $item) : ?>
												<option value="<?= $item->CatID ?>"><?= $item->name ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Publish</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<select class="form-control m-input" name="publish" id="publish" required>
												<option></option>
												<option value="publish">Publish</option>
												<option value="unpublish">UnPublish</option>

											</select>
										</div>
									</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Submit</button>
												<button type="button" class="btn btn-secondary" onclick="window.location.href = '<?= site_url() ?>Article'">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
<?php if($isEdit == true) : ?>
<script>
$(document).ready(function(){
	$("#title").val("<?= $data->title; ?>");
	$("#publish").val("<?= $data->status; ?>");
	$("#CatID").val("<?= $data->CatID; ?>");
	//$("#content").html('<?= $data->content; ?>');




});
</script>
<?php endif ?>

<script type="text/javascript">
        $(document).ready(function(){
            $('.summernote').summernote({
                height: "300px",
                callbacks: {
                    onImageUpload: function(image) {
                        uploadImage(image[0]);
                    },
                    onMediaDelete : function(target) {
                        deleteImage(target[0].src);
                    }
                }
            });
 
            function uploadImage(image) {
                var data = new FormData();
                data.append("image", image);
                $.ajax({
                    url: "<?php echo site_url('Article/uploadImage')?>",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    type: "post",
                    success: function(url) {
                        $('.summernote').summernote("insertImage", url);
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            }
 
            function deleteImage(src) {
                $.ajax({
                    data: {src : src},
                    type: "POST",
                    url: "<?php echo site_url('Article/deleteImage')?>",
                    cache: false,
                    success: function(response) {
                        console.log(response);
                    }
                });
            }
 
        });
         
    </script>
