
<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "List Media";

?>

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= $title ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <!-- <li class="m-portlet__nav-item">
                    <a href="<?= site_url()?>Memberships/Create" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Create</span>
                        </span>
                    </a>
                </li> -->
                <li class="m-portlet__nav-item"></li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <div id="gridContainer"></div>
    </div>
</div>
<script>
    (function ($) {
        var dataSource = {
            load: function () {
                var items = $.Deferred();
                var data = <?= $data; ?>;
                items.resolve(data);
                return items.promise();
            }
        };
        $("#gridContainer").dxDataGrid({
            dataSource: dataSource,
            showBorders: true,
            showRowLines: true,
            columnAutoWidth: true,
            allowColumnResizing: true,
            allowColumnReordering: true,
            filterRow: {
                visible: true,
                applyFilter: "auto"
            },
            headerFilter: {
                visible: true
            },
            paging: {
                pageSize: 10
			},
			groupPanel: {
                        visible: true
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20],
                showInfo: true
			},
			export: {
                enabled: true
            },
            columns: [
				{
                    caption: "Media Class",
                    dataField: "media_class",
                    // cellTemplate: function (container, options) {
					// 	console.log(options);
                    //     var id = options.key.seller_id;
                    //     var IsActive = options.key.is_membership;
                    //      var message = "";
                    //     var title = "";
                    //     if (IsActive != "1") {
                    //          message = "Verify Membership ?";
                    //         title = options.key.seller_name;
                    //         $("<div>")
                    //             .append($("<button type='button' id='btnApprove_'" + id + "  onclick='fNonActiveMembership(" + id + ",\""+title+"\",\"" + message + "\")' class='btn m-btn--pill btn-danger'>NONE</button>"))
                    //             .appendTo(container);
                    //     } else {
                    //          message = "Cancel Membership ?";
                    //         title = options.key.seller_name;
                    //         $("<div>")
                    //             .append($("<button type='button' id='btnApprove_'" + id + " onclick='fNonActiveMembership(" + id + ",\""+title+"\",\"" + message + "\")'  class='btn m-btn--pill btn-success'>ACTIVE</button>"))
                    //             .appendTo(container);
                    //     }

                    // }
				},
                {
                    caption: "Company Name",
                    dataField: "company_name",
				},

				{
                    caption: "Company Address",
                    dataField: "company_address",
				},

				{
                    caption: "City",
                    dataField: "city",
				},

				{
                    caption: "Media Phone",
                    dataField: "media_phone",
				},

				{
                    caption: "Full Name",
                    dataField: "full_name",
				},

				{
                    caption: "Email",
                    dataField: "email",
				},
				{
                    caption: "Position",
                    dataField: "position",
				},

				{
                    caption: "Mobile",
                    dataField: "mobile",
				},
                {
                    caption: "Active",
                    dataField: "is_active",
                    cellTemplate: function (container, options) {
						console.log(options);
                        var id = options.key.id;
                        var IsActive = options.key.is_active;
                         var message = "";
                        var title = "";
                        if (IsActive != "1") {
                             message = "Active ?";
                            title = options.key.company_name;
                            $("<div>")
                                .append($("<button type='button' id='btnApprove_'" + id + "  onclick='fNonActive(" + id + ",\""+title+"\",\"" + message + "\")' class='btn m-btn--pill btn-danger'>NON ACTIVE</button>"))
                                .appendTo(container);
                        } else {
                             message = "Non Active ?";
                            title = options.key.company_name;
                            $("<div>")
                                .append($("<button type='button' id='btnApprove_'" + id + " onclick='fNonActive(" + id + ",\""+title+"\",\"" + message + "\")'  class='btn m-btn--pill btn-success'>ACTIVE</button>"))
                                .appendTo(container);
                        }

                    }
				},
				
				
				{
                alignment: "center",
                pinned: true,
                cellTemplate: function (container, options) {
                 var id = options.key.id;
                    $("<div>")
                    //  .append($("<button type='button' onclick='flinkEdit(" + id + ")'  class='btn m-btn--pill btn-success'><i class='flaticon-edit'></i></button>"))
                    .append($("<a onclick='flinkEdit(" + id + ")'  class='btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only'><i class='flaticon-edit'></i></a>"))

                 .appendTo(container);
                }
                },
              

			],
			summary: {
                        totalItems: [
                            {
                                column: "company_name",
                                summaryType: "count",
                                //valueFormat:"#,##0.##"
                            },
                            
                        ]
                    }
        });
    })(jQuery);

    function fNonActive(id,title,msg) {
        swal({
            title: title,
            text: msg,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Memberships/nonActiveMedia/" + id,"_self");
            }
        })
	}
	
	function fNonActiveMembership(id,title,msg) {
        swal({
            title: title,
            text: msg,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Memberships/nonActiveSellerMembership/" + id,"_self");
            }
        })
	}
	
    function flinkEdit(id) {
                window.open("<?= base_url() ?>Memberships/Media/" + id,"_self");

    }
</script>

