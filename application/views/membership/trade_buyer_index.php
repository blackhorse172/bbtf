
<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "List Trade Buyer";

?>

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= $title ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <!-- <li class="m-portlet__nav-item">
                    <a href="<?= site_url()?>Memberships/Create" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Create</span>
                        </span>
                    </a>
                </li> -->
                <li class="m-portlet__nav-item"></li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <div id="gridContainer"></div>
    </div>
</div>
<script>
    (function ($) {
        var dataSource = {
            load: function () {
                var items = $.Deferred();
                var data = <?= $data; ?>;
                items.resolve(data);
                return items.promise();
            }
        };
        $("#gridContainer").dxDataGrid({
            dataSource: dataSource,
            showBorders: true,
            showRowLines: true,
            columnAutoWidth: true,
            allowColumnResizing: true,
            allowColumnReordering: true,
            filterRow: {
                visible: true,
                applyFilter: "auto"
            },
            headerFilter: {
                visible: true
            },
            paging: {
                pageSize: 10
			},
			groupPanel: {
                        visible: true
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20],
                showInfo: true
			},
			export: {
                enabled: true
            },
            columns: [
				{
                    caption: "Confirmation Status",
                    dataField: "membership_verify",
                    cellTemplate: function (container, options) {
						console.log(options);
                        var id = options.key.id;
                        var IsActive = options.key.membership_verify;
                         var message = "";
                        var title = "";
                        if (IsActive != "1") {
                             message = "is confirm ?";
                            title = options.key.company_name;
                            $("<div>")
                                .append($("<button type='button' id='btnApprove_'" + id + "  onclick='fNonActive(" + id + ",\""+title+"\",\"" + message + "\")' class='btn m-btn--pill btn-danger'>Unconfirm</button>"))
                                .appendTo(container);
                        } else {
                             message = "Non Active ?";
                            title = options.key.company_name;
                            $("<div>")
                                .append($("<button type='button' id='btnApprove_'" + id + " class='btn m-btn--pill btn-success'>Confirm</button>"))
                                .appendTo(container);
                        }

                    }
				},
                {
                    caption: "Trade Buyer Status",
					dataField: "status_tradebuyer",
					cellTemplate: function (container,options){
						var status = options.key.status_tradebuyer
						if(status == "NEW") {
                            $("<div>")
                                .append($("<span class='m-nav__link-badge'><span class='m-badge m-badge--warning m-badge--wide m-badge--rounded'> New </span></span>"))
                                .appendTo(container);
						}
						
						else if(status == "CLASSIFIED") {
                            $("<div>")
                                .append($("<span class='m-nav__link-badge'><span class='m-badge m-badge--focus m-badge--wide m-badge--rounded'> CLASSIFIED </span></span>"))
                                .appendTo(container);
						}
						
						else if(status == "PAID") {
                            $("<div>")
                                .append($("<span class='m-nav__link-badge'><span class='m-badge m-badge--success m-badge--wide m-badge--rounded'> Paid </span></span>"))
								.appendTo(container);
								
                        } else if(status == "REJECT") {
                            $("<div>")
                                .append($("<span class='m-nav__link-badge'><span class='m-badge m-badge--danger m-badge--wide m-badge--rounded'> Decline </span></span>"))
                                .appendTo(container);
						}

						else if(status == "IGNORE") {
                            $("<div>")
                                .append($("<span class='m-nav__link-badge'><span class='m-badge m-badge--focus m-badge--wide m-badge--rounded'> Ignore </span></span>"))
                                .appendTo(container);
						}
					}
				},

				{
                    caption: "Set Ignore",
					cellTemplate: function (container, options) {
						
                        var status = options.key.status_tradebuyer;
                         var trxstatus = options.key.trxstatus;
						var title = "";
						var id = options.key.id;
						if(status == "CLASSIFIED"){
							$("<div>")
                                .append($("<span onclick='fsetIgnore(" + id + ")' class='m-nav__link-badge'><span class='m-badge m-badge--focus m-badge--wide m-badge--rounded'>Set Ignore </span></span>"))
                                .appendTo(container);
						}
                       
                            
                        }
				},
				{
                    caption: "Send Inv Date",
                    dataField: "date_crt",
					format: "date"
				},
				{
                    caption: "Total Send Inv Days",
                    dataField: "total_days",
					format: "number"
				},

				{
                    caption: "Resend INV",
                    dataField: "total_days",
					cellTemplate: function (container, options) {
						
                        var status = options.key.total_days;
                        
						var id = options.key.id;
						if(status > 1 ){
							$("<div>")
                                .append($("<span onclick='fsetInv(" + id + ")' class='m-nav__link-badge'><span class='m-badge m-badge--focus m-badge--wide m-badge--rounded'>Resend</span></span>"))
                                .appendTo(container);
						}
                       
                            
                        }
				},


				{
                    caption: "Company Name",
                    dataField: "company_name",
				},


				{
                    caption: "Company Address",
                    dataField: "company_address",
				},

				{
                    caption: "City",
                    dataField: "city",
				},
				{
                    caption: "Office Phone",
                    dataField: "company_phone",
				},

				{
                    caption: "Delegate Name",
                    dataField: "full_name",
				},

				{
                    caption: "Email",
                    dataField: "company_email",
				},
				{
                    caption: "Position",
                    dataField: "position",
				},

				{
                    caption: "Mobile",
                    dataField: "mobile",
				},

                
				{
                alignment: "center",
                pinned: true,
                cellTemplate: function (container, options) {
                 var id = options.key.id;
                    $("<div>")
                    //  .append($("<button type='button' onclick='flinkEdit(" + id + ")'  class='btn m-btn--pill btn-success'><i class='flaticon-edit'></i></button>"))
                    .append($("<a onclick='flinkEdit(" + id + ")'  class='btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only'><i class='flaticon-edit'></i></a>"))
					.append("&nbsp;")
					.append($("<a onclick='flinkDelete(" + id + ")'  class='btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only'><i class='flaticon-cancel'></i></a>"))
                 .appendTo(container);
                }
                },
              

			],
			summary: {
                        totalItems: [
                            {
                                column: "company_name",
                                summaryType: "count",
                                //valueFormat:"#,##0.##"
                            },
                            
                        ]
                    }
        });
	})(jQuery);
	
	function flinkDelete(id,title,msg) {
        swal({
			title: "Delete",
            text: "Are You Sure ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>TradeBuyer/delete/" + id,"_self");
            }
        })
	}

	function fsetIgnore(id) {
        swal({
			title: "Ignore",
            text: "Do you want Set Ignore ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            //console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>TradeBuyer/setIgnoreStatus/" + id,"_self");
            }
        })
	}

	function fsetInv(id) {
        swal({
			title: "Resend Invoice",
            text: "Do you want Resend Invoice ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            //console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>TradeBuyer/resendInv/" + id,"_self");
            }
        })
	}
	

    function fNonActive(id,title,msg) {
        swal({
            title: title,
            text: msg,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>TradeBuyer/confirm/" + id,"_self");
            }
        })
	}
	
	function fNonActiveMembership(id,title,msg) {
        swal({
            title: title,
            text: msg,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Memberships/nonActiveSellerMembership/" + id,"_self");
            }
        })
	}
	
    function flinkEdit(id) {
                window.open("<?= base_url() ?>Memberships/TradeBuyer/" + id,"_self");

    }
</script>

