
<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "List Seller";

?>

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= $title ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <!-- <li class="m-portlet__nav-item">
                    <a href="<?= site_url()?>Memberships/Create" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Create</span>
                        </span>
                    </a>
                </li> -->
                <li class="m-portlet__nav-item"></li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <div id="gridContainer"></div>
    </div>
</div>
<script>
    (function ($) {
        var dataSource = {
            load: function () {
                var items = $.Deferred();
                var data = <?= $data; ?>;
                items.resolve(data);
                return items.promise();
            }
        };
        $("#gridContainer").dxDataGrid({
            dataSource: dataSource,
            showBorders: true,
            showRowLines: true,
            columnAutoWidth: true,
            allowColumnResizing: true,
            allowColumnReordering: true,
            filterRow: {
                visible: true,
                applyFilter: "auto"
            },
            headerFilter: {
                visible: true
            },
            paging: {
                pageSize: 10
			},
			groupPanel: {
                        visible: true
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20],
                showInfo: true
			},
			export: {
                enabled: true
            },
            columns: [
                {
                    caption: "Seller Name",
                    dataField: "seller_name",
				},

				{
                    caption: "Seller Category",
                    dataField: "category_name",
				},

				{
                    caption: "Seller email",
                    dataField: "seller_email",
				},

				{
                    caption: "Seller phone",
                    dataField: "seller_phone",
				},

				{
                    caption: "Seller Country",
                    dataField: "country_name",
				},
				{
					caption: "Seller Listing",
					dataField: "is_listing",
					cellTemplate: function (container, options) {
						//console.log(options);
                        var id = options.key.seller_id;
                        
						var listing = options.key.is_listing;
						var title = "";
						if(listing == "" || listing == null || listing == 0)
						{
							$("<div>")
                                .append($("<span onclick='fSendListing(" + id + ")' class='m-nav__link-badge'><span class='m-badge m-badge--danger m-badge--wide m-badge--rounded'>Un Listing</span></span>"))
								.appendTo(container);
								
						} else {
							$("<div>")
                                .append($("<span onclick='fSendListing(" + id + ")' class='m-nav__link-badge'><span class='m-badge m-badge--focus m-badge--wide m-badge--rounded'>Listing</span></span>"))
								.appendTo(container);
						}
                            
                        }
				},


                {
                    caption: "Active",
                    dataField: "is_active",
                    cellTemplate: function (container, options) {
						console.log(options);
                        var id = options.key.seller_id;
                        var IsActive = options.key.is_active;
                         var message = "";
                        var title = "";
                        if (IsActive != "1") {
                             message = "Active ?";
                            title = options.key.seller_name;
                            $("<div>")
                                .append($("<button type='button' id='btnApprove_'" + id + "  onclick='fNonActive(" + id + ",\""+title+"\",\"" + message + "\")' class='btn m-btn--pill btn-danger'>NON ACTIVE</button>"))
                                .appendTo(container);
                        } else {
                             message = "Non Active ?";
                            title = options.key.seller_name;
                            $("<div>")
                                .append($("<button type='button' id='btnApprove_'" + id + " onclick='fNonActive(" + id + ",\""+title+"\",\"" + message + "\")'  class='btn m-btn--pill btn-success'>ACTIVE</button>"))
                                .appendTo(container);
                        }

                    }
				},
				
				// {
                //     caption: "Membership",
                //     dataField: "is_membership",
                //     cellTemplate: function (container, options) {
				// 		console.log(options);
                //         var id = options.key.seller_id;
                //         var IsActive = options.key.is_membership;
                //          var message = "";
                //         var title = "";
                //         if (IsActive != "1") {
                //              message = "Verify Membership ?";
                //             title = options.key.seller_name;
                //             $("<div>")
                //                 .append($("<button type='button' id='btnApprove_'" + id + "  onclick='fNonActiveMembership(" + id + ",\""+title+"\",\"" + message + "\")' class='btn m-btn--pill btn-danger'>NONE</button>"))
                //                 .appendTo(container);
                //         } else {
                //              message = "Cancel Membership ?";
                //             title = options.key.seller_name;
                //             $("<div>")
                //                 .append($("<button type='button' id='btnApprove_'" + id + " onclick='fNonActiveMembership(" + id + ",\""+title+"\",\"" + message + "\")'  class='btn m-btn--pill btn-success'>ACTIVE</button>"))
                //                 .appendTo(container);
                //         }

                //     }
				// },
				{
                alignment: "center",
                pinned: true,
                cellTemplate: function (container, options) {
                 var id = options.key.seller_id;
                    $("<div>")
                    //  .append($("<button type='button' onclick='flinkEdit(" + id + ")'  class='btn m-btn--pill btn-success'><i class='flaticon-edit'></i></button>"))
                    .append($("<a onclick='flinkEdit(" + id + ")'  class='btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only'><i class='flaticon-edit'></i></a>"))

                 .appendTo(container);
                }
                },
              

			],
			masterDetail: {
                                enabled: true,
                                template: function(container, options) { 
                                    var masterData = options.data;
                                    $("<div>")
                                        .addClass("show-detail")
                                        .text("Loading....")
                                        .appendTo(container);
                                    var trx_detail = "<?php echo base_url() ?>Transaction/json_getSellerSub"
                                    $.ajax({
                                      url: trx_detail,
                                      type: 'post',
                                      dataType: 'json',
                                      data: {seller_id: masterData.seller_id},
                                    })
                                    .done(function(result) {
                                      data_detail = result;
                                      $("<div>")
                                        .addClass("master-detail-caption")
                                        .text("Sub Detail")
                                        .appendTo(container);
                                    $("<div>")
                                        .dxDataGrid({
                                            showBorders: true,
            showRowLines: true,
            columnAutoWidth: true,
            allowColumnResizing: true,
            allowColumnReordering: true,
            filterRow: {
                visible: true,
                applyFilter: "auto"
            },
            headerFilter: {
                visible: true
            },
            paging: {
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20],
                showInfo: true
			},
			
            columns: [
                {
                    caption: "Transaction Code",
                    dataField: "trx_code",
				},

				{
                    caption: "Transaction Status",
					dataField: "trx_status",
					alignment:"center",
					cellTemplate: function (container, options) {
						console.log(options);
                        var id = options.key.id;
                        var status = options.key.trx_status;
                        if (status == 0) {
                            $("<div>")
                                .append($("<span  class='m-badge m-badge--warning m-badge--wide'>WAITING PAYMENT</span>"))
                                .appendTo(container);
                        } else if(status == 1) {
                            
                            $("<div>")
                                .append($("<span  class='m-badge m-badge--brand m-badge--wide'>WAITING CONFIRMED</span>"))
                                .appendTo(container);
						}
						else if(status == 2) {
                            
                            $("<div>")
                                .append($("<span  class='m-badge m-badge--success m-badge--wide'>PAID</span>"))
                                .appendTo(container);
                        }

                    }
				},
				{
                    caption: "Transaction Date",
					dataField: "trx_date",
					dataType: "date"
				},
				// {
                //     caption: "Transaction Type",
				// 	dataField: "trx_type",
					
				// },

				{
                    caption: "Transaction SubTotal",
					dataField: "trx_subtotal",
					dataType: "number",
                	format: "#,##0.##"
				},
				{
                    caption: "Tax",
					dataField: "trx_tax",
					dataType: "number",
                	format: "#,##0.##"
				},
				{
                    caption: "Service Tax",
					dataField: "trx_service_tax",
					dataType: "number",
                	format: "#,##0.##"
				},

				{
                    caption: "TOTAL",
					dataField: "trx_total",
					dataType: "number",
                	format: "#,##0.##"
				},
				
				{
                    caption: "ACTION",
					dataField: "trx_status",
					cellTemplate: function (container, options) {
						//console.log(options);
						var id = options.key.id;
                        var trx_code = options.key.trx_code;
						
                        var status = options.key.trx_status;
                        if (status == 0) {
                            $("<div>")
                                .append($("-"))
                                .appendTo(container);
                        } else if(status == 1) {
                            
                            $("<div>")
                                .append($("<span onclick='flinkEdit(\""+trx_code+"\")' class='m-badge m-badge--brand m-badge--wide'>CONFIRM PAYMENT</span>"))
                                .appendTo(container);
						}
						else if(status == 2) {
                            
                            $("<div>")
                                .append($("<span onclick='fboothDetail(\""+trx_code+"\")'  class='m-badge m-badge--success m-badge--wide'>Booth Number</span>"))
                                .appendTo(container);
                        }

						else if(status == 3) {
                            
                            $("<div>")
                                .append($("<span class='m-badge m-badge--warning m-badge--wide'>Void</button>"))
                                .appendTo(container);
                        }

                    }
				},
                

			],
                                            dataSource: result
                                        }).appendTo(container);

                                        $('.show-detail').css("display","none");
                                    })
                                    .fail(function() {
                                      console.log("error");
                                    })
                                    .always(function() {
                                      
                                    });
                                }
                            },
			summary: {
                        totalItems: [
                            {
                                column: "seller_name",
                                summaryType: "count",
                                //valueFormat:"#,##0.##"
                            },
                            
                        ]
                    }
        });
    })(jQuery);

    function fNonActive(id,title,msg) {
        swal({
            title: title,
            text: msg,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Memberships/nonActiveSeller/" + id,"_self");
            }
        })
	}
	
	function fNonActiveMembership(id,title,msg) {
        swal({
            title: title,
            text: msg,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Memberships/nonActiveSellerMembership/" + id,"_self");
            }
        })
	}
	
    function flinkEdit(id) {
                window.open("<?= base_url() ?>Memberships/Seller/" + id,"_self");

    }

	function fboothDetail(id) {
                window.open("<?= base_url() ?>Seller/boothDetail/" + id,"_blank");

	}

	function fSendListing(id) {
        swal({
			title: "Listing",
            text: "Do you want listing / unlisting ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            //console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Seller/listing/" + id,"_self");
            }
        })
    }

</script>

