<div class="row">
<div class="col-xl-12">
<div class="m-portlet " id="m_portlet">
<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-file"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Media Profile
												</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<!-- <li class="m-portlet__nav-item">
													<a href="#" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air">
														<span>
															<i class="la la-plus"></i>
															<span>Add Event</span>
														</span>
													</a>
												</li> -->
											</ul>
										</div>
									</div>

<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= base_url()?>Memberships/update/media">
<input type="hidden" name="id" value="<?= $profile['id'] ?>">
<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['company_name'] ?>" class="form-control m-input" name="company_name" id="company_name" aria-describedby="bank_name" placeholder="company_name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Address*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['company_address'] ?>" class="form-control m-input" name="company_address" id="company_address" aria-describedby="bank_name" placeholder="company_address" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">City*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<input type="text" class="form-control m-input" value="<?= $profile['city'] ?>" name="city" id="city" aria-describedby="city" placeholder="city" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Phone*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['media_phone'] ?>" name="media_phone" id="media_phone" aria-describedby="media_phone" placeholder="media_phone" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">mr/mrs*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="gender" name="gender" required>
										<option value=""></option>
										<option value="MR">MR</option>
										<option value="MRS">MRS</option>
										<option value="MRS">MS</option>
									</select>	
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Full Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['full_name'] ?>" class="form-control m-input" name="full_name" id="full_name" aria-describedby="seller_name" placeholder="full_name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Position</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['position'] ?>" class="form-control m-input" name="position" id="position" aria-describedby="postion" placeholder="postion">
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Mobile</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['mobile'] ?>" class="form-control m-input" name="mobile" id="mobile" aria-describedby="mobile" placeholder="mobile" >
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Country*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control m-select2" id="country_code" name="country_code" required>
										<option value="">Select Country</option>
										
										<?php foreach ($country as $key => $value) : ?>
											<option value="<?= $value['country_code'] ?>"><?= $value['country_name'] ?></option>
										<?php endforeach;?>
									</select>	
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input value="<?= $profile['email'] ?>" type="email" class="form-control m-input" name="email" id="email" aria-describedby="email" placeholder="email" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Profile*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<textarea name="company_profile" class="form-control" id="" rows="10" required><?= $profile['company_profile'] ?></textarea>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Media Class*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="media_class" name="media_class" required>
										<option value=""></option>
										<option value="HOSTED_MEDIA">Hosted Media</option>
										<option value="MEDIA">Media</option>
									</select>	
										</div>
									</div>

								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Update</button>
											</div>
										</div>
									</div>
								</div>
							</form>

</div>
</div>
</div>

<script>
	
$ (function ($) {
	'use strict';
	$(".m-select2").select2();

	$("#country_code").val("<?= $profile['country_code'] ?>");
	$("#media_class").val("<?= $profile['media_class'] ?>");
	$("#gender").val("<?= $profile['gender'] ?>");

	$("#country_code").select2().trigger('change');


	// $.each(target, function (i, e) { 
	// 	 $("#"+e.replace(" ","_")).attr("checked",true);
	// });

	$("#email").change(function (e) { 
		e.preventDefault();
		$.ajax({
			type: "get",
			url: "<?= base_url() ?>Media/check_email_exist",
			data: {email: $(this).val()},
			dataType: "json",
			success: function (response) {
				if(response.status){
					$("#email").val("");
					$("#email").focus();
					swal({
                                title: "Warning",
                                text: "EMAIL IS EXIST, PLEASE LOG IN OR RESET PASSWORD",
                                type: "warning"
							});
					
					
				}
			}
		});
	});
});
</script>
