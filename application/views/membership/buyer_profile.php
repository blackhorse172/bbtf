<div class="row">
<div class="col-xl-12">
<div class="m-portlet " id="m_portlet">
<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-file"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Buyer
												</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
										<ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--right m-tabs-line-danger" role="tablist">
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link active" data-toggle="tab" href="#profile" role="tab">
														Profile
													</a>
												</li>
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#flight" role="tab">
														Flight
													</a>
												</li>
												
											</ul>
										</div>
									</div>
									<div class="m-portlet__body">
										<div class="tab-content">
											<div class="tab-pane active" id="profile">
											<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= base_url()?>Memberships/saveBuyer">
<input type="hidden" name="buyer_id" value="<?= $profile['buyer_id'] ?>">
<input type="hidden" name="user_id" value="<?= $profile['user_id'] ?>">
<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">mr/mrs*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="gender" name="gender" required>
										<option value=""></option>
										<option value="MR">MR</option>
										<option value="MRS">MRS</option>
										<option value="MS">MS</option>
										
										
									</select>	
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">First Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['first_name'] ?>" class="form-control m-input" name="first_name" id="first_name" aria-describedby="seller_name" placeholder="first_name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Last Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['last_name'] ?>" class="form-control m-input" name="last_name" id="last_name" aria-describedby="last_name" placeholder="last_name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['company_name'] ?>" class="form-control m-input" name="company_name" id="company_name" aria-describedby="bank_name" placeholder="company_name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Job Title</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['job_title'] ?>" class="form-control m-input" name="job_title" id="job_title" aria-describedby="job_title" placeholder="Job Title" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Profile*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<textarea name="buyer_company_profile" value="<?= $profile['buyer_company_profile'] ?>" class="form-control" id="" rows="10" required><?= $profile['buyer_company_profile'] ?></textarea>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Nature Of Business*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control m-select2" id="nature_of_business" name="nature_of_business" required>
										<option value="">- None -</option><option value="L">Leisure</option><option value="M">Mice</option><option value="S">Special Interest</option><option value="I">Incentive</option><option value="W">Wholesale</option><option value="E">Event Organizer</option><option value="R">Retail</option><option value="D">DMC</option>
									</select>	
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">City*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<input type="text" class="form-control m-input" value="<?= $profile['buyer_city'] ?>" name="buyer_city" id="buyer_city" aria-describedby="buyer_city" placeholder="Job Title" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Country*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control m-select2" id="buyer_country_code" name="buyer_country_code" required>
										<option value="">Select Country</option>
										
										<?php foreach ($country as $key => $value) : ?>
											<option value="<?= $value['country_code'] ?>"><?= $value['country_name'] ?></option>
										<?php endforeach;?>
									</select>	
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="email" readonly value="<?= $profile['buyer_email'] ?>" class="form-control m-input" name="email" id="email" aria-describedby="email" placeholder="email" required>
										</div>
									</div>
									<div class="form-group m-form__group row" style="display: none;">
										<label class="col-form-label col-lg-3 col-sm-12">Password*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="password" class="form-control m-input" name="password" min-length="8" id="password" aria-describedby="password" placeholder="Password min 8 length (fill the field if want change password)">
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Phone*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['buyer_phone'] ?>" name="buyer_phone" id="buyer_phone" aria-describedby="buyer_phone" placeholder="Phone" required>
										</div>
									</div>
									
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Website</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['buyer_website'] ?>" name="buyer_website" id="buyer_website" aria-describedby="buyer_website" placeholder="Website">
										</div>
									</div>
<!-- info -->
<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Do you selling Bali and Beyond product?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" value="<?= $profile['do_you_selling_bali_and_beyond_product'] ?>" id="do_you_selling_bali_and_beyond_product" name="do_you_selling_bali_and_beyond_product" required>
										<option value=""></option>
										<option value="Y">YES</option>
										<option value="N">NO</option>
										
									</select>	
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Please state your local partner in Bali/ Indonesia (preferable name and email address)?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['please_state_you_local_partner_in_bali_indonesia'] ?>" name="please_state_you_local_partner_in_bali_indonesia" id="please_state_you_local_partner_in_bali_indonesia" aria-describedby="please_state_you_local_partner_in_bali_indonesia" placeholder="" required>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Your ID member of ASITA, IATA, PATA, ASEANTA or others member organization</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['member_id'] ?>" name="member_id" id="member_id" aria-describedby="member_id" placeholder="" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Since when you promoted Indonesia destination?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input"  value="<?= $profile['since_when_you_promoted_indonesia_destination'] ?>" name="since_when_you_promoted_indonesia_destination" id="since_when_you_promoted_indonesia_destination" aria-describedby="since_when_you_promoted_indonesia_destination" placeholder="" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Is this your first time attending on BBTF?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['is_this_your_first_time_attending_on_bbtf'] ?>"  name="is_this_your_first_time_attending_on_bbtf" id="is_this_your_first_time_attending_on_bbtf" aria-describedby="buyer_website" placeholder="" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">How did you know about BBTF?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['how_did_you_know_about_bbtf'] ?>"  name="how_did_you_know_about_bbtf" id="how_did_you_know_about_bbtf" aria-describedby="" placeholder="" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Indicated 3 main destinations in Indonesia you want to meet in BBTF?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $profile['indicated_main_destination'] ?>" name="indicated_main_destination" id="indicated_main_destination" aria-describedby="how_did_you_know_about_bbtf" placeholder="" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Which products are you interested in?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<div class="m-checkbox-list">
											<?php foreach ($target_market as $key => $value) : ?>
														<label class="m-checkbox">
															
															<input type="checkbox" id="<?=  str_replace(' ','_',$value['gs_value']) ?>" value="<?=  $value['gs_value'] ?>" name="which_products_are_you_interested_in[]"> <?=  $value['gs_value'] ?>
															<span></span>
														</label>
											<?php endforeach; ?>
														
										</div>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Do you sell any package in South East Asia?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="do_you_sell_any_package_in_south_east_asia" name="do_you_sell_any_package_in_south_east_asia" required>
										<option value=""></option>
										<option value="Y">YES</option>
										<option value="N">NO</option>
									</select>	
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Please state your biggest partner in South East Asia (Company and Contact name)?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['biggest_partner'] ?>" class="form-control m-input" name="biggest_partner" id="biggest_partner" aria-describedby="biggest_partner" placeholder="" required>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Please state your potential group to Bali and Beyond in 2020-2021</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['potential_group'] ?>" class="form-control m-input" name="potential_group" id="potential_group" aria-describedby="potential_group" placeholder="" required>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Do you intending to explore beyond Bali after BBTF?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['explore_bbtf'] ?>" class="form-control m-input" name="explore_bbtf" id="explore_bbtf" aria-describedby="buyer_website" placeholder="" required>
										</div>
									</div>

									<?php if($profile['trxstatus'] == 'SUCCESS' ) : ?>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Buyer Hotel?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="buyer_hotel" name="buyer_hotel">
										<option></option>
										<?php foreach ($hotels as $key => $value) : ?>
											<option value="<?= $value['id'] ?>"><?= $value['hotel_name'] ?></option>
										<?php endforeach;?>
									</select>	
										</div>
									</div>
									<?php endif; ?>

									<?php if($profile['buyer_class'] == null || $profile['buyer_class'] == '' ) : ?>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Buyer Classification?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="buyer_class" name="buyer_class" required>
										<option value=""></option>
										<option value="1">FULLY HOSTED</option>
										<option value="2">Partial Hosted</option>
										<option value="6">Offer As Seller</option>
										<!-- <option value="4">Ignore</option> -->
										<option value="7">Offer As Trade Buyer</option>
									</select>	
										</div>
									</div>
									<?php endif; ?>


								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Update</button>
											</div>
										</div>
									</div>
								</div>
							</form>
								</div>
								<!-- flight detail -->
											<div class="tab-pane" id="flight">
<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= base_url()?>Buyer/saveEditFlight">
<input type="hidden" name="buyer_id" value="<?= $profile['buyer_id'] ?>">
								
<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-file"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Arrival Details
												</h3>
											</div>
										</div>
									</div>
									

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Arrival Date*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="arrival_date" name="arrival_date" required>
										<option value=""></option>
										<?php foreach ($arrivalDate as $key => $value) : ?>
											<option value="<?= $value['gs_value'] ?>"><?= date_format(date_create($value['gs_value']),"d-M-Y"); ?></option>
										<?php endforeach;?>
									</select>	
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Flight Number*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $flight['arrival_flight_number'] ?>" class="form-control m-input" name="arrival_flight_number" id="arrival_flight_number" aria-describedby="seller_name" placeholder="Flight Number" required>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Arrival Time*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $flight['arrival_time'] ?>" class="form-control m-input" name="arrival_time" id="arrival_time" aria-describedby="seller_name" placeholder="Arrival Time" required>
										
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Terminal</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="arrival_terminal" name="arrival_terminal" required>
										<option value=""></option>
										<option value="DOMESIC">DOMESIC</option>
										<option value="INTERNATIONAL">INTERNATIONAL</option>
									</select>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Arrange for arrival transport</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<div class="m-radio-inline">
																	<label class="m-radio">
																		<input type="radio" id="arrival_transport_1" name="arrival_transport" value="1"> Yes
																		<span></span>
																	</label>
																	<label class="m-radio">
																		<input type="radio" checked id="arrival_transport_2" name="arrival_transport" value="0"> No
																		<span></span>
																	</label>
																</div>
										</div>
									</div>

									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-file"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Departure Details
												</h3>
											</div>
										</div>
									</div>

								<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Departure Date*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="depart_date" name="depart_date" required>
										<?php foreach ($departDate as $key => $value) : ?>
											<option value="<?= $value['gs_value'] ?>"><?=  date_format(date_create($value['gs_value']),"d-M-Y");  ?></option>
										<?php endforeach;?>
									</select>	
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Flight Number*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $flight['depart_flight_number'] ?>" class="form-control m-input" name="depart_flight_number" id="depart_flight_number" aria-describedby="seller_name" placeholder="Flight Number" required>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Departure Time*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $flight['depart_time'] ?>" class="form-control m-input" name="depart_time" id="depart_time" aria-describedby="seller_name" placeholder="Arrival Time" required>
										
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Terminal</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="depart_terminal" name="depart_terminal" required>
										<option value=""></option>
										<option value="DOMESIC">DOMESIC</option>
										<option value="INTERNATIONAL">INTERNATIONAL</option>
									</select>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Arrange for Departure transport</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<div class="m-radio-inline">
										
																	<label class="m-radio">
																		<input type="radio" id="depart_transport_1"  name="depart_transport" value="1"> Yes
																		<span></span>
																	</label>
																	<label class="m-radio">
																		<input type="radio" checked id="depart_transport_2" name="depart_transport" value="0"> No
																		<span></span>
																	</label>
																</div>
										</div>
									</div>

									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-file"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Passport
												</h3>
											</div>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Passport</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
												<?php if($flight['buyer_passport'] != '' || $flight['buyer_passport'] != null): ?>
												<img width="600px" id="image_exits" class="img-responsive" src="<?= base_url() ?>files/passport/<?= $flight['buyer_passport'] ?>">
												<?php else: ?>
													<?php endif ?>
													<input type="file" class="form-control m-input" name="image" id="image">

										
										</div>
									</div>
								
										<div class="m-portlet__foot m-portlet__foot--fit">
											<div class="m-form__actions m-form__actions">
												<div class="row">
													<div class="col-lg-9 ml-lg-auto">
														<button type="submit" class="btn btn-brand">Update</button>

													</div>
												</div>
											</div>
										</div>
									

								
							</form>

</div>
</div>
</div>
<?php if($flight != null) : ?>

	<script>
	
	$ (function ($) {
		'use strict';
		$(".m-select2").select2();
	
		$("#arrival_date").val("<?= $flight['arrival_date'] ?>");
		$("#arrival_terminal").val("<?= $flight['arrival_terminal'] ?>");
	
		$("#depart_date").val("<?= $flight['depart_date'] ?>");
		$("#depart_terminal").val("<?= $flight['depart_terminal'] ?>");

		$("input[name=arrival_transport]").removeAttr("checked");
		$("input[name=depart_transport]").removeAttr("checked");

		if(<?= $flight['depart_transport'] ?> == 1){
			
			$("#depart_transport_1").attr("checked",true);
		} else {
			$("#depart_transport_2").attr("checked",true);
		}

		if(<?= $flight['arrival_transport'] ?> == 1){
			
			$("#arrival_transport_1").attr("checked",true);
		} else {
			$("#arrival_transport_2").attr("checked",true);
		}
		//depart_transport
			
		// $("input").attr("disabled",true);
		// $("select").attr("disabled",true);
		
		$('#depart_time').inputmask(
        "hh:mm", {
        placeholder: "HH:MM", 
        insertMode: false, 
        showMaskOnHover: false,
        max: 24
      }
      );

	  $('#arrival_time').inputmask(
        "hh:mm", {
        placeholder: "HH:MM", 
        insertMode: false, 
        showMaskOnHover: false,
        max: 24
      }
      );

	
		
	});
	</script>
<?php endif; ?>

											</div>
										</div>
									</div>


</div>
</div>
</div>
<?php $profile['which_products_are_you_interested_in'] = $profile['which_products_are_you_interested_in'] == "" ? [] : $profile['which_products_are_you_interested_in'];  ?>
<script>
	
$ (function ($) {
	'use strict';
	$(".m-select2").select2();

	$("#do_you_selling_bali_and_beyond_product").val("<?= $profile['do_you_selling_bali_and_beyond_product'] ?>");
	$("#buyer_country_code").val("<?= $profile['buyer_country_code'] ?>");
	$("#buyer_country_code").select2().trigger('change');
	$("#nature_of_business").val("<?= $profile['nature_of_business'] ?>");
	$("#nature_of_business").select2().trigger('change');
	$("#do_you_sell_any_package_in_south_east_asia").val("<?= $profile['do_you_sell_any_package_in_south_east_asia'] ?>");
	$("#gender").val("<?= $profile['gender'] ?>");
	$("#buyer_hotel").val("<?= $profile['buyer_hotel'] ?>");
	
	var target = <?= $profile['which_products_are_you_interested_in'] ?>;

	if(target.length > 0) {
		$.each(target, function (i, e) { 
		 $("#"+e.replace(" ","_")).attr("checked",true);
	});
	}
	

	$("#email").change(function (e) { 
		e.preventDefault();
		$.ajax({
			type: "get",
			url: "<?= base_url() ?>Seller/check_email_exist",
			data: {email: $(this).val()},
			dataType: "json",
			success: function (response) {
				if(response.status){
					$("#email").val("");
					$("#email").focus();
					swal({
                                title: "Warning",
                                text: "EMAIL IS EXIST, PLEASE LOG IN OR RESET PASSWORD",
                                type: "warning"
							});
					
					
				}
			}
		});
	});
});
</script>
