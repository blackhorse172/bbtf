<div class="row">
<div class="col-xl-12">
<div class="m-portlet " id="m_portlet">
<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-file"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Trade Buyer Profile
												</h3>
											</div>
										</div>
									</div>

<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= base_url()?>Memberships/saveTradeBuyer">
								<div class="m-portlet__body">
								<input type="hidden" value="<?= $profile['id'] ?>" name="id" />
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['company_name'] ?>" class="form-control m-input" name="company_name" id="company_name" aria-describedby="bank_name" placeholder="company_name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Address*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['company_address'] ?>" class="form-control m-input" name="company_address" id="company_address" aria-describedby="bank_name" placeholder="company_address" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">City*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<input type="text" value="<?= $profile['city'] ?>" class="form-control m-input" name="city" id="city" aria-describedby="city" placeholder="city" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Office Phone*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['company_phone'] ?>" class="form-control m-input" name="company_phone" id="company_phone" aria-describedby="company_phone" placeholder="company_phone" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Email *</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="email" value="<?= $profile['company_email'] ?>" class="form-control m-input" name="company_email" id="company_email" aria-describedby="company_email" placeholder="company_email" required>
										</div>
									</div>
									<!-- <div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">mr/mrs*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="gender" name="gender" required>
										<option value=""></option>
										<option value="MR">MR</option>
										<option value="MRS">MRS</option>
										<option value="MS">MS</option>
									</select>	
										</div>
									</div> -->
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Full Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['full_name'] ?>" class="form-control m-input" name="full_name" id="full_name" aria-describedby="seller_name" placeholder="full_name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Position</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['position'] ?>" class="form-control m-input" name="position" id="position" aria-describedby="postion" placeholder="postion">
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Mobile</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $profile['mobile'] ?>" class="form-control m-input" name="mobile" id="mobile" aria-describedby="mobile" placeholder="mobile" >
										</div>
									</div>
									

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="email" value="<?= $profile['company_email'] ?>" class="form-control m-input" name="email" id="email" aria-describedby="email" placeholder="email" required>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12"> Country*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control m-select2" id="country_code" name="country_code" required>
										<option value="">Select Country</option>
										<?php foreach ($country as $key => $value) : ?>
											<option value="<?= $value['country_code'] ?>"><?= $value['country_name'] ?></option>
										<?php endforeach;?>
									</select>	
										</div>
									</div>

									

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Number of days</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="number_of_days" name="number_of_days" required>
										<option value="">Select days</option>
										<?php for ($i=1; $i <= $days; $i++) : ?>
											<option value="<?= $i ?>"><?= $i ?></option>
										<?php endfor;?>
									</select>	
										</div>
									</div>
									
									
									
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Update</button>
											</div>
										</div>
									</div>
								</div>
							</form>

</div>
</div>
</div>

<script>
	
$ (function ($) {
	'use strict';
	$(".m-select2").select2();

	$("#number_of_days").val("<?= $profile['number_of_days'] ?>");
	$("#country_code").val("<?= $profile['country_code'] ?>");
	$("#country_code").select2().trigger('change');


$("#terms").click(function (e) { 
	e.preventDefault();
	$("#termsModal").modal({ backdrop: "static", keyboard: false });
	$("#termsModal").modal("show");

});

	$("#email__").change(function (e) { 
		e.preventDefault();
		$.ajax({
			type: "get",
			url: "<?= base_url() ?>Media/check_email_exist",
			data: {email: $(this).val()},
			dataType: "json",
			success: function (response) {
				if(response.status){
					$("#email").val("");
					$("#email").focus();
					swal({
                                title: "Warning",
                                text: "EMAIL IS EXIST, PLEASE LOG IN OR RESET PASSWORD",
                                type: "warning"
							});
					
					
				}
			}
		});
	});
});
</script>
