
<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "List Buyer";

?>

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= $title ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?= site_url()?>Memberships/Create" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Create</span>
                        </span>
                    </a>
                </li>
                <li class="m-portlet__nav-item"></li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <div id="gridContainer"></div>
    </div>
</div>
<div id="resetPasswordModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<form class="form-inline" method="post" action="<?= base_url() ?>Buyer/ResetPassword">
						<input type="hidden" id="buyerId" name="buyerId" value="">
						<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">New Password</label>
										<div class="col-lg-12 col-md-7 col-sm-12">
											<input type="text" readonly name="newPassword" id="newPassword" class="form-control">	
										</div>
						</div>
						<br>
						<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										<button type="submit" class="btn btn-brand">Submit Password</button>
									</div>
						
				</form>
			</div>
		</div>
	</div>
</div>
<script>
    (function ($) {
        var dataSource = {
            load: function () {
                var items = $.Deferred();
                var data = <?= $data; ?>;
                items.resolve(data);
                return items.promise();
            }
        };
        $("#gridContainer").dxDataGrid({
            dataSource: dataSource,
            showBorders: true,
            showRowLines: true,
            columnAutoWidth: true,
            allowColumnResizing: true,
            allowColumnReordering: true,
            filterRow: {
                visible: true,
                applyFilter: "auto"
            },
            headerFilter: {
                visible: true
            },
            paging: {
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20],
                showInfo: true
            },
            columns: [
				{
					caption: "Buyer Status",
					dataField: "buyer_status",
					cellTemplate: function (container, options) {
						//console.log(options);
                        var id = options.key.buyer_id;
                        var bClass = options.key.buyer_class;
                         var message = "";
						var title = "";
						var trxstatus = options.key.trxstatus;
						var status = options.key.buyer_status;

						if(status == "NEW") {
                            $("<div>")
                                .append($("<span class='m-nav__link-badge'><span class='m-badge m-badge--warning m-badge--wide m-badge--rounded'> New </span></span>"))
                                .appendTo(container);
						}
						
						else if(status == "CLASSIFIED") {
                            $("<div>")
                                .append($("<span class='m-nav__link-badge'><span class='m-badge m-badge--focus m-badge--wide m-badge--rounded'> CLASSIFIED </span></span>"))
                                .appendTo(container);
						}
						
						else if(status == "PAID") {
                            $("<div>")
                                .append($("<span class='m-nav__link-badge'><span class='m-badge m-badge--success m-badge--wide m-badge--rounded'> Paid </span></span>"))
								.appendTo(container);
								
                        } else if(status == "REJECT") {
                            $("<div>")
                                .append($("<span class='m-nav__link-badge'><span class='m-badge m-badge--danger m-badge--wide m-badge--rounded'> Decline </span></span>"))
                                .appendTo(container);
						}

						else if(status == "IGNORE") {
                            $("<div>")
                                .append($("<span class='m-nav__link-badge'><span class='m-badge m-badge--focus m-badge--wide m-badge--rounded'> Ignore </span></span>"))
                                .appendTo(container);
						}
						
				}
			},
			{
                    caption: "Buyer Classification",
                    dataField: "buyer_class",
					cellTemplate: function (container, options) {
						//console.log(options);
                        var id = options.key.buyer_id;
                        var bClass = options.key.buyer_class;
                         var message = "";
                        var title = "";
                        if (bClass == "1") {
                            $("<div>")
                                .append($("<span class='m-nav__link-badge'><span class='m-badge m-badge--success m-badge--wide m-badge--rounded'> Full Hosted </span></span>"))
                                .appendTo(container);
                        } else if(bClass == "2") {
                            $("<div>")
                                .append($("<span class='m-nav__link-badge'><span class='m-badge m-badge--success m-badge--wide m-badge--rounded'> Partial Hosted </span></span>"))
                                .appendTo(container);
                        }

						else if(bClass == "3") {
                            $("<div>")
                                .append($("<span class='m-nav__link-badge'><span class='m-badge m-badge--warning m-badge--wide m-badge--rounded'> Over AS Seller </span></span>"))
                                .appendTo(container);
                        }

						else if(bClass == "4") {
                            $("<div>")
                                .append($("<span class='m-nav__link-badge'><span class='m-badge m-badge--danger m-badge--wide m-badge--rounded'> IGNORE </span></span>"))
                                .appendTo(container);
                        }
						else if(bClass == "5") {
                            $("<div>")
                                .append($("<span class='m-nav__link-badge'><span class='m-badge m-badge--danger m-badge--wide m-badge--rounded'> Decline </span></span>"))
                                .appendTo(container);
                        }
						else if(bClass == "6") { 
							$("<div>")
                                .append($("<span class='m-nav__link-badge'><span class='m-badge m-badge--danger m-badge--wide m-badge--rounded'> Offer As Seller </span></span>"))
                                .appendTo(container);

						}

						else if(bClass == "7") { 
							$("<div>")
                                .append($("<span class='m-nav__link-badge'><span class='m-badge m-badge--danger m-badge--wide m-badge--rounded'> Offer As Trade Buyer </span></span>"))
                                .appendTo(container);

						}
							
                    
				}
				},
				{
                    caption: "Gender",
                    dataField: "gender",
				},
                {
                    caption: "First Name",
                    dataField: "first_name",
				},

				{
                    caption: "Last Name",
                    dataField: "last_name",
				},

				{
                    caption: "Company Name",
                    dataField: "company_name",
				},

				{
                    caption: "Email",
                    dataField: "buyer_email",
				},

				{
                    caption: "Buyer Phone",
                    dataField: "buyer_phone",
				},
				{
                    caption: "Date Register",
					dataField: "dtm_crt",
					dataType: "date",
					format: "dd-MMMM-yyyy HH:mm:ss"  
				},

				{
                    caption: "Total Send Inv Days",
                    dataField: "total_days",
					format: "number"
				},

				{
                    caption: "Resend INV",
                    dataField: "total_days",
					cellTemplate: function (container, options) {
						
                        var status = options.key.total_days;
                        
						var id = options.key.buyer_id;
						if(status > 1 ){
							$("<div>")
                                .append($("<span onclick='fsetInv(" + id + ")' class='m-nav__link-badge'><span class='m-badge m-badge--focus m-badge--wide m-badge--rounded'>Resend</span></span>"))
                                .appendTo(container);
						}
                       
                            
                        }
				},
				
				{
					caption: "Buyer Listing",
					dataField: "is_listing",
					cellTemplate: function (container, options) {
						//console.log(options);
                        var id = options.key.buyer_id;
                        var bClass = options.key.buyer_class;
						var listing = options.key.is_listing;
						var title = "";
						if(listing == "" || listing == null || listing == 0)
						{
							$("<div>")
                                .append($("<span onclick='fSendListing(" + id + ")' class='m-nav__link-badge'><span class='m-badge m-badge--danger m-badge--wide m-badge--rounded'>Un Listing</span></span>"))
								.appendTo(container);
								
						} else {
							$("<div>")
                                .append($("<span onclick='fSendListing(" + id + ")' class='m-nav__link-badge'><span class='m-badge m-badge--focus m-badge--wide m-badge--rounded'>Listing</span></span>"))
								.appendTo(container);
						}
                            
                        }
				},
				{
					caption: "Send Login Access",
					//dataField:"password"
					cellTemplate: function (container, options) {
                        var id = options.key.buyer_id;
						var bClass = options.key.buyer_class;
						var title = "Send Access";
						var color = "focus";
						var status = options.key.buyer_status;
						if(options.key.password != "")
						{
							title = "Sent";
							color = "success";
						}

						
                        if(status == "PAID")
						{
							$("<div>")
                                .append($("<span onclick='fSendLoginAccess(" + id + ")' class='m-nav__link-badge'><span class='m-badge m-badge--"+color+" m-badge--wide m-badge--rounded'>"+title+"</span></span>"))
                                .appendTo(container);
						}
                            
                        }
				},
				{
                    caption: "Reset Password",
					cellTemplate: function (container, options) {
						//console.log(options);
                        var id = options.key.buyer_id;
                        var bClass = options.key.buyer_class;
                         var message = "";
                        var title = "";
						var status = options.key.buyer_status;

						if(status == "PAID")
						{
							$("<div>")
                                .append($("<span onclick='fResetPassword(" + id + ")' class='m-nav__link-badge'><span class='m-badge m-badge--focus m-badge--wide m-badge--rounded'>Reset Password </span></span>"))
                                .appendTo(container);
						}
                       
                            
                        }
				},

				{
                    caption: "Password",
                    dataField: "password_view",
				},

				{
                    caption: "Set Ignore",
					cellTemplate: function (container, options) {
						//console.log(options);
                        var id = options.key.buyer_id;
                        var bClass = options.key.buyer_class;
                         var trxstatus = options.key.trxstatus;
						var title = "";
						
						if(trxstatus == null && (bClass !='4' && bClass !='5') ){
							$("<div>")
                                .append($("<span onclick='fsetIgnore(" + id + ")' class='m-nav__link-badge'><span class='m-badge m-badge--focus m-badge--wide m-badge--rounded'>Set Ignore </span></span>"))
                                .appendTo(container);
						}
                       
                            
                        }
				},

                {
                alignment: "center",
                pinned: true,
                cellTemplate: function (container, options) {
                 var id = options.key.buyer_id;
                    $("<div>")
                    //  .append($("<button type='button' onclick='flinkEdit(" + id + ")'  class='btn m-btn--pill btn-success'><i class='flaticon-edit'></i></button>"))
                    .append($("<a onclick='flinkEdit(" + id + ")'  class='btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only'><i class='flaticon-edit'></i></a>"))
                    .append("&nbsp;")
					.append($("<a onclick='flinkDelete(" + id + ")'  class='btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only'><i class='flaticon-cancel'></i></a>"))

                 .appendTo(container);
                }
                },

				{
                alignment: "center",
                pinned: true,
                cellTemplate: function (container, options) {
                 var id = options.key.buyer_id;
                    $("<div>")
                    //  .append($("<button type='button' onclick='flinkEdit(" + id + ")'  class='btn m-btn--pill btn-success'><i class='flaticon-edit'></i></button>"))

                 .appendTo(container);
                }
                },
              

            ]
        });
    })(jQuery);

    function fNonActive(id,title,msg) {
        swal({
			title: title,
            text: msg,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Memberships/nonActiveBuyer/" + id,"_self");
            }
        })
	}

	function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
}
	
	function fResetPassword(id) {
        swal({
			title: "Reset",
            text: "Do you want Reset Password ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            //console.log(e);
            if (e.value) {
				//window.open("<?= base_url() ?>Buyer/ResetPassword/" + id,"_self");
				$("#resetPasswordModal").modal("show");
				var newPass = randomString(6,"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
				$("#newPassword").val(newPass);
				$("#buyerId").val(id);

            }
        })
	}

	function fsetIgnore(id) {
        swal({
			title: "Ignore",
            text: "Do you want Set Ignore ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            //console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Buyer/setIgnoreStatus/" + id,"_self");
            }
        })
	}

	function fsetInv(id) {
        swal({
			title: "Resend Invoice",
            text: "Do you want Resend Invoice ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            //console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Buyer/resendInv/" + id,"_self");
            }
        })
	}
	
	function fSendLoginAccess(id) {
        swal({
			title: "Send Access",
            text: "Do you want Send Login Access ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            //console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Buyer/sendLoginAccess/" + id,"_self");
            }
        })
	}
	
	function fSendListing(id) {
        swal({
			title: "Listing",
            text: "Do you want listing / unlisting ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            //console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Buyer/listing/" + id,"_self");
            }
        })
    }

	function flinkDelete(id,title,msg) {
        swal({
			title: "Delete",
            text: "Are You Sure ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Buyer/delete/" + id,"_self");
            }
        })
    }

    function flinkEdit(id) {
                window.open("<?= base_url() ?>Memberships/Buyer/" + id,"_self");

    }
</script>

