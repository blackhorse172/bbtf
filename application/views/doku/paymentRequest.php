<?php
$title = "Please Wait"."&nbsp;";
$action = "https://staging.doku.com/Suite/Receive";

?>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />


<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?> <span id="timer"></span>
										</h3>
									</div>
								</div>
							</div>

							<form style="display: none" class="m-form m-form--fit m-form--label-align-right" action="https://staging.doku.com/Suite/Receive" id="MerchatPaymentPage" target="_self" name="MerchatPaymentPage" method="post">
								<div class="m-portlet__body">
								<table width="600" border="0" cellspacing="1" cellpadding="5">
	<!-- <tr>
		<td width="100" class="field_label">BASKET</td>
		<td width="500" class="field_input"><input name="BASKET" type="text" id="BASKET" value="FEE,1000000.00,1,1000000.00;" size="100" /></td>
	</tr> -->
	<input type="hidden" name="BASKET" type="text" id="BASKET" value="FEE,<?= $params['amount'] ?>,1,<?= $params['amount'] ?>;" size="100" />
	<!-- <tr>
		<td width="100" class="field_label">MALLID</td>
		<td width="500" class="field_input"></td>
	</tr> -->
	<input type="hidden" name="MALLID" class="form-control" type="text" id="MALLID" value="<?= $config['mallId'] ?>" size="12" />
	<!-- <tr>
		<td width="100" class="field_label">CHAINMERCHANT</td>
		<td width="500" class="field_input"></td>
	</tr> -->
	<input type="hidden" name="CHAINMERCHANT" type="text" id="CHAINMERCHANT" value="NA" size="12" />
	<!-- <tr>
		<td class="field_label">CURRENCY</td>
		<td class="field_input"></td>
	</tr>
	<tr>
		<td class="field_label">PURCHASECURRENCY</td>
		<td class="field_input"></td>
	</tr> -->
	<input type="hidden" name="CURRENCY" type="text" id="CURRENCY" value="360" size="3" maxlength="3" />
	<input type="hidden" name="PURCHASECURRENCY" type="text" id="PURCHASECURRENCY" value="360" size="3" maxlength="3" />
	<tr>
		<td class="field_label">AMOUNT</td>
		<td class="field_input"><input name="AMOUNT" type="text" id="AMOUNT" readonly value="<?= $params['amount'] ?>" size="12" /></td>
	</tr>
	<!-- <tr>
		<td class="field_label">PURCHASEAMOUNT</td>
		<td class="field_input"><input name="PURCHASEAMOUNT" type="text" id="PURCHASEAMOUNT" value="<?= $params['amount'] ?>" size="12" /></td>
	</tr> -->
	<input type="hidden" name="PURCHASEAMOUNT" type="text" id="PURCHASEAMOUNT" value="<?= $params['amount'] ?>" size="12" />
	<tr>
		<td class="field_label">TRANSIDMERCHANT</td>
		<td class="field_input"><input name="TRANSIDMERCHANT" value="<?= $config['invoice'] ?>" readonly type="text" id="TRANSIDMERCHANT" size="16" /></td>
	</tr>
	<!-- <tr>
		<td class="field_label">WORDS</td>
		<td class="field_input">
	</tr> -->
	<input type="hidden" type="text" value="<?= $words ?>"  id="WORDS" name="WORDS" size="60" />
	<!-- <tr>
		<td class="field_label">REQUESTDATETIME</td>
		<td class="field_input">
		</td>
	</tr> -->
	<input type="hidden" name="REQUESTDATETIME" value="<?= date('YmdHis') ?>" type="text" id="REQUESTDATETIME" size="14" maxlength="14" />
	<!-- <tr>
		<td class="field_label">SESSIONID</td>
		<td class="field_input"></td>
	</tr> -->
	<input type="hidden" type="text" size="40" value="<?= md5(rand(1000,9999)) ?>" id="SESSIONID" name="SESSIONID" />
	<input type="hidden" name="PAYMENTCHANNEL" value="<?= $params['paymentChannel'] ?>">
	<!-- <tr>
		<td class="field_label">PAYMENTCHANNEL</td>
		<td class="field_input">
			
			<select name="PAYMENTCHANNEL" onchange="inputCardNumber(this[this.selectedIndex].value);">
						<option selected="selected" value="">NONE</option>
						<option value="15">Visa/Mastercard</option>
														<option value="36">ATM</option>
														<option value="37">KREDIVO</option>
					  </select>
		</td>
	</tr> -->
	<tr>
		<td width="100" class="field_label">EMAIL</td>
		<td width="500" class="field_input"><input name="EMAIL" type="text" id="EMAIL" value="<?= $params['email'] ?>" size="12" /></td>
	</tr>
	<tr>
		<td class="field_label">NAME</td>
		<td class="field_input"><input name="NAME" type="text" id="NAME" value="<?= $params['name'] ?>" size="30" maxlength="50" /></td>
	</tr>
	<tr>
		<td class="field_label">PHONE</td>
		<td class="field_input"><input name=MOBILEPHONE type="text" id=MOBILEPHONE value="<?= $params['phone'] ?>" size="30" maxlength="50" /></td>
	</tr>
	

</table><br /><br />
<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
											<input name="submit" type="submit" class="btn btn-brand" id="submit" value="SUBMIT" />

											</div>
										</div>
									</div>
								</div>
								</div>

</form>

							<!--begin::Form-->
							<!-- <form class="m-form m-form--fit m-form--label-align-right" name="MerchatPaymentPage" target="_blank" method="post" enctype="multipart/form-data" action="<?= $action ?>">
								<div class="m-portlet__body">

								<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12"><a target="_blank" href="https://www.w3schools.com/colors/colors_picker.asp">Example</a></label>
										
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">BASKET</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="Item1,1000000.00" name="BASKET" class="form-control m-input" id="" placeholder="">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">MALLID</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $config['mallId'] ?>" name="MALLID" class="form-control m-input" id="" placeholder="">
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">CHAINMERCHANT</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="NA" name="CHAINMERCHANT" class="form-control m-input" id="" placeholder="">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">CURRENCY</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="360" name="CURRENCY" class="form-control m-input" id="" placeholder="">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">PURCHASECURRENCY</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="360" name="PURCHASECURRENCY" class="form-control m-input" id="" placeholder="">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">AMOUNT</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $params['amount'] ?>" name="AMOUNT" class="form-control m-input" id="" placeholder="">
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">PURCHASEAMOUNT</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $params['amount'] ?>" name="PURCHASEAMOUNT" class="form-control m-input" id="" placeholder="">
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">TRANSIDMERCHANT</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $config['invoice'] ?>" name="TRANSIDMERCHANT" class="form-control m-input" id="" placeholder="">
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">WORDS</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $words ?>" name="WORDS" class="form-control m-input" id="" placeholder="">
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">REQUESTDATETIME</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= date('YmdHis') ?>" name="REQUESTDATETIME" class="form-control m-input" id="" placeholder="">
										</div>
									</div>
								
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">SESSIONID</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= rand(1000,9999) ?>" name="SESSIONID" class="form-control m-input" id="" placeholder="">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">PAYMENTCHANNEL</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<select name="PAYMENTCHANNEL" class="form-control">
											<option value="15">CC</option>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">EMAIL</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="blackhorse172@gmail.com" name="EMAIL" class="form-control m-input" id="" placeholder="">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">NAME</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="ADIP" name="NAME" class="form-control m-input" id="" placeholder="">
										</div>
									</div>
								
									
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
											<input name="submit" type="submit" class="btn btn-default" id="submit" value="SUBMIT" />
											</div>
										</div>
									</div>
								</div>
							</form> -->
							<!--end::Form-->
						</div>
<script type="text/javascript">
var count = 3;
var redirect = "<?= $action ?>";
 
function countDown(){
    var timer = document.getElementById("timer");
    if(count > 0){
        count--;
        timer.innerHTML = "This page will redirect in "+count+" seconds.";
        setTimeout("countDown()", 1000);
    }else{
        //window.location.href = redirect;
		//alert("submit")
		$("#submit").click();
    }
}
countDown();
</script>
