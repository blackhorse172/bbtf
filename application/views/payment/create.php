
<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "Create Payment for ".$trxCode;
$action = base_url().'payment/save';
if($isEdit == true){
$action = base_url().'payment/saveEdit';
}
?>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
							<input type="hidden" name="trx_code" value="<?= $trxCode ?>"/>
							<?php if($profile['category_id'] == 2) : ?>
								<div class="m-portlet__body">
								<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Payment Type</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<select name="trxType" class="form-control" id="trxType" required>
												<option></option>
												<option value="1">Bank Transfer</option>
												<option value="2">LOC</option>
											</select>
										</div>
									</div>
									<div id="formTF" style="display: none">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Bank Name</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $data->bank_name ?>" name="bank_name" id="bank_name" aria-describedby="bank_name" placeholder="Bank Name" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Bank Account Number</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $data->bank_acc_number ?>" name="bank_acc" id="bank_acc" aria-describedby="bank_name" placeholder="Bank Account Number" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Bank Account Name</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $data->bank_acc_name ?>" name="bank_acc_name" id="bank_acc_name" aria-describedby="bank_name" placeholder="Bank Account Name" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Notes</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $data->notes ?>"name="notes" id="notes" aria-describedby="notes" placeholder="Notes" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Image</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<?php if($isEdit == true) : ?>
												<?php if($data->image != '' || $data->image != null): ?>
												<img width="600px" id="image" class="img-responsive" src="<?= base_url() ?>files/payment/<?= $data->image ?>">
												<?php endif ?>
											<?php endif ?>
										<input type="file" class="form-control m-input" id="image" name="image">
										</div>
									</div>
									</div>
									<div id="formLOC" style="display: none;">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Notes</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $data->notes ?>"name="notes" id="notes" aria-describedby="notes" placeholder="Notes" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Image</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<?php if($isEdit == true) : ?>
												<?php if($data->image != '' || $data->image != null): ?>
												<img width="600px" id="image" class="img-responsive" src="<?= base_url() ?>files/payment/<?= $data->image ?>">
												<?php endif ?>
											<?php endif ?>
										<input type="file" class="form-control m-input" id="image" name="image">
										<div class="form-control-feedback">*Only file .jpg .png .gif</div>
										</div>
									</div>
									</div>
									
									
								
								</div>
							<?php else: ?>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Bank Name</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $data->bank_name ?>" name="bank_name" id="bank_name" aria-describedby="bank_name" placeholder="Bank Name" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Bank Account Number</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $data->bank_acc_number ?>" name="bank_acc" id="bank_acc" aria-describedby="bank_name" placeholder="Bank Account Number" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Bank Account Name</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $data->bank_acc_name ?>" name="bank_acc_name" id="bank_acc_name" aria-describedby="bank_name" placeholder="Bank Account Name" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Notes</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $data->notes ?>"name="notes" id="notes" aria-describedby="notes" placeholder="Notes" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Image</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<?php if($isEdit == true) : ?>
												<?php if($data->image != '' || $data->image != null): ?>
												<img width="600px" id="image" class="img-responsive" src="<?= base_url() ?>files/payment/<?= $data->image ?>">
												<?php endif ?>
											<?php endif ?>
											<input type="file" class="form-control m-input" id="image" name="image">
											<div class="form-control-feedback">*Only file .jpg .png .gif</div>
										</div>
									</div>
								
								</div>
							<?php endif; ?>
								
									
									
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Submit</button>
												<button type="button" onclick="window.location.href = '<?= base_url() ?>Transaction'" class="btn btn-secondary">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>

<?php if($isEdit == true) : ?>
<script>
$(document).ready(function(){
	$("#name").val("<?= $data->membership_name; ?>");
	$("#fee").val("<?= $data->membership_fee; ?>");
	$("#category_id").val("<?= $data->category_id; ?>");
	



});
</script>
<?php endif ?>
<script>

$(document).ready(function () {
	$(".mask_number").inputmask({
                      'alias': 'numeric',
                      'groupSeparator': '.',
                    'radixPoint': ',',
                      'digits': 0,
                      'autoGroup': true
				});
$("#trxType").change(function (e) { 
	e.preventDefault();
	if($(this).val() == 1){
		$("#formTF").show();
		$("#formLOC").hide();
		$(".m-input").val("");

	} else {
		$("#formTF").hide();
		$("#formLOC").show();
		$(".m-input").val("");
	}
});	
});
</script>
