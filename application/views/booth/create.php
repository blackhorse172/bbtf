<?php

//$this->load->view('layouts/admin/metronic/devextreme');
$title = "Update Booth Name on ".$trxCode;
$action = base_url().'Booths/updateName';

?>
<style>
	.uppercase {
  text-transform: uppercase;
}
	</style>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
							<input type="hidden" name="trx_code" value="<?= $trxCode ?>"/>
								<div class="m-portlet__body">

								<?php foreach ($data as $key => $value): ?>
								<?php if($key == 0): ?>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12"><?= $value['product_name'] ?></label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="hidden" name="booth_id[]" value="<?= $value['booth_id'] ?>" >
											<input type="text" class="form-control m-input uppercase" id="booth_name" maxlength="25" value="<?= $value['seller_name'] ?>" name="booth_name[]" id="number" placeholder="<?php $name=""; ($value['is_packed'] == '1') ? $name="Table" : $name="Booth" ?> <?= $name ?> Name <?= $key+1 ?>" required>
										</div>
									</div>
								<?php else: ?>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12"><?= $value['product_name'] ?></label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="hidden" name="booth_id[]" value="<?= $value['booth_id'] ?>" >
											<input type="text" class="form-control m-input uppercase" id="booth_name" maxlength="25" value="<?= $value['booth_name'] ?>" name="booth_name[]" id="number" placeholder="<?php $name=""; ($value['is_packed'] == '1') ? $name="Table" : $name="Booth" ?> <?= $name ?> Name <?= $key+1 ?>" required>
										</div>
									</div>
								<?php endif; ?>
									
									
								<?php endforeach; ?>
									
									
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Confirm</button>
												<button type="button" onclick="window.location.href = '<?= base_url() ?>Admin/Transactions'" class="btn btn-secondary">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>


<script>

$(document).ready(function () {
	$(".mask_number").inputmask({
                      'alias': 'numeric',
                      'groupSeparator': '.',
                    'radixPoint': ',',
                      'digits': 0,
                      'autoGroup': true
				});

				$(".m-input").maxlength({alwaysShow:!0,threshold:5,warningClass:"m-badge m-badge--primary m-badge--rounded m-badge--wide",limitReachedClass:"m-badge m-badge--brand m-badge--rounded m-badge--wide"});
				
});
</script>
