<?php

//$this->load->view('layouts/admin/metronic/devextreme');
$title = "Update Seller Login on ".$trxCode;
$action = base_url().'Booths/updateDelegations';
// var_dump($data[0]['seller_name']);
// exit;
?>
<style>
.capslock {
	text-transform: uppercase;
}
</style>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
							

								<div class="m-portlet__body">
								<div class="m-form__content">
										<div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_1_msg">
											<div class="m-alert__icon">
												<i class="la la-warning"></i>
											</div>
											<div class="m-alert__text">
											Please create new password for PSA
											</div>
											<div class="m-alert__close">
												<button type="button" class="close" data-close="alert" aria-label="Close">
												</button>
											</div>
										</div>
									</div>

								<?php foreach ($data as $key => $value): ?>
									
									<?php 
												$email = $value['email_login'] == '' ? $value['seller_email'] : $value['email_login'];
												$emailReadonly = $value['email_login'] != '' ? "readonly" : $value['email_login'];
												$passwordReadonly = $value['password_login'] != '' ? "readonly" : "";

											?>
									<?php if($key==0) : ?>
										<?php if($value['email_login'] == null && $value['password_login'] == null) : ?>
											<input type="hidden" name="trx_code" value="<?= $trxCode ?>"/>
							<input type="hidden" name="delegation_id[]" value="<?= $value['id'] ?>" >
											<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Name</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<?php 
												$companyName = $value['seller_name'] == "" ? $value['seller_name'] : $value['seller_name'];
											?>
											<input type="text" class="form-control m-input capslock" <?= $emailReadonly ?> id="seller_name" maxlength="25" value="<?= $companyName ?>" name="seller_name[]" placeholder="seller_name <?= $key + 1 ?>">
										</div>
										</div>

										<div class="form-group m-form__group row">
											<label class="col-form-label col-lg-3 col-sm-12">Username</label>
											<div class="col-lg-7 col-md-7 col-sm-12">
												<input type="text" class="form-control m-input email" <?= $emailReadonly ?> id="email" maxlength="50" value="<?= $email ?>" name="email[]" placeholder="Email <?= $key + 1 ?>">
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label class="col-form-label col-lg-3 col-sm-12">Password</label>
											<div class="col-lg-7 col-md-7 col-sm-12">
												<input type="password" class="form-control m-input" <?= $passwordReadonly ?> id="password" maxlength="6" value="<?= $value['password'] ?>" name="password[]" placeholder="Please create 6 digits password for PSA">
											</div>
										</div>

										<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Confirm</button>
												<button type="button" onclick="window.location.href = '<?= base_url() ?>Admin/Transactions'" class="btn btn-secondary">Cancel</button>
											</div>
										</div>
									</div>
								</div>
										
										<?php endif;  ?>
												
									<?php else: ?>
												
										<?php if($value['email_login'] == null && $value['password_login'] == null) : ?>
											<input type="hidden" name="trx_code" value="<?= $trxCode ?>"/>
							<input type="hidden" name="delegation_id[]" value="<?= $value['id'] ?>" >
											<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Name</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input capslock" <?= $emailReadonly ?> id="seller_name" maxlength="25" value="<?= $value['company_name'] ?>" name="seller_name[]" placeholder="seller_name <?= $key + 1 ?>">
									</div>
								</div>
								<div class="form-group m-form__group row">
											<label class="col-form-label col-lg-3 col-sm-12">Username</label>
											<div class="col-lg-7 col-md-7 col-sm-12">
												<input type="text" class="form-control m-input email" <?= $emailReadonly ?> id="email" maxlength="50" value="<?= $value['email_login'] ?>" name="email[]" placeholder="Email <?= $key + 1 ?>">
											</div>
										</div>
										<div class="form-group m-form__group row">
											<label class="col-form-label col-lg-3 col-sm-12">Password</label>
											<div class="col-lg-7 col-md-7 col-sm-12">
												<input type="password" class="form-control m-input" <?= $passwordReadonly ?> id="password" maxlength="6" value="" name="password[]" placeholder="Please create 6 digits password for PSA">
											</div>
										</div>

										<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Confirm</button>
												<button type="button" onclick="window.location.href = '<?= base_url() ?>Admin/Transactions'" class="btn btn-secondary">Cancel</button>
											</div>
										</div>
									</div>
								</div>
										<?php endif; ?>	
									

								<?php endif; ?>
									
									
								<?php endforeach; ?>
									
									
								
							</form>
							<!--end::Form-->
						</div>

<script>

$(document).ready(function () {

	$("#email").change(function (e) { 
		e.preventDefault();
		$.ajax({
			type: "get",
			url: "<?= base_url() ?>Seller/check_email_exist_sub",
			data: {email: $(this).val()},
			dataType: "json",
			success: function (response) {
				if(response.status){
					$("#email").val("");
					$("#email").focus();
					swal({
                                title: "Warning",
                                text: "EMAIL IS EXIST, PLEASE LOG IN OR RESET PASSWORD",
                                type: "warning"
							});
					
					
				}
			}
		});
	});


	$(".mask_number").inputmask({
                      'alias': 'numeric',
                      'groupSeparator': '.',
                    'radixPoint': ',',
                      'digits': 0,
                      'autoGroup': true
				});

				$(".m-input").maxlength({alwaysShow:!0,threshold:5,warningClass:"m-badge m-badge--primary m-badge--rounded m-badge--wide",limitReachedClass:"m-badge m-badge--brand m-badge--rounded m-badge--wide"});
				
				$(".email").inputmask({
            mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
            greedy: !1,
            onBeforePaste: function(m, a) {
                return (m = m.toLowerCase()).replace("mailto:", "")
            },
            definitions: {
                "*": {
                    validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~-]",
                    cardinality: 1,
                    casing: "lower"
                }
            }
        })
});
</script>
