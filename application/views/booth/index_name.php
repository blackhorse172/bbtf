
<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "List Paid Transaction";

?>

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= $title ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <!-- <li class="m-portlet__nav-item">
                    <a href="<?= site_url()?>Transaction/Create" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Create</span>
                        </span>
                    </a>
                </li> -->
                <li class="m-portlet__nav-item"></li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <div id="gridContainer"></div>
    </div>
</div>
<div id="boothModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<h3>Update Booth Number</h3>
				<form class="m-form m-form--fit m-form--label-align-right" method="post" action="<?= base_url() ?>Admin/Transactions/UpdateBooth">
				<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Booth Number*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="number" class="form-control m-input" value="" required>
										</div>
									</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
    (function ($) {
        var dataSource = {
            load: function () {
                var items = $.Deferred();
                var data = <?= $data; ?>;
                items.resolve(data);
                return items.promise();
            }
        };
        $("#gridContainer").dxDataGrid({
            dataSource: dataSource,
            showBorders: true,
            showRowLines: true,
            columnAutoWidth: true,
            allowColumnResizing: true,
            allowColumnReordering: true,
            filterRow: {
                visible: true,
                applyFilter: "auto"
            },
            headerFilter: {
                visible: true
            },
            paging: {
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20],
                showInfo: true
			},
			onRowClick: function (e) {
				var data = e.data;
				var grid = e.component; 

				// if(data.trx_status == "2")
				// {
				// 	window.open("<?= base_url() ?>Admin/Transactions/updateBooth/"+data.trx_code);
				// 	//showModalBoothNumber(data);
				// }
				
			 },
            columns: [
                {
                    caption: "Transaction Code",
                    dataField: "trx_code",
				},

				
				
				{
                    caption: "Transaction Date",
					dataField: "trx_date",
					dataType: "date"
				},
				

				{
                    caption: "TOTAL",
					dataField: "trx_total",
					dataType: "number",
                	format: "#,##0.##"
				},

				{
                    caption: "ACTION",
					dataField: "trx_status",
					cellTemplate: function (container, options) {
						//console.log(options);
						var id = options.key.id;
                        var trx_code = options.key.trx_code;
						
                        var status = options.key.trx_status;
                        if (status == 0) {
                            $("<div>")
                                .append($("-"))
                                .appendTo(container);
                        } else if(status == 1) {
                            
                            $("<div>")
                                .append($("<button type='button' onclick='flinkEdit(\""+trx_code+"\")' class='btn m-btn--pill btn-warning'>CONFIRM PAYMENT</button>"))
                                .appendTo(container);
						}
						else if(status == 2) {
                            
                            $("<div>")
                                .append($("<span onclick='flinkEdit(\""+trx_code+"\")'  class='m-badge m-badge--danger m-badge--wide'>Update Booth Name</span>"))
                                .appendTo(container);
                        }

                    }
				},
                

			],
			masterDetail: {
                                enabled: true,
                                template: function(container, options) { 
                                    var masterData = options.data;
                                    $("<div>")
                                        .addClass("show-detail")
                                        .text("Loading....")
                                        .appendTo(container);
                                    var trx_detail = "<?php echo base_url() ?>transaction/json_getDetail"
                                    $.ajax({
                                      url: trx_detail,
                                      type: 'post',
                                      dataType: 'json',
                                      data: {trx_code: masterData.trx_code,trx_type: masterData.trx_type },
                                    })
                                    .done(function(result) {
                                      data_detail = result;
                                      $("<div>")
                                        .addClass("master-detail-caption")
                                        .text("Transaction Detail")
                                        .appendTo(container);
                                    $("<div>")
                                        .dxDataGrid({
                                            columnAutoWidth: true,
                                            showBorders: true,
                                            columns: [
                                            {
                                                caption: "Product Name",
                                                dataField: "product_name",
                                                width: 120
                                            },{
                                                caption: "Amount",
                                                dataField: "product_amount",
                                                width: 70
                                            },
                                            {
                                                caption: "Product Price",
												dataField: "product_price",
												dataType: "number",
                                                 format: "Rp #,##0.##"
                                            },
											// {
											// 	caption: "Booth Number",
											// 	dataField: "booth_number",
											// 	cellTemplate: function (container, options) {
											// 		console.log(options);
											// 		var id = options.key.id;
											// 		var trx_code = options.key.trx_code;
													
											// 		var status = options.key.booth_number;
											// 		if(status =="" || status == null)
											// 		{
											// 				$("<div>")
											// 				.append($("<button type='button'onclick='flinkEditBooth(\""+id+"\",\""+status+"\")' class='btn m-btn--pill btn-warning'>Update Booth Number</button>"))
											// 				.appendTo(container);
													
											// 		} else {
											// 				$("<div>")
											// 				.append($("<span>"+status+"</span>"))
											// 				.appendTo(container);
											// 		}
												

											// 	}
											// },

											],
                                            dataSource: result
                                        }).appendTo(container);

                                        $('.show-detail').css("display","none");
                                    })
                                    .fail(function() {
                                      console.log("error");
                                    })
                                    .always(function() {
                                      
                                    });
                                }
                            }
        });
    })(jQuery);

    function fNonActive(id,title,msg) {
        swal({
            title: title,
            text: msg,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Transaction/nonActive/" + id,"_self");
            }
        })
    }

	function flinkEditBooth(id,status) {

		$("#detail_id").val(id);

		$("#boothModal").modal("show");
        // swal({
        //     title: title,
        //     text: msg,
        //     type: "warning",
        //     showCancelButton: !0,
        //     confirmButtonText: "Yes!"
        // }).then(function (e) {
        //     //e.value && swal("Deleted!", "Your file has been deleted.", "success")
        //     //$("#FormTrxOrder").submit();
        //     console.log(e);
        //     if (e.value) {
        //         window.open("<?= base_url() ?>Transaction/nonActive/" + id,"_self");
        //     }
        // })
    }

    function flinkEdit(id) {
                window.open("<?= base_url() ?>Booths/createName/" + id,"_self");

	}
	
	function showModalBoothNumber(data) {
		$("#boothModal").modal("show");
	}
</script>

