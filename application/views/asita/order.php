
<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "Booth Order";
?>

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= $title ?>
                </h3>
            </div>
        </div>
        <!-- <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?= site_url()?>Seller/CreateOrder" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Submit Order</span>
                        </span>
                    </a>
                </li>
                <li class="m-portlet__nav-item"></li>
            </ul>
        </div> -->
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
		<!-- <div id="gridContainer"></div> -->
		<form id="boothForm" action="<?= site_url()?>Seller/CreateOrder" method="POST">
		<div class="form-group m-form__group row" id="packageList">
										<div class="col-lg-12 col-md-7 col-sm-12">
										<table class="table">
											<thead class="thead-dark">
												<tr>
												<th scope="col">#</th>
												<th scope="col">Name</th>
												<th scope="col">Price IDR</th>
												<th scope="col">Quantity</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach($products as $i => $val) : ?>
												<tr>
												<th scope="row"><?= $i + 1; ?></th>
												<td>
												<input type="hidden" name="booth_product_id[]" value="<?= $val->product_id ?>">
												<input type="hidden" name="booth_product_name[]" value="<?= $val->product_name ?>">
												<input type="hidden" name="booth_product_is_package[]" value="<?= $val->is_package ?>">	
												<?=$val->product_name ?></td>
												<td> <input type="hidden" name="booth_product_price[]" id="price_<?= $val->product_id ?>" value="<?= $val->product_price ?>"> <span id="priceview_<?= $val->product_id ?>"><?= number_format($val->product_price,2) ?></span> <input type="hidden" value="<?= $val->total_stock ?>" id="stock_<?= $val->product_id ?>"/></td>
												<td><select class="form-control m-input" onchange="checkAmount(<?= $val->product_id ?>)" name="booth_product_amount[]" id="amount_<?= $val->product_id ?>">
												<option value="0">0</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												</select></td>
												<!-- <td><input type="text" class="form-control m-input mask_number" onchange="checkAmount(<?= $val->product_id ?>)" id="amount_<?= $val->product_id ?>" name="booth_product_amount[]"></td> -->
												</tr>
												<?php endforeach; ?>
												<tr>
												<td colspan="3"><h4>TOTAL</h4></td>
												<td><h4><span id="totalPrice"></span></h4></td>

												</tr>
											</tbody>
											</table>
										</div>
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="button" onclick="fsubmit()" id="submitBtn" class="btn btn-brand">Submit</button>
												<button type="button" class="btn btn-secondary" onclick="window.location.href = '<?= site_url() ?>Stock'">Cancel</button>
											</div>
										</div>
									</div>
								</div>
		</form>
		
    </div>
</div>
<script>
    (function ($) {
		$(".mask_number").inputmask({
                      'alias': 'numeric',
                      'groupSeparator': '.',
                    'radixPoint': ',',
                      'digits': 0,
                      'autoGroup': true,
					  'min':1
				});
				$(".mask_number").inputmask('Regex', { regex: "^[1-9][0-9]?$|^1000$" });
    })(jQuery);

	function checkAmount(id) 
{
	//alert("change");
	var stock = $("#stock_"+id).val();
	//var price = $("#price_"+id).val();
	var amounts = $("#amount_"+id).val().replace(".","");
	var total = 0;
	if(parseInt(amounts) > parseInt(stock)){
		$("#amount_"+id).focus();
		$("#amount_"+id).val("");
		swal({
            title: "Warning",
            text: "the number cannot be more than product stock",
            type: "warning",
        });
	}

	$("select[name='booth_product_amount[]']").each(function (i) {
				if($(this).val() != ""){
					var amount = parseInt($(this).val());
					var thisId = $(this).attr("id").split("_")[1];
					var price = $("#price_"+thisId).val();

					total = total + (parseInt(price) * amount);
					$("#totalPrice").html(numberWithCommas(total));
				}
				
		 });

}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function fsubmit() {
	swal({
            title: "CONFIRM",
            text: "Are you sure to submit ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            //console.log(e);
            if (e.value) {
                $("#boothForm").submit();
            }
        })

}

	 

    function fNonActive(id,title,msg) {
        swal({
            title: title,
            text: msg,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Memberships/nonActiveSeller/" + id,"_self");
            }
        })
	}

	function sendRequest(url, method, data) {
        var d = $.Deferred();
    
        method = method || "GET";

        //logRequest(method, url, data);
    
        $.ajax(url, {
            method: method || "GET",
            data: data,
            cache: false,
            xhrFields: { withCredentials: true }
        }).done(function(result) {
			d.resolve(method === "GET" ? result.data : result);
			
			//window.open("<?= base_url() ?>Seller/Order","_self");
			//window.open("<?= base_url() ?>Seller/Order","_self");
			
        }).fail(function(xhr) {
            d.reject(xhr.responseJSON ? xhr.responseJSON.Message : xhr.statusText);
		});
		
		//window.open("<?= base_url() ?>Seller/Order","_self");
    
        return d.promise();
    }

	
	function fNonActiveMembership(id,title,msg) {
        swal({
            title: title,
            text: msg,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Memberships/nonActiveSellerMembership/" + id,"_self");
            }
        })
	}
	
    function flinkEdit(id) {
                window.open("<?= base_url() ?>Memberships/Seller/" + id,"_self");

    }
</script>

