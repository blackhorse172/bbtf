<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "Create Products Category";

?>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" action="<?= base_url() ?>Products/saveCategory">
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Name</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" id="name" name="name">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Desc</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" id="desc" name="desc">
										</div>
									</div>
									<input type="hidden" name="isEdit" value="<?= $isEdit ?>" />
									<?php if($isEdit) : ?><input type="hidden" name="id" value="<?= $data->category_id ?>" /><?php endif; ?>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Submit</button>
												<button type="button" onclick="window.location.href = <?= base_url().'Products/category' ?>" class="btn btn-secondary">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
<?php if($isEdit == true) : ?>
<script>
   $(document).ready(function () {
	   
		$("#name").val("<?= $data->category_name; ?>");
		$("#desc").val("<?= $data->category_desc; ?>");

	});
</script>
<?php endif; ?>
