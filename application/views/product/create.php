<?php
$title = "Create Products";
$action = base_url().'Products/Save';
if($isEdit == true){
$action = base_url().'Products/SaveEdit';
}
?>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
							<?php if($isEdit == true) : ?><input type="hidden" name="id" value="<?= $data->id ?>"/><?php endif ?>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Product Name</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="name" class="form-control m-input" id="name" placeholder="Enter Product Name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Product Image</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<?php if($isEdit == true) : ?>
												<?php if($data->image != '' || $data->image != null): ?>
												<img width="600px" id="image_exits" class="img-responsive" src="<?= base_url() ?>files/product/<?= $data->image ?>">
												<?php endif ?>
											<?php endif ?>
										<input type="file" class="form-control m-input" name="image" id="exampleInputEmail1">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Product Desc</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<textarea class="summernote" name="content" id="content"><?php if($isEdit == true) echo $data->product_desc; ?></textarea>
										</div>
									</div>
								</div>
								<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Category</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<select class="form-control m-input" name="category_id" id="category_id" required>
												<option></option>
												<?php foreach($category as $item) : ?>
												<option value="<?= $item->category_id ?>"><?= $item->category_name ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
									<!-- <div class="form-group m-form__group row" id="parentForm" style="display:none;">
										<label class="col-form-label col-lg-3 col-sm-12">Parent</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<select class="form-control m-input" id="parent">
												<option></option>
												<?php foreach($parent as $item) : ?>
												<option value="<?= $item->id ?>"><?= $item->product_name ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div> -->
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Product Price</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="price" class="form-control m-input mask_number" id="price" placeholder="Enter Price" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Product Type</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<select class="form-control m-input" name="is_package" id="is_package" required>
											<option value="">"SELECT TYPE"</option>
												<option value="0">Non Package</option>
												<option value="1">Package</option>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Discount Non Member Type</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<select class="form-control m-input" name="discount_type" id="discount_type" required>
											<option value="2">non active</option>
												<option value="0">By %</option>
												<option value="1">By Value</option>
											</select>
										</div>
									</div>
									<div id="dscpersen" style="display: none;">
									<div class="form-group m-form__group row" >
										<label class="col-form-label col-lg-3 col-sm-12">Discount</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="discount" class="form-control m-input mask_number" id="discount" placeholder="Enter discount">
										</div>
									</div>
									</div>

									<div id="dscamount" style="display: none;">
									<div class="form-group m-form__group row" >
										<label class="col-form-label col-lg-3 col-sm-12">Discount By Amount</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="discount_value" class="form-control m-input mask_number" id="discount_value" placeholder="Enter discount">
										</div>
									</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Discount Non Member End Date</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="date" name="discount_date" class="form-control m-input" id="discount_date" placeholder="Enter discount_date">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Discount Member Type</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<select class="form-control m-input" name="discount_member_type" id="discount_member_type" required>
												<option value="2">non active</option>
												<option value="0">By %</option>
												<option value="1">By Value</option>
											</select>
										</div>
									</div>
									<div id="dscpersenMember" style="display: none;">
									<div class="form-group m-form__group row" >
										<label class="col-form-label col-lg-3 col-sm-12">Discount Member</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="discount_member" class="form-control m-input mask_number" id="discount_member" placeholder="Enter discount">
										</div>
									</div>
									</div>

									<div id="dscamountMember" style="display: none;">
									<div class="form-group m-form__group row" >
										<label class="col-form-label col-lg-3 col-sm-12">Discount Member By Amount</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="discount_member_value" class="form-control m-input mask_number" id="discount_member_value" placeholder="Enter discount">
										</div>
									</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Discount Member End Date</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="date" name="discount_member_date" class="form-control m-input" id="discount_member_date" placeholder="Enter discount_date">
										</div>
									</div>

								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Submit</button>
												<button type="button" class="btn btn-secondary" onclick="window.location.href = '<?= site_url() ?>Products'">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
<?php if($isEdit == true) : ?>
<script>
$(document).ready(function(){
	$("#name").val("<?= $data->product_name; ?>");
	$("#price").val("<?= $data->product_price; ?>");
	$("#category_id").val("<?= $data->category_id; ?>");
	$("#discount").val("<?= $data->discount; ?>");
	$("#discount_value").val("<?= $data->discount_value; ?>");
	$("#discount_date").val("<?= $data->discount_date; ?>");

	$("#discount_member").val("<?= $data->discount_member; ?>");
	$("#discount_member_value").val("<?= $data->discount_member_value; ?>");
	$("#discount_member_date").val("<?= $data->discount_member_date; ?>");

	$("#discount_type").val("<?= $data->discount_type; ?>");
	$("#discount_member_type").val("<?= $data->discount_member_type; ?>");
	$("#is_package").val("<?= $data->is_package; ?>");

	if($("#discount_member_type").val() == '0')
	{
		//$("#discount_type").val("0");
		$("#dscpersenMember").show();
	}else if($("#discount_member_type").val() == '1')
	{
		//$("#discount_type").val("1");
		$("#dscamountMember").show();
	} else {
		$("#dscamountMember").hide();
		$("#dscpersenMember").hide();

	}

	
	if($("#category_id").val() == 2){
		$("#parentForm").show();
		$("#parent").val("<?= $data->parent; ?>");
	}

	if($("#discount_type").val() == 0)
	{
		//$("#discount_type").val("0");
		$("#dscpersen").show();
	}else if($("#discount_type").val() == 1)
	{
		//$("#discount_type").val("1");
		$("#dscamount").show();
	} else {
		$("#dscamount").hide();
		$("#dscpersen").hide();
	}



});
</script>
<?php endif ?>

<script type="text/javascript">
        $(document).ready(function(){
            $('.summernote').summernote({
                height: "300px",
                callbacks: {
                    onImageUpload: function(image) {
                        uploadImage(image[0]);
                    },
                    onMediaDelete : function(target) {
                        deleteImage(target[0].src);
                    }
                }
            });
 
            function uploadImage(image) {
                var data = new FormData();
                data.append("image", image);
                $.ajax({
                    url: "<?php echo site_url('Products/uploadImage')?>",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    type: "post",
                    success: function(url) {
                        $('.summernote').summernote("insertImage", url);
                    },
                    error: function(data) {
                        console.log(data);
                    }
                });
            }
 
            function deleteImage(src) {
                $.ajax({
                    data: {src : src},
                    type: "POST",
                    url: "<?php echo site_url('Products/deleteImage')?>",
                    cache: false,
                    success: function(response) {
                        console.log(response);
                    }
                });
            }
 
        });
         
    </script>
<script>


$(document).ready(function () {

	$("#category_id").change(function (e) { 
		e.preventDefault();
		if($(this).val() == 2){
			$("#parent").attr("name","parent");
			$("#parent").attr("required",true);

			$("#parentForm").show();
		} else {
			$("#parentForm").hide();
			$("#parent").removeAttr("name");
			$("#parent").removeAttr("required");


		}
	});

	$(".mask_number").inputmask({
                      'alias': 'numeric',
                      'groupSeparator': '.',
                    'radixPoint': ',',
                      'digits': 0,
                      'autoGroup': true
				});


				$("#discount_type").change(function (e) { 
		e.preventDefault();
		if($(this).val() == 0)
		{
			$("#dscpersen").show();
			$("#dscamount").hide();

		}

		if($(this).val() == 1)
		{
			$("#dscamount").show();
			$("#dscpersen").hide();
			
		}
	});

	$("#discount_member_type").change(function (e) { 
		e.preventDefault();
		if($(this).val() == '0')
		{
			$("#dscpersenMember").show();
			$("#dscamountMember").hide();

		} else if($(this).val() == '1')
		{
			$("#dscamountMember").show();
			$("#dscpersenMember").hide();
			
		}

		
	});
				
});
</script>
