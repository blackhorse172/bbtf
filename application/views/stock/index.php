
<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "List Products Stock";

?>

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= $title ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?= site_url()?>stock/Create" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Add Stock</span>
                        </span>
                    </a>
                </li>
                <li class="m-portlet__nav-item"></li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <div id="gridContainer"></div>
    </div>
</div>
<script>
    (function ($) {
        var dataSource = {
            load: function () {
                var items = $.Deferred();
                var data = <?= $data; ?>;
                items.resolve(data);
                return items.promise();
            }
        };
        $("#gridContainer").dxDataGrid({
            dataSource: dataSource,
            showBorders: true,
            showRowLines: true,
            columnAutoWidth: true,
            allowColumnResizing: true,
            allowColumnReordering: true,
            filterRow: {
                visible: true,
                applyFilter: "auto"
            },
            headerFilter: {
                visible: true
            },
            paging: {
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20],
                showInfo: true
            },
            columns: [
                {
                    caption: "Name",
                    dataField: "product_name",
				},
				{
                    caption: "STOCK",
					dataField: "total_stock",
					dataType:"number",
					format: "#,##0.##"
                },
				{
                    caption: "Price",
					dataField: "product_price",
					dataType: "number",
                	format: "#,##0.##"
				},
				{
                    caption: "Price After discount",
					dataField: "price_after_discount",
					dataType: "number",
                	format: "#,##0.##"
				},

				{
                    caption: "Price After discount Member",
					dataField: "price_after_discount_member",
					dataType: "number",
                	format: "#,##0.##"
				},
				{
                    caption: "Products Category",
					dataField: "category_name",
                },
				{
                    caption: "Discount",
					dataField: "discount",
					dataType:"number"
				},
				{
                    caption: "Discount Value",
					dataField: "discount_value",
					dataType:"number",
					format: "#,##0.##"
				},
				{
                    caption: "Discount End",
					dataField: "discount_date",
					dataType: "date",
                	//format: "#,##0.##"
				},

				

				//member
				{
                    caption: "Discount Member",
					dataField: "discount_member",
					dataType:"number"
				},
				{
                    caption: "Discount Member Value",
					dataField: "discount_member_value",
					dataType:"number",
					format: "#,##0.##"
				},
				{
                    caption: "Discount End",
					dataField: "discount_member_date",
					dataType: "date",
                	//format: "#,##0.##"
				},

				

				
                // {
                //     caption: "Products Active",
                //     dataField: "IsActive",
                //     cellTemplate: function (container, options) {
                //         var id = options.key.id;
                //         var IsActive = options.key.active;
                //          var message = "";
                //         var title = "";
                //         if (IsActive != true) {
                //              message = "Active ?";
                //             title = options.key.product_name;
                //             $("<div>")
                //                 .append($("<button type='button' id='btnApprove_'" + id + "  onclick='fNonActive(" + id + ",\""+title+"\",\"" + message + "\")' class='btn m-btn--pill btn-danger'>NON ACTIVE</button>"))
                //                 .appendTo(container);
                //         } else {
                //              message = "Non Active ?";
                //             title = options.key.product_name;
                //             $("<div>")
                //                 .append($("<button type='button' id='btnApprove_'" + id + " onclick='fNonActive(" + id + ",\""+title+"\",\"" + message + "\")'  class='btn m-btn--pill btn-success'>ACTIVE</button>"))
                //                 .appendTo(container);
                //         }

                //     }
                // },
                 {
                alignment: "center",
                pinned: true,
                cellTemplate: function (container, options) {
				 var id = options.key.id;
				 var group = options.key.group_id;
                    $("<div>")
                    //  .append($("<button type='button' onclick='flinkEdit(" + id + ")'  class='btn m-btn--pill btn-success'><i class='flaticon-edit'></i></button>"))
                    .append($("<a onclick='flinkEdit(" + id + ")'  class='btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only'><i class='flaticon-edit'></i></a>"))
						
                 .appendTo(container);
                }
                },

            ]
        });
    })(jQuery);

    function fNonActive(id,title,msg) {
        swal({
            title: title,
            text: msg,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Products/nonActive/" + id,"_self");
            }
        })
    }
    function flinkEdit(id) {
                window.open("<?= base_url() ?>Products/edit/" + id,"_self");

    }
</script>

