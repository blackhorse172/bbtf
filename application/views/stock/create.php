<?php
$title = "Add Products Stock";
$action = base_url().'Stock/save';
if($isEdit == true){
$action = base_url().'Stock/SaveEdit';
}
?>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
							<?php if($isEdit == true) : ?><input type="hidden" name="id" value="<?= $data->id ?>"/><?php endif ?>
								<div class="m-portlet__body">
								<input type="hidden" id="is_package" name="is_package" value="0">
								<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Product</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<select class="form-control m-input" name="product_id" id="product_id" required>
												<option></option>
												<?php foreach($product as $item) : ?>
												<option value="<?= $item->id ?>"><?= $item->product_name ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
									<div class="form-group m-form__group row" id="packageList" style="display: none">
										<label class="col-form-label col-lg-3 col-sm-12">Package</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<table class="table">
											<thead class="thead-dark">
												<tr>
												<th scope="col">#</th>
												<th scope="col">Name</th>
												<th scope="col">Stock</th>
												<th scope="col">Amount</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach($boothProduct as $i => $val) : ?>
												<tr>
												<th scope="row"><?= $i + 1; ?></th>
												<td>
												<input type="hidden" name="booth_product_id[]" value="<?= $val->product_id ?>">	
												<?=$val->product_name ?></td>
												<td> <span id="product_<?= $val->product_id ?>"><?=$val->total_stock ?></span></td>
												<td><input type="text" class="form-control m-input mask_number" onchange="checkAmount(<?= $val->product_id ?>)" id="amount_<?= $val->product_id ?>" name="booth_product_amount[]"></td>
												</tr>
												<?php endforeach; ?>
											</tbody>
											</table>
										</div>
									</div>
									<!-- <div class="form-group m-form__group row" id="parentForm" style="display:none;">
										<label class="col-form-label col-lg-3 col-sm-12">Parent</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<select class="form-control m-input" id="parent">
												<option></option>
												<?php foreach($parent as $item) : ?>
												<option value="<?= $item->id ?>"><?= $item->product_name ?></option>
												<?php endforeach ?>
											</select>
										</div>
									</div> -->

									<div class="form-group m-form__group row" style="display:none;" id="amountPsa">
										<label class="col-form-label col-lg-3 col-sm-12">Amount PSA</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="amountPSA" class="form-control m-input mask_number" id="amountPSA" placeholder="Enter amount psa" required>
										</div>
									</div>

									<div class="form-group m-form__group row" id="amountStock">
										<label class="col-form-label col-lg-3 col-sm-12">Amount Stock</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="amount" class="form-control m-input mask_number" id="amount" placeholder="Enter amount" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Name</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="company_name" class="form-control m-input" id="company_name" placeholder="Enter Company" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Note</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="note" class="form-control m-input" id="note" placeholder="Enter Company" required>
										</div>
									</div>
								
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Submit</button>
												<button type="button" class="btn btn-secondary" onclick="window.location.href = '<?= site_url() ?>Stock'">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
<?php if($isEdit == true) : ?>
<script>
$(document).ready(function(){
	$("#name").val("<?= $data->product_name; ?>");
	$("#price").val("<?= $data->product_price; ?>");
	$("#category_id").val("<?= $data->category_id; ?>");
	$("#discount").val("<?= $data->discount; ?>");
	$("#discount_date").val("<?= $data->discount_date; ?>");

	if($("#category_id").val() == 2){
		$("#parentForm").show();
		$("#parent").val("<?= $data->parent; ?>");
	}


});
</script>
<?php endif ?>

<script>

function checkAmount(id) 
{
	var stock = $("#product_"+id).html();
	var amount = $("#amount_"+id).val().replace(".","");

	if(parseInt(amount) > parseInt(stock)){
		$("#amount_"+id).focus();
		$("#amount_"+id).val("");
		swal({
            title: "Warning",
            text: "the number cannot be more than product stock",
            type: "warning",
        });
		

	}
}

$(document).ready(function () {

	$("#category_id").change(function (e) { 
		e.preventDefault();
		if($(this).val() == 2){
			$("#parent").attr("name","parent");
			$("#parent").attr("required",true);

			$("#parentForm").show();
		} else {
			$("#parentForm").hide();
			$("#parent").removeAttr("name");
			$("#parent").removeAttr("required");
		}
	});

	$("#product_id").change(function (e) { 
		e.preventDefault();
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>Products/isPackage",
			data: {id: $(this).val()},
			dataType: "json",
			success: function (response) {
				if(response){
					$("#packageList").show();
					$("input[name='booth_product_amount[]']").attr("required",true);
					$("#is_package").val(1);
					$("#amountPsa").show();
				} else {
					$("#packageList").hide();
					$("#is_package").val(0);
					$("#amountPsa").val("");
					$("#amountPsa").hide();

				}
			}
		});
	});

	$("#amount").change(function (e) { 
		e.preventDefault();
		var ret = false;
		var total = 0;
		$("input[name='booth_product_amount[]']").each(function (i) { 
				var amount = (parseInt($(this).val()) * parseInt($("#amount").val()));
				var thisId = $(this).attr("id").split("_")[1];
				if(amount > parseInt($("#product_"+ thisId).html()) ){
					ret = true;
				}

				if(ret) {
					swal({
						title: "Warning",
						text: "insufficient stock",
						type: "warning",
					});
					$(this).val("")
					$("#amount").val("");

				}
				
		 });

		 

	});

	$(".mask_number").inputmask({
                      'alias': 'numeric',
                      'groupSeparator': '.',
                    'radixPoint': ',',
                      'digits': 0,
                      'autoGroup': true
				});
				
});
</script>
