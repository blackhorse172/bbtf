<?php
$title = "Create Hotel";
$action = base_url().'Admin/Hotel/Save';
if($isEdit == true){
$action = base_url().'Admin/Hotel/SaveEdit';
}
?>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
							<?php if($isEdit == true) : ?><input type="hidden" name="id" value="<?= $data->id ?>"/><?php endif ?>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Hotel Name</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="hotel_name" class="form-control m-input" id="hotel_name" placeholder="Hotel Name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Hotel Address</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="hotel_address" class="form-control m-input" id="hotel_address" placeholder="Hotel Address" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Hotel Email</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="email" name="hotel_email" class="form-control m-input" id="hotel_email" placeholder="Hotel Email" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Hotel Phone</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" name="hotel_phone" class="form-control m-input" id="hotel_phone" placeholder="Hotel Phone" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Available Amount</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="number" name="stock" class="form-control m-input" id="stock" placeholder="Available" required>
										</div>
									</div>
									
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Submit</button>
												<button type="button" class="btn btn-secondary" onclick="window.location.href = '<?= site_url() ?>Admin/Hotel'">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
<?php if($isEdit == true) : ?>
<script>
$(document).ready(function(){
	$("#hotel_name").val("<?= $data->hotel_name; ?>");
	$("#hotel_address").val("<?= $data->hotel_address; ?>");
	$("#hotel_email").val("<?= $data->hotel_email; ?>");
	$("#stock").val("<?= $data->stock; ?>");

});
</script>
<?php endif ?>
