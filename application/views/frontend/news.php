<?php
function limit_text($text, $limit) {
	if (str_word_count($text, 0) > $limit) {
		$words = str_word_count($text, 2);
		$pos = array_keys($words);
		$text = substr($text, 0, $pos[$limit]) . '...';
	}
	return $text;
  }
?>
<!--Content-->
<section class="wow fadeIn sm-padding-20px-top xs-padding-10px-top margin-100px-bottom" id="inner">
    <div class="container-fluid"> 
		
	<?php if($sub_image != '' || $sub_image != null) : ?>
			<div id="banner" class="banner" style="background:url('<?=base_url() ?>files/sub_header/<?= $sub_image ?>')"> 
            <h1>News</h1>
		</div>
		<?php else: ?>
			<div id="banner" class="banner" style="background:gray"> 
			<h1>News</h1>
			</div>
		<?php endif;?>
		<p></p>
        <div class="row col-4-nth sm-col-2-nth" style="margin-left: -8px">	
			<?php if(count($articles) > 0) : ?>
				<?php foreach ($articles as $key => $value) : ?>
<!-- start post item -->
<div class="col-md-3 col-sm-6 col-xs-12 margin-50px-bottom last-paragraph-no-margin xs-margin-30px-bottom wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
							<div class="blog-post blog-post-style1 xs-text-center">
                    <div class="blog-post-images overflow-hidden margin-25px-bottom sm-margin-20px-bottom">
                        <a href="<?= base_url() ?>news.html/<?= $value->slug ?>">
							<?php if($value->image == '' || $value->image == null) : ?>
								<img style="height: 222px;" src="http://placehold.it/1200x840" alt="" data-no-retina="">
							<?php else: ?>
								<img style="height: 222px;"  src="<?= base_url() ?>files/blog/title_image/<?= $value->image ?>" alt="" data-no-retina="">
							<?php endif; ?>

                        </a>
                    </div>
                    <div class="post-details">
                        <a href="<?= base_url() ?>news.html/<?= $value->slug ?>" class="post-title text-medium text-extra-dark-gray width-90 display-block margin-20px-bottom sm-width-100">
                            <?= $value->title; ?></a>
                        <span class="post-author text-extra-small text-medium-gray text-uppercase display-block margin-10px-bottom xs-margin-5px-bottom"><?= $value->created ?></span>
                        <div class="separator-line-horrizontal-full bg-medium-light-gray margin-20px-tb sm-margin-15px-tb"></div>
                        <p class="width-90 xs-width-100"><?=  limit_text($value->content_text,20) ?></p>
                    </div>
                </div>
            </div>
            <!-- end post item -->
				<?php endforeach;?>
			
			<?php else: ?>
				
			<?php endif; ?>
            
        </div>
        <!-- start slider pagination -->
        <div class=" text-center margin-100px-bottom sm-margin-50px-top wow fadeInUp">
            <div class="pagination text-small text-uppercase text-extra-dark-gray">
                <!-- <ul>
                    <li><a href="#"><i class="fas fa-long-arrow-alt-left margin-5px-right xs-display-none"></i> Prev</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">Next <i class="fas fa-long-arrow-alt-right margin-5px-left xs-display-none"></i></a></li>
				</ul> -->
				<?php 
					echo $pagination;
				?>
            </div>
        </div>
        <!-- end slider pagination -->
    </div>
</section>		
<!--Content End-->
