<style>
	#company_name {
		text-transform:uppercase;
	}
</style>
<div class="row">
<div class="col-xl-12">
<div class="m-portlet " id="m_portlet">
<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-file"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Trade Buyer Registration
												</h3>
											</div>
										</div>
									</div>

<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" id="tradebuyerForm" action="<?= base_url()?>TradeBuyer/create">
								<div class="m-portlet__body">
								
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" maxlength="25" name="company_name" id="company_name" aria-describedby="bank_name" placeholder="company_name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Address*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="company_address" id="company_address" aria-describedby="bank_name" placeholder="company_address" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">City*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<input type="text" class="form-control m-input" name="city" id="city" aria-describedby="city" placeholder="city" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Country*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control m-select2" id="country_code" name="country_code" required>
										<option value="">Select Country</option>
										<?php foreach ($country as $key => $value) : ?>
											<option value="<?= $value['country_code'] ?>"><?= $value['country_name'] ?></option>
										<?php endforeach;?>
									</select>	
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Office Phone*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="company_phone" id="company_phone" aria-describedby="company_phone" placeholder="company_phone" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="email" class="form-control m-input" name="company_email" id="company_email" aria-describedby="company_email" placeholder="company_email" required>
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Full Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" maxlength="15" name="full_name" id="full_name" aria-describedby="seller_name" placeholder="full_name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Position</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="position" id="position" aria-describedby="postion" placeholder="postion">
										</div>
									</div>
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Mobile</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="mobile" id="mobile" aria-describedby="mobile" placeholder="mobile" >
										</div>
									</div>
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-file"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Registration Fee : IDR <?= number_format($fee) ?>
												</h3>
											</div>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Terms & Condition*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<div class="m-checkbox-list">
														<label class="m-checkbox">
															<input type="checkbox" disabled required value="1" id="termsCk" name="is_read_terms_condition"><b>I have read and agree to the terms and conditions</b>
															<span></span>
														</label>
														
										</div>
										<div id="termsModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document" style="max-width:100%; min-height:100%;">
		<div class="modal-content">
			<div class="modal-body">
				<?=$terms['gs_value']; ?>
				<br>
			</div>
			<div class="modal-footer">
                        <button type="button" onclick="fAccept()" class="btn btn-secondary" data-dismiss="modal">Accept</button>
                    </div>
		</div>
	</div>
</div>
											<button class="btn btn-primary" id="terms" type="button">Read Terms & Condition</button>
										</div>
									</div>
									
									
									<div class="form-group m-form__group row">
									<label class="col-form-label col-lg-3 col-sm-12">Human Challenge*</label>
									<div class="col-lg-7 col-md-7 col-sm-12">
									<?php echo $recaptcha_html ?>
									<?php echo $recaptcha_script ?>
										</div>
								</div>	
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
											<button type="button" onclick="fsubmit()" class="btn btn-brand">Submit Registration</button>
												<button style="display: none" type="submit" id="btnSubmit" class="btn btn-brand">Submit Registration</button>
											</div>
										</div>
									</div>
								</div>
							</form>

</div>
</div>
</div>

<script>
	function fAccept() { 
		$("#termsCk").removeAttr("disabled");
		$("#termsCk").attr("checked",true);

	 }
$ (function ($) {

	

	'use strict';
	$(".m-select2").select2();
$("#terms").click(function (e) { 
	e.preventDefault();
	$("#termsModal").modal({ backdrop: "static", keyboard: false });
	$("#termsModal").modal("show");

});

	$("#email__").change(function (e) { 
		e.preventDefault();
		$.ajax({
			type: "get",
			url: "<?= base_url() ?>TradeBuyer/check_email_exist",
			data: {email: $(this).val()},
			dataType: "json",
			success: function (response) {
				if(response.status){
					$("#email").val("");
					$("#email").focus();
					swal({
                                title: "Warning",
                                text: "EMAIL IS EXIST, PLEASE LOG IN OR RESET PASSWORD",
                                type: "warning"
							});
					
					
				}
			}
		});
	});
});

// $(".mask_number").inputmask({
//                       'alias': 'numeric',
//                       'groupSeparator': '.',
//                     'radixPoint': ',',
//                       'digits': 0,
//                       'autoGroup': true
// 				});

				$("#company_name").maxlength({alwaysShow:!0,threshold:5,warningClass:"m-badge m-badge--primary m-badge--rounded m-badge--wide",limitReachedClass:"m-badge m-badge--brand m-badge--rounded m-badge--wide"});
				$("#full_name").maxlength({alwaysShow:!0,threshold:5,warningClass:"m-badge m-badge--primary m-badge--rounded m-badge--wide",limitReachedClass:"m-badge m-badge--brand m-badge--rounded m-badge--wide"});
				
</script>


<script>
	var FormControls= {
    init:function() {
        $("#tradebuyerForm").validate( {
            rules: {
                email: {
                    required: !0, email: !0, minlength: 10
                } 
            }
            , invalidHandler:function(e, r) {
                //$("#m_form_1_msg").removeClass("m--hide").show(), mUtil.scrollTop()
            }
            , submitHandler:function(e) {
				//$("#sellerForm").submit();
				//console.log(e);
				$(form).submit();
			}
        }
        )
    }
}

;
jQuery(document).ready(function() {
    FormControls.init()
}

);



function fsubmit() {
	

	if(grecaptcha.getResponse() == "") {
		//e.preventDefault();
		swal({
            title: "Warning",
            text: "Captcha is empty",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        })
		return false;
	} else {
		swal({
            title: "CONFIRM",
            text: "Are you sure to submit ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            //console.log(e);
            if (e.value) {
                $("#btnSubmit").click()
            }
        })
		//return true;
	}

	

}

</script>
