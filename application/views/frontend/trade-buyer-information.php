<!--Content-->				
<section id="inner" class="padding-80px-bottom">
    <div class="container-fluid">
	<?php if($sub_image != '' || $sub_image != null) : ?>
			<div id="banner" class="banner" style="background:url('<?=base_url() ?>files/sub_header/<?= $sub_image ?>')"> 
            <h1>Trade Buyer Information</h1>
		</div>
		<?php else: ?>
			<div id="banner" class="banner" style="background:gray"> 
			<h1>Trade Buyer Information</h1>
			</div>
		<?php endif;?>
		<div>
		<div class="container-fluid">
		<ul style="color: rgb(86, 73, 66); font-family: &quot;Open Sans&quot;; font-size: 14px;"><li><p><strong>Event and Organizer</strong></p><p>BBTF event&nbsp;is organized by ASITA BALI Chapter which the event will be held in Nusa Dua, Bali&nbsp;for B2B session</p></li><li><p><strong>BBTF 2020&nbsp;Trade Buyer&nbsp;Profile:</strong></p><ul><li>Travel wholesalers</li><li>Retailers and agents</li><li>Convention and incentive organizers</li></ul></li><li><p>An administration fee for Trade Buyer&nbsp;is IDR. 1,450,000/person WITHOUT Lunch,&nbsp;&nbsp;inclusive of:</p><ul><li>Admission to BtoB Travex</li><li>Access to onsite appointments</li></ul></li><li><p><strong>Dress Code</strong></p><p>Must wear business attire during BBTF events at Nusa Dua,Bali</p></li></ul>
		</div>
    </div>
</section>
<!--Content End-->
