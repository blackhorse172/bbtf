<div class="m-grid__item m-grid__item--fluid m-grid  m-error-1" style="background-image: url(<?= base_url() ?>assets/app/media/img//error/bg1.jpg);">
				<div class="m-error_container">
					<span class="m-error_number">
						<h1>Sorry!</h1>
					</span>
					<p class="m-error_desc">
						Registration for BBTF 2021 will be open soon
					</p>
				</div>
			</div>
