<!--Content-->				
<section id="inner" class="padding-80px-bottom">
    <div class="container-fluid">
		<?php if($sub_image != '' || $sub_image != null) : ?>
			<div id="banner" class="banner" style="background:url('<?=base_url() ?>files/sub_header/<?= $sub_image ?>')"> 
            <h1>About Us</h1>
		</div>
		<?php else: ?>
			<div id="banner" class="banner" style="background:gray"> 
			<h1>About Us</h1>
			</div>
		<?php endif;?>
	
		<div>
		<div class="container-fluid">
<p style="text-align: justify;">BBTF is an annual event with the intention to establish a benchmark for Travel &amp; Tourism events and destinations in Indonesia. Supported by the Ministry of Tourism of The Republic of Indonesia, BBTF aims to advance the tourism industry through business sessions and direct promotion of best practices in MICE and Leisure management.</p>
<p style="text-align: justify;">Together with exhibitors and buyers from numerous countries around the world, BBTF facilitates Pre-Scheduled Appointments (PSA) and business sessions for buyers and sellers to negotiate long-term leisure and corporate travel needs. Corporations, Travel Agents, Hotel &amp; Resort Chains, Cruise Operators, Convention Planners, Venue Operators and travel buyers leverage the two days of business-to-business sessions to enhance their respective brands in the Indonesian and Asia Pacific markets.</p>
		</div>
		</div>
    </div>
</section>
<!--Content End-->
