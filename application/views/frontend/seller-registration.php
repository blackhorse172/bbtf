<div class="row">
<div class="col-xl-12">
<div class="m-portlet " id="m_portlet">
<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-file"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Seller Registration
												</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<!-- <li class="m-portlet__nav-item">
													<a href="#" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air">
														<span>
															<i class="la la-plus"></i>
															<span>Add Event</span>
														</span>
													</a>
												</li> -->
											</ul>
										</div>
									</div>

<form class="m-form m-form--fit m-form--label-align-right" id="sellerForm" method="post" enctype="multipart/form-data" action="<?= base_url()?>Seller/create">
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="seller_name" id="seller_name" aria-describedby="seller_name" placeholder="Company Name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Address*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="seller_address" id="seller_address" aria-describedby="bank_name" placeholder="Address" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">City*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="seller_city" id="seller_city" aria-describedby="bank_name" placeholder="City" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Province</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="seller_province" id="seller_province" aria-describedby="notes" placeholder="Province">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Country*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control m-select2" id="seller_country_code" name="seller_country_code" required>
										<option value="">Select Country</option>
										
										<?php foreach ($country as $key => $value) : ?>
											<option value="<?= $value['country_code'] ?>"><?= $value['country_name'] ?></option>
										<?php endforeach;?>
									</select>	
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="email" class="form-control m-input" name="email" id="email" aria-describedby="email" placeholder="email" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Password*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="password" class="form-control m-input" name="password" min-length="8" id="password" aria-describedby="password" placeholder="Password min 8 length" required>
										</div>
									</div>
									
									<input type="hidden" class="form-control m-input" name="full_delegate_name" id="full_delegate_name" aria-describedby="" placeholder="Full Delegate Name">
									<input type="hidden" class="form-control m-input" name="co_delegate_name" id="co_delegate_name" aria-describedby="co_delegate_name" placeholder="Co Delegate Name">
									<!-- <div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Full Delegate</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="full_delegate_name" id="full_delegate_name" aria-describedby="" placeholder="Full Delegate Name">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Co Delegate</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="co_delegate_name" id="co_delegate_name" aria-describedby="co_delegate_name" placeholder="Co Delegate Name">
										</div>
									</div> -->
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Registration Person</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="registration_person" id="registration_person" aria-describedby="registration_person" placeholder="Registration Person">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Job Title</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="job_title" id="job_title" aria-describedby="job_title" placeholder="Job Title" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Phone*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="seller_phone" id="seller_phone" aria-describedby="seller_phone" placeholder="Phone" required>
										</div>
									</div><div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Fax</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="seller_fax" id="seller_fax" aria-describedby="seller_fax" placeholder="Fax">
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Target Market*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<div class="m-checkbox-list">
											<?php foreach ($target_market as $key => $value) : ?>
														<label class="m-checkbox">
															
															<input type="checkbox" id="checkbox" value="<?=  $value['gs_value'] ?>" name="seller_target_market[]"> <?=  $value['gs_value'] ?>
															<span></span>
														</label>
											<?php endforeach; ?>
														
										</div>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Category*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control m-select2" id="category_id" name="category_id" required>
										<option value="">Select Category</option>
										<?php foreach ($category as $key => $value) : ?>
											<option value="<?= $value['category_id'] ?>"><?= $value['category_name'] ?></option>
										<?php endforeach;?>
									</select>	
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Profile*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<textarea name="seller_company_profile" class="form-control" id="" rows="10" required></textarea>
										</div>
									</div><div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Website</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" name="seller_website" id="seller_website" aria-describedby="seller_website" placeholder="Website">
										</div>
									</div>
									<div class="form-group m-form__group row" id="packageList">
									<label class="col-form-label col-lg-3 col-sm-12">Stand Rental*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<table class="table">
											<thead class="thead-dark">
												<tr>
												<th scope="col">#</th>
												<th scope="col">Name</th>
												<th scope="col">Price IDR</th>
												<th scope="col">Quantity</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach($products as $i => $val) : ?>
												<tr>
												<th scope="row"><?= $i + 1; ?></th>
												<td>
												<input type="hidden" name="booth_product_id[]" value="<?= $val->product_id ?>">
												<input type="hidden" name="booth_product_name[]" value="<?= $val->product_name ?>">
												<input type="hidden" name="booth_product_is_package[]" value="<?= $val->is_package ?>">	
												<?=$val->product_name ?></td>
												<td> <input type="hidden" name="booth_product_price[]" id="price_<?= $val->product_id ?>" value="<?= $val->product_price ?>"> <span id="priceview_<?= $val->product_id ?>"><?= number_format($val->product_price,2) ?></span> <input type="hidden" value="<?= $val->total_stock ?>" id="stock_<?= $val->product_id ?>"/></td>
												<td><select class="form-control m-input" onchange="checkAmount(<?= $val->product_id ?>)" name="booth_product_amount[]" id="amount_<?= $val->product_id ?>">
												<option value="0">0</option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												</select></td>
												<!-- <td><input type="text" class="form-control m-input mask_number" onchange="checkAmount(<?= $val->product_id ?>)" id="amount_<?= $val->product_id ?>" name="booth_product_amount[]"></td> -->
												</tr>
												<?php endforeach; ?>
												<tr>
												<td colspan="3"><h4>TOTAL</h4></td>
												<td><h4><span id="totalPrice"></span></h4></td>

												</tr>
											</tbody>
											</table>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Terms & Condition*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<div class="m-checkbox-list">
														<label class="m-checkbox">
															<input type="checkbox" disabled required value="1" id="termsCk" name="is_read_terms_condition"><b>I have read and agree to the terms and conditions</b>
															<span></span>
														</label>
														
										</div>
										<div id="termsModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document" style="max-width:100%; min-height:100%;">
		<div class="modal-content">
			<div class="modal-body">
				<?=$terms['gs_value']; ?>
				<br>
			</div>
			<div class="modal-footer">
			<button type="button" onclick="fAccept()" class="btn btn-secondary" data-dismiss="modal">Accept</button>
                    </div>
		</div>
	</div>
</div>
											<button class="btn btn-primary" id="terms" type="button">Read Terms & Condition</button>
										</div>
									</div>
									
									<?php if( CNF_RECAPTCHA ) { ?>
								<div class="form-group m-form__group row">
									<label class="col-form-label col-lg-3 col-sm-12">Human Challenge*</label>
									<div class="col-lg-7 col-md-7 col-sm-12">
									<?php echo $recaptcha_html ?>
									<?php echo $recaptcha_script ?>
										</div>
								</div>
								<?php } ?>
									
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="button" onclick="fsubmit()" class="btn btn-brand">Submit Registration</button>
												<button style="display: none" type="submit" id="btnSubmit" class="btn btn-brand">Submit Registration</button>

											</div>
										</div>
									</div>
								</div>
							</form>

</div>
</div>
</div>
<script>
	var FormControls= {
    init:function() {
        $("#sellerForm").validate( {
            rules: {
                email: {
                    required: !0, email: !0, minlength: 10
                }
                , seller_target_market: {
                    required: !0,minlength: 1
                }
                , password: {
                    required: !0,  minlength : 8,alphanumeric: true
                }
                , seller_phone: {
					required: !0,  // <-- no such method called "matches"!
					minlength:8,
					maxlength:13
				},
				is_read_terms_condition: {
                    required: !0,minlength: 1
                }
				
               
            }
            , invalidHandler:function(e, r) {
                //$("#m_form_1_msg").removeClass("m--hide").show(), mUtil.scrollTop()
            }
            , submitHandler:function(e) {
				//$("#sellerForm").submit();
				//console.log(e);
				$(form).submit();
			}
        }
        )
    }
}

;
jQuery(document).ready(function() {
    FormControls.init()
}

);
</script>
<script>

function fAccept() { 
		$("#termsCk").removeAttr("disabled");
		$("#termsCk").attr("checked",true);
}

function fsubmit() {
	var price = $("#totalPrice").html();

	if(price == "" || price == 0){
		swal({
            title: "Warning",
            text: "Booth order is empty",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        })

		return false;
	}

	if(grecaptcha.getResponse() == "") {
		//e.preventDefault();
		swal({
            title: "Warning",
            text: "Captcha is empty",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        })
		return false;
	} else {
		swal({
            title: "CONFIRM",
            text: "Are you sure to submit ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            //console.log(e);
            if (e.value) {
                $("#btnSubmit").click()
            }
        })
		//return true;
	}

	

}

	function checkAmount(id) 
{
	//alert("change");
	var stock = $("#stock_"+id).val();
	//var price = $("#price_"+id).val();
	var amounts = $("#amount_"+id).val().replace(".","");
	var total = 0;
	if(parseInt(amounts) > parseInt(stock)){
		$("#amount_"+id).focus();
		$("#amount_"+id).val("");
		swal({
            title: "Warning",
            text: "the number cannot be more than product stock",
            type: "warning",
        });
	}

	$("select[name='booth_product_amount[]']").each(function (i) {
				if($(this).val() != ""){
					var amount = parseInt($(this).val());
					var thisId = $(this).attr("id").split("_")[1];
					var price = $("#price_"+thisId).val();

					total = total + (parseInt(price) * amount);
					$("#totalPrice").html(numberWithCommas(total));
				}
				
		 });

}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

$ (function ($) {
	'use strict';
	$(".m-select2").select2();
$("#terms").click(function (e) { 
	e.preventDefault();
	$("#termsModal").modal({ backdrop: "static", keyboard: false });
	$("#termsModal").modal("show");

});

	$("#email").change(function (e) { 
		e.preventDefault();
		$.ajax({
			type: "get",
			url: "<?= base_url() ?>Seller/check_email_exist",
			data: {email: $(this).val()},
			dataType: "json",
			success: function (response) {
				if(response.status){
					$("#email").val("");
					$("#email").focus();
					swal({
                                title: "Warning",
                                text: "EMAIL IS EXIST, PLEASE LOG IN OR RESET PASSWORD",
                                type: "warning"
							});
					
					
				}
			}
		});
	});
});
</script>
