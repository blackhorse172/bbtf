

<?php 
$testimonies = $this->db->get_where('testimony', array('is_active'=>1,"show_home"=>1))->result_array();
?>



<!--Content-->
<section class="wow fadeIn margin-10px-top" style="visibility: visible; animation-name: fadeIn;" id="inner">
    <div class="container-fluid padding-25px-lr"> 
        <div id="banner" class="banner margin-10px-bottom" style="background:url('<?= base_url() ?>assets/frontend/images/foto02.png')"> 
            <h1>Testimonial</h1>
        </div>
        <div class="row col-4-nth sm-col-2-nth margin-30px-top margin-100px-lr sm-margin-10px-lr">
		<?php if(count($testimonies) > 0) : ?>
			<?php foreach ($testimonies as $key => $value) : ?>
  <!-- start testimony item -->
            
  <div class="row padding-20px-lr bg-gray margin-20px-bottom sm-margin-15px-tb">
            <div class="col-md-12 col-sm-6 col-xs-12 margin-30px-bottom last-paragraph-no-margin sm-margin-30px-top">
                <div class="blog-post blog-post-style1 xs-text-center">
                    <div class="post-details">
                        <h3><?= $value['title'] ?></h3>
                        <div class="separator-line-horrizontal-full bg-medium-light-gray margin-20px-tb sm-margin-15px-tb"></div>
                        <p class="width-90 xs-width-100">"<?= $value['content'] ?>"</p>
                        <div class="text-extra-small text-medium-gray text-uppercase display-block margin-10px-bottom xs-margin-5px-bottom float-right" >
                            <p style="text-align: right;"><?= $value['name'] ?></p>
                            <span><?= $value['company_name'] ?></span>
                        </div>
                    </div>
                </div>
            </div>
            </div>
			<!-- end testimony item -->
			
	<?php endforeach;?>
			
			<?php else: ?>
					<h3> No Data....</h3>
			<?php endif; ?>

          
        </div>
    </div>
</section>		
<!--Content End-->
