<?php 

//$res = $this->db->get('v_seller_list_new')->result_array();
$this->db->where('is_listing', 1);
$this->db->join('apps_countries', 'apps_countries.country_code = seller_sub.seller_country_code');
$res = $this->db->get('seller_sub')->result_array();

?>
<!-- start list-->
<section class="wow fadeIn padding-seven-bottom text-center text-md-left" id="inner">
    <div class="container-fluid">
	<?php if($sub_image != '' || $sub_image != null) : ?>
			<div id="banner" class="banner" style="background:url('<?=base_url() ?>files/sub_header/<?= $sub_image ?>')"> 
            <h1>Seller List</h1>
		</div>
		<?php else: ?>
			<div id="banner" class="banner" style="background:gray"> 
			<h1>Seller List</h1>
			</div>
		<?php endif;?>
    </div>
    <div class="container">
        <div class="row">
            <table class="table text-left">
                <thead>
                    <tr>
                        <th>No</th>
						<th>Company</thstyle="width: 1%;>
                        <!-- <th>Booth Number</th>
						<th>Fascia Name</th> -->
						<th>Country</th>
						<th>Booth Number</th>

                    </tr>
                </thead>
                <tbody>
					<?php foreach ($res as $key => $value) :?>
					<tr>
                        <td><?= $key + 1 ?></td>
						<td><?= $value['seller_name'] ?></td>
                        <!-- <td><?= $value['booth_number'] ?></td>
                        <td><?= $value['booth_name'] ?></td>-->
						<td><?= $value['country_name'] ?></td>
						<td><?= $value['booth_number'] ?>

                    </tr>
					<?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<!-- end list-->
