<?php 

$this->db->where('is_listing', 1);
$res = $this->db->get('buyer_list')->result_array();

?>
<!-- start list-->
<section class="wow fadeIn padding-seven-bottom text-center text-md-left" id="inner">
    <div class="container-fluid">
	<?php if($sub_image != '' || $sub_image != null) : ?>
			<div id="banner" class="banner" style="background:url('<?=base_url() ?>files/sub_header/<?= $sub_image ?>')"> 
            <h1>Buyer List</h1>
		</div>
		<?php else: ?>
			<div id="banner" class="banner" style="background:gray"> 
			<h1>Buyer List</h1>
			</div>
		<?php endif;?>
    </div>
    <div class="container">
        <div class="row">
            <table class="table text-left">
                <thead>
                    <tr>
                        <th>No</th>
						<th>Company</thstyle="width: 1%;>
                        <th>Full name</thstyle="width: 1%;>
                        <th>Country</th>
                    </tr>
                </thead>
                <tbody>
					<?php foreach ($res as $key => $value) :?>
					<tr>
                        <td><?= $key + 1 ?></td>
                        <td><?= $value['company_name'] ?></td>
                        <td><?= $value['first_name']." ".$value['last_name'] ?></td>
                        <td><?= $value['country_name'] ?></td>
                    </tr>
					<?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<!-- end list-->
