<?php 
function limit_text($text, $limit) {
	if (str_word_count($text, 0) > $limit) {
		$words = str_word_count($text, 2);
		$pos = array_keys($words);
		$text = substr($text, 0, $pos[$limit]) . '...';
	}
	return $text;
  }
?>
<!--Content-->
<section class="wow fadeIn margin-10px-top" style="visibility: visible; animation-name: fadeIn;" id="inner">
    <div class="container-fluid padding-25px-lr sm-padding-10px-lr"> 
        <div class="margin-100px-top margin-50px-bottom margin-100px-lr"> 
		<h1><?= $detail['title'] ?></h1>
        </div>
        <div class="row col-4-nth sm-col-2-nth">
            <!-- start post item -->
            <div class="col-md-12 col-sm-12 col-xs-12 margin-50px-bottom padding-100px-lr last-paragraph-no-margin xs-margin-30px-bottom wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                <div class="blog-post blog-post-style1 xs-text-center">
                    <div class="post-details">
                        <span class="post-author text-medium-gray text-uppercase display-block margin-10px-top margin-10px-bottom xs-margin-5px-bottom"><?= $detail['created'] ?></span>
                        <div class="separator-line-horizontal-full bg-medium-light-gray margin-20px-tb sm-margin-15px-tb"></div>
                        <?= $detail['content'] ?>
                    </div>
                </div>
            </div>
            <!-- end post item -->
        </div>
    </div>
</section>
<!-- Content End -->
<!-- Other Post -->
<section>
    <hr class="no-margin-bottom">
    <div class="padding-25px-lr">
        <div class="row"> 
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 text-LEFT" style="margin-bottom: 3px;">
                        <h4>
                        OTHER NEWS</h4>
                    </div>
                    <!-- start other post -->
                    <div class="row col-4-nth sm-col-2-nth">
					<?php if(count($otherNews) > 0) : ?>
				<?php foreach ($otherNews as $key => $value) : ?>
<!-- start post item -->
<div class="col-md-3 col-sm-6 col-xs-12 margin-50px-bottom last-paragraph-no-margin xs-margin-30px-bottom wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
							<div class="blog-post blog-post-style1 xs-text-center">
                    <div class="blog-post-images overflow-hidden margin-25px-bottom sm-margin-20px-bottom">
                        <a href="<?= base_url() ?>news.html/<?= $value['slug']?>">
							<?php if($value['image'] == '' || $value['image'] == null) : ?>
								<img style="height: 222px;" src="http://placehold.it/1200x840" alt="" data-no-retina="">
							<?php else: ?>
								<img style="height: 222px;"  src="<?= base_url() ?>files/blog/title_image/<?= $value['image'] ?>" alt="" data-no-retina="">
							<?php endif; ?>

                        </a>
                    </div>
                    <div class="post-details">
                        <a href="<?= base_url() ?>news.html/<?= $value['slug']?>" class="post-title text-medium text-extra-dark-gray width-90 display-block margin-20px-bottom sm-width-100">
                            <?= $value['title']; ?></a>
                        <span class="post-author text-extra-small text-medium-gray text-uppercase display-block margin-10px-bottom xs-margin-5px-bottom"><?= $value['created'] ?></span>
                        <div class="separator-line-horrizontal-full bg-medium-light-gray margin-20px-tb sm-margin-15px-tb"></div>
                        <p class="width-90 xs-width-100"><?=  limit_text($value['content_text'],20) ?></p>
                    </div>
                </div>
            </div>
            <!-- end post item -->
				<?php endforeach;?>
			
			<?php else: ?>
					<h3> No Data....</h3>
			<?php endif; ?>
                       
                    </div>
                    <!-- end other posts -->
                </div>
            </div>
        </div>
    </div>
</section>		
<!-- Other Posts End-->
