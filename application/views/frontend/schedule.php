
<?php
$header = $this->db->get_where('general_settings',array('gs_code'=>'header_schedule_color'))->row()->gs_value;
$subHeader = $this->db->get_where('general_settings',array('gs_code'=>'sub_header_schedule_color'))->row()->gs_value;
?>

<!-- start list-->
<section class="wow fadeIn padding-four-bottom text-center text-md-left" id="inner">
    <div class="container-fluid">
	<?php if($sub_image != '' || $sub_image != null) : ?>
			<div id="banner" class="banner" style="background:url('<?=base_url() ?>files/sub_header/<?= $sub_image ?>')"> 
            <h1>Schedule</h1>
		</div>
		<?php else: ?>
			<div id="banner" class="banner" style="background:gray"> 
			<h1>Schedule</h1>
			</div>
		<?php endif;?>
		<p></p>
    <div class="container-fluid padding-25px-lr">
        <div class="row">
            <table class="table">
                <tbody>
                    <tr class="bg-orange text-center text-white alt-font" style="background: <?= $header ?>">
                        <td>DAY/DATE</td>
                        <td>PROGRAM</td>
                        <td>VENUE</td>
                    </tr>
                    <tr class="bg-medium-gray text-center alt-font" style="background: <?= $subHeader ?>">
                        <td>&nbsp;</td>
                        <td>Tue, 9 June 2020</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>All Day</td>
                        <td>Participant Buyer / Trade Buyer / Media Arrival</td>
                        <td>Ngurah Rai International Airport</td>
                    </tr>
                    <tr>
                        <td>12.00 - 18.00</td>
                        <td>Seller Registration</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr>
                        <td>14.00 - 20.00</td>
                        <td>Booth Setup for Exhibitor / Seller</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr class="bg-medium-gray text-center alt-font"  style="background: <?= $subHeader ?>">
                        <td>&nbsp;</td>
                        <td>Wed, 10 June 2020</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>09.00 - 18.00</td>
                        <td>Seller / Buyer / Trade Buyer / Media Registration</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr>
                        <td>08.00 - 14.00</td>
                        <td>Booth Setup for Exhibitor / Seller</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr>
                        <td>13.00 - 16.00</td>
                        <td>BBTF Tourism Seminar</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr>
                        <td>16.30 - 18.00</td>
                        <td>BBTF Press Conference</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr>
                        <td>16.00 - 16.30</td>
                        <td>Seller &amp; Buyer Briefing Session</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr>
                        <td>16.30 - 17.30</td>
                        <td>Seller Meets Buyer Session</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr>
                        <td>17.30 - 18.00</td>
                        <td>Buyer Meets Seller Session</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr>
                        <td>18.00 - 19.00</td>
                        <td>Shuttle to Welcome Dinner Venueee</td>
                        <td>NUSA DUA</td>
                    </tr>
                    <tr>
                        <td>19.00 - 22.30</td>
                        <td>Welcome Dinner &amp; BBTF Opening Ceremony</td>
                        <td>NUSA DUA</td>
                    </tr>
                    <tr class="bg-medium-gray text-center alt-font"  style="background: <?= $subHeader ?>">
                        <td>&nbsp;</td>
                        <td>Thu, 11 June 2020</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>09.00 - 12.00</td>
                        <td>B2B BBTF TRAVEX</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr>
                        <td>12.00 - 13.00</td>
                        <td>Hosted Lunch for Buyer &amp; Media Only</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr>
                        <td>13.00 - 18.00</td>
                        <td>B2B BBTF TRAVEX</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr>
                        <td>09.00 - 18.00</td>
                        <td>Media Talks Show Session</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr class="bg-medium-gray text-center alt-font"  style="background: <?= $subHeader ?>">
                        <td>&nbsp;</td>
                        <td>Fri, 12 June 2020</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>09.00 - 12.00</td>
                        <td>B2B BBTF TRAVEX</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr>
                        <td>12.00 - 13.00</td>
                        <td>Hosted Lunch for Buyer &amp; Media Only</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr>
                        <td>13.00 - 18.00</td>
                        <td>B2B BBTF TRAVEX</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr>
                        <td>09.00 - 15.00</td>
                        <td>Media Talks Show Session</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr>
                        <td>16.00 - 17.30</td>
                        <td>Closing BBTF Press Conference</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr>
                        <td>18.00 - 22.00</td>
                        <td>Booth Dismantle</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr>
                        <td>18.00 - 19.00</td>
                        <td>Shuttle to Farewell Party Venue</td>
                        <td>BNDCC</td>
                    </tr>
                    <tr>
                        <td>19.00 - 20.30</td>
                        <td>Farewell Party for Buyer &amp; Media Only</td>
                        <td>NUSA DUA</td>
                    </tr>
                    <tr class="bg-medium-gray text-center alt-font"  style="background: <?= $subHeader ?>">
                        <td>&nbsp;</td>
                        <td>Sat, 13 June 2020</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>06.00 - 13.00</td>
                        <td>Transfer to Airport &amp; Post Tour</td>
                        <td>NUSA DUA</td>
                    </tr>
                </tbody>
            </table>
            
            
        </div>
    </div>
</section>
<!-- end list-->
