<!--Content-->				
<section id="inner" class="padding-80px-bottom">
    <div class="container-fluid">
	<?php if($sub_image != '' || $sub_image != null) : ?>
			<div id="banner" class="banner" style="background:url('<?=base_url() ?>files/sub_header/<?= $sub_image ?>')"> 
            <h1>Media Information</h1>
		</div>
		<?php else: ?>
			<div id="banner" class="banner" style="background:gray"> 
			<h1>Media Information</h1>
			</div>
		<?php endif;?>
		<div>
		<div class="container-fluid">
		<ul style="color: rgb(86, 73, 66); font-family: &quot;Open Sans&quot;; font-size: 14px;"><li><p><strong>Event and Organizer</strong></p><p>BBTF event&nbsp;is organized by ASITA BALI Chapter and will be held in Nusa Dua, Bali&nbsp;for B2B session</p></li><li><p><strong>BBTF 2020 Media Profile:</strong></p><ul><li>Editors</li><li>Journalists</li><li>Reporters</li><li>Other editorial representatives of bonafide leisure travel trade and tourism publishers from around the world</li></ul><p>All registrants must undergo accreditation before confirmation as a&nbsp;Media Participants</p></li><li><p><strong>Media Entitlement:</strong><br>All media should received all below item upon your online registration</p><ul><li>Admission to Travex</li><li>Entry to BBTF 2020’s Media Briefings</li><li>Entry to Opening Press Conference &amp; Closing Press Conference</li><li>Entry to attend Media Talk Show</li><li>Entry to &nbsp;Welcome Diner party, Lunch &amp; Farewell Cocktail Party</li><li>Complimentary return airport transfers to official hotels at stipulated timing</li><li>Complimentary daily shuttles to/from official hotels, venue and official functions</li></ul></li><li><p><strong>Dress Code</strong><br>All Media must wear business attire during BBTF events at Nusa Dua ,Bali</p></li><li><strong>Dates : 9th - 13th June 2020</strong></li><li><strong>REGISTRATION</strong>&nbsp;; Media Registration online will be opened by Mid January 2020&nbsp;</li><li><strong>M</strong><strong>edia Information</strong><br>All BBTF update &amp; information will be handle by :<a href="mailto:media@bbtf.or.id" style="color: rgb(86, 73, 66);">media@bbtf.or.id</a></li></ul>
		</div>
    </div>
</section>
<!--Content End-->
