
<!--Content-->
<section class="wow fadeIn">

    <div class="container-fluid" style="padding: 0 30px;">
	<div class="row margin-ten-bottom padding-five-all cover-background"  data-stellar-background-ratio="0.5" style="background-image: url(&quot;<?= base_url() ?>assets/frontend/images/new-banner.png&quot;); background-position: 0px 0px; visibility: visible; animation-name: fadeIn;">                  
            <div class="opacity-medium bg-extra-dark-gray"></div>
            <div class="col-lg-7 col-md-7 col-sm-12 pull-right bg-white padding-five-bottom">
                <div class="padding-ten-lr padding-five-tb sm-padding-70px-all xs-padding-25px-all bg-white box-shadow-light">
                    <h1 class="text-extra-dark-gray margin-50px-bottom text-center sm-margin-20px-bottom alt-font">LOGIN</h1>
                    <div class="col-md-6 col-md-offset-3 col-sm-12">
						
					<form id="contact-form-3" action="<?= base_url() ?>user/userPostLogin" method="post">
                            <div class="row">
                                <!-- <div class="col-md-12">
                                    <div class="select-style big-select">
                                        <select name="budget" id="budget" class="bg-transparent no-margin-bottom">
                                             <option value="">Login As</option>
                                            <option value="1">Seller</option>
                                            <option value="2">Buyer</option>
                                        </select>
                                    </div>
								</div> -->
								<div class="col-md-12">
								
								
								</div>
                                <div class="col-md-12">
									<input type="text" name="email" id="email" value="<?php echo $this->input->post('email'); ?>"  placeholder="Email" class="big-input" required>
									<?php echo form_error('email');  ?>
                                </div>
                                <div class="col-md-12">
                                    <input type="password" name="password" value="<?php echo $this->input->post('password'); ?>" id="password" placeholder="Password" required class="big-input">
									<?php echo form_error('password');  ?>

								</div>

                                <div class="col-md-12 text-center">
                                    <button id="login-button" type="submit" class="btn btn-orange btn-large margin-20px-top">Login</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Content End-->
<?php if($this->session->flashdata("success_register") == true){ ?>
<script>
$(document).ready(function () {
	$("#modalDetail").modal("show")
});
</script>  
<?php } ?>

<?php 
									if($this->session->flashdata("message") == true){ ?>
									<script>
$(document).ready(function () {
	$("#modalDetail").modal("show")
});
</script>  
									<?php 
									}
								?>
								
<div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg" role="document" > <!-- style="max-width:100%; min-height:100%;"> -->
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 id="empName" class="m--font-bold"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
					<?php echo $this->session->flashdata("message") ?><?php echo $this->session->flashdata("success_register") ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
