<?php 
$this->db->where('category_id', 4);
$res = $this->db->get('product')->result_array();

function limit_text($text, $limit) {
	if (str_word_count($text, 0) > $limit) {
		$words = str_word_count($text, 2);
		$pos = array_keys($words);
		$text = substr($text, 0, $pos[$limit]) . '...';
	}
	return $text;
  }
?>
<!-- start list-->
<section class="wow fadeIn padding-seven-bottom text-center text-md-left" id="inner">
<div class="container-fluid">
	<?php if($sub_image != '' || $sub_image != null) : ?>
		<div id="banner" class="banner margin-50px-bottom" style="background:url('<?=base_url() ?>files/sub_header/<?= $sub_image ?>')"> 
            <h1>Hosted Tour</h1>
		</div>
		<?php else: ?>
			<div id="banner" class="banner margin-50px-bottom" style="background:gray"> 
			<h1>Hosted Tour</h1>
			</div>
		<?php endif;?>
    </div>
    <div class="padding-25px-lr">
        <!--First Row-->
        <div class="row" style="margin-top: -43px; margin-right: -10px;">
          <div class="container-fluid">
              <div class="row" id="hosted">
			  <?php foreach ($res as $key => $value) :?>
				<div class="col-md-3 col-sm-6 col-xs-12 team-block text-left no-padding-right feature-box-15 last-paragraph-no-margin wow fadeInUp ">
                    <a href="<?= base_url() ?>hosted-tour-detail.html/<?= $value['id'] ?>">
                    <figure>
                        <div class="feature-box-content sm-width-100">
                            <div class="feature-box-image"><img src="<?= base_url() ?>files/product/<?= $value['image'] ?>" alt="" data-no-retina="" class="w-100" style="height: 265px;"></div>
                            <div class="hover-content bg-orange d-flex justify-content-center align-items-center">
                                <div class="padding-twelve-lr">
                                    <span class="text-white d-inline-block text-center"><?= $value['product_name']?></span>
                                </div>
                            </div> 
                        </div>
                        <figcaption>
                            <div class="text-center margin-25px-top">
                                <div class="text-medium alt-font font-weight-600 text-uppercase"><span><?= $value['product_name'] ?></span></div> 
                            </div>   
                        </figcaption>
                    </figure>
                    </a>
                </div>
			  <?php endforeach; ?>

            </div>  
            </div>
            </div>
            <!--Second Row-->
            
      </div>
</section>
<!-- end list-->
