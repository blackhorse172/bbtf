
<section class="wow fadeIn sm-padding-20px-top xs-padding-10px-top margin-100px-bottom" id="inner">
    <div class="container-fluid">
	<?php if($sub_image != '' || $sub_image != null) : ?>
			<div id="banner" class="banner" style="background:url('<?=base_url() ?>files/sub_header/<?= $sub_image ?>')"> 
            <h1>Gallery</h1>
		</div>
		<?php else: ?>
			<div id="banner" class="banner" style="background:gray"> 
			<h1>Gallery</h1>
			</div>
		<?php endif;?>
	</div>
	<p></p>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <!-- start filter navigation -->
                <ul class="portfolio-filter nav nav-tabs border-none portfolio-filter-tab-1 font-weight-600 alt-font text-uppercase text-center text-small margin-15px-top" style="margin-bottom: 22px;">
                    <!-- <li class="nav active"><a href="javascript:void(0);" data-filter="*" class="xs-display-inline light-gray-text-link text-very-small">All</a></li> -->
					<?php if(count($categories) >0) : ?>
					<?php foreach ($categories as $key => $v) : ?>
						<li class="nav"><a href="javascript:void(0);" data-filter=".<?= $v['category_id'] ?>" class="xs-display-inline light-gray-text-link text-very-small"><?= $v['category_name'] ?></a></li>
					<?php endforeach; ?>
					<?php endif; ?>
					
                </ul>                                                                           
                <!-- end filter navigation -->
            </div>
        </div>
    </div>
    <!-- start filter content -->
    <div class="container-fluid padding-25px-lr">
        <div class="row lightbox-gallery">
            <div class="col-md-12 no-padding xs-padding-15px-lr">
                <div class="filter-content overflow-hidden">
                    <ul class="portfolio-grid work-4col hover-option4 gutter-very-small">
						<li class="grid-sizer"></li>
						<?php if(count($gallery) > 0) : ?>
						<?php foreach ($gallery as $key => $v) : ?>
<!-- start portfolio-item item -->
<li class="grid-item <?= $v->category_id ?> wow fadeInUp">
                            <a style="height: 215px" href="<?= base_url().'files/gallery/'.$v->image ?>" title="<?= $v->title ?>">
                                <figure>
                                    <div class="portfolio-img bg-extra-dark-gray"><img src="<?= base_url().'files/gallery/'.$v->image ?>" alt=""/></div>
                                    <figcaption>
                                        <div class="portfolio-hover-main text-center">
                                            <div class="portfolio-hover-box vertical-align-middle">
                                                <div class="portfolio-hover-content position-relative">
                                                    <div class="bg-orange center-col separator-line-horrizontal-medium-light3 margin-25px-bottom"></div>
                                                    <span class="font-weight-600 letter-spacing-1 alt-font text-white text-uppercase margin-5px-bottom display-block"><?= $v->descr ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </li>
                        <!-- end portfolio item -->
						<?php endforeach;?>
						<?php else: ?>
						
						<?php endif; ?>
                        
                        
                    </ul>
                </div>
            </div>
        </div>
	</div>
	<!-- start slider pagination -->
	<div class=" text-center margin-100px-bottom sm-margin-50px-top wow fadeInUp">
            <div class="pagination text-small text-uppercase text-extra-dark-gray">
                <!-- <ul>
                    <li><a href="#"><i class="fas fa-long-arrow-alt-left margin-5px-right xs-display-none"></i> Prev</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">Next <i class="fas fa-long-arrow-alt-right margin-5px-left xs-display-none"></i></a></li>
				</ul> -->
				<?php 
				if(count($gallery) > 8) {
					//echo $pagination;

				}
				?>
            </div>
        </div>
        <!-- end slider pagination -->
</section>
