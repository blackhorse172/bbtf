<div class="row">
<div class="col-xl-12">
<div class="m-portlet " id="m_portlet">
<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-file"></i>
												</span>
												<h3 class="m-portlet__head-text">
													Buyer Registration
												</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<!-- <li class="m-portlet__nav-item">
													<a href="#" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air">
														<span>
															<i class="la la-plus"></i>
															<span>Add Event</span>
														</span>
													</a>
												</li> -->
											</ul>
										</div>
									</div>

<form class="m-form m-form--fit m-form--label-align-right" method="post" id="buyerForm" enctype="multipart/form-data" action="<?= base_url()?>Buyer/create">
								<div class="m-portlet__body">
								<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">mr/mrs*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="gender" name="gender" required>
										<option value=""></option>
										<option value="MR">MR</option>
										<option value="MRS">MRS</option>
										<option value="MS">MS</option>
										
										
									</select>	
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">First Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $this->input->post('first_name') ?>" class="form-control m-input" name="first_name" id="first_name" aria-describedby="seller_name" placeholder="first_name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Last Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $this->input->post('last_name') ?>" name="last_name" id="last_name" aria-describedby="last_name" placeholder="last_name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Email *</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="email" value="<?= $this->input->post('email') ?>" class="form-control m-input" name="email" id="email" aria-describedby="email" placeholder="email" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Name*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $this->input->post('company_name') ?>" class="form-control m-input" name="company_name" id="company_name" aria-describedby="bank_name" placeholder="company_name" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Job Title</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $this->input->post('job_title') ?>" class="form-control m-input" name="job_title" id="job_title" aria-describedby="job_title" placeholder="Job Title" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Company Profile*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<textarea name="buyer_company_profile" class="form-control" id="" rows="10" required><?= $this->input->post('buyer_company_profile') ?></textarea>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Nature Of Business*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control m-select2" id="nature_of_business" name="nature_of_business" required>
										<option value="">- None -</option><option value="L">Leisure</option><option value="M">Mice</option><option value="S">Special Interest</option><option value="I">Incentive</option><option value="W">Wholesale</option><option value="E">Event Organizer</option><option value="R">Retail</option><option value="D">DMC</option>
									</select>	
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">City*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<input type="text" required value="<?= $this->input->post('buyer_city') ?>" class="form-control m-input" name="buyer_city" id="buyer_city" aria-describedby="buyer_city" placeholder="City" >
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Country*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control m-select2" id="buyer_country_code" name="buyer_country_code" required>
										<option value="">Select Country</option>
										
										<?php foreach ($country as $key => $value) : ?>
											<option value="<?= $value['country_code'] ?>"><?= $value['country_name'] ?></option>
										<?php endforeach;?>
									</select>	
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Phone*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $this->input->post('buyer_phone') ?>" class="form-control m-input" name="buyer_phone" id="buyer_phone" aria-describedby="buyer_phone" placeholder="Phone" required>
										</div>
									</div>
									
									
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Website</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $this->input->post('buyer_website') ?>" class="form-control m-input" name="buyer_website" id="buyer_website" aria-describedby="buyer_website" placeholder="Website">
										</div>
									</div>
<!-- info -->
<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Do you selling Bali and Beyond product?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="do_you_selling_bali_and_beyond_product" name="do_you_selling_bali_and_beyond_product" required>
										<option value=""></option>
										<option value="Y">YES</option>
										<option value="N">NO</option>
										
									</select>	
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Please state your local partner in Bali/ Indonesia (preferable name and email address)?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $this->input->post('please_state_you_local_partner_in_bali_indonesia') ?>" class="form-control m-input" name="please_state_you_local_partner_in_bali_indonesia" id="please_state_you_local_partner_in_bali_indonesia" aria-describedby="please_state_you_local_partner_in_bali_indonesia" placeholder="" required>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Your ID member of ASITA, IATA, PATA, ASEANTA or others member organization</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $this->input->post('member_id') ?>" class="form-control m-input" name="member_id" id="member_id" aria-describedby="member_id" placeholder="" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Since when you promoted Indonesia destination?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $this->input->post('since_when_you_promoted_indonesia_destination') ?>" class="form-control m-input" name="since_when_you_promoted_indonesia_destination" id="since_when_you_promoted_indonesia_destination" aria-describedby="since_when_you_promoted_indonesia_destination" placeholder="" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Is this your first time attending on BBTF?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $this->input->post('is_this_your_first_time_attending_on_bbtf') ?>" class="form-control m-input" name="is_this_your_first_time_attending_on_bbtf" id="is_this_your_first_time_attending_on_bbtf" aria-describedby="buyer_website" placeholder="" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">How did you know about BBTF?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $this->input->post('how_did_you_know_about_bbtf') ?>" class="form-control m-input" name="how_did_you_know_about_bbtf" id="how_did_you_know_about_bbtf" aria-describedby="" placeholder="" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Indicated 3 main destinations in Indonesia you want to meet in BBTF?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $this->input->post('indicated_main_destination') ?>" class="form-control m-input" name="indicated_main_destination" id="indicated_main_destination" aria-describedby="how_did_you_know_about_bbtf" placeholder="" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Which products are you interested in?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<div class="m-checkbox-list">
											<?php foreach ($target_market as $key => $value) : ?>
														<label class="m-checkbox">
															<input type="checkbox" value="<?=  $value['gs_value'] ?>" name="which_products_are_you_interested_in[]"> <?=  $value['gs_value'] ?>
															<span></span>
														</label>
											<?php endforeach; ?>
														
										</div>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Do you sell any package in South East Asia?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<select class="form-control" id="do_you_sell_any_package_in_south_east_asia" name="do_you_sell_any_package_in_south_east_asia" required>
										<option value=""></option>
										<option value="Y">YES</option>
										<option value="N">NO</option>
										
									</select>	
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Please state your biggest partner in South East Asia (Company and Contact name)?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $this->input->post('biggest_partner') ?>" class="form-control m-input" name="biggest_partner" id="biggest_partner" aria-describedby="biggest_partner" placeholder="" required>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Please state your potential group to Bali and Beyond in 2020-2021</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $this->input->post('potential_group') ?>" class="form-control m-input" name="potential_group" id="potential_group" aria-describedby="potential_group" placeholder="" required>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Do you intending to explore beyond Bali after BBTF?</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $this->input->post('explore_bbtf') ?>" class="form-control m-input" name="explore_bbtf" id="explore_bbtf" aria-describedby="buyer_website" placeholder="" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Terms & Condition*</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<div class="m-checkbox-list">
														<label class="m-checkbox">
															<input type="checkbox" disabled required value="1" id="termsCk" name="is_read_terms_condition"><b>I have read and agree to the terms and conditions</b>
															<span></span>
														</label>
														
										</div>
										<div id="termsModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document" style="max-width:100%; min-height:100%;">
		<div class="modal-content">
			<div class="modal-body">
				<?=$terms['gs_value']; ?>
				<br>
			</div>
			<div class="modal-footer">
                        <button type="button" onclick="fAccept()" class="btn btn-secondary" data-dismiss="modal">Accept</button>
                    </div>
		</div>
	</div>
</div>
											<button class="btn btn-primary" id="terms" type="button">Read Terms & Condition</button>
										</div>
									</div>
									
									
								<div class="form-group m-form__group row">
									<label class="col-form-label col-lg-3 col-sm-12">Human Challenge*</label>
									<div class="col-lg-7 col-md-7 col-sm-12">
									<?php echo $recaptcha_html ?>
									<?php echo $recaptcha_script ?>
										</div>
								</div>

								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
											<button type="button" onclick="fsubmit()" class="btn btn-brand">Submit Registration</button>
												<button style="display: none" type="submit" id="btnSubmit" class="btn btn-brand">Submit Registration</button>

											</div>
										</div>
									</div>
								</div>
							</form>

</div>
</div>
</div>

<script>
	function fAccept() { 
		$("#termsCk").removeAttr("disabled");
		$("#termsCk").attr("checked",true);

	 }

$ (function ($) {
	'use strict';
	$(".m-select2").select2();
$("#terms").click(function (e) { 
	e.preventDefault();
	$("#termsModal").modal({ backdrop: "static", keyboard: false });
	$("#termsModal").modal("show");

});

	$("#email").change(function (e) { 
		e.preventDefault();
		$.ajax({
			type: "get",
			url: "<?= base_url() ?>Buyer/check_email_exist",
			data: {email: $(this).val()},
			dataType: "json",
			success: function (response) {
				if(response.status){
					$("#email").val("");
					$("#email").focus();
					swal({
                                title: "Warning",
                                text: "EMAIL IS EXIST, PLEASE LOG IN OR RESET PASSWORD",
                                type: "warning"
							});
					
					
				}
			}
		});
	});

	
});
</script>

<script>
	var FormControls= {
    init:function() {
        $("#buyerForm").validate( {
            rules: {
                email: {
                    required: !0, email: !0, minlength: 10
                } 
            }
            , invalidHandler:function(e, r) {
                //$("#m_form_1_msg").removeClass("m--hide").show(), mUtil.scrollTop()
            }
            , submitHandler:function(e) {
				//$("#sellerForm").submit();
				//console.log(e);
				$(form).submit();
			}
        }
        )
    }
}

;
jQuery(document).ready(function() {
    FormControls.init()
}

);



function fsubmit() {
	

	if(grecaptcha.getResponse() == "") {
		//e.preventDefault();
		swal({
            title: "Warning",
            text: "Captcha is empty",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        })
		return false;
	} else {
		swal({
            title: "CONFIRM",
            text: "Are you sure to submit ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            //console.log(e);
            if (e.value) {
                $("#btnSubmit").click()
            }
        })
		//return true;
	}

	

}

</script>
