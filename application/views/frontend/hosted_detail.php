<?php

$this->db->where('product_id', $detail['id']);
$this->db->where('user_id', $this->session->userdata('uid'));
$row = $this->db->get('hosted_tour_request');


$this->db->where('user_id', $this->session->userdata('uid'));
$this->db->where('status', 1);

$approve = $this->db->get('hosted_tour_request');

?>
<section>
    <hr class="no-margin-bottom">
    <div class="padding-25px-lr">
	</div>
</section>
<section class="no-padding wow fadeIn bg-light-gray" style="visibility: visible; animation-name: fadeIn;">
    <div class="container-fluid padding-30px-lr">
        <div class="row equalize sm-equalize-auto">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 cover-background sm-height-500px xs-height-350px wow fadeInLeft" style="background-image: url(&quot;<?= base_url() ?>files/product/<?= $detail['image'] ?>&quot;); visibility: visible; animation-name: fadeInLeft; height: 535px;"><div class="xs-height-400px"></div></div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 wow fadeInRight" style="visibility: visible; animation-name: fadeInRight; height: 535px;">
                <div class="padding-ten-lr padding-three-tb pull-left md-padding-ten-all sm-no-padding-lr xs-padding-50px-tb xs-no-padding-lr">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-four-bottom sm-margin-40px-bottom xs-margin-30px-bottom xs-no-padding-lr">
                        <h2 class="alt-font text-extra-dark-gray sm-text-center sm-width-70 sm-margin-lr-auto xs-width-100 text-uppercase font-weight-700 sm-no-margin-bottom"><?= $detail['product_name'] ?></h2>
                    </div>
                    <div class="col-2-nth xs-text-center">
                        <!-- start feature box item-->
                        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12 margin-six-bottom padding-20px-right md-no-padding-right md-margin-30px-bottom xs-margin-30px-bottom xs-no-padding last-paragraph-no-margin">
                            <p class="width-90 xs-width-100"><?= $detail['product_desc'] ?></p>
						</div>
						<?php if($this->session->userdata('logged_in') && ($this->session->userdata('gid') == 4)) : ?>
						<div class="col-lg-12 col-md-12 col-sm-6 col-xs-12 margin-six-bottom padding-20px-right md-no-padding-right md-margin-30px-bottom xs-margin-30px-bottom xs-no-padding last-paragraph-no-margin">
							<!-- <form action="<?= base_url() ?>Admin/HostedTours/sendRequest" method="POST"> -->
							<input type="hidden" id="id" name="id" value="<?= $detail['id'] ?>">
							<input type="hidden" id="user_id" name="user_id" value="<?= $this->session->userdata('uid') ?>">
							<?php if($row->num_rows() > 0) : ?>
								<button type="button" class="btn btn-success" > Request has sent</button>
							<?php else: ?>
									<?php if($approve->num_rows() > 0) : ?>
										
									<?php else: ?>
										<button type="button" onclick="submitForm()" class="btn btn-warning" > Send Request</button>
									<?php endif; ?>
								
							<?php endif; ?>

							<!-- </form> -->
						</div>
						<?php else: ?>
							<div class="col-lg-12 col-md-12 col-sm-6 col-xs-12 margin-six-bottom padding-20px-right md-no-padding-right md-margin-30px-bottom xs-margin-30px-bottom xs-no-padding last-paragraph-no-margin">
							<button type="button" onclick="directLogin()" class="btn btn-warning" >Please login, to your member id to send request</button>
							<!-- </form> -->
						</div>
						<?php endif; ?>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</section>
<section>
    <hr class="no-margin-bottom">
    <div class="padding-25px-lr">
	</div>
</section>
<script>
function submitForm(){
	var product_id = $('#id').val();
	var user_id = $('#user_id').val();
	
	$.ajax({
		type: "post",
		url: "<?= base_url() ?>HostedTours/sendRequest",
		data: {product_id:product_id, user_id:user_id},
		dataType: "json",
		success: function (response) {
			if(response.result == 1) {
				alert("Request Has Send")
				location.reload();

			} else {
				alert("ups, something wrong! request Failed")
			}
		}
	});
}

function directLogin(){
	window.open("<?= base_url() ?>login.html","_self");
}
</script>
