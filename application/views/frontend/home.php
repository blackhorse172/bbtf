
<?php 
$banner = $this->db->get_where('banner', array('status'=>1))->row();
$slider = $this->db->get_where('slider', array('status'=>1))->result_array();
$sponsor1 = $this->db->get_where('sponsor', array('status'=>1,'position'=>1))->result_array();
$sponsor2 = $this->db->get_where('sponsor', array('status'=>1,'position'=>2))->result_array();




?>

<!-- ---------------------------------------------
					Banner
---------------------------------------------- --> 
    <!-- Modal -->
	<div class="modal fade" id="loginMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header more-close">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="modalMenu">
          <ul>
              <li><a href="seller-registration.html"><img src="<?= base_url() ?>assets/frontend/images/icons/ico1.png">SELLER REGISTRATION</a></li>
              <li><a href="buyer-registration.html"><img src="<?= base_url() ?>assets/frontend/images/icons/ico2.png">BUYER REGISTRATION</a></li>
              <li><a href="media-registration.html"><img src="<?= base_url() ?>assets/frontend/images/icons/ico7.png">MEDIA REGISTRATION</a></li>
              <li><a href="trade-buyer-registration.html"><img src="<?= base_url() ?>assets/frontend/images/icons/ico8.png">TRADE BUYER REGISTRATION</a></li>
          </ul>
        </div>
        
      </div>
    </div>
  </div>
<?php 

$aboutColor = $this->db->get_where('general_settings',array('gs_code'=>'about_us_color'))->row()->gs_value;
$schedule_color = $this->db->get_where('general_settings',array('gs_code'=>'schedule_color'))->row()->gs_value;
$news_color = $this->db->get_where('general_settings',array('gs_code'=>'news_color'))->row()->gs_value;
$hosted_tour_color = $this->db->get_where('general_settings',array('gs_code'=>'hosted_tour_color'))->row()->gs_value;
$video_color = $this->db->get_where('general_settings',array('gs_code'=>'video_color'))->row()->gs_value;
$maps_color = $this->db->get_where('general_settings',array('gs_code'=>'maps_color'))->row()->gs_value;
$register_color = $this->db->get_where('general_settings',array('gs_code'=>'register_color'))->row()->gs_value;
$login_color = $this->db->get_where('general_settings',array('gs_code'=>'login_color'))->row()->gs_value;

?>
  <section id="banner" style="margin-top: -10px;"> 
   <ul class="container-fluid"> 
    <li style="background-image: url('<?= base_url() ?>files/banner/<?= $banner->image; ?>'); background-size: cover; border: 1px solid #4d3a21;" class=""><a href="#"></a></li> 
	<li style="background: <?= $aboutColor ?>"><a href="about-us.html"><img src="<?= base_url() ?>assets/frontend/images/icons/ico10.png"> <p>ABOUT US</p> </a></li>
    <li style="background: <?= $schedule_color ?>" class="half"><a href="schedule.html"><img src="<?= base_url() ?>assets/frontend/images/icons/ico3.png"><p>SCHEDULE<br>THE DATE</p></a></li>
    <li style="background: <?= $news_color ?>" class="half"><a href="#"><img src="<?= base_url() ?>assets/frontend/images/icons/ico4.png"> <p>HOSTED<br>TOUR</p> </a></li> 
    <li style="background: <?= $hosted_tour_color ?>" class="half"><a href="news.html"><img src="<?= base_url() ?>assets/frontend/images/icons/ico5.png"> <p>NEWS<br>UPDATE</p> </a></li> 
    <li style="background: <?= $video_color ?>" class="half"><a href="video.html"><img src="<?= base_url() ?>assets/frontend/images/icons/ico6.png"> <p>OFFICIAL<br>VIDEO</p> </a></li> 
    <li style="background: <?= $maps_color ?>" class=""><a href="map.html"><img src="<?= base_url() ?>assets/frontend/images/icons/ico11.png"> <p>MAPS</p> </a></li> 
    <li style="background: <?= $register_color ?>"><a href="#" data-toggle="modal" data-target="#loginMenu"><img src="<?= base_url() ?>assets/frontend/images/icons/ico9.png"> <p>REGISTER</p> </a></li> 
    <li style="background: <?= $login_color ?>"><a href="login.html"><img src="<?= base_url() ?>assets/frontend/images/icons/ico12.png"> <p>LOGIN</p> </a></li>  
	<li class="lazy slider" style="overflow: hidden">
	<?php if(count($slider) > 0) : ?>
	<?php foreach ($slider as $key => $value) :?>
		<div>
            <img src="<?= base_url() ?>files/slider/<?= $value['image'] ?>">
            <div class="slider-text">
                
            </div>
        </div>
	<?php endforeach; ?>
	<?php endif; ?>

        <!-- <div>
            <img src="<?= base_url() ?>assets/frontend/images/tour1.jpg">
            <div class="slider-text">
                <h3>BANGLI TOUR</h3>
                
            </div>
        </div>
        <div>
            <img src="<?= base_url() ?>assets/frontend/images/tour2.jpg">
            <div class="slider-text">
                <h3>KARANGASEM TOUR</h3>
                
              </div>
        </div>
        <div>
            <img src="<?= base_url() ?>assets/frontend/images/tour3.jpg">
            <div class="slider-text">
                <h3>KOMODO TOUR</h3>
                
              </div>
        </div> -->
    </li> 
   </ul> 
  </section>
  <section class="sm-no-padding-top md-no-padding-top padding-60px-top">
    <div class="container-fluid">
	<div class="row padding-10px-lr">
          
          <!-- start portfolio section -->
              <div class="col-md-4 text-left sm-text-center">
                <h2 class="no-margin-top">GALLERY</h2>
			  </div>
	</div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- start filter navigation -->
                <ul class="portfolio-filter nav nav-tabs border-none portfolio-filter-tab-1 font-weight-600 alt-font text-uppercase text-center text-small margin-15px-top" style="margin-bottom: 22px;">
					<?php if(count($categories) >0) : ?>
					<?php foreach ($categories as $key => $v) : ?>
						<li class="nav"><a href="javascript:void(0);" onclick="changeGallery(<?= $v['category_id'] ?>)" data-filter=".<?= $v['category_id'] ?>" class="xs-display-inline light-gray-text-link text-very-small"><?= $v['category_name'] ?></a></li>
					<?php endforeach; ?>
					<?php endif; ?>
					
                </ul>                                                                           
                <!-- end filter navigation -->
            </div>
        </div>
    </div>
    <!-- start filter content -->
    <div class="container-fluid">
        <div class="row lightbox-gallery padding-10px-lr">
             <div class="col-md-12 px-3 p-md-0">
                <div class="filter-content overflow-hidden">
				<ul class="portfolio-grid work-5col hover-option4 gutter-medium">
						<li class="grid-sizer"></li>
						
						<?php if(count($gallery) > 0) : ?>
						<?php foreach ($gallery as $key => $v) : ?>
<!-- start portfolio-item item -->

<li class="grid-item <?= $v->image ?> wow fadeInUp">
                            <a href="<?= base_url().'files/gallery/'.$v->image ?>" title="<?= $v->title ?>">
                                <figure>
								<div class="portfolio-img bg-gold"><img src="<?= base_url().'files/gallery/'.$v->image ?>" class="project-img-gallery" /></div>
                                    <figcaption>
                                        <div class="portfolio-hover-main text-center">
                                            <div class="portfolio-hover-box vertical-align-middle">
                                                <div class="portfolio-hover-content position-relative">
                                                    <span class="font-weight-600 letter-spacing-1 alt-font text-uppercase margin-5px-bottom display-block"><?= $v->descr ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                            </a>
                        </li>
                        <!-- end portfolio item -->
						<?php endforeach;?>
					
						<?php endif; ?>
                        
                        
                    </ul>
                </div>
            </div>
		</div>
		<div class="col-12 text-right wow fadeInUp margin-20px-top">
              <a class="btn btn-lg btn-orange" href="photo-gallery.html">View More</a>
          </div>
	</div>

</section>

<?php 
$this->db->order_by('id', 'asc');
$suported = $this->db->get_where('suported',array('status'=>1))->result_array();

?>
<section style="padding: 10px 0;">
  <div class="container-fluid">
    <div class="row">
    <div class="col-md-12 text-center" style="margin-bottom: 30px;">
          <h2>
            SUPPORTED BY</h2>
	  </div>
		</div>
	</div>
</section>
<?php if(count($suported) > 0) : ?>
	
<section class="customer-logos slider" style="padding: 20px 0; padding-left:62px;padding-right:62px;">
<?php foreach ($suported as $key => $v) : ?>
      <div class="slide"><img style="max-width:50%;
  max-height:50%;
  width: auto;
  height: auto;" src="<?= base_url() ?>files/suported/<?= $v['image'] ?>"></div>
			<?php endforeach;?>
	 </section>
	 <?php endif; ?>

	 <section style="padding: 10px 0;">
  <div class="container-fluid">
    <div class="row">
    <div class="col-md-12 text-center" style="margin-bottom: 30px;">
          <h2>
					SPONSORED BY</h2>
	  </div>
		</div>
	</div>
</section>
<section class="customer-logos1 slider" style="padding-left:25%">
<?php foreach ($sponsor1 as $key => $v) : ?>
      <div class="slide"><a href="http:\\<?= $v['link'] ?>" target="_blank"><img src="<?= base_url() ?>files/sponsor/<?= $v['image'] ?>"></a></div>
			<?php endforeach;?>
	 </section>

	 <section class="customer-logos2 slider" style="padding-left:25%">
<?php foreach ($sponsor2 as $key => $v) : ?>
	
      <div class="slide"><a href="http:\\<?= $v['link'] ?>" target="_blank"><img src="<?= base_url() ?>files/sponsor/<?= $v['image'] ?>"></a></div>
			<?php endforeach;?>
	
	 </section>

<!--Sponsors-->
<section style="padding: 10px 0;">
  <div class="container-fluid">
    <div class="row">
      <!-- <div class="col-md-12 text-center" style="margin-bottom: 30px;">
          <h2>
            SUPPORTED BY</h2>
	  </div>
	  <?php if(count($suported) > 0) : ?>

      <div class="col-lg-3">

      </div>
      <div class="col-lg-6 col-md-12 text-center">
		<?php foreach ($suported as $key => $v) : ?>
			<div class="col-lg-3 col-md-12">
            <img style="display: block;max-width:150px;max-height:150px;width: auto;height: auto;" width="150px" src="<?= base_url() ?>files/suported/<?= $v['image'] ?>" class="w-100 sm-margin-auto sm-margin-30px-bottom md-margin-auto md-w-30 sm-w-70">
			</div>
			<?php endforeach;?>
		
	  </div> -->
      <div class="col-lg-3">

	  <!-- sponsor -->
	  </div>
		<?php endif; ?>
	  
      <!-- <div class="col-md-12 text-center margin-30px-bottom margin-70px-top">
          <h4>
            SPONSORED BY</h4>
	  </div> -->
	  <!-- <div class="col-lg-12">
	  <?php if(count($sponsor1) > 0) : ?>
			<?php
				$offset = "";
				foreach ($sponsor1 as $key => $v) : 
				$first = ($key==0) ? 'col-md-6 col-lg-offset-2' : '';
				?>
				<div class="col-md-3">
					<a target="_blank" href="http://<?= $v['link'] ?>"><img src="<?= base_url() ?>files/sponsor/<?= $v['image'] ?>" style="display: block;max-width:250px;max-height:250px;width: auto;height: auto;padding-left: 50px;" class="sm-w-100 ipad-w-90 sm-w-100 padding-50px-bottom"></a>
					</div>
	  		<?php endforeach;?>
		<?php else: ?>
		<p> NO DATA </p>
		<?php endif; ?>
		</div> -->
		<!-- <?php if(count($sponsor2) > 0) : ?>
			<?php foreach ($sponsor2 as $key => $v) :
			$first = ($key==0) ? 'col-md-6 col-lg-offset-2' : '';
			?>
          <div class="col-md-2 <?= $first ?>"><a target="_blank" href="http://<?= $v['link'] ?>"><img src="<?= base_url() ?>files/sponsor/<?= $v['image'] ?>" class="sm-margin-auto sm-margin-30px-bottom w-30 md-w-20 md-margin-auto md-margin-30px-bottom"></a></div>
	  		<?php endforeach;?>
		<?php endif; ?> -->
        
         

    </div>
  </div>
</section>

<?php 
$this->db->limit(4);
$this->db->order_by('dtm_crt', 'desc');

$testimonies = $this->db->get_where('testimony', array('is_active'=>1,"show_home"=>1))->result_array();
?>

<?php if(count($testimonies) > 0) : ?>
	<section style="margin-top: 0px; padding-top: 30px;" id="news" class="sm-no-padding">
    <div class="container-fluid padding-0px-top padding-10px-lr">
      <div class="row padding-30px-tb padding-20px-lr bg-gray">
        <div class="col-lg-12 text-left sm-text-center margin-30px-bottom">
          <h2>TESTIMONIAL</h2>
        </div>
        <div class="col-md-12">
          <div class="testimony slider">

				<?php foreach ($testimonies as $key => $value) : ?>
  <!--Start Testimonial-->
  <div class="col-md-3 sm-margin-two-bottom testimonial-style3 last-paragraph-no-margin wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
          <div class="testimonial-content-box padding-four-all bg-white border-radius-6 box-shadow arrow-bottom sm-padding-eight-all">
              "<?= $value['content'] ?>"
          </div>
          <div class="testimonial-box padding-25px-all xs-padding-20px-all">
              <div class="name-box padding-20px-left">
                  <div class="alt-font font-weight-600 text-small text-uppercase text-extra-dark-gray"><?= $value['name'] ?></div>
                  <p class="text-extra-small text-uppercase text-medium-gray"><?= $value['company_name'] ?></p>
              </div>
          </div>
      </div>
        <!-- End Testimonial-->
				<?php endforeach;?>

				</div>
			</div>
	  <div class="col-12 text-right wow fadeInUp margin-20px-top">
              <a class="btn btn-lg btn-orange" href="testimonies.html">View More</a>
          </div>
      </div>
    </div>
  </section>
			<?php endif; ?>


      
<!--News-->
<?php
$this->db->order_by('created', 'desc');
$this->db->limit(8);
$data = $this->db->get_where('tb_blogs', array("status"=>"publish"))->result_array();
function limit_text($text, $limit) {
	if (str_word_count($text, 0) > $limit) {
		$words = str_word_count($text, 2);
		$pos = array_keys($words);
		$text = substr($text, 0, $pos[$limit]) . '...';
	}
	return $text;
  }
?>
<section style="margin-top: 0px; padding-top: 30px;" id="news" class="sm-no-padding">
    <div class="container-fluid -30px-top padding-10px-lr">
      <div class="row padding-20px-lr bg-gray">
        <div class="padding-30px-top col-lg-12 text-left sm-text-center" style="margin-bottom: 30px;">
          <h2>NEWS UPDATE</h2>
        </div>
        <div class="col-md-12">
          <div class="regular slider">  
            
		  <?php if(count($data) > 0) : ?>
				<?php foreach ($data as $key => $value) : ?>
<!-- start post item -->
<div class="col-md-3 col-sm-6 col-xs-12 margin-50px-bottom last-paragraph-no-margin xs-margin-30px-bottom wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
							<div class="blog-post blog-post-style1 xs-text-center">
                    <div class="blog-post-images overflow-hidden margin-25px-bottom sm-margin-20px-bottom">
                        <a href="<?= base_url() ?>news.html/<?= $value['slug']?>">
							<?php if($value['image'] == '' || $value['image'] == null) : ?>
								<img style="height: 222px;" src="http://placehold.it/1200x840" alt="" data-no-retina="">
							<?php else: ?>
								<img style="height: 222px;"  src="<?= base_url() ?>files/blog/title_image/<?= $value['image'] ?>" alt="" data-no-retina="">
							<?php endif; ?>

                        </a>
                    </div>
                    <div class="post-details">
                        <a href="<?= base_url() ?>news.html/<?= $value['slug']?>" class="post-title text-medium text-extra-dark-gray width-90 display-block margin-20px-bottom sm-width-100">
                            <?= $value['title']; ?></a>
                        <span class="post-author text-extra-small text-medium-gray text-uppercase display-block margin-10px-bottom xs-margin-5px-bottom"><?= $value['created'] ?></span>
                        <div class="separator-line-horrizontal-full bg-medium-light-gray margin-20px-tb sm-margin-15px-tb"></div>
                        <p class="width-90 xs-width-100"><?=  limit_text($value['content_text'],20) ?></p>
                    </div>
                </div>
            </div>
            <!-- end post item -->
				<?php endforeach;?>
			<?php endif; ?>

          </div>
        </div>
        

        

      </div>
    </div>
</section>

<!--Tour-->
<?php 
$this->db->where('category_id', 4);
$this->db->where('active', 1);
$this->db->order_by('dtm_upd', 'desc');
$res = $this->db->get('product')->result_array();

// function limit_text($text, $limit) {
// 	if (str_word_count($text, 0) > $limit) {
// 		$words = str_word_count($text, 2);
// 		$pos = array_keys($words);
// 		$text = substr($text, 0, $pos[$limit]) . '...';
// 	}
// 	return $text;
//   }
?>
 <?php if(count($res) > 0) :?>
<section class="md-no-padding-tb padding-30px-top sm-padding-30px-top">
    <div class="padding-25px-lr">
        <div class="row">
          <div class="col-lg-12 text-center" style="margin-bottom: 50px;">
              <h2>
                HOSTED TOUR</h2>
          </div>
          <div class="container-fluid">
		  <?php foreach ($res as $key => $value) :?>
				<div class="col-md-3 col-sm-6 col-xs-12 team-block text-left no-padding-right feature-box-15 last-paragraph-no-margin wow fadeInUp ">
                    <a href="<?= base_url() ?>hosted-tour-detail.html/<?= $value['id'] ?>">
                    <figure>
                        <div class="feature-box-content sm-width-100">
                            <div class="feature-box-image"><img src="<?= base_url() ?>files/product/<?= $value['image'] ?>" alt="" data-no-retina="" class="w-100" style="height: 265px;"></div>
                            <div class="hover-content bg-orange d-flex justify-content-center align-items-center">
                                <div class="padding-twelve-lr">
                                    <span class="text-white d-inline-block text-center"><?= $value['product_name']?></span>
                                </div>
                            </div> 
                        </div>
                        <figcaption>
                            <div class="text-center margin-25px-top">
                                <div class="text-medium alt-font font-weight-600 text-uppercase"><span><?= $value['product_name'] ?></span></div> 
                            </div>   
                        </figcaption>
                    </figure>
                    </a>
                </div>
			  <?php endforeach; ?>
			</div>

        </div>
      </div>
</section>
<?php endif;?>

<?php
$this->db->order_by('dtm_crt', 'desc');
$this->db->limit(4);
$tours = $this->db->get_where('official_partner', array('is_active'=>1))->result_array();
?>
<?php if(count($tours) > 0) : ?>
<!--Commercial Tour-->
<section class="padding-90px-bottom sm-padding-30px-tb" id="news">
  <hr class="no-padding">
    <div class="container-fluid margin-30px-top">
        <div class="row">
          <div class="col-lg-12 text-left sm-padding-20px-lr">
              <h2 style="line-height: 1;">
                OFFICIAL PARTNER</h2>
                <!-- <p style="line-height: 2;"> dolor gravida. Maecenas odio purus, euismod eu mauris quis, pharetra molestie nibh. Nulla a lorem tempor...</p> -->
          </div>
          
          <div class="col-md-12">
              <div class="comm slider">
				<?php foreach ($tours as $key => $value) : ?>
					<!-- start team item -->
					<div class="swiper-slide team-block text-left team-style-1 lg-margin-40px-bottom wow fadeInUp swiper-slide-active" style="width: 292.5px; visibility: visible; animation-name: fadeInUp;">
                    <figure>
                            <div class="grid-item col-12 col-md-12 col-lg-12 md-margin-30px-bottom text-center text-md-left wow fadeInUp" style="visibility: visible; animation-name: fadeInUp; padding: 0 5px">
                                    <div class="blog-post bg-white">
                                        <div class="blog-post-images overflow-hidden position-relative">
                                            <a href="">
												<?php if($value['image'] == "" || $value['image']== null ) : ?>
													<img style="height: 250px;" src="http://placehold.it/250x2	50" alt="" data-no-retina="">
												<?php else: ?>
													<img style="height: 250px;" src="<?= base_url() ?>files/partner/<?= $value['image'] ?>" alt="" data-no-retina="">
												<?php endif; ?> 
                                            </a>
                                        </div>
                                        <div class="post-details padding-15px-all md-padding-20px-all card">
                                            <a href="<?=  base_url() ?>Partner/detail/<?= $value['id'] ?>" class="alt-font post-title text-large text-link-gold w-100 d-block lg-width-100 margin-15px-bottom" style="font-weight: 700;"><?= $value['title'] ?></a>
                                            <span class="d-block margin-20px-top"><span class="glyphicon glyphicon-map-marker"></span> <?= $value['location'] ?></span>
                                            <div class="separator-line-horrizontal-full bg-medium-gray margin-20px-tb"></div>
                                            <a href="<?=  base_url() ?>Partner/detail/<?= $value['id'] ?>" class="alt-font post-title text-large text-link-gold w-100 d-block lg-width-100 margin-15px-bottom" style="font-weight: 700;">IDR <?= number_format($value['price'],2)  ?></a>
                                            <div class="author">
                                                <a class="btn btn-medium btn-orange lg-margin-15px-bottom margin-10px-top d-table d-lg-inline-block md-margin-lr-auto" href="<?=  base_url() ?>Partner/detail/<?= $value['id'] ?>">Details</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </figure>
                </div>
                <!-- end team item -->
					<?php endforeach;?>
				<?php endif; ?>
                  
              </div>
          </div>
        </div>
    </div>
</section>
<p></p>
<script>
	$(document).ready(function(){
    $('.customer-logos').slick({
				//slidesToShow: 6,
				centerMode: true,
				centerPadding: '60px',
				slidesToShow: 3,
        //slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 3
            }
        }]
		});
		
		$('.customer-logos1').slick({
        slidesToShow: 6,
        //centerMode: true,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 3
            }
        }]
		});
		
		$('.customer-logos2').slick({
        slidesToShow: 10,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 3
            }
        }]
    });
});

function changeGallery(catId)
{
	$.ajax({
		type: "post",
		url: "<?= base_url() ?>Page/json_getByCatId",
		data: {catId : catId },
		dataType: "html",
		success: function (response) {
		
			//$('.lightbox-gallery').html("");
			// /new WOW().init();
		
    	    $('.lightbox-gallery').html(response);
			//$("#galleryRow").text(response);
			var $portfolio_filter = $('.portfolio-grid');
$portfolio_filter.imagesLoaded(function() {
    $portfolio_filter.isotope({
        layoutMode: 'masonry',
        itemSelector: '.grid-item',
        percentPosition: true,
        masonry: {
            columnWidth: '.grid-sizer'
        }
    });
    $portfolio_filter.isotope();
});
		}
	});
}

</script>
