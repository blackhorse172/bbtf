<!--Content-->				
<section id="inner" class="padding-80px-bottom">
    <div class="container-fluid">
		<?php if($sub_image != '' || $sub_image != null) : ?>
			<div id="banner" class="banner" style="background:url('<?=base_url() ?>files/sub_header/<?= $sub_image ?>')"> 
            <h1>Buyer Information</h1>
		</div>
		<?php else: ?>
			<div id="banner" class="banner" style="background:gray"> 
			<h1>Buyer Information</h1>
			</div>
		<?php endif;?>
	
		<div>
		<div class="container-fluid">
<h1 class="title" id="page-title" style="font-size: 18px; text-align: center; color: orange; margin: 20px auto; display: table; font-family: &quot;Open Sans&quot;;"><div class="inner" style="max-width: 1100px; width: 167.2px; margin: auto; display: table; position: relative;"></div></h1><div class="region region-page" style="color: rgb(86, 73, 66); font-family: &quot;Open Sans&quot;; font-size: 14px;"><div id="block-system-main" class="block block-system"><div class="content"><div id="node-8" class="node node-page clearfix"><div class="content clearfix"><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><ul><li><p><strong>Event and Organizer</strong></p><p>BBTF event&nbsp;is organized by ASITA BALI Chapter which the event will be held in Nusa Dua,Bali&nbsp;for B2B session</p></li><li><p><strong>BBTF 2020 Buyer Profile:</strong></p><p>All registrants must undergo accreditation before confirmation as a&nbsp;Buyer Participants</p><ul><li>Travel wholesalers</li><li>Retailers and agents</li><li>Convention and incentive organizers</li><li>Other Tourism related</li></ul></li></ul><ul><li><strong>Buyer Mandatory:</strong></li><li><ul><li>Partially Hosted Buyers must fulfilling 80% appointments Travex ( 26 appointments)</li><li>Fully Hosted Buyers must fulfilling 100% appointments Travex ( 32&nbsp;appointments)</li><li>An administrative fee of IDR.2,900,000&nbsp;for Fully Hosted Buyer&nbsp;&amp; IDR. 2,175,000&nbsp;for Partially Hosted Buyer and NON REFUNDABLE</li><li>Payment by credit card will added a surcharge 4 % (master card &amp; visa logo only)</li></ul></li><li><p><strong>Hosted Buyer Privileges</strong></p><ul><li>Fully Hosted&nbsp;Buyers receive:<ul><li>Reimbursement for economy class flight tickets (subject to a cap amount which will be advised)*&nbsp;</li><li>Free Admission to Travex</li><li>Entry to Welcome Diner Party, Luncheon, Farewell Cocktail Party and Tourism Seminar</li><li>Complimentary return airport transfer to official hotels at stipulated timing</li><li>Complimentary daily shuttles to/from official hotels, venue and official functions</li><li>Complimentary accommodation for 4 nights/5 days including&nbsp;daily&nbsp;breakfast</li><li>Special rates for post tours</li></ul></li><li>Partial Hosted Buyers&nbsp;receive:<ul><li>Free Admission to Travex</li><li>Entry to Welcome Diner Party, Luncheon, Farewell Cocktail Party and Tourism Seminar</li><li>Complimentary return airport transfer to official hotels at stipulated timing</li><li>Complimentary daily shuttles to/from official hotels, venue and official functions</li><li>Complimentary accommodation for 4 nights/5 days including&nbsp;daily&nbsp;breakfast</li><li>Special rates for post tours</li></ul></li><li><strong>Cancellation</strong><ul><li><strong>​NON REFUNDABLE administrative fee IDR.2,900,000&nbsp;for Fully Hosted Buyer &amp; IDR. 2,175,000&nbsp;for Partially Hosted Buyer applied before 3 months prior to events</strong></li><li><strong>NON REFUNDABLE administrative fee IDR.2,900,000&nbsp;for Fully Hosted Buyer &amp; IDR.&nbsp;2,175,000&nbsp;for Partially Hosted Buyer applied for NO SHOW BUYER</strong></li></ul></li></ul></li></ul><p><strong>​​&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<strong>Dress Code</strong></strong></p><ul><li><p>All Buyers must wear business attire during BBTF events at Nusa Dua, Bali</p></li></ul></div></div></div></div></div></div></div></div>
		</div>
		</div>
    </div>
</section>
<!--Content End-->
