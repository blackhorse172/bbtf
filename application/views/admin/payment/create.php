<?php

//$this->load->view('layouts/admin/metronic/devextreme');
$title = "Confirm Payment for ".$trxCode;
$action = base_url().'Admin/payments/save';
if($isEdit == true){
$action = base_url().'Admin/payments/save';
}
?>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
							<input type="hidden" name="trx_code" value="<?= $trxCode ?>"/>
								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Bank Name</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $data->bank_name ?>" name="bank_name" id="bank_name" aria-describedby="bank_name" placeholder="Bank Name" readonly>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Bank Account Number</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $data->bank_acc_number ?>" name="bank_acc" id="bank_acc" aria-describedby="bank_name" placeholder="Bank Account Number" readonly>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Bank Account Name</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $data->bank_acc_name ?>" name="bank_acc_name" id="bank_acc_name" aria-describedby="bank_name" placeholder="Bank Account Name" readonly>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Notes</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" class="form-control m-input" value="<?= $data->notes ?>"name="notes" id="notes" aria-describedby="notes" placeholder="Notes" readonly>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Image</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
										<?php if($isEdit == true) : ?>
												<?php if($data->image != '' || $data->image != null): ?>
												<img width="600px" id="image" class="img-responsive" src="<?= base_url() ?>files/payment/<?= $data->image ?>">
												<?php endif ?>
											<?php endif ?>
										</div>
									</div>
								
									
									
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Confirm</button>
												<button type="button" onclick="window.location.href = '<?= base_url() ?>Admin/Transactions'" class="btn btn-secondary">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>

<?php if($isEdit == true) : ?>
<script>
$(document).ready(function(){
	
});
</script>
<?php endif ?>
<script>

$(document).ready(function () {
	$(".mask_number").inputmask({
                      'alias': 'numeric',
                      'groupSeparator': '.',
                    'radixPoint': ',',
                      'digits': 0,
                      'autoGroup': true
				});
				
});
</script>
