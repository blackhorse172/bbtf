
<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "List Transaction Doku";

?>

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= $title ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <!-- <li class="m-portlet__nav-item">
                    <a href="<?= site_url()?>Transaction/Create" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Create</span>
                        </span>
                    </a>
                </li> -->
                <li class="m-portlet__nav-item"></li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <div id="gridContainer"></div>
    </div>
</div>
<div id="boothModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<p>Update Booth Number</p>
				<form method="post" action="<?= base_url() ?>Admin/Transactions/UpdateBooth">
					<div class="form-check form-check-inline">
						<input id="detail_id" class="form-check-input" type="hidden" name="detail_id" value="">
						<input id="booth_number" class="form-check-input" type="text" name="booth_number" value="">
						<button type="submit">Update</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
    (function ($) {
        var dataSource = {
            load: function () {
                var items = $.Deferred();
                var data = <?= $data; ?>;
                items.resolve(data);
                return items.promise();
            }
        };
        $("#gridContainer").dxDataGrid({
            dataSource: dataSource,
            showBorders: true,
            showRowLines: true,
            columnAutoWidth: true,
            allowColumnResizing: true,
            allowColumnReordering: true,
            filterRow: {
                visible: true,
                applyFilter: "auto"
            },
            headerFilter: {
                visible: true
            },
            paging: {
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20],
                showInfo: true
            },
            columns: [
                {
                    caption: "Transaction Code",
                    dataField: "transidmerchant",
				},

				{
                    caption: "Amount",
                    dataField: "totalamount",
					dataType: "number",
                	format: "#,##0.##"
				},
				
				{
                    caption: "Transaction Date",
					dataField: "payment_date_time",
					dataType: "date"
				},
				{
                    caption: "User Type",
					dataField: "user_type",
					
				},
				

				{
                    caption: "Transaction Status",
					dataField: "trxstatus",
					cellTemplate: function (container, options) {
						//console.log(options);
						var id = options.key.id;
                        var trx_code = options.key.trxcode;
						
                        var status = options.key.trxstatus;
                        if (status != null) {
                            $("<div>")
							.append($("<span class='m-badge m-badge--success m-badge--wide m-badge--rounded'>"+status+"</span>"))
                                .appendTo(container);
                        } else if(status == null) {
                            
                            $("<div>")
                                .append($("<span class='m-badge m-badge--danger m-badge--wide m-badge--rounded'>Requested</span>"))
                                .appendTo(container);
						}
						else if(status == 2) {
                            
                            $("<div>")
                                .append($("<button type='button'  class='btn m-btn--pill btn-success'>PAID</button>"))
                                .appendTo(container);
                        } 

                    }
				},

				{
                    caption: "Action",
					dataField: "trxstatus",
					cellTemplate: function (container, options) {
						//console.log(options);
						var userId = options.key.ref_user_id;
                        var trx = options.key.transidmerchant;
                        var type = options.key.user_type;

                        var is_send = options.key.is_send;
						
						
                        var status = options.key.trxstatus;
						

                        if (status == 0) {
                            $("<div>")
                                .append($("-"))
                                .appendTo(container);
                        } else if(status == "SUCCESS" && is_send != 1) {
                            
                            $("<div>")
                                .append($("<button type='button'onclick='fsendPaid(\""+userId+"\",\""+type+"\",\""+trx+"\")' class='btn m-btn--pill btn-warning'>Send Paid Confirmation</button>"))
                                .appendTo(container);
						} else if(status == "SUCCESS" && is_send == 1) {
                            
                            $("<div>")
                                .append($("<button type='button' class='btn m-btn--pill btn-success'>Sent</button>"))
                                .appendTo(container);
                        }

                    }
				},
                

			],
			masterDetail: {
                                enabled: true,
                                template: function(container, options) { 
                                    var masterData = options.data;
                                    $("<div>")
                                        .addClass("show-detail")
                                        .text("Loading....")
                                        .appendTo(container);
                                    var trx_detail = "<?php echo base_url() ?>Buyer/getProfile"
                                    $.ajax({
                                      url: trx_detail,
                                      type: 'post',
                                      dataType: 'json',
                                      data: {buyerId: masterData.ref_user_id,type: masterData.user_type },
                                    })
                                    .done(function(result) {
                                      data_detail = result;
                                      $("<div>")
                                        .addClass("master-detail-caption")
                                        .text("User Detail")
                                        .appendTo(container);
                                    $("<div>")
                                        .dxDataGrid({
                                            columnAutoWidth: true,
                                            showBorders: true,
                                            dataSource: result
                                        }).appendTo(container);

                                        $('.show-detail').css("display","none");
                                    })
                                    .fail(function() {
                                      console.log("error");
                                    })
                                    .always(function() {
                                      
                                    });
                                }
                            }
        });
    })(jQuery);

    function fNonActive(id,title,msg) {
        swal({
            title: title,
            text: msg,
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Transaction/nonActive/" + id,"_self");
            }
        })
    }

	
	function fsendPaid(id,type,trx) {

		swal({
		    title: "SEND PAID CONFIRMATION",
		    text: "send the confirmation ?",
		    type: "warning",
		    showCancelButton: !0,
		    confirmButtonText: "Yes!"
		}).then(function (e) {
		    //e.value && swal("Deleted!", "Your file has been deleted.", "success")
		    //$("#FormTrxOrder").submit();
		    console.log(e);
		    if (e.value) {
		        window.open("<?= base_url() ?>Admin/Transactions/paidConfirm/" + id+"/"+type+"/"+trx,"_self");
		    }
		})
		}

    function flinkEdit(id) {
                window.open("<?= base_url() ?>Admin/Payments/confirm/" + id,"_self");

    }
</script>

