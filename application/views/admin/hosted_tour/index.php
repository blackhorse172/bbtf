
<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "List Hosted Tour Request";

?>

<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?= $title ?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <!-- <li class="m-portlet__nav-item">
                    <a href="<?= site_url()?>Transaction/Create" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-plus"></i>
                            <span>Create</span>
                        </span>
                    </a>
                </li> -->
                <li class="m-portlet__nav-item"></li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <div id="gridContainer"></div>
    </div>
</div>
<script>
    (function ($) {
        var dataSource = {
            load: function () {
                var items = $.Deferred();
                var data = <?= $data; ?>;
                items.resolve(data);
                return items.promise();
            }
        };
        $("#gridContainer").dxDataGrid({
            dataSource: dataSource,
            showBorders: true,
            showRowLines: true,
            columnAutoWidth: true,
            allowColumnResizing: true,
            allowColumnReordering: true,
            filterRow: {
                visible: true,
                applyFilter: "auto"
            },
            headerFilter: {
                visible: true
            },
            paging: {
                pageSize: 10
            },
            pager: {
                showPageSizeSelector: true,
                allowedPageSizes: [5, 10, 20],
                showInfo: true
            },
            columns: [
                {
                    caption: "Name",
                    dataField: "product_name",
				},
				{
                    caption: "Request BY",
					dataField: "first_name",
					cellTemplate: function (container, options) {
						console.log(options);
                        var id = options.key.ref_user;
                        var name = options.key.first_name;
                       
                            $("<div>")
                                .append($("<span><a href='<?= base_url() ?>Memberships/Buyer/"+id+"'>"+name+"</a></span>"))
                                .appendTo(container);
                     

                    }
				},
				{
                    caption: "User Type",
                    dataField: "group_name",
				},
				{
                    caption: "Request Date",
                    dataField: "date_req",
					dateType:'date'
				},
				{
                    caption: "Approve By",
                    dataField: "verify_by",
				},
				{
                    caption: "Status",
					dataField: "status",
					cellTemplate: function (container, options) {
						console.log(options);
                        var id = options.key.id;
                        var status = options.key.status;
                        if (status == 0) {
                            $("<div>")
                                .append($("<button type='button'  class='btn m-btn--pill btn-danger'>WAITING Approve</button>"))
                                .appendTo(container);
                        } else if(status == 1) {
                            
                            $("<div>")
                                .append($("<button type='button'  class='btn m-btn--pill btn-warning'>Approved</button>"))
                                .appendTo(container);
						}
						else if(status == 2) {
                            
                            $("<div>")
                                .append($("<button type='button'  class='btn m-btn--pill btn-danger'>REJECT</button>"))
                                .appendTo(container);
                        }

                    }
				},
				{
                    caption: "ACTION",
					cellTemplate: function (container, options) {
						console.log(options);
                        var id = options.key.id;
                        var status = options.key.status;
                        var approve = 1;
                        var reject = 2;
                        var cancel = 0;



                        if (status == 0) {
                            $("<div>")
                                .append($("<button type='button' onclick='approve(\""+id+"\",\""+approve+"\")'  class='btn m-btn--pill btn-success'>APPROVE</button>"))
                                .append($("<button type='button' onclick='reject(\""+id+"\",\""+reject+"\")'  class='btn m-btn--pill btn-danger'>REJECT</button>"))
                                
								.appendTo(container);
                        }

						if (status != 0) {
                            $("<div>")
                                .append($("<button type='button' onclick='cancel(\""+id+"\",\""+cancel+"\")'  class='btn m-btn--pill btn-success'>Cancel</button>"))
                                
								.appendTo(container);
                        } 
						// else if(status == 1) {
                            
                        //     $("<div>")
                        //         .append($("<button type='button' onclick='fNonActive(\""+id+"\")'  class='btn m-btn--pill btn-warning'>Approved</button>"))
                        //         .appendTo(container);
						// }
						// else if(status == 2) {
                            
                        //     $("<div>")
                        //         .append($("<button type='button'  class='btn m-btn--pill btn-success'>PAID</button>"))
                        //         .appendTo(container);
                        // }

                    }
				},
				

				
                

			],
			// masterDetail: {
            //                     enabled: true,
            //                     template: function(container, options) { 
            //                         var masterData = options.data;
            //                         $("<div>")
            //                             .addClass("show-detail")
            //                             .text("Loading....")
            //                             .appendTo(container);
            //                         var trx_detail = "<?php echo base_url() ?>transaction/json_getDetail"
            //                         $.ajax({
            //                           url: trx_detail,
            //                           type: 'post',
            //                           dataType: 'json',
            //                           data: {trx_code: masterData.trx_code,trx_type: masterData.trx_type },
            //                         })
            //                         .done(function(result) {
            //                           data_detail = result;
            //                           $("<div>")
            //                             .addClass("master-detail-caption")
            //                             .text("Transaction Detail")
            //                             .appendTo(container);
            //                         $("<div>")
            //                             .dxDataGrid({
            //                                 columnAutoWidth: true,
            //                                 showBorders: true,
            //                                 columns: [
            //                                 {
            //                                     caption: "Product Name",
            //                                     dataField: "product_name"
                                                
            //                                 },{
            //                                     caption: "Product Amount",
            //                                     dataField: "product_amount",
                                                
            //                                 },
            //                                 {
            //                                     caption: "Product Price",
			// 									dataField: "product_price",
			// 									dataType: "number",
            //                                      format: "Rp #,##0.##"
            //                                 }],
            //                                 dataSource: result
            //                             }).appendTo(container);

            //                             $('.show-detail').css("display","none");
            //                         })
            //                         .fail(function() {
            //                           console.log("error");
            //                         })
            //                         .always(function() {
                                      
            //                         });
            //                     }
            //                 }
        });
    })(jQuery);

    function approve(id,status,msg) {
        swal({
            title: "Approve",
            text: "this data approve ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            console.log(e);
            if (e.value) {
                window.open("<?= base_url() ?>Admin/HostedTours/updateStatus/" + id + "/" + status,"_self");
            }
        })
    }

	function reject(id,status,msg) {
        swal({
            title: "Reject",
            text: "this data reject ?",
            type: "danger",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            if (e.value) {
                window.open("<?= base_url() ?>Admin/HostedTours/updateStatus/" + id+ "/"+status,"_self");
            }
        })
    }

	function cancel(id,status,msg) {
        swal({
            title: "Cance",
            text: "this data Cancel ?",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes!"
        }).then(function (e) {
            //e.value && swal("Deleted!", "Your file has been deleted.", "success")
            //$("#FormTrxOrder").submit();
            if (e.value) {
                window.open("<?= base_url() ?>Admin/HostedTours/updateStatus/" + id+ "/"+status,"_self");
            }
        })
    }

    function flinkEdit(id) {
                window.open("<?= base_url() ?>Admin/HostedTours/edit/" + id,"_self");

    }
</script>

