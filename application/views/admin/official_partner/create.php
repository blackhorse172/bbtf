<?php
//$this->load->view('layouts/admin/metronic/devextreme');
$title = "Create Official Partner";
$action = base_url().'Admin/OfficialPartner/save';
if($isEdit == true){
$action = base_url().'Admin/OfficialPartner/saveEdit';
}
?>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
							<?php if($isEdit == true) : ?><input type="hidden" name="id" value="<?= $data->id ?>"/><?php endif ?>

								<div class="m-portlet__body">
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Title</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text"
											<?php if($isEdit == true) : ?>
												value="<?= $data->title; ?>"
											<?php endif ?>
											class="form-control m-input" id="title" name="title" placeholder="Enter your caption here" required>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Location</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" 
											<?php if($isEdit == true) : ?>
												value="<?= $data->location; ?>"
											<?php endif ?>
											class="form-control m-input" id="location" name="location" placeholder="Location" required>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Price</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="number" 
											<?php if($isEdit == true) : ?>
												value="<?= $data->price; ?>"
											<?php endif ?>
											class="form-control m-input" id="price" name="price" placeholder="price" required>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Tour Name</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text"
											<?php if($isEdit == true) : ?>
												value="<?= $data->tour_name; ?>"
											<?php endif ?>
											class="form-control m-input" id="tour_name" name="tour_name" placeholder="tour_name" required>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">desc</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text"
											<?php if($isEdit == true) : ?>
												value="<?= $data->desc; ?>"
											<?php endif ?>
											class="form-control m-input" id="desc" name="desc" placeholder="desc" required>
										</div>
									</div>

									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">Image thumbnail</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<?php if($isEdit == true) : ?>
												<?php if($data->image != '' || $data->image != null): ?>
												<img width="600px" id="image_exits" class="img-responsive" src="<?= base_url() ?>files/partner/<?= $data->image ?>">
												<?php endif ?>
											<?php endif ?>
										<input type="file" class="form-control m-input" name="image" id="exampleInputEmail1">
										</div>
									</div>


									
									<!-- <div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12">End Show</label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="date" id="end_show" name="end_show" class="form-control m-input" />
										</div>
									</div> -->
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Submit</button>
												<button type="button" class="btn btn-secondary" onclick="window.location.href = '<?= site_url() ?>a'">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>
						<?php if($isEdit == true) : ?>
<script>
$(document).ready(function(){
	
});
</script>
<?php endif ?>
