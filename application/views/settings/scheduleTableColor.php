<?php
$title = "Schedule Table Color Settings";
$action = base_url().'Settings/ScheduleTableColorSave';

?>
<!--begin::Portlet-->
<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
											<?= $title ?>
										</h3>
									</div>
								</div>
							</div>

							<!--begin::Form-->
							<form class="m-form m-form--fit m-form--label-align-right" method="post" enctype="multipart/form-data" action="<?= $action ?>">
								<div class="m-portlet__body">

								<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12"><a target="_blank" href="https://www.w3schools.com/colors/colors_picker.asp">Example</a></label>
										
									</div>

								<?php foreach($data as $key => $v) : ?>
									<?php if($isEdit == true) : ?><input type="hidden" name="id[]" value="<?= $v['id'] ?>"/><?php endif ?>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-3 col-sm-12"><?= $v['gs_code'] ?></label>
										<div class="col-lg-7 col-md-7 col-sm-12">
											<input type="text" value="<?= $v['gs_value'] ?>" name="color[]" class="form-control m-input" id="<?= $v['gs_code'] ?>" placeholder="<?= $v['gs_code'] ?>">
										</div>
									</div>
								<?php endforeach; ?>
									
									

								</div>
								
									
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions m-form__actions">
										<div class="row">
											<div class="col-lg-9 ml-lg-auto">
												<button type="submit" class="btn btn-brand">Submit</button>
												<button type="button" class="btn btn-secondary" onclick="window.location.href = '<?= site_url() ?>Settings'">Cancel</button>
											</div>
										</div>
									</div>
								</div>
							</form>

							<!--end::Form-->
						</div>



