<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class My_libraries
{
	protected $ci;

	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->library('email');
		$this->ci->load->helper('url');
		$this->ci->load->config('email');
		$this->ci->load->model('GeneralSettings');

	}


	public function registrationSeller($data,$attachment,$invAttachment)
	{
		
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
        $subject = SELLER_REGISTRATION;
        $message = $this->ci->load->view('layouts/email/login_access', $data, TRUE);
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
        $this->ci->email->to($to);
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);
		$this->ci->email->attach("./files/pdf/".$attachment);
        $this->ci->email->attach("./files/pdf/".$invAttachment);
		
		$this->ci->email->cc('info@bbtf.or.id,exhibitor@bbtf.or.id');

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	public function registrationSubSeller($data)
	{
		
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
        $subject = "Seller Login";
        $message = $this->ci->load->view('layouts/email/login_access_sellersub', $data, TRUE);
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
        $this->ci->email->to($to);
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);
		//$this->ci->email->attach("./files/pdf/".$attachment);
        //$this->ci->email->attach("./files/pdf/".$invAttachment);
		
		$this->ci->email->cc('info@bbtf.or.id,exhibitor@bbtf.or.id');

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	
	public function Mediaregistration($data)
	{
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
        $subject = 'MEDIA_REGISTRATION';
        $message = $this->ci->load->view('layouts/email/login_access_media', $data, TRUE);
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
        $this->ci->email->to($to);
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	public function MediaClass($data)
	{
		
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
        $subject = 'MEDIA_CLASSIFICATION';
        $message = $this->ci->load->view('layouts/email/media_class', $data, TRUE);
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
        $this->ci->email->to($to);
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}


	public function sellerInvoice($data)
	{
		
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
        $subject = "Invoice";
        $message = $this->ci->load->view('layouts/email/seller_invoice', $data, TRUE);
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
        $this->ci->email->to($to);
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);
		$this->ci->email->cc('acct@bbtf.or.id,info@bbtf.or.id,exhibitor@bbtf.or.id');

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	public function sellerPaidConfirm($data)
	{
		
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
        $subject = "PAID";
        $message = $this->ci->load->view('layouts/email/seller_paid_confirmation', $data, TRUE);
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
        $this->ci->email->to($to);
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);
		$this->ci->email->cc('acct@bbtf.or.id,info@bbtf.or.id,exhibitor@bbtf.or.id');

        if ($this->ci->email->send()) {
			
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	public function TradeBuyerPaidConfirm($data)
	{
		
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
        $subject = "PAID";
        $message = $this->ci->load->view('layouts/email/tradeBuyer_paid_confirm', $data, TRUE);
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
        $this->ci->email->to($to);
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);
        $this->ci->email->cc('acct@bbtf.or.id,info@bbtf.or.id,buyer@bbtf.or.id');

        if ($this->ci->email->send()) {
			
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}


	function buyerInvoice($data)
	{

		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
        $subject = "Invoice";
        $message = $this->ci->load->view('layouts/email/buyer_invoice', $data, TRUE);
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
        $this->ci->email->to($to);
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);
        $this->ci->email->cc('acct@bbtf.or.id,info@bbtf.or.id,buyer@bbtf.or.id');

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	function buyerSendClass($data,$attachment)
	{
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
		$subject = "Invoice";
		if($data['buyer_type'] == "Fully Hosted")
		{
			$message = $this->ci->load->view('layouts/email/buyer_invoice_fully', $data, TRUE);
		} else 
		{
			$message = $this->ci->load->view('layouts/email/buyer_invoice_partial', $data, TRUE);
		}
        
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
		$this->ci->email->to($to);
        $this->ci->email->cc('acct@bbtf.or.id,info@bbtf.or.id,buyer@bbtf.or.id');
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);
		$this->ci->email->attach("./files/pdf/".$attachment);

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}


	function buyerAsSeller($data)
	{
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
		$subject = "BBTF";
		$message = $this->ci->load->view('layouts/email/as_seller', $data, TRUE);
        
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
		$this->ci->email->to($to);
        $this->ci->email->cc('acct@bbtf.or.id,info@bbtf.or.id,buyer@bbtf.or.id');
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);
		//$this->ci->email->attach("./files/pdf/".$attachment);

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	function buyerAsTradeBuyer($data)
	{
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
		$subject = "BBTF";
		$message = $this->ci->load->view('layouts/email/as_tradebuyer', $data, TRUE);
        
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
		$this->ci->email->to($to);
        $this->ci->email->cc('acct@bbtf.or.id,info@bbtf.or.id,buyer@bbtf.or.id');
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);
		//$this->ci->email->attach("./files/pdf/".$attachment);

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	function buyerAfterPayment($data)
	{
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
		$subject = "WAIT CONFIRMATION";
		$message = $this->ci->load->view('layouts/email/buyer_after_payment', $data, TRUE);
        
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
		$this->ci->email->to($to);
        $this->ci->email->cc('acct@bbtf.or.id,info@bbtf.or.id,buyer@bbtf.or.id');
		
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	function tradeBuyerInvoice($data,$attachment)
	{
		
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
        $subject = "Invoice & Confirmation";
        $message = $this->ci->load->view('layouts/email/tradeBuyer_invoice', $data, TRUE);
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
        $this->ci->email->to($to);
        $this->ci->email->subject($subject);
		$this->ci->email->message($message);
		$this->ci->email->cc('acct@bbtf.or.id,info@bbtf.or.id,buyer@bbtf.or.id');
		$this->ci->email->attach("./files/pdf/".$attachment);

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	function buyerConfirmClass($data)
	{
		
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
		$subject = "CONFIRMATION";
		$message ="";
		if($data['buyer_class'] == 1)
		{
			$message = $this->ci->load->view('layouts/email/buyer_full_hosted', $data, TRUE);
		} else if ($data['buyer_class'] == 2)
		{
			$message = $this->ci->load->view('layouts/email/buyer_partial', $data, TRUE);
		} else if ($data['buyer_class'] == 3)
		{
			$message = $this->ci->load->view('layouts/email/as_seller', $data, TRUE);
		}
		else if ($data['buyer_class'] == 4)
		{
			$message = $this->ci->load->view('layouts/email/buyer_decline', $data, TRUE);
		}else if ($data['buyer_class'] == 5)
		{
			$message = $this->ci->load->view('layouts/email/buyer_decline', $data, TRUE);
		} else {
			$message = $this->ci->load->view('layouts/email/buyer_decline', $data, TRUE);
		}
		
        
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
        $this->ci->email->to($to);
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	function buyerPaid($data)
	{
		
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
		$subject = "PAID CONFIRMATION";
		$message = $this->ci->load->view('layouts/email/buyer_paid_confirm', $data, TRUE);
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
        $this->ci->email->to($to);
        $this->ci->email->subject($subject);
		$this->ci->email->message($message);
		$this->ci->email->cc('acct@bbtf.or.id,info@bbtf.or.id,buyer@bbtf.or.id');

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	function tradeBuyerPaid($data)
	{
		
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
		$subject = "PAID CONFIRMATION";
		$message = $this->ci->load->view('layouts/email/tradeBuyer_paid_confirm', $data, TRUE);
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
        $this->ci->email->to($to);
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);
		$this->ci->email->cc('acct@bbtf.or.id,info@bbtf.or.id,buyer@bbtf.or.id');

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	function registrationBuyer($data)
	{
		
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
        $subject = BUYER_REGISTRATION;
        $message = $this->ci->load->view('layouts/email/buyer_registration', $data, TRUE);
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
		$this->ci->email->to($to);
		$this->ci->email->cc('info@bbtf.or.id,buyer@bbtf.or.id');
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);
		//$this->ci->email->attach("./files/pdf/".$attachment);

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	function SendBuyerData($data)
	{
		
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
		$to ="info@bbtf.or.id";
        //$to ="blackhorse172@gmail.com";
		
		$subject = "NEW BUYER DATA";

		$country = $this->ci->db->get_where('apps_countries',array('country_code'=>$data['buyer_country_code']))->row_array();
		$nature = $this->ci->db->get_where('general_settings',array('gs_code'=>$data['nature_of_business']))->row_array();

		$data['country_name'] = $country['country_name'];
		$data['nature_of_business'] = $nature['gs_value'];
		//$data['buyer_email'] = $data['email'];
		$data['which_products_are_you_interested_in'] = implode(",",$data['which_products_are_you_interested_in']);
		$data['profile'] = $data;
		// print_r($data);
		// exit;
        $message = $this->ci->load->view('layouts/email/buyer_data', $data, TRUE);
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
		$this->ci->email->to($to);
		//$this->ci->email->cc('info@bbtf.or.id,buyer@bbtf.or.id');
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);
		//$this->ci->email->attach("./files/pdf/".$attachment);

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	function sendTradeBuyerData($data)
	{
		
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
		$to ="info@bbtf.or.id";
        //$to ="blackhorse172@gmail.com";
		$country = $this->ci->db->get_where('apps_countries',array('country_code'=>$data['country_code']))->row_array();
		$data['country_name'] = $country['country_name'];
		$subject = "NEW TRADE BUYER DATA";
		$data['profile'] = $data;
        $message = $this->ci->load->view('layouts/email/tradebuyer_data', $data, TRUE);
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
		$this->ci->email->to($to);
		//$this->ci->email->cc('info@bbtf.or.id,buyer@bbtf.or.id');
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);
		//$this->ci->email->attach("./files/pdf/".$attachment);

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	function EmailRegistration($post)
	{
		$post['year'] = date('Y');
		$from = $this->config->item('smtp_user');
        $to =$post['email'];
        $subject = SELLER_REGISTRATION;
        $message = $this->load->view('layouts/email/login_access', $post,false);
		//$this->email->initialize($this->config->item());
		
        $this->email->set_newline("\r\n");
        $this->email->from($from);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);

        if ($this->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->email->print_debugger());
            //return false;
		}
		
	}

	function test($data)
	{

		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
        $subject = "test";
        $message = $this->ci->load->view('layouts/email/test', $data, TRUE);
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
        $this->ci->email->to($to);
        $this->ci->email->subject($subject);
		$this->ci->email->message($message);
        $this->ci->email->attach("./files/pdf/MELIA.pdf");
		

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return log_message('debug', $this->ci->email->print_debugger());;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return log_message('error', $this->ci->email->print_debugger());;
		}
		
	}


	function getChargeCreditCard(){

		$this->ci->db->where('gs_code', 'fee_cc_register');
		
		return $this->ci->db->get('general_settings')->row()->gs_value;
		

	}


	function resetPasswordBuyer($data)
	{
		
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
        $subject = "RESET PASSWORD";
        $message = $this->ci->load->view('layouts/email/reset_password_buyer', $data, TRUE);
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
		$this->ci->email->to($to);
		$this->ci->email->cc('info@bbtf.or.id,buyer@bbtf.or.id,marketing@bbtf.or.id');
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	function resetPasswordSeller($data)
	{
		
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
        $subject = "RESET PASSWORD";
        $message = $this->ci->load->view('layouts/email/reset_password_seller', $data, TRUE);
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
		$this->ci->email->to($to);
		$this->ci->email->cc('info@bbtf.or.id,buyer@bbtf.or.id,marketing@bbtf.or.id');
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	function sendLoginAccessBuyer($data)
	{
		
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
        $subject = "LOGIN ACCESS";
        $message = $this->ci->load->view('layouts/email/login_access_buyer', $data, TRUE);
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
		$this->ci->email->to($to);
		$this->ci->email->cc('info@bbtf.or.id,buyer@bbtf.or.id,marketing@bbtf.or.id');
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	function sendIgnoreBuyer($data)
	{
		
		$data['year'] = $this->ci->GeneralSettings->getYearEvent();
		$data['date'] = $this->ci->GeneralSettings->getDateEvent();
		
		$from = $this->ci->config->item('smtp_user');
        $to =$data['email'];
        $subject = "IGNORE STATUS";
        $message = $this->ci->load->view('layouts/email/buyer_ignore', $data, TRUE);
		
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from($from);
		$this->ci->email->to($to);
		$this->ci->email->cc('info@bbtf.or.id,buyer@bbtf.or.id,marketing@bbtf.or.id');
        $this->ci->email->subject($subject);
        $this->ci->email->message($message);

        if ($this->ci->email->send()) {
			log_message('debug', $this->ci->email->print_debugger());
            return true;
        } else {
			log_message('error', $this->ci->email->print_debugger());
            return false;
		}
		
	}

	

}

/* End of file Email.php */

?>
