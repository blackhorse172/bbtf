<?php

defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH.'/third_party/mpdf/mpdf.php';

class M_pdf
{
	protected $ci;
	public $param;
	public $pdf;
	
	public function __construct($param = "'c', 'A4-L'")
	
	{
	
	
		$this->param =$param;
	
		$this->ci =& get_instance();
	
		$this->pdf = new mPDF($this->param);
	
	}
	

}

/* End of file Mpdf.php */

?>
