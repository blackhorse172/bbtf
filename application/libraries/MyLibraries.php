<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyLibraries
{
	protected $ci;

	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->model("Articles");
	}

	public function getRandomNews($id="",$limit)
	{
		return $this->ci->Articles->getRandomNews($id,$limit);
	}

	public function limit_text($text, $limit) {
		if (str_word_count($text, 0) > $limit) {
			$words = str_word_count($text, 2);
			$pos = array_keys($words);
			$text = substr($text, 0, $pos[$limit]) . '...';
		}
		return $text;
	  }

	

}

/* End of file MyLibrariesName.php */


?>
