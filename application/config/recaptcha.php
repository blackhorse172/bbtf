<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// $config['public_key']   = CNF_RECAPTCHA_PUBLIC ;
// $config['private_key']  = CNF_RECAPTCHA_PRIVATE;

// // Set Recaptcha options
// // Reference at https://developers.google.com/recaptcha/docs/customization
// $config['recaptcha_options']  = array(
//     'theme'=>'red', // red/white/blackglass/clean
//     'lang' => 'en' // en/nl/fl/de/pt/ru/es/tr
//     //  'custom_translations' - Use this to specify custom translations of reCAPTCHA strings.
//     //  'custom_theme_widget' - When using custom theming, this is a div element which contains the widget. See the custom theming section for how to use this.
//     //  'tabindex' - Sets a tabindex for the reCAPTCHA text box. If other elements in the form use a tabindex, this should be set so that navigation is easier for the user
// );

// To use reCAPTCHA, you need to sign up for an API key pair for your site.
// link: http://www.google.com/recaptcha/admin
$config['recaptcha_site_key'] = CNF_RECAPTCHA_PUBLIC;
$config['recaptcha_secret_key'] = CNF_RECAPTCHA_PRIVATE;

// reCAPTCHA supported 40+ languages listed here:
// https://developers.google.com/recaptcha/docs/language
$config['recaptcha_lang'] = 'en';

/* End of file recaptcha.php */
/* Location: ./application/config/recaptcha.php */
?>
