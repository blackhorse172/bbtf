<?php 
$route["home.html"] = "page/index/home";
$route["home.html/(:any)"] = "page/index/home/%1";
$route["seller-information.html"] = "page/index/seller-information";
$route["seller-information.html/(:any)"] = "page/index/seller-information/%1";
$route["floor-layout.html"] = "page/index/floor-layout";
$route["floor-layout.html/(:any)"] = "page/index/floor-layout/%1";
$route["media-information.html"] = "page/index/media-information";
$route["media-information.html/(:any)"] = "page/index/media-information/%1";
$route["seller-list.html"] = "page/index/seller-list";
$route["seller-list.html/(:any)"] = "page/index/seller-list/%1";
$route["buyer-list.html"] = "page/index/buyer-list";
$route["buyer-list.html/(:any)"] = "page/index/buyer-list/%1";
$route["trade-buyer-information.html"] = "page/index/trade-buyer-information";
$route["trade-buyer-information.html/(:any)"] = "page/index/trade-buyer-information/%1";
$route["seller-registration.html"] = "page/index/seller-registration";
$route["seller-registration.html/(:any)"] = "page/index/seller-registration/%1";
$route["buyer-registration.html"] = "page/index/buyer-registration";
$route["buyer-registration.html/(:any)"] = "page/index/buyer-registration/%1";
$route["login.html"] = "page/index/login";
$route["login.html/(:any)"] = "page/index/login/%1";
$route["schedule.html"] = "page/index/schedule";
$route["schedule.html/(:any)"] = "page/index/schedule/%1";
$route["hosted-tour.html"] = "page/index/hosted-tour";
$route["hosted-tour.html/(:any)"] = "page/index/hosted-tour/%1";
$route["news.html"] = "page/index/news";
$route["news.html/(:any)"] = "page/index/news/%1";
$route["photo-gallery.html"] = "page/index/photo-gallery";
$route["photo-gallery.html/(:any)"] = "page/index/photo-gallery/%1";
$route["testimonies.html"] = "page/index/testimonies";
$route["testimonies.html/(:any)"] = "page/index/testimonies/%1";
$route["about-us.html"] = "page/index/about-us";
$route["about-us.html/(:any)"] = "page/index/about-us/%1";
$route["video.html"] = "page/index/video";
$route["video.html/(:any)"] = "page/index/video/%1";
$route["buyer-information.html"] = "page/index/buyer-information";
$route["buyer-information.html/(:any)"] = "page/index/buyer-information/%1";
$route["map.html"] = "page/index/map";
$route["map.html/(:any)"] = "page/index/map/%1";
$route["map.html"] = "page/index/map";
$route["map.html/(:any)"] = "page/index/map/%1";
?>