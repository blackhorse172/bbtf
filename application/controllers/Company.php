<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);
		$this->load->model('Companies');
		
	}
	

	public function index()
	{
		$data = $this->Companies->get(1);
		$this->data['data'] = $data;
		$this->data['isEdit'] = true;

		$this->data['content'] = $this->load->view('company/index',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}



	public function Save()
	{
		$fileName = '';
		$id = $this->input->post('id');
		$company = $this->Companies->get($id);

		if($_FILES["image"]["name"] != "")
		{
			//$this->debug($_FILES);
			$config['upload_path'] = './files/company/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				
				//validation image has error
				SiteHelpers::alert('error',$this->upload->display_errors());
				redirect('Company/index');
				} else {
					$data = $this->upload->data();
					$fileName = $data["file_name"];
					$company->company_logo = $fileName;	

				}
		}

		if($_FILES["caption"]["name"] != "")
		{
			//$this->debug($_FILES);
			$config['upload_path'] = './files/company/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '1024';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('caption')){
				$error = array('error' => $this->upload->display_errors());
				
				//validation image has error
				SiteHelpers::alert('error',$this->upload->display_errors());
				redirect('Company/index');
				} else {
					$data = $this->upload->data();
					$fileName = $data["file_name"];
					$company->caption = $fileName;	

				}
		}
		
		
		$company->company_name = $this->input->post('company_name');
		//$company->caption = $this->input->post('caption');
		$company->company_address = $this->input->post('company_address');
		$company->company_email = $this->input->post('company_email');
		$company->company_telp = $this->input->post('company_telp');
		$company->company_telp2 = $this->input->post('company_telp2');
		$company->tax = $this->input->post('tax');
		$company->service_tax = $this->input->post('service_tax');


	
		//$this->debug($company);
		$id = $company->save();
		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Company',301);
	}




}

/* End of file Article.php */
