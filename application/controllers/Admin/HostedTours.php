<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class HostedTours extends MY_Controller {

	var $tableView;
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('HostedTours');
		$this->tableView = 'v_product_stock';
	}
	

	public function index()
	{
		$data = $this->HostedTours->join();		
		//$this->debug($data);
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('admin/hosted_tour/index',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
		
	}

	

	public function updateStatus($id,$status){
		$data["status"] = $status;
		$data['verify_by'] = $this->session->userdata('userName');
		$data['verify_date'] = date('Y-m-d');

		//$this->debug($id);

		$this->db->where('id', $id);
		$this->db->update('hosted_tour_request', $data);
		

		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Admin/HostedTours',301);
	}

}

/* End of file HostedTours.php */
