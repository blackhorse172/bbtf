<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);

		$this->load->model('Transactions');
		$this->load->model('Dokus');
		$this->load->model('Booths');
		
		//$this->load->model('TransactionsCategories');
		
		//Do your magic here
	}
	

	public function index()
	{
		$data = $this->Transactions->join();

		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('admin/transaction/index',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	// public function ()
	// {
	// 	$data = $this->Transactions->join();

	// 	$this->data['data'] = json_encode($data);
	// 	$this->data['content'] = $this->load->view('admin/transaction/index',$this->data,true);    
    // 	$this->load->view('layouts/main',$this->data);
	// }

	public function doku()
	{
		$this->db->order_by('id', 'desc');
		$data = $this->Dokus->getList();
		//$this->debug($data);
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('admin/doku/index',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function TradeBuyer()
	{
		$data = $this->Transactions->joinTradeBuyer();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('admin/transaction/index_tradeBuyer',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Create()
	{
		
		$group = $this->TransactionsCategories->get_list();
		$this->data['isEdit'] = false;
		$this->data['group'] = $group;
		$this->data['content'] = $this->load->view('admin/transaction/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function edit($id)
	{
		$data = $this->Transactions->get($id);
		if($data == '')
		{
			redirect('error/notFound');
		}
		//$this->debug($data);
		$group = $this->TransactionsCategories->get_list();
		$this->data['isEdit'] = true;
		$this->data['group'] = $group;
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('admin/transaction/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function save()
	{
		//$this->debug($this->input->post());
		$transactions = $this->Transactions->new_row();

		$transactions->transactions_name = $this->input->post('name');
		$transactions->transactions_fee = (int)str_replace('.','',$this->input->post('fee')) ;	
		$transactions->category_id = $this->input->post('category_id');
		$transactions->dtm_crt = date('Y-m-d H:i:s');
		$transactions->usr_crt = $this->session->userdata('userName');

		$transactions->active = 1;	

		$id = $transactions->save();
		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Transactions',301);
	}

	public function saveEdit()
	{
		$id = $this->input->post('id');
		$transactions = $this->Transactions->get($id);
		//$this->debug($this->input->post());
		
		$transactions->transactions_name = $this->input->post('name');
		$transactions->transactions_fee = (int)str_replace('.','',$this->input->post('fee')) ;	
		$transactions->category_id = $this->input->post('category_id');
		$transactions->dtm_upd = date('Y-m-d H:i:s');
		$transactions->usr_upd = $this->session->userdata('userName');

		$id = $transactions->save();
		SiteHelpers::alert('success'," Data has Edited succesfuly !");

		redirect('Transactions',301);
	}

	public function getExistEmail()
	{
		$email = $this->input->post('email');
		
		$res = false;
		$this->db->where('email', $email);
		$this->db->from('tb_transactions');
		$q = $this->db->get();
		
		if($q->num_rows() > 0) $res = true;

		echo json_encode($res);
		
	}

	public function nonActive($id)
	{
		$transactions = $this->Transactions->get($id);

		if($transactions->active == 1){
			$transactions->active = 0;
 		} else {
			$transactions->active = 1;
		 }

		 $transactions->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Transactions',301);
	}


	public function void($trxCode)
	{
		
		$object['trx_status'] = 3;
		$this->db->where('trx_code', $trxCode);
		$this->db->update('transaction_h', $object);

		//return to stock after void
		$this->db->where('ref_trans', $trxCode);
		$stock = $this->db->get('stock')->result_array();
		
		foreach ($stock as $key => $value) {
			# code...
			$data['product_id'] = $value['product_id'];
			$data['amount'] = $value['amount'] * -1;
			$data['ref_trans'] =$trxCode;
			$data['type'] ="VOID";
			$data['dtm_crt'] = date('Y-m-d H:i:s');

			$this->db->insert('stock', $data);
			
		}

		
		SiteHelpers::alert('success'," Void succesfuly !");

		redirect('Admin/Transactions',301);
	}

	

	

	public function json_getDetail()
	{
		$code = $this->input->post('trx_code');
		$type = $this->input->post('trx_type');
		
		if($type == SELLER_REGISTRATION){
			$this->db->where('trx_code', $code);
			$res = $this->db->get('v_product_registration_detail')->result_array();
			
			echo json_encode($res);	
		}

		if($type == MEMBERSHIP_ORDER){
			$this->db->where('trx_code', $code);
			$res = $this->db->get('v_product_membership_detail')->result_array();
			
			return json_encode($res);
			
		}

		if($type == PRODUCT_ORDER){
			$this->db->where('trx_code', $code);
			$res = $this->db->get('v_product_detail')->result_array();
			
			return json_encode($res);
			
		}
		
	}

	// public function UpdateBooth()
	// {
	// 	$post = $this->input->post();
	// 	$object['booth_number'] = $post['booth_number'];

	// 	$this->db->where('id', $post['detail_id']);
	// 	$this->db->update('transaction_d', $object);
		
	// 	return redirect('Admin/Transactions');
	// }

	public function updateBooth($trxCode)
	{
		$data = $this->db->get_where('v_booth_name',array('trx_code'=>$trxCode))->result_array(); //get_where('ref_trans');
		
		$this->data['data'] = $data;
		$this->data['trxCode'] = $trxCode;
		$this->data['content'] = $this->load->view('admin/transaction/update_booth',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function paidConfirm($id,$type,$trx)
	{
		# code...
		if($type == "BUYER")
		{
			$this->load->library('my_libraries');
			$profile = $this->db->get_where('buyer',array('buyer_id'=>$id))->row_array();
			$email['email'] = $profile['buyer_email'];
			$email['gender'] = $profile['gender'];
			$email['buyer_name'] = $profile['first_name']." ".$profile['last_name'];

			$this->my_libraries->buyerPaid($email);
			$update['is_paid_confirm'] = 1;
			$this->db->where('buyer_id', $id);
			$this->db->update('buyer', $update);
		} else {
			$this->load->library('my_libraries');
			$profile = $this->db->get_where('trade_buyer',array('id'=>$id))->row_array();
			$email['email'] = trim($profile['company_email']);
			$email['full_name'] = trim($profile['full_name']);
			$email['company_name'] = $profile['company_name'];
			//$this->debug($email);
			$this->my_libraries->tradeBuyerPaid($email);
			$update['is_paid_confirm'] = 1;
			$this->db->where('id', $id);
			$this->db->update('trade_buyer', $update);
		}
		

			$data['is_send'] = 1;
			$this->db->where('transidmerchant', $trx);
			$this->db->update('doku', $data);

		SiteHelpers::alert('success'," Confirm save succesfuly !");
		redirect('Admin/Transactions/Doku',301);
		
		
	}
	

}

/* End of file transactions.php */
