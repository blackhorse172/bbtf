<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);

		$this->load->model('Payments');
		//$this->load->model('Transactions');

		//$this->load->model('PaymentCategories');
		
		//Do your magic here
	}
	

	public function index()
	{
		$data = $this->Payments->get_list();
		
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('admin/payment/index',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Create($trxCode)
	{
		$trxData = $this->db->get_where('transaction_h',array("trx_code"=>$trxCode))->row();

		if($trxData == null) {
			redirect('');
		}

		$this->data['isEdit'] = false;
		$this->data['trxCode'] = $trxCode;
		$this->data['content'] = $this->load->view('admin/payment/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function confirm($trxCode)
	{
		$data = $this->Payments->where("trx_code",$trxCode)->get();
		if($data == '')
		{
			redirect('error/notFound');
		}
		//$this->debug($data->image);
		//$group = $this->paymentCategories->get_list();
		$this->data['isEdit'] = true;
		$this->data['trxCode'] = $trxCode;
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('admin/payment/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function confirmTradeBuyer($trxCode)
	{
		$data = $this->db->get_where('transaction_register_trade_buyer',array('trx_code'=>$trxCode))->row_array();
		
		if($data == '')
		{
			redirect('error/notFound');
		}

		$profile = $this->db->get_where('trade_buyer',array('id'=>$data['user_id']))->row_array();

		$data['trx_status'] = 2;
		$data['dtm_upd'] = date('Y-m-d H:i:s');
		$data['usr_crt'] = $this->session->userdata('userName');

		$this->db->update('transaction_register_trade_buyer', $data);

		//email paid confirmation
		$this->load->library('my_libraries');
		$email['email'] = trim($profile['company_email']);
		$email['company_name'] =  $profile['company_name'];
		$this->my_libraries->TradeBuyerPaidConfirm($email);


		SiteHelpers::alert('success',$trxCode." Has Confirmed Payment !");
		redirect('Admin/Transactions/TradeBuyer',301);
	}

	public function save()
	{
		$payment["approve_by"] = $this->session->userdata('userName');
		$payment["approve_date"] = date('Y-m-d H:i:s');
		$payment["payment_status"] = 1;
		$user = $this->db->get_where('tb_users',array('id'=> $this->session->userdata('uid')))->row_array();
		//select user id by payment
		$trxHeader = $this->db->get_where('transaction_h',array('trx_code'=>$this->input->post('trx_code')))->row_array();
		$user = $this->db->get_where('tb_users',array('id'=> $trxHeader['user_id']))->row_array();
		
		$this->load->library('my_libraries');
		$email['email'] = trim($user['email']);
		$email['seller_name'] =  $user['first_name'];
		$this->my_libraries->sellerPaidConfirm($email);
		//$this->db->where('trx_code', $this->input->post('trx_code'));
    	$this->db->update('payment', $payment);
		//$id = $this->db->insert_id();
		$this->updateTrx($this->input->post('trx_code'),2);
		SiteHelpers::alert('success',$this->input->post('trx_code')." Has Confirmed Payment !");

		redirect('Admin/transactions',301);
	}

	public function updateTrx($trxCode,$status)
	{
		# code...
		
		$data["trx_status"] = $status;
		$data["dtm_upd"] = date("Y-m-d H:i:s");

		$this->db->where('trx_code', $trxCode);
		$this->db->update('transaction_h', $data);
		

	}

	public function saveEdit()
	{
		$id = $this->input->post('id');
		$payment = $this->Payments->get($id);
		//$this->debug($this->input->post());
		
		$payment->payment_name = $this->input->post('name');
		$payment->payment_fee = (int)str_replace('.','',$this->input->post('fee')) ;	
		$payment->category_id = $this->input->post('category_id');
		$payment->dtm_upd = date('Y-m-d H:i:s');
		$payment->usr_upd = $this->session->userdata('userName');

		$id = $payment->save();
		SiteHelpers::alert('success'," Data has Edited succesfuly !");

		redirect('payment',301);
	}

	public function getExistEmail()
	{
		$email = $this->input->post('email');
		
		$res = false;
		$this->db->where('email', $email);
		$this->db->from('tb_payment');
		$q = $this->db->get();
		
		if($q->num_rows() > 0) $res = true;

		echo json_encode($res);
		
	}

	public function nonActive($id)
	{
		$payment = $this->Payments->get($id);

		if($payment->active == 1){
			$payment->active = 0;
 		} else {
			$payment->active = 1;
		 }

		 $payment->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('payment',301);
	}

	public function categoryNonActive($id)
	{
		$payments = $this->PaymentCategories->get($id);

		if($payments->is_active == '1'){
			$payments->is_active = '0';
 		} else {
			$payments->is_active = '1';
		 }

		 $payments->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Payments/category',301);
	}

	public function category()
	{
		$data = $this->PaymentCategories->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('admin/payment/category',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function createCategory()
	{
		$this->data['isEdit'] = false;
		
		$this->data['content'] = $this->load->view('admin/payment/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function editCategory($id)
	{
		$this->data['isEdit'] = true;
		$data = $this->PaymentCategories->get($id);
		//$this->debug($data);
		if($data == null){
			SiteHelpers::alert('warning'," Data not found !");

			redirect('Payments/category',301);
		}
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('admin/payment/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function saveCategory()
	{
		$id = $this->input->post('id');
		
		if($this->input->post('isEdit') == '') {
			$category = $this->PaymentCategories->new_row();
			$msg = " Data has been Create succesfuly !";
		} else {
			$category = $this->PaymentCategories->get($id);
			$msg = " Data has been Create succesfuly !";
		}
		$category->category_name = $this->input->post('name');
		$category->category_desc =  $this->input->post('desc');
		$category->is_active =  '1';

		$id = $category->save();	
		SiteHelpers::alert('success',$msg);
		redirect('Payments/category',301);
	}

}

/* End of file payments.php */
