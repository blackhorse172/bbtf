<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class OfficialPartner extends MY_Controller {

	var $tableView;
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		//$this->load->model('HostedTours');
		//$this->tableView = 'v_product_stock';
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);

	}
	

	public function index()
	{
		//$data = $this->db->get_where('official_partner',array('is_active'=>1))->result_array();
		$this->db->order_by('dtm_crt', 'desc');
		$data = $this->db->get('official_partner')->result_array();
		//$this->debug($data);
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('admin/official_partner/index',$this->data,true);   
    	$this->load->view('layouts/main',$this->data);
		
	}


	public function Create()
	{
		$this->data['isEdit'] = false;
		//$this->data['category'] = $this->GalleryCategories->isActive()->get_list();
		$this->data['content'] = $this->load->view('admin/official_partner/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function edit($id)
	{
		$data = $this->db->get_where('official_partner',array('id'=>$id) )->row();
		
		$this->data['isEdit'] = true;
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('admin/official_partner/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function save()
	{

		//$this->debug($this->input->post());
		$data = $this->input->post();
		$data['dtm_crt'] = date('Y-m-d');
		$data['usr_crt'] = $this->session->userdata('userName');
		$fileName = '';
		if(isset($_FILES["image"]["name"]))
		{
			//$this->debug($_FILES);
			$config['upload_path'] = './files/partner/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				
				//validation image has error
				SiteHelpers::alert('error',$this->upload->display_errors());
				redirect('Admin/OfficialPartner/Create');
				} else {
					$upload = $this->upload->data();
					$fileName = $upload["file_name"];
				}
		}
		$data['image'] = $fileName;
		
		$this->db->insert('official_partner', $data);

		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Admin/OfficialPartner',301);
		
		
	}

	public function saveEdit()
	{
		$update = $this->input->post();
		$update['dtm_crt'] = date('Y-m-d');
		//$this->debug($data);
		if(isset($_FILES["image"]["name"]))
		{
			$partner = $this->db->get_where('official_partner',array('id'=>$update['id']))->row();
			//$partner = $this->db->get_where('official_partner',array('id'=>$data['id']))->row();
			//$this->debug(count($this->input->post($_FILES["image"])));
			$config['upload_path'] = './files/partner/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';
			
			if(count($this->input->post($_FILES["image"])) > 3){
					//unlink exist data
					if($partner->image != '' || $partner->image != null){
						$image = $config['upload_path'].$partner->image;
						unlink($image);
					}
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if (!$this->upload->do_upload('image')){
						$error = array('error' => $this->upload->display_errors());
						
						//validation image has error
						SiteHelpers::alert('error',$this->upload->display_errors());
						redirect('Admin/OfficialPartner/Create');
						} else {
							$data = $this->upload->data();
							$fileName = $data["file_name"];
							$update['image'] = $fileName;

						}
			}
			
		}
		

		$this->db->where('id', $update['id']);
		$this->db->update('official_partner', $update);

		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Admin/OfficialPartner',301);
		
	}

	public function NonActive($id)
	{
		$officialPartner = $this->db->get_where('official_partner',array('id'=>$id))->row();
		//$this->Galleries->get($id);
		
		if($officialPartner->is_active == 1){
			$officialPartner->is_active = 0;
 		} else {
			$officialPartner->is_active = 1;
		 }

		 $this->db->where('id', $id);
		 $this->db->update('official_partner', $officialPartner);
		 

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Admin/OfficialPartner',301);
	}

}

/* End of file HostedTours.php */
