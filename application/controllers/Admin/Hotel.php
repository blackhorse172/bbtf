<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hotel extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);
		$this->load->model('Hotels');
		
	}
	
	public function index()
	{
		$this->db->order_by('id', 'desc');
		$data = $this->Hotels->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('hotel/index',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function create()
	{
		//$this->data['category'] = $this->ArticleCategories->isActive()->get_list();
		$this->data['isEdit'] = false;
		//$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('hotel/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function edit($id)
	{
		$data = $this->Hotels->get($id);
		if($data == null) {
			SiteHelpers::alert('error'," Id not found !");

			redirect('Admin/Hotel',301);
		}
		
		$this->data['isEdit'] = true;
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('hotel/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Save()
	{
		
		
		$hotel = $this->Hotels->new_row();

		$hotel->hotel_name = $this->input->post('hotel_name');
		$hotel->hotel_address = $this->input->post('hotel_address');	
		$hotel->hotel_email = $this->input->post('hotel_email');	
		$hotel->hotel_phone = $this->input->post('hotel_phone');	
		$hotel->stock = (int)$this->input->post('stock');	
		$hotel->year = date('Y');
		$hotel->dtm_crt = date('Y-m-d H:i:s');
		$hotel->usr_crt = $this->session->userdata('userName');
		$hotel->is_active =1;

		//$this->debug($hotel);
		$id = $hotel->save();
		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Admin/Hotel',301);
	}

	public function saveEdit()
	{
		
		$id = $this->input->post('id');
		$hotel = $this->Hotels->get($id);
		
		$hotel->hotel_name = $this->input->post('hotel_name');
		$hotel->hotel_address = $this->input->post('hotel_address');	
		$hotel->hotel_email = $this->input->post('hotel_email');	
		$hotel->hotel_phone = $this->input->post('hotel_phone');	
		$hotel->stock = (int)$this->input->post('stock');	
		$hotel->year = date('Y');
		$hotel->dtm_upd = date('Y-m-d H:i:s');
		$hotel->usr_upd = $this->session->userdata('userName');
		//$hotel->is_active =1;


		$id = $hotel->save();
		SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Admin/Hotel',301);
	}

	public function nonActive($id)
	{
		$hotel = $this->Hotels->get($id);

		if($hotel->is_active == 1){
			$hotel->is_active = 0;
 		} else {
			$hotel->is_active = 1;
		 }

		 $hotel->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Admin/Hotel',301);
	}

}

/* End of file Hotel.php */
?>
