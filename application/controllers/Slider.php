<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);      
		$this->load->model('Sliders');
		$this->load->model('SliderCategories');

		
	}
	

	public function index()
	{
		$data = $this->Sliders->join();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('slider/index',$this->data,true);   
    	$this->load->view('layouts/main',$this->data);
	}

	public function Create()
	{
		$this->data['isEdit'] = false;
		$this->data['category'] = $this->SliderCategories->isActive()->get_list();
		$this->data['content'] = $this->load->view('slider/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function edit($id)
	{
		$data = $this->Sliders->get($id);
		$this->data['category'] = $this->SliderCategories->isActive()->get_list();
		$this->data['isEdit'] = true;
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('slider/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Save()
	{
		$fileName = '';
		//var_dump((int)$this->input->post('status'));
		//$this->debug($this->input->post('status'));
		if(isset($_FILES["image"]["name"]))
		{
			//$this->debug($_FILES);
			$config['upload_path'] = './files/slider/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				
				//validation image has error
				SiteHelpers::alert('error',$this->upload->display_errors());
				redirect('Slider/Create');
				} else {
					$data = $this->upload->data();
					$fileName = $data["file_name"];
				}
		}
		
		$Slider = $this->Sliders->new_row();

		$Slider->category_id = $this->input->post('category_id');
		$Slider->title = $this->input->post('title');
		$Slider->descr = $this->input->post('desc');	
		$Slider->dtm_crt = date('Y-m-d H:i:s');
		$Slider->usr_crt = $this->session->userdata('userName');
		$Slider->status = (int)$this->input->post('status');	
		$Slider->end_show = $this->input->post('end_show');

		$Slider->image = $fileName;	
		//$this->debug($Slider);
		$id = $Slider->save();
		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Slider',301);
	}

	public function saveEdit()
	{
		$fileName = '';
		$id = $this->input->post('slider_id');
		$Slider = $this->Sliders->get($id);
		
		if(isset($_FILES["image"]["name"]))
		{
			$config['upload_path'] = './files/slider/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';
			
			if(count($this->input->post($_FILES["image"])) > 3){
			
					//unlink exist data
					if($Slider->image != '' || $Slider->image != null){
						$image = $config['upload_path'].$Slider->image;
						unlink($image);
					}
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if (!$this->upload->do_upload('image')){
						$error = array('error' => $this->upload->display_errors());
						
						//validation image has error
						SiteHelpers::alert('error',$this->upload->display_errors());
						redirect('slider/create');
						} else {
							$data = $this->upload->data();
							$fileName = $data["file_name"];
							$Slider->image = $fileName;

						}
			}
			
		}
		$Slider->category_id = $this->input->post('category_id');
		$Slider->title = $this->input->post('title');
		$Slider->descr = $this->input->post('desc');	
		$Slider->dtm_crt = date('Y-m-d H:i:s');
		$Slider->usr_crt = $this->session->userdata('userName');
		$Slider->status = (int)$this->input->post('status');	
		$Slider->end_show = $this->input->post('end_show');
		$id = $Slider->save();
		SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Slider',301);
	}

	public function nonActive($id)
	{
		$slider = $this->Sliders->get($id);
		
		if($slider->status == 1){
			$slider->status = 0;
 		} else {
			$slider->status = 1;
		 }

		 $slider->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Slider',301);
	}

	public function categoryNonActive($id)
	{
		$slider = $this->SliderCategories->get($id);

		if($slider->is_active == '1'){
			$slider->is_active = '0';
 		} else {
			$slider->is_active = '1';
		 }

		 $slider->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Slider/category',301);
	}

	public function category()
	{
		$data = $this->SliderCategories->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('slider/category',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function createCategory()
	{
		$this->data['isEdit'] = false;
		
		$this->data['content'] = $this->load->view('slider/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function editCategory($id)
	{
		$this->data['isEdit'] = true;
		$data = $this->SliderCategories->get($id);
		//$this->debug($data);
		if($data == null){
			SiteHelpers::alert('warning'," Data not found !");

			redirect('Slider/category',301);
		}
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('slider/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function saveCategory()
	{
		$id = $this->input->post('id');
		
		if($this->input->post('isEdit') == '') {
			$category = $this->SliderCategories->new_row();
			$msg = " Data has been Create succesfuly !";
		} else {
			$category = $this->SliderCategories->get($id);
			$msg = " Data has been Create succesfuly !";
		}
		$category->category_name = $this->input->post('name');
		$category->category_desc =  $this->input->post('desc');
		$category->is_active =  '1';

		$id = $category->save();	
		SiteHelpers::alert('success',$msg);
		redirect('Slider/category',301);
	}


	public function delete($id)
	{
		$Slider = $this->Sliders->get($id);
		$config['upload_path'] = './files/slider/';
		$image = $config['upload_path'].$Slider->image;
		unlink($image);

		$this->db->where('slider_id', $id);
		$this->db->delete('slider');

		SiteHelpers::alert('success',"Delete Success");
		redirect('Slider',301);
		
	}

}

/* End of file slider.php */
