<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Buyer extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->library('form_validation');
		$this->load->model('GeneralSettings');
	}

	private function getYearEvent()
	{
		return $this->GeneralSettings->getYearEvent();
	}
	
	public function index()
	{
		if(!$this->session->userdata('logged_in')) redirect('login.html',301);   
		$where['is_active'] = 1;
		$where['user_id'] = $this->session->userdata('uid');
		$target_market = $this->db->get_where('general_settings',array('gs_name'=>'seller_target_market'))->result_array();
		$country = $this->db->get('apps_countries')->result_array();
		//$category = $this->db->get_where('category_seller',array('is_active'=>1))->result_array();
		$hotel = $this->db->get_where('buyer_hotel',array('is_active'=>1,'year'=>date('Y'),'stock >'=>0))->result_array();
		
		$this->data['hotels'] = $hotel;

		$this->data['country'] = $country;
		$this->data['target_market'] = $target_market;
		$this->data['profile'] = $this->db->get_where('buyer_list',$where)->row_array();
		//$this->debug($this->data['profile']);
		$this->data['content'] = $this->load->view('buyer/dashboard',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function flight()
	{
		if(!$this->session->userdata('logged_in')) redirect('login.html',301);   
		$where['is_active'] = 1;
		$where['user_id'] = $this->session->userdata('uid');
		$target_market = $this->db->get_where('general_settings',array('gs_name'=>'seller_target_market'))->result_array();
		$country = $this->db->get('apps_countries')->result_array();
		//$category = $this->db->get_where('category_seller',array('is_active'=>1))->result_array();
		$profile = $this->db->get_where('buyer_list',$where)->row_array();
		$flight = $this->db->get_where('buyer_flight',array("buyer_id"=>$profile['buyer_id']))->row_array();
		$arrivalDate = $this->db->get_where('general_settings',array("gs_name"=>"arrival_dates"))->result_array();
		$departDate = $this->db->get_where('general_settings',array("gs_name"=>"departure_dates"))->result_array();

		
		$this->data['country'] = $country;
		$this->data['target_market'] = $target_market;
		$this->data['profile'] = $profile;
		$this->data['flight'] = $flight;
		$this->data['arrivalDate'] = $arrivalDate;
		$this->data['departDate'] = $departDate;
		
		$this->data['content'] = $this->load->view('buyer/flight',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function listFlight()
	{
		if(!$this->session->userdata('logged_in')) redirect('login.html',301); 
		$this->db->join('buyer', 'buyer.buyer_id = buyer_flight.buyer_id');
		$data = $this->db->get('buyer_flight')->result_array();

		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('buyer/listFlight',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function saveFlight()
	{
		if(!$this->session->userdata('logged_in')) redirect('login.html',301);   
		$post = $this->input->post();

		if(isset($_FILES["image"]["name"]))
		{
			//$this->debug($_FILES);
			$config['upload_path'] = './files/passport/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '1024';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				
				//validation image has error
				SiteHelpers::alert('error',$this->upload->display_errors());
				redirect('Buyer/flight');
				} else {
					$data = $this->upload->data();
					$fileName = $data["file_name"];
				}
		}
		$post['buyer_passport']= $fileName;
		//$this->debug($post);
	
		$this->db->insert('buyer_flight', $post);
		
		
		SiteHelpers::alert('success'," Data has been saved succesfuly !");
		redirect('Buyer/flight',301);
	}


	public function saveEditFlight()
	{
		if(!$this->session->userdata('logged_in')) redirect('login.html',301);   
		$post = $this->input->post();
		$flight = $this->db->get('buyer_flight',array('buyer_id'=>$post['buyer_id']))->row();
		//$this->debug($post);
		 
		if(isset($_FILES["image"]["name"]))
		{
			//$this->debug(count($this->input->post($_FILES["image"])));
			$config['upload_path'] = './files/passport/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '1024';
			
			if(count($this->input->post($_FILES["image"])) > 3){
					//unlink exist data
					if($flight->buyer_passport != '' || $flight->buyer_passport != null){
						$image = $config['upload_path'].$flight->buyer_passport;
						unlink($image);
					}
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if (!$this->upload->do_upload('image')){
						$error = array('error' => $this->upload->display_errors());
						
						//validation image has error
						SiteHelpers::alert('error',$this->upload->display_errors());
						redirect('Memberships/Buyer/'.$post['buyer_id']);
						} else {
							$data = $this->upload->data();
							$fileName = $data["file_name"];
							$flight->buyer_passport = $fileName;

						}
			}
			
		}

		$flight->arrival_date = $post['arrival_date'];
		$flight->arrival_time = $post['arrival_time'];
		$flight->arrival_flight_number = $post['arrival_flight_number'];
		$flight->arrival_terminal = $post['arrival_terminal'];
		$flight->arrival_transport = $post['arrival_transport'];
		$flight->depart_date = $post['depart_date'];
		$flight->depart_time = $post['depart_time'];
		$flight->depart_flight_number = $post['depart_flight_number'];
		$flight->depart_terminal	 = $post['depart_terminal'];
		$flight->depart_transport	 = $post['depart_transport'];

		$this->db->where('buyer_id', $post['buyer_id']);
		$this->db->update('buyer_flight', $flight);
		
		SiteHelpers::alert('success'," Data Flight has been saved succesfuly !");
		redirect('Memberships/Buyer/'.$post['buyer_id']);
	}

	public function create()
	{
		
		$post = $this->input->post();

		$recaptcha = $this->input->post('g-recaptcha-response');
        if (!empty($recaptcha)) {
            $response = $this->recaptcha->verifyResponse($recaptcha);
            if (isset($response['success']) and $response['success'] === true) {
				
		$data["company_name"] = $post["company_name"];

		$data["buyer_name"] = $post["first_name"];
		$data["is_read_terms_condition"] = $post["is_read_terms_condition"];
		$data["first_name"] = $post["first_name"];
		$data["last_name"] = $post["last_name"];
		$data["gender"] = $post["gender"];
		$data["job_title"] = $post["job_title"];

		$data["buyer_city"] = $post["buyer_city"];
		$data["buyer_country_code"] = $post["buyer_country_code"];
		$data["buyer_email"] = $post["email"];
		$data["buyer_phone"] = $post["buyer_phone"];
		
		$data["nature_of_business"] = $post["nature_of_business"];//json_encode($post["target_market"]);
		$data["buyer_company_profile"] =$post["buyer_company_profile"];
		$data["buyer_website"] = $post["buyer_website"];
		$data["is_active"] = 0;
		$data["dtm_crt"] = date('Y-m-d H:i:s');

		$this->load->library('my_libraries');
		$post['seller_name'] = $post["first_name"]." ".$post["last_name"];
		$this->my_libraries->SendBuyerData($post);
		$this->my_libraries->registrationBuyer($post);

		$this->db->insert('buyer', $data);
		
		$id =  $this->db->insert_id();
		
		$info["buyer_id"] = $id;
		$info["do_you_selling_bali_and_beyond_product"] = $post["do_you_selling_bali_and_beyond_product"];
		$info["please_state_you_local_partner_in_bali_indonesia"] = $post["please_state_you_local_partner_in_bali_indonesia"];
		$info["member_id"] = $post["member_id"];
		$info["since_when_you_promoted_indonesia_destination"] = $post["since_when_you_promoted_indonesia_destination"];
		$info["is_this_your_first_time_attending_on_bbtf"] = $post["is_this_your_first_time_attending_on_bbtf"];
		$info["how_did_you_know_about_bbtf"] = $post["how_did_you_know_about_bbtf"];
		//$info["buyer_fax"] = $post["fax"];
		$info["indicated_main_destination"] = $post["indicated_main_destination"];//json_encode($post["target_market"]);
		$info["which_products_are_you_interested_in"] = json_encode($post["which_products_are_you_interested_in"]); 
		$info["do_you_sell_any_package_in_south_east_asia"] = $post["do_you_sell_any_package_in_south_east_asia"];
		$info["biggest_partner"] = $post["biggest_partner"];
		$info["potential_group"] = $post["potential_group"];
		$info["explore_bbtf"] = $post["explore_bbtf"];

		$this->db->insert('buyer_additional_info', $info);

		$userId = $this->insertTblUser($id,$post);

		$msg = "<h1>Registration Successful</h1>";
		$msg.= "<p>Your registration successfully sent.</p>";
		$msg.= "<p>Thank you very much for your participation for BBTF ".$this->getYearEvent().".</p>";
		$msg.= "<p>Please kindly contact us if you have not received Buyer Confirmation within 3 working days.</p>";
		$msg.= "<p>Should you need further information, please feel free to contact us.</p>";
		$msg.= "<br>";
		$msg.= "<br>";
		$msg.= "<p>Sincerely yours</p>";
		$msg.= "<br>";
		$msg.= "<p>Committee of BBTF ".date('Y')."</p>";

		$this->session->set_flashdata('success_register',$msg);

		redirect('',301);
            } else {
				SiteHelpers::alert('error'," upss captcha invalid!!");
				redirect('buyer-registration.html');
			}
		}
		

		
	}

	public function Registration(){
		$this->load->library('recaptcha');
		
		$this->data['recaptcha_html'] = $this->recaptcha->getWidget();
		$this->data['recaptcha_script'] = $this->recaptcha->getScriptTag();


		$country = $this->db->get('apps_countries')->result_array();
		$category = $this->db->get_where('category_seller',array('is_active'=>1))->result_array();
		$target_market = $this->db->get_where('general_settings',array('gs_name'=>'seller_target_market'))->result_array();
		$terms = $this->db->get_where('general_settings',array('gs_code'=>'buyer_terms_condition'))->row_array();

		$this->data['country'] = $country;
		$this->data['category'] = $category;
		$this->data['target_market'] = $target_market;
		$this->data['terms'] = $terms;

		$this->data['company'] = $this->db->get('company')->row_array();
		$this->data['pageTitle'] = "Buyer Registration";


		$this->data['content'] = $this->load->view('frontend/buyer-registration',$this->data,true);    
    	$this->load->view('layouts/registration/main',$this->data);

	}

	public function saveBuyer(){
		$post = $this->input->post();

		$data["company_name"] = $post["company_name"];
		$data["buyer_name"] = $post["first_name"];
		//$data["is_read_terms_condition"] = $post["is_read_terms_condition"];
		$data["first_name"] = $post["first_name"];
		$data["last_name"] = $post["last_name"];
		$data["gender"] = $post["gender"];
		$data["job_title"] = $post["job_title"];

		$data["buyer_city"] = $post["buyer_city"];
		$data["buyer_country_code"] = $post["buyer_country_code"];
		$data["buyer_email"] = $post["email"];
		$data["buyer_phone"] = $post["buyer_phone"];
		
		$data["nature_of_business"] = $post["nature_of_business"];//json_encode($post["target_market"]);
		$data["buyer_company_profile"] =$post["buyer_company_profile"];
		$data["buyer_website"] = $post["buyer_website"];
		$data["is_active"] = 1;
		$data["buyer_class"] = $post['buyer_class'];
		$data["dtm_upd"] = date('Y-m-d H:i:s');

		if($post['password'] != "")
		{
				$user['password'] = md5($post['password']);
				$this->db->where('id', $post['user_id']);
				$this->db->update('tb_users', $user);
		}

		$this->db->where('buyer_id', $post['buyer_id']);
		$this->db->update('buyer', $data);
		
		//$info["buyer_id"] = $id;
		$info["do_you_selling_bali_and_beyond_product"] = $post["do_you_selling_bali_and_beyond_product"];
		$info["please_state_you_local_partner_in_bali_indonesia"] = $post["please_state_you_local_partner_in_bali_indonesia"];
		$info["member_id"] = $post["member_id"];
		$info["since_when_you_promoted_indonesia_destination"] = $post["since_when_you_promoted_indonesia_destination"];
		$info["is_this_your_first_time_attending_on_bbtf"] = $post["is_this_your_first_time_attending_on_bbtf"];
		$info["how_did_you_know_about_bbtf"] = $post["how_did_you_know_about_bbtf"];
		//$info["buyer_fax"] = $post["fax"];
		$info["indicated_main_destination"] = $post["indicated_main_destination"];//json_encode($post["target_market"]);
		$info["which_products_are_you_interested_in"] = json_encode($post["which_products_are_you_interested_in"]); 
		$info["do_you_sell_any_package_in_south_east_asia"] = $post["do_you_sell_any_package_in_south_east_asia"];
		$info["biggest_partner"] = $post["biggest_partner"];
		$info["potential_group"] = $post["potential_group"];
		$info["explore_bbtf"] = $post["explore_bbtf"];

		$this->db->where('buyer_id', $post['buyer_id']);
		$this->db->update('buyer_additional_info', $info);

	
			
		SiteHelpers::alert('success'," Data has been saved succesfuly !");
		redirect('Memberships/Buyer',301);
	}
	
	public function confirmClass($id)
	{
		//$where['is_active'] = 1;
		$where['user_id'] = $this->session->userdata('uid');
		$profile = $this->db->get_where('buyer_list',$where)->row_array();
		$product = "";
		if($profile['buyer_class'] == 1 ) 
		{
			$buyer['membership_verify'] = 1;
			$this->db->where('buyer_id', $id);
			$this->db->update('buyer', $buyer);
			
			$product = $this->db->get_where('product',array('category_id'=>2,'product_name'=>'FULLY HOSTED'))->row_array();
			if($product != "") {
				$this->load->library('my_libraries');

				$email['email'] = $profile['buyer_email'];
				$email['gender'] = $profile['gender'];
				$email['buyer_type'] = "Fully Hosted";
				$email['buyer_name'] = $profile['first_name'];
				$email['fee'] = number_format($product['price'],2);
				$email['invoice_date'] = date('Y-m-d');
				$email['invoice_due_date'] = date('Y-m-d',strtotime($email['invoice_date']."+7 days"));

				$this->my_libraries->buyerInvoice($email);
			}
			
		} else if ($profile['buyer_class'] == 2)
		{
			
			$product = $this->db->get_where('product',array('category_id'=>2,'product_name'=>'PARTIAL HOSTED'))->row_array();
			if($product != "") {
				$this->load->library('my_libraries');
			$email['email'] = $profile['buyer_email'];
			$email['gender'] = $profile['gender'];
			$email['buyer_type'] = "Partially Hosted";
			$email['buyer_name'] = $profile['first_name'];
			$email['fee'] = number_format($product['price'],2);
			$email['invoice_date'] = date('Y-m-d');
			$email['invoice_due_date'] = date('Y-m-d',strtotime($email['invoice_date']."+7 days"));
			$this->my_libraries->buyerInvoice($email);
			}
			
		}

		if($product != "") {

		$data["trx_code"] = $this->trx_code("TRX");;
		$data["trx_datetime"] =  date('Y-m-d H:i:s');
		$data["trx_subtotal"] =$product['product_price'];
		$data["trx_status"] = 0;
		$data["user_id"] =$where['user_id'];
		$data["trx_date"] = date('Y-m-d H:i:s');
		$data["trx_type"] =BUYER_REGISTRATION;
		$data["dtm_crt"] =date('Y-m-d H:i:s');
		$data["trx_tax"] = 0;
		$data["trx_service_tax"] = 0;
		$data["trx_total"] = $product['product_price'];
		$data["trx_payment_type"] =1;
		$data["trx_invoice"] = "";

		$this->db->insert('transaction_h', $data);
		$id =  $this->db->insert_id();

		$detail["trx_code"] = $data["trx_code"];
		$detail["product_id"] = $product["id"];
		$detail["amount"] = 1;
		$detail["price"] = $product['product_price'];
		$detail['discount'] = 0;
		$detail['discount_price'] = 0;

				$this->db->insert('transaction_d', $detail);

				//insert_stock
				$stock['amount'] = $detail['amount'] * -1;
				$stock['product_id'] = $detail['product_id'];
				$stock['company_name'] = '-';
				$stock['ref_trans'] =  $data["trx_code"];
				$stock['type'] =  'TRANS';
				$stock['dtm_crt'] =  date('Y-m-d');

				$this->db->insert('stock', $stock);
				$buyer['membership_verify'] = 1;
				$this->db->where('buyer_id', $id);
				$this->db->update('buyer', $buyer);
		}
		
	
				SiteHelpers::alert('success'," Data has been Submit, Please Make Payment !");
				redirect('Transaction',301);
	}

	public function check_email_exist() {
		$email = $this->input->get('email');
		//$this->debug($email);

		$this->db->where('email', $email);
		$this->db->where('active', 1);
		$this->db->where('group_id', 4);
		$row = $this->db->get('tb_users')->num_rows();
		if($row == 1){
			echo json_encode(array('status'=> true));

		} else {
			echo json_encode(array('status'=> false));
		}

	}

	public function insertTblUser($sellerId = null,$post = array () ){

		$data["email"] = $post["email"];
		$data["password"] = "";
		$data["username"] = $post["first_name"];
		$data["first_name"] = $post["last_name"];
		$data["active"] = 1;
		$data["created_at"] = date('Y-m-d');
		$data["group_id"] = 4;
		$data["ref_user"] = (int)$sellerId;

		$this->db->insert('tb_users', $data);
		
		$id =  $this->db->insert_id();
		
		return $id;

	}

	public function insertTblSeller($post = array () ){
		
		$data["buyer_name"] = $post["company_name"];
		$data["buyer_address"] = $post["adress"];
		$data["buyer_city"] = $post["city"];
		$data["buyer_country_code"] = $post["country"];
		$data["buyer_email"] = $post["email"];
		$data["buyer_phone"] = $post["phone"];
		//$data["buyer_fax"] = $post["fax"];
		$data["nature_of_business"] = $post["nature_of_business"];//json_encode($post["target_market"]);
		$data["buyer_company_profile"] =$post["company_profile"];
		$data["buyer_website"] = $post["company_website"];
		//$data["membership_id"] = $post["membership_id"];
		$data["is_active"] = 0;
		$data["dtm_crt"] = date('Y-m-d H:i:s');



		$this->db->insert('buyer', $data);
		
		$id =  $this->db->insert_id();

		$info["buyer_id"] = $id;
		$info["do_you_selling_bali_and_beyond_product"] = $post["do_you_selling_bali_and_beyond_product"];
		$info["please_state_you_local_partner_in_bali_indonesia"] = $post["please_state_you_local_partner_in_bali_indonesia"];
		$info["member_id"] = $post["member_id"];
		$info["since_when_you_promoted_indonesia_destination"] = $post["since_when_you_promoted_indonesia_destination"];
		$info["is_this_your_first_time_attending_on_bbtf"] = $post["is_this_your_first_time_attending_on_bbtf"];
		$info["how_did_you_know_about_bbtf"] = $post["how_did_you_know_about_bbtf"];
		//$info["buyer_fax"] = $post["fax"];
		$info["indicated_main_destination"] = $post["indicated_main_destination"];//json_encode($post["target_market"]);
		$info["which_products_are_you_interested_in"] = json_encode($post["which_products_are_you_interested_in"]); 
		$info["do_you_sell_any_package_in_south_east_asia"] = $post["do_you_sell_any_package_in_south_east_asia"];
		$info["biggest_partner"] = $post["biggest_partner"];
		$info["potential_group"] = $post["potential_group"];
		$info["explore_bbtf"] = $post["explore_bbtf"];

		$this->db->insert('buyer_additional_info', $info);
		
		return $id;

	}

	public function insertTblTrx($userId = "",$post = array () ){

		$data["trx_code"] = $this->trx_code("TRX");;
		$data["trx_datetime"] =  date('Y-m-d H:i:s');
		$data["trx_subtotal"] =$this->_setSubTotal($post);
		$data["trx_status"] = $post["country"];
		$data["user_id"] = $userId;
		$data["trx_date"] = date('Y-m-d H:i:s');
		$data["trx_type"] =BUYER_REGISTRATION;
		$data["dtm_crt"] =date('Y-m-d H:i:s');
		$data["trx_tax"] = ( ((int)$this->company()->tax/100) * (int)$data["trx_subtotal"]);
		$data["trx_service_tax"] = ( ((int)$this->company()->service_tax/100) * (int)$data["trx_subtotal"]);
		$data["trx_total"] = $data["trx_subtotal"] + ($data["trx_subtotal"] * ($this->company()->tax/100)) + ( $data["trx_subtotal"] * ($this->company()->service_tax/100));
		$data["trx_payment_type"] = $post["paymentType"];
		$data["trx_invoice"] = "";

		$this->db->insert('transaction_h', $data);
		$id =  $this->db->insert_id();

		$this->_insertTrxDetail($data["trx_code"],$post);
		
		return $id;

	}

	private function _insertTrxDetail($trxCode,$post="")
	{
		
		for ($i=0; $i < count($post['product_id']); $i++) { 
			# code...
			if($post['amount'][$i] != "" || $post['amount'][$i] != null)
			{
				$where = array(
					'product_id'  => $post["product_id"][$i]
				  );
			  
				  $row = $this->db->get_where('v_product_stock',$where)->row();

				$data["trx_code"] = $trxCode;
				$data["product_id"] = $post["product_id"][$i];
				$data["amount"] = $post["amount"][$i];
				$price = $row->product_price;
				$discountPrice = 0;
				$dsc = 0; 
			  if( $row->discount_date != null && (strtotime($row->discount_date) >= strtotime(date('Y-m-d')))){
				  $price = $row->price_after_discount;
				  $discountPrice = ($row->discount/100) * $row->product_price;
				  $dsc = $row->discount;
			  }

				$data["price"] = $price;
				$data['discount'] = $dsc;
				$data['discount_price'] = $discountPrice;


				$this->db->insert('transaction_d', $data);

				//insert_stock
				$stock['amount'] = $data['amount'] * -1;
				$stock['product_id'] = $data['product_id'];
				$stock['company_name'] = '-';
				$stock['ref_trans'] =  $trxCode;
				$stock['type'] =  'TRANS';
				$stock['dtm_crt'] =  date('Y-m-d');

				$this->db->insert('stock', $stock);
				


				
			}
		}

	}

	public function insertTblTrxMembership($userId = "",$post = array () ){


		$membership = $this->db->get_where('membership',array('membership_id'=>(int)$post["membership_id"]))->row();
		$data["trx_code"] = $this->trx_code("MEM");;
		$data["trx_datetime"] =  date('Y-m-d H:i:s');
		$data["trx_subtotal"] =  $membership->membership_fee;//$this->_setSubTotal($post);
		$data["trx_status"] = 0;//$post["country"];
		$data["user_id"] = $userId;
		$data["trx_date"] = date('Y-m-d H:i:s');
		$data["trx_type"] =MEMBERSHIP_ORDER;
		$data["dtm_crt"] =date('Y-m-d H:i:s');
		$data["trx_tax"] = 0;//( ((int)$this->company()->tax/100) * (int)$data["trx_subtotal"]);
		$data["trx_service_tax"] =0; //( ((int)$this->company()->service_tax/100) * (int)$data["trx_subtotal"]);
		$data["trx_total"] = $membership->membership_fee; //$this->_setSubTotal($post);//$data["trx_subtotal"] + ($data["trx_subtotal"] * ($this->company()->tax/100)) + ( $data["trx_subtotal"] * ($this->company()->service_tax/100));
		$data["trx_payment_type"] = $post["paymentType"];
		$data["trx_invoice"] = "";

		$this->db->insert('transaction_h', $data);
		$id =  $this->db->insert_id();

		$this->_insertTrxDetailMembership($data["trx_code"],$post);
		
		return $id;

	}

	private function _insertTrxDetailMembership($trxCode,$post="")
	{
			$membership = $this->db->get_where('membership',array('membership_id'=>(int)$post["membership_id"]))->row();
			
				$data["trx_code"] = $trxCode;
				$data["product_id"] = $post["membership_id"];
				$data["amount"] = 1;
				$data["price"] = $membership->membership_fee;
				$data['discount'] = 0;
				$data['discount_price'] = 0;


				$this->db->insert('transaction_d', $data);

	}

	private function _setSubTotal($post="")
	{
		$subTotal = 0;
		$reg = array();
		for ($i=0; $i < count($post['product_id']); $i++) { 
			# code...
			if($post['amount'][$i] != "" || $post['amount'][$i] != null)
			{
				$reg[$i]["product_id"] = $post["product_id"][$i];
				$reg[$i]["amount"] = $post["amount"][$i];

				array_values($reg);
			}
		}

		foreach ($reg as $i => $v) {
			# code...
			$where = array(
				'product_id'  => $v["product_id"]
			  );
		  
			  $row = $this->db->get_where('v_product_stock',$where)->row();
			  //$this->debug($row);
			  $price = $row->product_price;
			  if( $row->discount_date != null && (strtotime($row->discount_date) >= strtotime(date('Y-m-d')))){
				  $price = $row->price_after_discount;
			  }
			  
				$subTotal = $subTotal + ($price * (int)$v["amount"]);
		}

			return $subTotal;
	}

	public function declineClass($invoiceNo)
	{
		# code...
		$dokuTmp = $this->db->get_where('doku_tmp',array('transidmerchant'=>$invoiceNo))->row_array();
		$profile = $this->db->get_where('buyer',array('buyer_id'=>$dokuTmp['ref_user_id']))->row_array();
		$data['trxstatus'] = "DECLINE";
		$this->db->where('transidmerchant', $invoiceNo);
		$this->db->update('doku_tmp', $data);

		$profileBuyer['buyer_class'] = 5;
		$this->db->where('buyer_id', $profile['buyer_id']);
		$this->db->update('buyer', $profileBuyer);

		$this->load->library('my_libraries');

		$msg = "<h1>You Have Decline Status</h1>";
		$msg.= "<p>Thank you very much for your participation for BBTF ".$this->getYearEvent().".</p>";
		$msg.= "<p>Should you have any questions or concerns, please feel free to contact us.</p>";
		$msg.= "<br>";
		$msg.= "<br>";
		$msg.= "<p>Thank you for your attention. Sincerely yours</p>";
		$msg.= "<br>";
		$msg.= "<p>BBTF Committe</p>";

		$this->session->set_flashdata('success_register',$msg);
		redirect('',301);
		
	}

	public function delete($id)
	{
		# code...
		
		$this->db->where('buyer_id', $id);
		$this->db->delete('buyer');

		$this->db->where('ref_user', $id);
		$this->db->delete('tb_users');

		$this->db->where('buyer_id', $id);
		$this->db->delete('buyer_additional_info');

		SiteHelpers::alert('success'," Data has been saved succesfuly !");
		redirect('Memberships/Buyer',301);

		
	}

	public function getProfile()
	{
		$id = $this->input->post('buyerId');
		$type = $this->input->post('type');

		if($type == "BUYER")
		{
			$profile = $this->db->get_where('buyer',array('buyer_id'=>$id))->result_array();

		} else {
			$profile = $this->db->get_where('trade_buyer',array('id'=>$id))->result_array();
		}
		
		$data["data"] = $profile;
		

		echo json_encode($profile);
	}

	public function ResetPassword()
	{
		$post = $this->input->post();
		$id = $post['buyerId'];
		$newPassword = $post['newPassword'];
		$profile = $this->db->get_where('buyer',array('buyer_id'=>$id))->row_array();
		$user = $this->db->get_where('tb_users',array('ref_user'=>$profile['buyer_id']))->row_array();
		$update['password'] = md5($newPassword);
		$update['password_view'] = $newPassword;
		
		$this->db->where('id', $user['id']);
		$this->db->update('tb_users', $update);

		$this->load->library('my_libraries');
		$email['buyer_name'] = $profile['buyer_name'];
		$email['email'] = $profile['buyer_email'];
		$email['gender'] = $profile['gender'];
		$email['password'] = $newPassword;
		$this->my_libraries->resetPasswordBuyer($email);

		SiteHelpers::alert('success'," Reset Passwod has been succesfuly !");
		redirect('Memberships/Buyer',301);

	}

	public function sendLoginAccess($id)
	{
		$profile = $this->db->get_where('buyer',array('buyer_id'=>$id))->row_array();
		$user = $this->db->get_where('tb_users',array('ref_user'=>$profile['buyer_id']))->row_array();

		if($user['password'] != "" || $user['password'] != null)
		{
			SiteHelpers::alert('error'," Password has been set !");
			//$this->debug("test");
			redirect('Memberships/Buyer',301);
		} else {

			$newPassword = $this->generateString(6);
			$update['password'] = md5($newPassword);
			$update['password_view'] = $newPassword;

			$this->db->where('id', $user['id']);
			$this->db->update('tb_users', $update);
	
			$this->load->library('my_libraries');
			$email['buyer_name'] = $profile['first_name']." ".$profile['last_name'];
			$email['email'] = $profile['buyer_email'];
			$email['gender'] = $profile['gender'];
			$email['password'] = $newPassword;
			$this->my_libraries->sendLoginAccessBuyer($email);
			
			SiteHelpers::alert('success'," Send Access has been succesfuly !");
			redirect('Memberships/Buyer',301);
		}
		

	}

	public function listing($id)
	{
		$profile = $this->db->get_where('buyer',array('buyer_id'=>$id))->row_array();
		$user = $this->db->get_where('tb_users',array('ref_user'=>$profile['buyer_id']))->row_array();
		$val = "";
		if($profile['is_listing'] == 1) {$val = 0;}
		if($profile['is_listing'] == 0 || $profile['is_listing'] == "" ) {$val = 1;}
		$update['is_listing'] = $val;
		// /$this->debug($update);
		$this->db->where('buyer_id', $id);
		$this->db->update('buyer', $update);
		

		SiteHelpers::alert('success'," Listing has been succesfuly !");
		redirect('Memberships/Buyer',301);

	}

	public function setIgnoreStatus($id)
	{
		$profile = $this->db->get_where('buyer',array('buyer_id'=>$id))->row_array();
		$user = $this->db->get_where('tb_users',array('ref_user'=>$profile['buyer_id']))->row_array();
		
		$update['buyer_class'] = 4;
		// /$this->debug($update);
		$this->db->where('buyer_id', $id);
		$this->db->update('buyer', $update);
		
		$this->load->library('my_libraries');
		$email['email'] = $profile['buyer_email'];
		$email['gender'] = $profile['gender'];
		$email['buyer_name'] = $profile['first_name']." ".$profile['last_name'];
		$this->my_libraries->sendIgnoreBuyer($email);

		SiteHelpers::alert('success'," Ignore has been succesfuly !");
		redirect('Memberships/Buyer',301);

	}

	public function resendInv($id)
	{
		$where['user_id'] = $this->session->userdata('uid');
		$profile = $this->db->get_where('buyer_list',$where)->row_array();
		$product = "";
		if($profile['buyer_class'] == 1 ) 
		{
			$buyer['membership_verify'] = 1;
			$this->db->where('buyer_id', $id);
			$this->db->update('buyer', $buyer);
			
			$product = $this->db->get_where('product',array('category_id'=>2,'product_name'=>'FULLY HOSTED'))->row_array();
			if($product != "") {
				$this->load->library('my_libraries');

				$email['email'] = $profile['buyer_email'];
				$email['gender'] = $profile['gender'];
				$email['buyer_type'] = "Fully Hosted";
				$email['buyer_name'] = $profile['first_name'];
				$email['fee'] = number_format($product['price'],2);
				$email['invoice_date'] = date('Y-m-d');
				$email['invoice_due_date'] = date('Y-m-d',strtotime($email['invoice_date']."+7 days"));

				$this->my_libraries->buyerInvoice($email);
			}
			
		} else if ($profile['buyer_class'] == 2)
		{
			
			$product = $this->db->get_where('product',array('category_id'=>2,'product_name'=>'PARTIAL HOSTED'))->row_array();
			if($product != "") {
				$this->load->library('my_libraries');
			$email['email'] = $profile['buyer_email'];
			$email['gender'] = $profile['gender'];
			$email['buyer_type'] = "Partially Hosted";
			$email['buyer_name'] = $profile['first_name'];
			$email['fee'] = number_format($product['price'],2);
			$email['invoice_date'] = date('Y-m-d');
			$email['invoice_due_date'] = date('Y-m-d',strtotime($email['invoice_date']."+7 days"));
			$this->my_libraries->buyerInvoice($email);
			}
			
		}
		

		SiteHelpers::alert('success'," Resend succesfuly !");
		redirect('Memberships/Buyer',301);

	}

	public function getProfilePsa($id){
		$this->db->where('buyer_id', $id);
		$res = $this->db->get('buyer_list');
		$whichProduct = json_decode($res->row()->which_products_are_you_interested_in);
		$data = $res->row_array();
		$data['which_products_are_you_interested_in'] = implode(" , ",$whichProduct);
		$data['nature_of_business'] = $this->getNatureOfBusiness($res->row()->nature_of_business);

		$this->data['profile'] = $data;
		$html = $this->load->view('buyer/profile', $this->data, true);
		
		echo $html;
	}

}

/* End of file Controllername.php */
