<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);
	}

	public function index()
	{

		
	}

	public function MenuColor()
	{
		$data =$this->db->get_where('general_settings',array('gs_name'=>'home_menu_color'))->result_array();
		
		$this->data['data'] = $data;
		$this->data['isEdit'] = true;

		$this->data['content'] = $this->load->view('settings/menuColor',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function ScheduleTableColor()
	{
		$data =$this->db->get_where('general_settings',array('gs_name'=>'schedule_table_color'))->result_array();
		
		$this->data['data'] = $data;
		$this->data['isEdit'] = true;

		$this->data['content'] = $this->load->view('settings/scheduleTableColor',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Psa()
	{
		# code...
		$data =$this->db->get_where('general_settings',array('gs_name'=>'psa_settings'))->result_array();
		$action = base_url().'Settings/psaSettingsSave';
		$this->data['data'] = $data;
		$this->data['isEdit'] = true;
		$this->data['action'] = $action;
		$this->data['title'] = 'PSA SETTINGS';

		$this->data['content'] = $this->load->view('settings/settingForm',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function psaSettingsSave()
	{
		$post = $this->input->post();
		
		for ($i=0; $i < count($post['id']); $i++) { 
			# code...
			//$this->debug(count($post['id']));
			$update['gs_value'] = $post['color'][$i];
			$this->db->where('id', $post['id'][$i]);
			$this->db->update('general_settings', $update);
			
		}
		
		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Settings/Psa',301);
	}

	public function menuColorSave()
	{

		
		$post = $this->input->post();
		
		for ($i=0; $i < count($post['id']); $i++) { 
			# code...
			//$this->debug(count($post['id']));
			$update['gs_value'] = $post['color'][$i];
			$this->db->where('id', $post['id'][$i]);
			$this->db->update('general_settings', $update);
			
		}
		
		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Settings/MenuColor',301);
	}

	public function ScheduleTableColorSave()
	{

		
		$post = $this->input->post();
		
		for ($i=0; $i < count($post['id']); $i++) { 
			# code...
			//$this->debug(count($post['id']));
			$update['gs_value'] = $post['color'][$i];
			$this->db->where('id', $post['id'][$i]);
			$this->db->update('general_settings', $update);
			
		}
		
		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Settings/ScheduleTableColor',301);
	}

}

/* End of file Settings.php */

?>
