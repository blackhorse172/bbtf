<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Booths extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);
		$this->load->model('Transactions');
	}
	

	public function index()
	{
		
	}


	public function name()
	{
		$this->db->where('trx_status', 2);
		$data = $this->Transactions->where("user_id",(int)$this->session->userdata('uid'))->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('booth/index_name',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function delegations()
	{
		$this->db->where('trx_status', 2);
		$data = $this->Transactions->where("user_id",(int)$this->session->userdata('uid'))->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('booth/index_delegations',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function createName($trxCode)
	{
		$data = $this->db->get_where('seller_sub',array('ref_trans'=>$trxCode))->result_array(); //get_where('ref_trans');
		
		$this->data['data'] = $data;
		$this->data['trxCode'] = $trxCode;
		$this->data['content'] = $this->load->view('booth/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function createDelegations($trxCode)
	{
		$data = $this->db->get_where('seller_sub',array('ref_trans'=>$trxCode))->result_array();
		//$user = $this->db->get_where('tb_users',array('ref_user'=>$this->session->userdata('uid'),'group_id'=>5))->row_array(); //get_where('ref_trans');
		//$this->debug($data);
		$this->data['data'] = $data;
		$this->data['trxCode'] = $trxCode;
		$this->data['content'] = $this->load->view('booth/create_delegations',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	

	public function updateNumber()
	{
		# code...
		$post = $this->input->post();
		for ($i=0; $i < count($post['booth_id']); $i++) { 
			# code...
			$object['booth_number'] = $post['booth_number'][$i];
			$this->db->where('id', $post['booth_id'][$i]);
			$this->db->update('seller_sub', $object);
		}
		SiteHelpers::alert('success'," Booth Number has been saved succesfuly !");

		redirect('Admin/Transactions',301);
	}

	public function updateName()
	{
		# code...
		$post = $this->input->post();
		for ($i=0; $i < count($post['booth_id']); $i++) { 
			# code...
			$object['booth_name'] = $post['booth_name'][$i];
			$this->db->where('id', $post['booth_id'][$i]);
			$this->db->update('seller_sub', $object);
		}
		SiteHelpers::alert('success'," Booth Name has been saved succesfuly !");

		redirect('Booths/name',301);
	}

	public function updateDelegations()
	{
		# code...
		$post = $this->input->post();
		$this->load->library('my_libraries');

		for ($i=0; $i < count($post['seller_name']); $i++) { 
			# code...
			if($post['seller_name'][$i] != "" && $post['email'][$i] != "" && $post['password'][$i] != "")
			{
				

				$object['seller_name'] = $post['seller_name'][$i];

			

				if(isset($post['email'][$i])){
					$object['email_login'] = $post['email'][$i];
				}
			
				if(isset($post['password'][$i])){
					$object['password_login'] = md5($post['password'][$i]);
					$object['password_view'] = $post['password'][$i];
				}
	
				//$this->debug($post);
				$this->db->where('id', $post['delegation_id'][$i]);
				$this->db->update('seller_sub', $object);
	
				$email['seller_name'] = $post['seller_name'][$i];
				$email['email'] = $post["email"][$i];
				$email['seller_name'] = $post['seller_name'][$i];
				$email['password'] = $post['password'][$i];
	
				$this->my_libraries->registrationSubSeller($email);
			}
			


		}
		SiteHelpers::alert('success'," Booth Delegation has been saved succesfuly !");

		redirect('Booths/delegations',301);
	}

	public function updateFullDelegation()
	{
		# code...
		$post = $this->input->post();
		for ($i=0; $i < count($post['booth_id']); $i++) { 
			# code...
			$object['delegation_name'] = $post['delegation_name'][$i];
			$object['email'] = $post['email'][$i];
			$object['password'] = md5($post['password'][$i]);

			
			$this->db->where('id', $post['booth_id'][$i]);
			$this->db->update('booth_delegation', $object);
		}
		SiteHelpers::alert('success'," Booth Number has been saved succesfuly !");

		redirect('Booths/delegations',301);
	}

}

/* End of file Boots.php */

?>
