<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('Schedules');
		
	}
	

	public function index()
	{
		$data = $this->Schedules->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('schedule/index',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function create($id = null)
	{
		$data = array();

		if($id != null){
			$data = $this->Schedules->get($id);
			$this->data['isEdit'] = true;
			$this->data['data'] = $data;


		} else {
			$this->data['isEdit'] = false;

		}

		$this->data['content'] = $this->load->view('schedule/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function save($id = null)
	{
		if($id != null){
			$data = $this->input->post();
			$data['usr_upd'] = $this->session->userdata('userName');
			$data['dtm_upd'] = date('Y-m-d H:i:s');
			if($_FILES["image"]["name"] !="")
			{
				$this->uploadImage();
				$data['file'] = $_FILES["image"]["name"];
			}
			
			$this->Schedules->save_data($data,$id);
			SiteHelpers::alert('success','Schedule Edited !');
			redirect('Schedule',301);
		} else {
			$data = $this->input->post();
			$data['usr_crt'] = $this->session->userdata('userName');
			$data['dtm_upd'] = date('Y-m-d H:i:s');

			$this->Schedules->save_data($data);


			SiteHelpers::alert('success','Schedule Created !');
		redirect('Admin/Schedule',301);

		}
	}

	public function uploadImage()	{
		if($_FILES["image"]["name"] !="")
		{
			
			$config['upload_path'] = './files/schedule/';
			$config['allowed_types'] = 'pdf|xls|ppt|pptx|rar|zip|doc|docx|xlsx';
			$config['max_size']  = '2024';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			//$this->debug($this->upload->do_upload("image"));
			if (!$this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				echo json_encode($error);
			}
			else{
				
				$data = $this->upload->data();
					//Compress Image
					// $config['image_library']='gd2';
					// $config['source_image']='./assets/images/'.$data['file_name'];
					// $config['create_thumb']= FALSE;
					// $config['maintain_ratio']= TRUE;
					// $config['quality']= '60%';
					// $config['width']= 800;
					// $config['height']= 800;
					// $config['new_image']= './files/product/'.$data['file_name'];
					// $this->load->library('image_lib', $config);
					// $this->image_lib->resize();

					echo base_url().'files/schedule/'.$data['file_name'];
			}
		}
		
	}

	public function deleteImage(){
		$src = $this->input->post('src');
		$file_name = str_replace(base_url(), '', $src);
		
		if(unlink($file_name)){
			echo 'File Delete Successfully';
		}
	}

}

/* End of file Schedule.php */
