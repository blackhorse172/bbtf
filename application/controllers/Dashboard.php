<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	function __construct()
    {
        parent::__construct();    
    if(!$this->session->userdata('logged_in')) redirect('user/login',301);      
    }

	public function index()
	{
		if($this->session->userdata('gid') == 1 || $this->session->userdata('gid') == 2) {
			$this->data['content'] = $this->load->view('dashboard/Index',$this->data,true);    

		} else if($this->session->userdata('gid') == 4) {
			redirect("Buyer/index");
			//$this->data['content'] = $this->load->view('dashboard/index_buyer',$this->data,true);    

		}else if($this->session->userdata('gid') == 5) {
			    redirect("Seller/index");
		} else {
			$this->data['content'] = $this->load->view('dashboard/Index_user',$this->data,true);    
		}
    $this->load->view('layouts/main',$this->data);
	}

}

/* End of file Controllername.php */
