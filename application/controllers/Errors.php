<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Errors extends MY_Controller {

	public function index()
	{
		$this->data['title'] = $this->input->get('title');
			$this->data['message'] = $this->input->get('message');
			$this->data['content'] = $this->load->view('errors/custom/404',$this->data,true);    
			$this->load->view('layouts/main',$this->data);
	}

}

/* End of file Error.php */
