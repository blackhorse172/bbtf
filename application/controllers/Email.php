<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends MY_Controller {

	public function index()
	{
		$this->data['content'] = $this->load->view('email/index',$this->data,true);
		$this->load->view('layouts/main',$this->data);
		
	}

	public function send()
	{
		$post = $this->input->post();
		
			$this->load->library('my_libraries');
			$email['email'] = $post['email'];
			$email['message'] = $post['message'];
			echo $this->my_libraries->test($email);
	}

	public function check()
	{
		$this->data['content'] = $this->load->view('layouts/email/login_access',$this->data,true);
		$this->load->view('layouts/main',$this->data);
		
	}


}

/* End of file Controllername.php */

?>
