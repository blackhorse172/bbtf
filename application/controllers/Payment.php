<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);

		$this->load->model('Payments');
		//$this->load->model('Transactions');

		//$this->load->model('PaymentCategories');
		
		//Do your magic here
	}
	

	public function index()
	{
		
		$data = $this->Payments->where("user_id",(int)$this->session->userdata("uid"))->get_list();
		
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('payment/index',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Create($trxCode)
	{
		$trxData = $this->db->get_where('transaction_h',array("trx_code"=>$trxCode))->row_array();
		//$this->debug($trxData);
		if($trxData == null) {
			redirect('');
		}

		$trx = $this->db->get_where('transaction_h',array('trx_code'=>$trxCode))->row_array();
		//$trxdetail = $this->db->get_where('v_product_detail',array('trx_code'=>$trxCode))->result_array();
		$user = $this->db->get_where('tb_users',array("id"=>$trx['user_id']))->row_array();
		$profile = $this->db->get_where('seller',array("seller_id"=>$user['ref_user']))->row_array();
		
		//$this->debug($profile);
		$this->data['isEdit'] = false;
		$this->data['trxCode'] = $trxCode;
		$this->data['profile'] = $profile;
		$this->data['content'] = $this->load->view('payment/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function edit($id)
	{
		$data = $this->payments->get($id);
		if($data == '')
		{
			redirect('error/notFound');
		}
		//$this->debug($data);
		$group = $this->paymentCategories->get_list();
		$this->data['isEdit'] = true;
		$this->data['group'] = $group;
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('payment/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function save()
	{
		
		$payment["trx_code"] = $this->input->post('trx_code');
		$payment["payment_date"] = date('Y-m-d H:i:s');
		$payment["notes"] = $this->input->post('notes');
		$payment["bank_name"] = $this->input->post('bank_name');
		$payment["bank_acc_name"] = $this->input->post('bank_acc_name');
		$payment["bank_acc_number"] = $this->input->post('bank_acc');
		$payment["payment_code"] = $this->trx_code("PM");
		$payment["payment_status"] = 0;
		$payment["user_id"] = $this->session->userdata("uid");

		$fileName = '';
		
		if(isset($_FILES["image"]["name"]))
		{
			//$this->debug($_FILES);
			$config['upload_path'] = './files/payment/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				
				//validation image has error
				SiteHelpers::alert('error',$this->upload->display_errors());
				redirect('Transaction');
				} else {
					$data = $this->upload->data();
					$fileName = $data["file_name"];
				}
		}
		
		$payment["image"] =  $fileName;
		$this->db->insert('payment', $payment);
		$id = $this->db->insert_id();
		$this->updateTrx($payment["trx_code"]);
		SiteHelpers::alert('success',$payment["trx_code"]." Has Created Payment !");

		redirect('transaction',301);
	}

	public function updateTrx($trxCode)
	{
		# code...
		$post = $this->input->post();
		$type = 1;
		if(isset($post['trxType'])){
			$type=$post['trxType'];
		}
		$data["trx_status"] = 1;//confirm
		$data["trx_payment_type"] = $type;
		$data["dtm_upd"] = date("Y-m-d H:i:s");
		//$this->debug($data);

		$this->db->where('trx_code', $trxCode);
		$this->db->update('transaction_h', $data);
		

	}

	public function saveEdit()
	{
		$id = $this->input->post('id');
		$payment = $this->Payments->get($id);
		//$this->debug($this->input->post());
		
		$payment->payment_name = $this->input->post('name');
		$payment->payment_fee = (int)str_replace('.','',$this->input->post('fee')) ;	
		$payment->category_id = $this->input->post('category_id');
		$payment->dtm_upd = date('Y-m-d H:i:s');
		$payment->usr_upd = $this->session->userdata('userName');

		$id = $payment->save();
		SiteHelpers::alert('success'," Data has Edited succesfuly !");

		redirect('payment',301);
	}

	public function getExistEmail()
	{
		$email = $this->input->post('email');
		
		$res = false;
		$this->db->where('email', $email);
		$this->db->from('tb_payment');
		$q = $this->db->get();
		
		if($q->num_rows() > 0) $res = true;

		echo json_encode($res);
		
	}

	public function nonActive($id)
	{
		$payment = $this->Payments->get($id);

		if($payment->active == 1){
			$payment->active = 0;
 		} else {
			$payment->active = 1;
		 }

		 $payment->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('payment',301);
	}

	public function categoryNonActive($id)
	{
		$payments = $this->PaymentCategories->get($id);

		if($payments->is_active == '1'){
			$payments->is_active = '0';
 		} else {
			$payments->is_active = '1';
		 }

		 $payments->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Payments/category',301);
	}

	public function category()
	{
		$data = $this->PaymentCategories->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('payment/category',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function createCategory()
	{
		$this->data['isEdit'] = false;
		
		$this->data['content'] = $this->load->view('payment/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function editCategory($id)
	{
		$this->data['isEdit'] = true;
		$data = $this->PaymentCategories->get($id);
		//$this->debug($data);
		if($data == null){
			SiteHelpers::alert('warning'," Data not found !");

			redirect('Payments/category',301);
		}
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('payment/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function saveCategory()
	{
		$id = $this->input->post('id');
		
		if($this->input->post('isEdit') == '') {
			$category = $this->PaymentCategories->new_row();
			$msg = " Data has been Create succesfuly !";
		} else {
			$category = $this->PaymentCategories->get($id);
			$msg = " Data has been Create succesfuly !";
		}
		$category->category_name = $this->input->post('name');
		$category->category_desc =  $this->input->post('desc');
		$category->is_active =  '1';

		$id = $category->save();	
		SiteHelpers::alert('success',$msg);
		redirect('Payments/category',301);
	}

}

/* End of file payments.php */
