<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class HostedTours extends MY_Controller {

	var $tableView;
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('HostedTours');
		$this->tableView = 'v_product_stock';
	}
	

	public function index()
	{
		$data = $this->HostedTours->joinById();		
		//$this->debug($data);
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('hosted_tour/index',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
		
	}

	public function updateStatus($id,$status){
		$data["status"] = $status;
		$data['verify_by'] = $this->session->userdata('userName');
		$data['verify_date'] = date('Y-m-d');
		//$this->debug($data);
		$this->db->update('hosted_tour_request', $data);
		$this->db->where('id', $id);
		

		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Admin/HostedTours',301);
	}

	public function sendRequest()
	{
		$data = $this->input->post();
		$data['date_req'] =  date('Y-m-d H:i:s');
		$data['status'] =  0;

		$this->db->insert('hosted_tour_request', $data);
		
		echo json_encode(array('result'=>1));
		//SiteHelpers::alert('success'," Data has been Request !");

		//redirect('HostedTours',301);
	}

}

/* End of file HostedTours.php */
