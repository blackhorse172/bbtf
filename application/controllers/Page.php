<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends MY_controller {

	protected $_key 	= 'id';
	protected $_class	= 'page';
	protected $layout = "layouts/main";
	
	function __construct()
	{
		parent::__construct();	
		//$this->layout = 'layouts/'.CNF_THEME.'/index';
		$this->layout = 'layouts/frontend/main';
		$this->load->model('Articles');
		$this->load->model('Galleries');
		$this->load->library('MyLibraries');
		$this->load->library('pagination');
		

	}
	
	
	public function index( $page = null)
	{
		
		if(CNF_FRONT =='false' && $this->uri->segment(1) =='' ) :
			redirect('dashboard',301);
		endif; 
		

		if($page != null) :
			$row = $this->db->query("SELECT * FROM tb_pages WHERE alias ='$page' and status=1 ")->row();
			
			if($row != NULL)
			{
				$this->data['pageTitle'] = $row->title;
				$this->data['pageNote'] = $row->note;		
				$this->data['breadcrumb'] = 'active';				
				$this->data['sub_image'] = $row->sub_image;				
				
				// if($row->access !='')
				// {
				// 	$access = json_decode($row->access,true)	;	
				// } else {
				// 	$access = array();
				// }	

				// If guest not allowed 
				// if($row->allow_guest !=1)
				// {	
				// 	$group_id = $this->session->userdata('gid');				
				// 	$isValid =  (isset($access[$group_id]) && $access[$group_id] == 1 ? 1 : 0 );	
				// 	if($isValid ==0)
				// 	{
				// 		redirect('',301);				
				// 	}
				// }				
				// if($row->template =='backend')
				// {
				// 	 $this->layout = "layouts/main";
				// }

						
				
				$filename = "application/views/frontend/".$row->filename.".php";
				if(file_exists($filename))
				{
					$page = $row->filename.".php";
				} else {
					redirect('',301);						
				}
				
			} else {
				redirect('',301);			
			}
			
			
		else :
			$this->data['pageTitle'] = 'Home';
			$this->data['pageNote'] = 'Welcome To Our Site';
			$this->data['breadcrumb'] = 'inactive';			
			$page = 'home';
		endif;	

		$this->db->order_by('dtm_crt', 'desc');
		$gallery = $this->Galleries->where('status',1)->pagination(1, 5)->order_by('dtm_crt')->get_list();

			$this->data['gallery'] = $gallery;
			$this->data['categories'] = $this->db->get_where('category_gallery',array('is_active'=>1))->result_array();

			
			
		$this->data['company'] = $this->db->get('company')->row_array();
		//$this->debug($this->data['company']);
		$commonJs = false;

		
		
		$this->data['commonJs'] = $commonJs;
		$this->data['content'] = $this->load->view('frontend/'.$page,$this->data, true );		
    	$this->load->view($this->layout, $this->data );
		
	}

	function  submitcontact()
	{
	
		$rules = array(
			array('field'   => 'name','label'   => ' Please Fill Name','rules'   => 'required'),
			array('field'   => 'email','label'   => 'email ','rules'   => 'required|email'),
			array('field'   => 'message','label'   => 'message','rules'   => 'required'),
		);	


		$this->form_validation->set_rules( $rules );
		if( $this->form_validation->run() )
		{
			
			$data = array(
				'name'=>$this->input->post('name',true),
				'email'=>$this->input->post('email',true),
				'subject'=> 'New Form Submission',
				'notes'=>$this->input->post('message',true)
			); 
			$message = $this->load->view('emails/contact', $data,true); 
			
			
			$to 		= 	CNF_EMAIL;
			$subject 	= 'New Form Submission';
			$headers  	= 'MIME-Version: 1.0' . "\r\n";
			$headers 	.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers 	.= 'From: '.$this->input->post('name',true).' <'.$this->input->post('sender',true).'>' . "\r\n";
				//mail($to, $subject, $message, $headers);			
			$message = "Thank You , Your message has been sent !";
			$this->session->set_flashdata('message',SiteHelpers::alert('success',$message));
			redirect('contact-us',301);
			
				
		} else {
			$message = "The following errors occurred";
			$this->session->set_flashdata(array(
					'message'=>SiteHelpers::alert('error',$message),
					'errors'	=> validation_errors('<li>', '</li>')
			));
			redirect('contact-us',301);	
		}		
	}

	public function lang($lang)
	{

		$this->session->set_userdata('lang',$lang);	
		 redirect($_SERVER['HTTP_REFERER']);  	
	}

	public function announcment()
	{
		$this->data['content'] = $this->load->view('frontend/announcment', $this->data, true );
		$this->load->view('layouts/registration/main',$this->data);
	}


	public function skin($skin = 'sximo')
	{

		$this->session->set_userdata('themes',$skin);	
		 redirect($_SERVER['HTTP_REFERER']);  	
	}
	
	public function hosted_tour($id = '')
	{
		
		if($id ==''){
			redirect('');
		}

		if($id == NULL){
			redirect('');
		}

		$data = $this->db->get_where('product',array('id'=>$id))->row_array();
		if($data == NULL){
			redirect('');
		}

		$this->data['company'] = $this->db->get('company')->row_array();
		//$this->debug($this->data['company']);
		$this->data['detail'] = $data;
		$this->data['content'] = $this->load->view('frontend/hosted_detail', $this->data, true );		
    	$this->load->view($this->layout, $this->data );
	}

	public function news($slug="")
	{
		$thisPage = $this->db->query("SELECT * FROM tb_pages WHERE alias ='news' and status=1 ")->row();
		$sub_image = $thisPage == null ? '' : $thisPage->sub_image;
		if($slug =='' || $slug== NULL){

			$page = ($this->input->get('page') > 0)  ? $this->input->get('page')  : 1;
			$config['per_page'] = $page;
			$displayRow = 8;
			$config['base_url'] = base_url().'news.html';
			$config['total_rows'] = count($this->Articles->where('status','publish')->get_list());
			$config["uri_segment"] = 3;  // uri parameter
			$choice = $config["total_rows"] / $displayRow;
			$config["num_links"] = floor($choice);//$page;
		
			$this->pagination->initialize($config);
			$articles = $this->Articles->where('status','publish')->pagination($config['per_page'], $displayRow)->order_by('created')->get_list();

			$company = $this->db->get('company')->row_array();
			$company['company_name'] = "BBTF - ".$slug;
			$this->data['company'] = $company;
			$this->data['articles'] = $articles;
			$this->data['sub_image'] = $sub_image;
			$this->data['pagination'] = $this->pagination->create_links();
			$this->data['content'] = $this->load->view('frontend/news', $this->data, true );
		
		} else {
			$data = $this->db->get_where('tb_blogs',array('slug'=>$slug))->row_array();
			if($data == NULL){
				redirect('');
			}
	
			# code...
			$company = $this->db->get('company')->row_array();
			$company['company_name'] = "BBTF - ".$slug;
			$this->data['company'] = $company;
			$this->data['detail'] = $data;
			$this->data['sub_image'] = $sub_image;
			$this->data['otherNews'] = $this->Articles->getRandomNews($data['blogID'],4);
	
			$this->data['content'] = $this->load->view('frontend/news_detail', $this->data, true );		
		}

		$this->load->view($this->layout, $this->data);
		
	}

	public function gallery()
	{
			$thisPage = $this->db->query("SELECT * FROM tb_pages WHERE alias ='photo-gallery' and status=1 ")->row();
			$sub_image = $thisPage == null ? '' : $thisPage->sub_image;

			$page = ($this->input->get('page') > 0)  ? $this->input->get('page')  : 1;
			$config['per_page'] = $page;
			$displayRow = 8;
			$config['base_url'] = base_url().'photo-gallery.html';
			$config['total_rows'] = count($this->Galleries->where('status',1)->get_list());
			$config["uri_segment"] = 5;  // uri parameter
			$choice = $config["total_rows"] / $displayRow;
			$config["num_links"] = floor($choice);
		
			$this->pagination->initialize($config);
			$gallery = $this->Galleries->where('status',1)->order_by('gallery_id')->get_list();
			//$gallery = $this->Galleries->where('status',1)->pagination($config['per_page'], $displayRow)->order_by('gallery_id')->get_list();
			
			$company = $this->db->get('company')->row_array();
			$company['company_name'] = "BBTF - Galleries";
			$this->data['company'] = $company;
			$this->data['gallery'] = $gallery;
			$this->data['categories'] = $this->db->get_where('category_gallery',array('is_active'=>1))->result_array();
			$this->data['sub_image'] = $sub_image;

			$this->data['pagination'] = $this->pagination->create_links();
			$this->data['content'] = $this->load->view('frontend/photo-gallery', $this->data, true );
		

		$this->load->view($this->layout, $this->data);
		
	}

	public function json_getByCatId()
	{
		# code...
		$catId = $this->input->post('catId');
		// $where = array(

		// );
		if($catId != "ALL")
		{
			$this->db->where('category_id', $catId);

		}
		$this->db->where('status', 1);
		$this->db->order_by('rand()');
		$this->db->limit(5);
		$res = $this->db->get('gallery')->result();
		$this->data['gallery'] = $res;
		$html = $this->load->view('gallery/json_list',$this->data,true);
		
		$result['data'] = $html;
			
		echo $html;
		
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/page.php */ 
