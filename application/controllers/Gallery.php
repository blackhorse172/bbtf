<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);      
		$this->load->model('Galleries');
		$this->load->model('GalleryCategories');

		
	}
	
	public function index()
	{
		
		$data = $this->Galleries->join();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('gallery/index',$this->data,true);   
    	$this->load->view('layouts/main',$this->data);
	}

	public function Create()
	{
		$this->data['isEdit'] = false;
		$this->data['category'] = $this->GalleryCategories->isActive()->get_list();
		$this->data['content'] = $this->load->view('gallery/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function edit($id)
	{
		$data = $this->Galleries->get($id);
		$this->data['category'] = $this->GalleryCategories->isActive()->get_list();
		$this->data['isEdit'] = true;
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('gallery/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Save()
	{
		$fileName = '';
		//var_dump((int)$this->input->post('status'));
		//$this->debug($this->input->post('status'));
		if(isset($_FILES["image"]["name"]))
		{
			//$this->debug($_FILES);
			$config['upload_path'] = './files/gallery/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				
				//validation image has error
				SiteHelpers::alert('error',$this->upload->display_errors());
				redirect('Gallery/Create');
				} else {
					$data = $this->upload->data();
					$fileName = $data["file_name"];
				}
		}
		
		$Gallery = $this->Galleries->new_row();

		$Gallery->category_id = $this->input->post('category_id');
		$Gallery->title = $this->input->post('title');
		$Gallery->descr = $this->input->post('desc');	
		$Gallery->dtm_crt = date('Y-m-d H:i:s');
		$Gallery->usr_crt = $this->session->userdata('userName');
		$Gallery->status = (int)$this->input->post('status');	
		//$Gallery->end_show = $this->input->post('end_show');

		$Gallery->image = $fileName;	
		//$this->debug($Gallery);
		$id = $Gallery->save();
		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Gallery',301);
	}

	public function saveEdit()
	{
		$fileName = '';
		$id = $this->input->post('gallery_id');
		$Gallery = $this->Galleries->get($id);
		
		if(isset($_FILES["image"]["name"]))
		{
			$config['upload_path'] = './files/gallery/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';
			
			if(count($this->input->post($_FILES["image"])) > 3){
			
					//unlink exist data
					if($Gallery->image != '' || $Gallery->image != null){
						$image = $config['upload_path'].$Gallery->image;
						unlink($image);
					}
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if (!$this->upload->do_upload('image')){
						$error = array('error' => $this->upload->display_errors());
						
						//validation image has error
						SiteHelpers::alert('error',$this->upload->display_errors());
						redirect('gallery/create');
						} else {
							$data = $this->upload->data();
							$fileName = $data["file_name"];
							$Gallery->image = $fileName;

						}
			}
			
		}
		$Gallery->category_id = $this->input->post('category_id');
		$Gallery->title = $this->input->post('title');
		//$Gallery->descr = $this->input->post('desc');	
		$Gallery->dtm_crt = date('Y-m-d H:i:s');
		$Gallery->usr_crt = $this->session->userdata('userName');
		$Gallery->status = (int)$this->input->post('status');	
		//$Gallery->end_show = $this->input->post('end_show');
		$id = $Gallery->save();
		SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Gallery',301);
	}

	public function nonActive($id)
	{
		$Gallery = $this->Galleries->get($id);
		
		if($Gallery->status == 1){
			$Gallery->status = 0;
 		} else {
			$Gallery->status = 1;
		 }

		 $Gallery->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Gallery',301);
	}

	public function categoryNonActive($id)
	{
		$gallery = $this->GalleryCategories->get($id);

		if($gallery->is_active == '1'){
			$gallery->is_active = '0';
 		} else {
			$gallery->is_active = '1';
		 }

		 $gallery->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Gallery/category',301);
	}

	public function category()
	{
		$data = $this->GalleryCategories->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('gallery/category',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function createCategory()
	{
		$this->data['isEdit'] = false;
		
		$this->data['content'] = $this->load->view('gallery/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function editCategory($id)
	{
		$this->data['isEdit'] = true;
		$data = $this->GalleryCategories->get($id);
		//$this->debug($data);
		if($data == null){
			SiteHelpers::alert('warning'," Data not found !");

			redirect('Gallery/category',301);
		}
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('gallery/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function saveCategory()
	{
		$id = $this->input->post('id');
		
		if($this->input->post('isEdit') == '') {
			$category = $this->GalleryCategories->new_row();
			$msg = " Data has been Create succesfuly !";
		} else {
			$category = $this->GalleryCategories->get($id);
			$msg = " Data has been Create succesfuly !";
		}
		$category->category_name = $this->input->post('name');
		$category->category_desc =  $this->input->post('desc');
		$category->is_active =  '1';

		$id = $category->save();	
		SiteHelpers::alert('success',$msg);
		redirect('Gallery/category',301);
	}

	public function delete($id)
	{
		$gallery = $this->Galleries->get($id);
		$config['upload_path'] = './files/gallery/';
		$image = $config['upload_path'].$gallery->image;
		unlink($image);

		$this->db->where('gallery_id', $id);
		$this->db->delete('gallery');

		SiteHelpers::alert('success',"Delete Success");
		redirect('Gallery',301);
		
	}

	public function json_getByCatId()
	{
		# code...
		$catId = $this->input->post('catId');
		// $where = array(

		// );
		if($catId != "ALL")
		{
			$this->db->where('category_id', $catId);

		}
		$this->db->where('status', 1);
		$this->db->order_by('rand()');
		$this->db->limit(5);
		$res = $this->db->get('gallery')->result();
		$this->data['gallery'] = $res;
		$html = $this->load->view('gallery/json_list',$this->data,true);
		
		$result['data'] = $html;
			
		echo $html;
		
	}
	

}

/* End of file gallery.php */
