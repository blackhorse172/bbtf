<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Doku extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		
	}
	

	public function index()
	{
		$this->load->config('doku');
		$config['mallId'] = $this->config->item('mallId');
		$config['sharedKey'] = $this->config->item('sharedKey');
		$config['invoice'] = $this->incrementalHash();//"INV".date('Ymd').rand(10000,99999);
		$params = array('amount' => '1000000.00','invoice' => $config['invoice'], 'currency' => '360');
		$words = sha1($params['amount'].$config['mallId'].$config['sharedKey'].$config['invoice']);
		$this->data['config'] = $config;
		//$this->debug($config);
		$this->data['words'] = $words;
		$this->data['params'] = $params;
		$this->data['content'] = $this->load->view('doku/paymentRequest',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function paymentRequest($invoice)
	{

		$dokuTmp = $this->db->get_where('doku_tmp',array("transidmerchant"=> $invoice))->row_array();

		$isExist = $this->db->get_where('doku',array("transidmerchant"=> $invoice))->row_array();

		if($dokuTmp['trxstatus']== "DECLINE")
		{
			$msg = "<h1>Payment</h1>";
			$msg.= "<p>Your have already Decline.</p>";
			$msg.= "<p>Should you have any questions or concerns, please feel free to contact us.</p>";
			$msg.= "<br>";
			$msg.= "<br>";
			$msg.= "<p>Thank you for your attention. Sincerely yours</p>";
			$msg.= "<br>";
			$msg.= "<p>Committee of  BBTF 2020</p>";

			$this->session->set_flashdata('success_register',$msg);
			redirect('',301);
		}

		if($isExist != null){
		$msg = "<h1>Payment</h1>";
		$msg.= "<p>Your have already payment.</p>";
		$msg.= "<p>Please kindly contact us if you have not received Paid Confirmation within 3 working days.</p>";
		$msg.= "<p>Should you have any questions or concerns, please feel free to contact us.</p>";
		$msg.= "<br>";
		$msg.= "<br>";
		$msg.= "<p>Thank you for your attention. Sincerely yours</p>";
		$msg.= "<br>";
		$msg.= "<p>Committee of  BBTF 2020</p>";

		$this->session->set_flashdata('success_register',$msg);
			redirect('',301);
		}

		if($dokuTmp == null){
			$msg = "<h1>WARNING</h1>";
			$msg.= "<p>Your transaction id not found.</p>";
			$msg.= "<p>Please kindly contact us if you have not make payment.</p>";
			$msg.= "<br>";
			$msg.= "<br>";
			$msg.= "<p>Thank you for your attention.</p>";
			$msg.= "<br>";
			$msg.= "<p>Sincerely yours</p>";
			$msg.= "<p>Committee of  BBTF 2020</p>";

		$this->session->set_flashdata('success_register',$msg);
			redirect('',301);

		}

		$this->load->config('doku');
		if($dokuTmp['user_type'] == "BUYER"){
			$profile = $this->db->get_where('buyer',array("buyer_id"=> $dokuTmp['ref_user_id']))->row_array();
			$params['email'] = $profile['buyer_email'];
			$params['name'] = $profile['buyer_name'];
			$params['phone'] = $profile['buyer_phone'];


		} else {
			$profile = $this->db->get_where('trade_buyer',array("id"=> $dokuTmp['ref_user_id']))->row_array();
			$params['email'] = $profile['company_email'];
			$params['name'] = $profile['company_name'];
			$params['phone'] = $profile['company_phone'];
			

		}
		
		$config['mallId'] = $this->config->item('mallId');
		$config['sharedKey'] = $this->config->item('sharedKey');
		$config['invoice'] = $invoice;//"INV".date('Ymd').rand(10000,99999);
		$params['amount'] = $dokuTmp['totalamount'].'.00';
		$params['invoice'] = $config['invoice'];
		$params['currency'] = '360';
		$params['paymentChannel'] = $dokuTmp['payment_channel'];

		$words = sha1($params['amount'].$config['mallId'].$config['sharedKey'].$config['invoice']);
		$this->data['config'] = $config;
		//$this->debug($config);
		$this->data['words'] = $words;
		$this->data['params'] = $params;
		$this->data['pageTitle '] = "Payment Request";
		$this->data['content'] = $this->load->view('doku/paymentRequest',$this->data,true);    
    	$this->load->view('layouts/payment_req/main',$this->data);
	}

	public function incrementalHash($length=10){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function notify()
	{
		# code...
		$post = $this->input->post();
		if($_SERVER['REQUEST_METHOD'] === 'POST'){
		$log['form'] =json_encode($post);
		$this->db->insert('log_notify',$log);
		$this->load->config('doku');
		$mallId = $this->config->item('mallId');
		$sharedKey = $this->config->item('sharedKey');

			
			$data['transidmerchant'] = $post['TRANSIDMERCHANT'];
			$data['totalamount'] = $post['AMOUNT'];
			$data['words']    = $post['WORDS'];
			$data['statustype'] = $post['STATUSTYPE'];
			$data['response_code'] = $post['RESPONSECODE'];
			$data['approvalcode']   = $post['APPROVALCODE'];
			$data['trxstatus']         = $post['RESULTMSG'];
			$data['payment_channel'] = $post['PAYMENTCHANNEL'];
			$data['paymentcode'] = $post['PAYMENTCODE'];
			$data['session_id'] = $post['SESSIONID'];
			$data['bank_issuer'] = $post['BANK'];
			$data['creditcard'] = $post['MCN'];
			$data['payment_date_time'] = $post['PAYMENTDATETIME'];
			$data['verifyid'] = $post['VERIFYID'];
			$data['verifyscore'] = $post['VERIFYSCORE'];
			$data['verifystatus'] = $post['VERIFYSTATUS'];


			$WORDS_GENERATED = sha1($data['totalamount'].$mallId.$sharedKey.$data['transidmerchant'].$data['trxstatus'].$data['verifystatus']);    
			if ( $data['words'] == $WORDS_GENERATED ) {
				//$this->db->set($data);
				$this->db->where('transidmerchant', $data['transidmerchant']);
				$this->db->update('doku',$data);
				
				echo "Continue";
			} else {
				echo "Stop";
			}
				
		} else {
			echo "Stop";
		}
		

	}

	public function redirect()
	{
		# code...
		//$this->debug($this->input->post());
		$this->load->library('my_libraries');
		$post = $this->input->post();
		$dokuTmp = $this->db->get_where('doku_tmp',array("transidmerchant"=> $post['TRANSIDMERCHANT']))->row_array();
		if($dokuTmp['user_type'] == "BUYER")
		{
			$profile = $this->db->get_where('buyer',array("buyer_id"=> $dokuTmp['ref_user_id']))->row_array();
			$email['email'] = $profile["buyer_email"];
			$email['name'] = $profile["buyer_name"];
			$email['gender'] = $profile["gender"];
			
		} else {
			$profile = $this->db->get_where('trade_buyer',array("id"=> $dokuTmp['ref_user_id']))->row_array();
			$email['email'] = $profile["email"];
			$email['name'] = $profile["full_name"];
			$email['gender'] = $profile["gender"];
			
		}

		$data['transidmerchant'] = $post['TRANSIDMERCHANT'];
		$data['totalamount'] = $post['AMOUNT'];
		$data['payment_channel'] = $post['PAYMENTCHANNEL'];
		$data['words'] = $post['WORDS'];
		$data['session_id']= $post['SESSIONID'];
		$data['paymentcode']= $this->incrementalHash();
		$data['response_code']= $post['STATUSCODE'];
		$data['ref_user_id']= $dokuTmp['ref_user_id'];
		$data['user_type']= $dokuTmp['user_type'];
		$data['payment_date_time']= date('Y-m-d H:i:s');

		if($post['STATUSCODE'] == 0000){
			$this->db->insert('doku', $data);
			//$this->my_libraries->buyerAfterPayment($email);
		$msg = "<h1>Payment Successful</h1>";
		$msg.= "<p></p>";
		$msg.= "<p>Your payment successfully sent. Please kindly contact us if you have not received a paid email confirmation within 3 working days.</p>";
		$msg.= "<p>Should you need further information, please feel free to contact us.</p>";
		$msg.= "<br>";
		$msg.= "<br>";
		$msg.= "<p>Sincerely yours,</p>";
		$msg.= "<br>";
		$msg.= "<p>BBTF Committe</p>";
		$this->session->set_flashdata('success_register',$msg);
		redirect('',301);
		} else {
			$this->db->where('transidmerchant', $post['TRANSIDMERCHANT']);
			$this->db->update('doku_tmp', $data);
			$msg = "<h1>Payment Failed</h1>";
			// $msg.= "<p>Please kindly contact us for mor.</p>";
			$msg.= "<br>";
			$msg.= "<br>";
			$msg.= "<p>Thank you for your attention. Sincerely yours</p>";
			$msg.= "<br>";
			$msg.= "<p>BBTF Committe</p>";
			$this->session->set_flashdata('success_register',$msg);
			redirect('',301);
		}

	}

	public function createPaymentRequest()
	{
		$post = $this->input->post();
		$this->load->config('doku');
		$config['mallId'] = $this->config->item('mallId');
		$config['sharedKey'] = $this->config->item('sharedKey');
		$config['invoice'] = $this->incrementalHash();//"INV".date('Ymd').rand(10000,99999);
		$product = $this->db->get_where('product',array('id'=>$post['id']))->row_array();
		$params = array('amount' => $product['product_price'].'.00','invoice' => $config['invoice'], 'currency' => '360');
		$words = sha1($params['amount'].$config['mallId'].$config['sharedKey'].$config['invoice']);



	}




}

/* End of file Doku.php */

?>
