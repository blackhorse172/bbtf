<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//if(!$this->session->userdata('logged_in')) redirect('user/login',301);

		//$this->load->model('Transactions');
		//$this->load->model('TransactionsCategories');
		
		//Do your magic here
	}
	

	public function index()
	{
		// $data = $this->Transactions->where("user_id",(int)$this->session->userdata('uid'))->get_list();
		// $this->data['data'] = json_encode($data);
		// $this->data['content'] = $this->load->view('transaction/index',$this->data,true);    
    	// $this->load->view('layouts/main',$this->data);
	}

	public function detail($id)
	{
		
		$detail = $this->db->get_where('official_partner',array('id'=>$id))->row_array();
		$this->data['isEdit'] = false;
        $this->data['detail'] = $detail;
		$this->data['company'] = $this->db->get('company')->row_array();
		//$this->data['otherPartner'] = $this->db->get('company')->row_array();
		$this->data['content'] = $this->load->view('partner/detail',$this->data,true);    
        $this->load->view('layouts/frontend/main',$this->data);
        
	}

	public function edit($id)
	{
		$data = $this->Transactions->get($id);
		if($data == '')
		{
			redirect('error/notFound');
		}
		//$this->debug($data);
		$group = $this->TransactionsCategories->get_list();
		$this->data['isEdit'] = true;
		$this->data['group'] = $group;
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('transaction/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function save()
	{
		//$this->debug($this->input->post());
		$transactions = $this->Transactions->new_row();

		$transactions->transactions_name = $this->input->post('name');
		$transactions->transactions_fee = (int)str_replace('.','',$this->input->post('fee')) ;	
		$transactions->category_id = $this->input->post('category_id');
		$transactions->dtm_crt = date('Y-m-d H:i:s');
		$transactions->usr_crt = $this->session->userdata('userName');

		$transactions->active = 1;	

		$id = $transactions->save();
		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Transactions',301);
	}

	public function saveEdit()
	{
		$id = $this->input->post('id');
		$transactions = $this->Transactions->get($id);
		//$this->debug($this->input->post());
		
		$transactions->transactions_name = $this->input->post('name');
		$transactions->transactions_fee = (int)str_replace('.','',$this->input->post('fee')) ;	
		$transactions->category_id = $this->input->post('category_id');
		$transactions->dtm_upd = date('Y-m-d H:i:s');
		$transactions->usr_upd = $this->session->userdata('userName');

		$id = $transactions->save();
		SiteHelpers::alert('success'," Data has Edited succesfuly !");

		redirect('Transactions',301);
	}

	public function getExistEmail()
	{
		$email = $this->input->post('email');
		
		$res = false;
		$this->db->where('email', $email);
		$this->db->from('tb_transactions');
		$q = $this->db->get();
		
		if($q->num_rows() > 0) $res = true;

		echo json_encode($res);
		
	}

	public function nonActive($id)
	{
		$transactions = $this->Transactions->get($id);

		if($transactions->active == 1){
			$transactions->active = 0;
 		} else {
			$transactions->active = 1;
		 }

		 $transactions->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Transactions',301);
	}

	public function categoryNonActive($id)
	{
		$transactions = $this->TransactionsCategories->get($id);

		if($transactions->is_active == '1'){
			$transactions->is_active = '0';
 		} else {
			$transactions->is_active = '1';
		 }

		 $transactions->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Transaction/category',301);
	}

	public function category()
	{
		$data = $this->TransactionsCategories->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('transaction/category',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function createCategory()
	{
		$this->data['isEdit'] = false;
		
		$this->data['content'] = $this->load->view('transaction/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function editCategory($id)
	{
		$this->data['isEdit'] = true;
		$data = $this->TransactionsCategories->get($id);
		//$this->debug($data);
		if($data == null){
			SiteHelpers::alert('warning'," Data not found !");

			redirect('Transaction/category',301);
		}
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('transaction/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function saveCategory()
	{
		$id = $this->input->post('id');
		
		if($this->input->post('isEdit') == '') {
			$category = $this->TransactionsCategories->new_row();
			$msg = " Data has been Create succesfuly !";
		} else {
			$category = $this->TransactionsCategories->get($id);
			$msg = " Data has been Create succesfuly !";
		}
		$category->category_name = $this->input->post('name');
		$category->category_desc =  $this->input->post('desc');
		$category->is_active =  '1';

		$id = $category->save();	
		SiteHelpers::alert('success',$msg);
		redirect('Transaction/category',301);
	}

	public function json_getDetail()
	{
		$code = $this->input->post('trx_code');
		$type = $this->input->post('trx_type');
		
		if($type == SELLER_REGISTRATION){
			$this->db->where('trx_code', $code);
			$res = $this->db->get('v_product_detail')->result_array();
			
			echo json_encode($res);	
		}

		if($type == BUYER_REGISTRATION){
			$this->db->where('trx_code', $code);
			$res = $this->db->get('v_product_detail')->result_array();
			
			echo json_encode($res);	
		}

		if($type == MEMBERSHIP_ORDER){
			$this->db->where('trx_code', $code);
			$res = $this->db->get('v_product_detail')->result_array();
			
			echo json_encode($res);
			
		}

		if($type == PRODUCT_ORDER){
			$this->db->where('trx_code', $code);
			$res = $this->db->get('v_product_detail')->result_array();
			
			echo json_encode($res);
			
		}
		
	}

	public function getDetailTrx($code,$type)
	{
		
		if($type == SELLER_REGISTRATION){
			$this->db->where('trx_code', $code);
			$res = $this->db->get('v_product_detail')->result();
			
			return $res;	
		}

		if($type == BUYER_REGISTRATION){
			$this->db->where('trx_code', $code);
			$res = $this->db->get('v_product_detail')->result_array();
			
			echo json_encode($res);	
		}

		if($type == MEMBERSHIP_ORDER){
			$this->db->where('trx_code', $code);
			$res = $this->db->get('v_product_detail')->result();
			
			return $res;
			
		}

		if($type == PRODUCT_ORDER){
			$this->db->where('trx_code', $code);
			$res = $this->db->get('v_product_detail')->result();
			
			return $res;
			
		}
		
	}

	public function invoice($trxCode)
	{
		$company = $this->company();
		$trxHeader = $this->Transactions->where("trx_code",$trxCode)->get();
		$invoiceNo = str_replace("TRX","INV",$trxCode);
		$this->data["invoiceNo"] = $invoiceNo;
		$this->data["trxHeader"] = $trxHeader;
		$this->data["trxDetail"] = $this->getDetailTrx($trxCode,$trxHeader->trx_type);
		$this->data["company"] = $company;
		$this->data["user"] = $this->session->userdata('userName');
		$this->data["payment"] =$this->db->get_where('payment',array("trx_code"=>$trxCode))->row();
		// $this->debug($this->data["payment"]);


		$this->load->view('transaction/invoice',$this->data);    


		# code...
	}

}

/* End of file transactions.php */
