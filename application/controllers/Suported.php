<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Suported extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);      
		$this->load->model('Suporteds');
	}
	

	public function index()
	{
		$this->load->helper('inflector');
		$this->db->order_by('id', 'desc');
		$data = $this->Suporteds->get_list();
		
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('suported/index',$this->data,true);   
    	$this->load->view('layouts/main',$this->data);
	}

	public function Create()
	{
		$this->data['isEdit'] = false;
		//$this->data['category'] = $this->SponsorCategories->isActive()->get_list();
		$this->data['content'] = $this->load->view('suported/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function edit($id)
	{
		$data = $this->Suporteds->get($id);
		//$this->data['category'] = $this->SponsorCategories->isActive()->get_list();
		$this->data['isEdit'] = true;
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('suported/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Save()
	{
		$fileName = '';
		//var_dump((int)$this->input->post('status'));
		//$this->debug($this->input->post());
		if(isset($_FILES["image"]["name"]))
		{
			//$this->debug($_FILES);
			$config['upload_path'] = './files/suported/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				
				//validation image has error
				SiteHelpers::alert('error',$this->upload->display_errors());
				redirect('Suported/Create');
				} else {
				
				
					$data = $this->upload->data();
					$fileName = $data["file_name"];

				$configR['image_library']='gd2';
                $configR['source_image']='./files/suported/'.$fileName;
                $configR['create_thumb']= FALSE;
                $configR['maintain_ratio']= TRUE;
                $configR['quality']= '60%';
                //$configR['width']= 600;
                //$configR['height']= 400;
				$configR['new_image']= './files/suported/'.$fileName;
                $this->load->library('image_lib', $configR);
				$this->image_lib->resize();

				}
		}
		
		$Sponsor = $this->Suporteds->new_row();

		//$Sponsor->category_id = $this->input->post('category_id');
		$Sponsor->title = $this->input->post('title');
		$Sponsor->link = $this->input->post('link');
		$Sponsor->position = $this->input->post('position');

		$Sponsor->dtm_crt = date('Y-m-d H:i:s');
		$Sponsor->usr_crt = $this->session->userdata('userName');
		$Sponsor->status = (int)$this->input->post('status');	
		//$Sponsor->end_show = $this->input->post('end_show');

		$Sponsor->image = $fileName;	
		//$this->debug($Sponsor);
		$id = $Sponsor->save();

		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Suported',301);
	}

	public function saveEdit()
	{
		$fileName = '';
		$id = $this->input->post('id');
		$Sponsor = $this->Suporteds->get($id);
		//$this->debug($this->input->post());
		if(isset($_FILES["image"]["name"]))
		{
			$config['upload_path'] = './files/suported/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';

			
			if(count($this->input->post($_FILES["image"])) > 3){
			
					//unlink exist data
					if($Sponsor->image != '' || $Sponsor->image != null){
						$image = $config['upload_path'].$Sponsor->image;
						unlink($image);
					}
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if (!$this->upload->do_upload('image')){
						$error = array('error' => $this->upload->display_errors());
						
						//validation image has error
						SiteHelpers::alert('error',$this->upload->display_errors());
						redirect('sponsor/create');
						} else {
							$data = $this->upload->data();
							$fileName = $data["file_name"];
							$Sponsor->image = $fileName;

							$configR['image_library']='gd2';
                $configR['source_image']='./files/suported/'.$fileName;
                $configR['create_thumb']= FALSE;
                $configR['maintain_ratio']= TRUE;
                $configR['quality']= '60%';
                //$configR['width']= 600;
                //$configR['height']= 400;
				$configR['new_image']= './files/suported/'.$fileName;
                $this->load->library('image_lib', $configR);
				$this->image_lib->resize();

						}
			}
			
		}
		//$Sponsor->category_id = $this->input->post('category_id');
		$Sponsor->title = $this->input->post('title');
		$Sponsor->link = $this->input->post('link');
		$Sponsor->position = $this->input->post('position');
		$Sponsor->dtm_crt = date('Y-m-d H:i:s');
		$Sponsor->usr_crt = $this->session->userdata('userName');
		$Sponsor->status = (int)$this->input->post('status');	
		//$Sponsor->end_show = $this->input->post('end_show');
		//$this->debug($Sponsor->position);
		$id = $Sponsor->save();
		SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Suported',301);
	}

	public function nonActive($id)
	{
		$Suported = $this->Suporteds->get($id);
		
		if($Suported->status == 1){
			$Suported->status = 0;
 		} else {
			$Suported->status = 1;
		 }

		 $Suported->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Suported',301);
	}

	public function categoryNonActive($id)
	{
		$sponsor = $this->SponsorCategories->get($id);

		if($sponsor->is_active == '1'){
			$sponsor->is_active = '0';
 		} else {
			$sponsor->is_active = '1';
		 }

		 $sponsor->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Sponsor/category',301);
	}

	public function category()
	{
		$data = $this->SponsorCategories->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('sponsor/category',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function createCategory()
	{
		$this->data['isEdit'] = false;
		
		$this->data['content'] = $this->load->view('sponsor/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function editCategory($id)
	{
		$this->data['isEdit'] = true;
		$data = $this->SponsorCategories->get($id);
		//$this->debug($data);
		if($data == null){
			SiteHelpers::alert('warning'," Data not found !");

			redirect('Sponsor/category',301);
		}
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('sponsor/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function saveCategory()
	{
		$id = $this->input->post('id');
		
		if($this->input->post('isEdit') == '') {
			$category = $this->SponsorCategories->new_row();
			$msg = " Data has been Create succesfuly !";
		} else {
			$category = $this->SponsorCategories->get($id);
			$msg = " Data has been Create succesfuly !";
		}
		$category->category_name = $this->input->post('name');
		$category->category_desc =  $this->input->post('desc');
		$category->is_active =  '1';

		$id = $category->save();	
		SiteHelpers::alert('success',$msg);
		redirect('Sponsor/category',301);
	}

}

/* End of file sponsor.php */
