<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Seller extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		   
		$this->load->library('form_validation');
		$this->data['pageTitle'] = "Seller Registration";
		$this->load->model('Stocks');
		$this->load->model('Products');
		$this->load->model('Transactions');
		$this->load->library('M_pdf');
		//$this->load->library('Emails_libraries');
		$this->load->library('recaptcha');
		$this->load->model('GeneralSettings');

	}

	private function getYearEvent()
	{
		return $this->GeneralSettings->getYearEvent();
	}

	private function getDateEvent()
	{
		return $this->GeneralSettings->getDateEvent();
	}
	

	public function index()
	{
		if(!$this->session->userdata('logged_in')) redirect('login.html',301);   
		$where['is_active'] = 1;
		$where['user_id'] = $this->session->userdata('uid');
		$target_market = $this->db->get_where('general_settings',array('gs_name'=>'seller_target_market'))->result_array();
		$country = $this->db->get('apps_countries')->result_array();
		$category = $this->db->get_where('category_seller',array('is_active'=>1))->result_array();
		

		$this->data['country'] = $country;
		$this->data['category'] = $category;
		$this->data['target_market'] = $target_market;
		$this->data['profile'] = $this->db->get_where('seller_list',$where)->row_array();
		$this->data['content'] = $this->load->view('seller/dashboard',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}


	public function boothDetail($trxCode)
	{
		if(!$this->session->userdata('logged_in')) redirect('login.html',301);   

		$trx = $this->db->get_where('transaction_h',array('trx_code'=>$trxCode))->row_array();
		$user = $this->db->get_where('tb_users',array("id"=>$trx['user_id']))->row_array();
		$profile = $this->db->get_where('seller',array("seller_id"=>$user['ref_user']))->row_array();

		
		$detail = $this->db->get_where('seller_sub',array('ref_trans'=>$trxCode))->result_array();

		
		$this->data['data'] = json_encode($detail);
		$this->data['profile'] = $profile;
		
		$this->data['content'] = $this->load->view('seller/boothDetail',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Order()
	{
		if(!$this->session->userdata('logged_in')) redirect('login.html',301);   
		$where['is_active'] = 1;
		$where['user_id'] = $this->session->userdata('uid');
		$profile =  $this->db->get_where('seller_list',$where)->row_array();
		$where_data['is_active'] = 1;
		$where_data['user_id'] = $this->session->userdata('uid');
		$data = $this->db->get_where('transaction_d_tmp',$where_data)->result_array();
		$this->db->where('category_id', 1);
		$this->db->where('total_stock >', 0);

		$product = $this->Stocks->getList()->result();
		//$this->debug($product);
		$this->data['data'] = json_encode($data);
		$this->data['booth'] = json_encode($product);
		$this->data['products'] = $product;
		$this->data['isMember'] = (int)$profile['is_membership'] == 1 ? true : false;

		$this->data['profile'] = $profile;
		$this->data['content'] = $this->load->view('seller/order',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function InsertOrder($sellerId,$userId)
	{
		
		$post = $this->input->post();

		$total = 0;

		$data["trx_code"] = $this->trx_code("TRX");
		for ($i=0; $i < count($post['booth_product_id']); $i++) { 
			
			# code...
			if($post['booth_product_amount'][$i] > 0) {
				
				$total = $total + ($post['booth_product_amount'][$i] * $post['booth_product_price'][$i]);

				//set transaction detail email
				$detailEmail[$i]['product_id'] = $post['booth_product_id'][$i];
				$detailEmail[$i]['product_name'] = $post['booth_product_name'][$i];
				$detailEmail[$i]['product_price'] =  $post['booth_product_price'][$i];
				$detailEmail[$i]['product_amount'] =$post['booth_product_amount'][$i];
				$detailEmail[$i]['trx_code'] = $data["trx_code"];

				$detailEmail = array_values($detailEmail);

				//set transaction detail
				$detail[$i]['product_id'] = $post['booth_product_id'][$i];
				$detail[$i]['price'] =  $post['booth_product_price'][$i];
				$detail[$i]['amount'] =$post['booth_product_amount'][$i];
				$detail[$i]['trx_code'] = $data["trx_code"];
				$detail = array_values($detail);
				//$this->db->insert('transaction_d', $detail);
				
				//set update stock
				$stock[$i]['amount'] = -1 * $post['booth_product_amount'][$i];
				$stock[$i]['product_id'] = $post['booth_product_id'][$i];
				$stock[$i]['company_name'] = '-';
				$stock[$i]['ref_trans'] =  $data["trx_code"];
				$stock[$i]['type'] =  'TRANS';
				$stock[$i]['dtm_crt'] =  date('Y-m-d');
				$stock = array_values($stock);
				//$this->db->insert('stock', $stock);

			}
		}

		//$this->debug($post);

		$data["trx_datetime"] =  date('Y-m-d H:i:s');
		$data["trx_subtotal"] = $total;//$this->_setSubTotal($post);
		$data["trx_status"] = 0;//$post["country"];
		$data["user_id"] = $userId;
		$data["trx_date"] = date('Y-m-d H:i:s');
		$data["trx_type"] = SELLER_REGISTRATION;
		$data["dtm_crt"] =date('Y-m-d H:i:s');
		$data["trx_tax"] = 0;//( ((int)$this->company()->tax/100) * (int)$data["trx_subtotal"]);
		$data["trx_service_tax"] = 0;//( ((int)$this->company()->service_tax/100) * (int)$data["trx_subtotal"]);
		$data["trx_total"] = $total; //$data["trx_subtotal"] + ($data["trx_subtotal"] * ($this->company()->tax/100)) + ( $data["trx_subtotal"] * ($this->company()->service_tax/100));
		$data["trx_payment_type"] = 1;
		$data["trx_invoice"] = $this->getInvoiceNo($data["trx_code"]);

		$this->db->insert('transaction_h', $data);
		$trxId = $this->db->insert_id();
		
		//insert detail and set booth name and delegation
		$this->db->insert_batch('transaction_d', $detail);
		//update and set booth name and delegation
		$this->db->insert_batch('stock', $stock);
		//insert booth name and delegation
		for ($i=0; $i < count($post['booth_product_id']); $i++) { 
			# code...
			if($post['booth_product_amount'][$i] > 0) {
				
				if($post['booth_product_is_package'][$i] == 1)
				{
					
					$amountBooth = $this->getproductPackageAmount($post['booth_product_id'][$i]) * $post['booth_product_amount'][$i];
				
					if($amountBooth > 0 )
					{
						for ($x=0; $x < $amountBooth; $x++) { 
							//set table sub seller
							$boothName['product_id'] = $post['booth_product_id'][$i];
							$boothName['transaction_id'] = $trxId;
							$boothName['ref_seller_id'] =$sellerId;
							$boothName['ref_user_id'] =$userId;
							$boothName['ref_trans'] = $data["trx_code"];
							$boothName['is_packed'] = 1;

							$boothName["seller_name"] = $post["seller_name"];
							$boothName["seller_address"] = $post["seller_address"];
							$boothName["seller_city"] = $post["seller_city"];
							$boothName["seller_country_code"] = $post["seller_country_code"];
							$boothName["seller_email"] = $post["email"];
							$boothName["seller_phone"] = $post["seller_phone"];
							$boothName["seller_fax"] = $post["seller_fax"];
							$boothName["seller_target_market"] = json_encode($post["seller_target_market"]);
							$boothName["seller_company_profile"] =$post["seller_company_profile"];
							$boothName["seller_website"] = $post["seller_website"];
							$boothName["category_id"] = $post["category_id"];
							$boothName["is_active"] = 1;
							$boothName["dtm_crt"] = date('Y-m-d H:i:s');

							$boothName["is_read_terms_condition"] = $post["is_read_terms_condition"];
							$boothName["full_delegate_name"] = $post["full_delegate_name"];
							$boothName["co_delegate_name"] = $post["co_delegate_name"];
							$boothName["registration_person"] = $post["registration_person"];
							$boothName["job_title"] = $post["job_title"];
							$boothName["seller_province"] = $post["seller_province"];

							$this->db->insert('seller_sub', $boothName);
	
						}
					}

				} else {
					for ($x=0; $x < $post['booth_product_amount'][$i]; $x++) { 
						//set table booth and delegate
						$boothName['product_id'] = $post['booth_product_id'][$i];
							$boothName['transaction_id'] = $trxId;
							$boothName['ref_seller_id'] =$sellerId;
							$boothName['ref_user_id'] =$userId;
							$boothName['ref_trans'] = $data["trx_code"];

							$boothName["seller_name"] = $post["seller_name"];
							$boothName["seller_address"] = $post["seller_address"];
							$boothName["seller_city"] = $post["seller_city"];
							$boothName["seller_country_code"] = $post["seller_country_code"];
							$boothName["seller_email"] = $post["email"];
							$boothName["seller_phone"] = $post["seller_phone"];
							$boothName["seller_fax"] = $post["seller_fax"];
							$boothName["seller_target_market"] = json_encode($post["seller_target_market"]);
							$boothName["seller_company_profile"] =$post["seller_company_profile"];
							$boothName["seller_website"] = $post["seller_website"];
							$boothName["category_id"] = $post["category_id"];
							$boothName["is_active"] = 1;
							$boothName["dtm_crt"] = date('Y-m-d H:i:s');

							$boothName["is_read_terms_condition"] = $post["is_read_terms_condition"];
							$boothName["full_delegate_name"] = $post["full_delegate_name"];
							$boothName["co_delegate_name"] = $post["co_delegate_name"];
							$boothName["registration_person"] = $post["registration_person"];
							$boothName["job_title"] = $post["job_title"];
							$boothName["seller_province"] = $post["seller_province"];

							$this->db->insert('seller_sub', $boothName);

						
					}
					
				}
				


			}
		}		
			//send invoice
			$this->load->library('my_libraries');
			$email['invoiceNo'] = $data["trx_invoice"];
			$email['seller_name'] = $post["seller_name"];
			$email['seller_phone'] = $post["seller_phone"];
			$email['email'] = $post["email"];
			$email['total'] = $total;
			$email['date'] = date('Y-m-d');
			$email['dueDate'] = date('Y-m-d',strtotime($email['date'].'+10 days'));
			$email['res'] = $detailEmail;
			
			$invAttachment = $this->createInvoice($email);
			$emailTnc = $this->createTNC($post);

			//$this->debug($invAttachment);
			$this->my_libraries->registrationSeller($post,$emailTnc,$invAttachment);
	
			//$this->my_libraries->sellerInvoice($email);
		
	}

	public function DeleteOrder()
	{
		if(!$this->session->userdata('logged_in')) redirect('login.html',301);   
		$post = $this->input->post('key');
		//$this->debug($post['trx_code']);
		$this->db->where('id', $post['id']);

		$this->db->delete('transaction_d_tmp');
	
		
		
		echo json_encode(array('status'=> 200));
		
	}


	public function CreateOrder()
	{
		if(!$this->session->userdata('logged_in')) redirect('login.html',301);  
		$post = $this->input->post();

		$where['is_active'] = 1;
		$where['user_id'] = $this->session->userdata('uid');
		$profile =  $this->db->get_where('seller_list',$where)->row_array();

		$tmp_where['is_active'] = 1;
		$tmp_where['user_id'] = $this->session->userdata('uid');
		//$tmp = $this->db->get_where('transaction_d_tmp',$tmp_where)->result_array();
		$total = 0;

		$data["trx_code"] = $this->trx_code("TRX");
		for ($i=0; $i < count($post['booth_product_id']); $i++) { 
			
			# code...
			if($post['booth_product_amount'][$i] > 0) {
				
				$total = $total + ($post['booth_product_amount'][$i] * $post['booth_product_price'][$i]);

				//set transaction detail email
				$detailEmail[$i]['product_id'] = $post['booth_product_id'][$i];
				$detailEmail[$i]['product_name'] = $post['booth_product_name'][$i];
				$detailEmail[$i]['product_price'] =  $post['booth_product_price'][$i];
				$detailEmail[$i]['product_amount'] =$post['booth_product_amount'][$i];
				$detailEmail[$i]['trx_code'] = $data["trx_code"];

				$detailEmail = array_values($detailEmail);

				//set transaction detail
				$detail[$i]['product_id'] = $post['booth_product_id'][$i];
				$detail[$i]['price'] =  $post['booth_product_price'][$i];
				$detail[$i]['amount'] =$post['booth_product_amount'][$i];
				$detail[$i]['trx_code'] = $data["trx_code"];
				$detail = array_values($detail);
				//$this->db->insert('transaction_d', $detail);
				
				//set update stock
				$stock[$i]['amount'] = -1 * $post['booth_product_amount'][$i];
				$stock[$i]['product_id'] = $post['booth_product_id'][$i];
				$stock[$i]['company_name'] = '-';
				$stock[$i]['ref_trans'] =  $data["trx_code"];
				$stock[$i]['type'] =  'TRANS';
				$stock[$i]['dtm_crt'] =  date('Y-m-d');
				$stock = array_values($stock);
				//$this->db->insert('stock', $stock);

			}
		}

		//$this->debug($post);

		$data["trx_datetime"] =  date('Y-m-d H:i:s');
		$data["trx_subtotal"] = $total;//$this->_setSubTotal($post);
		$data["trx_status"] = 0;//$post["country"];
		$data["user_id"] = $this->session->userdata('uid');
		$data["trx_date"] = date('Y-m-d H:i:s');
		$data["trx_type"] = SELLER_REGISTRATION;
		$data["dtm_crt"] =date('Y-m-d H:i:s');
		$data["trx_tax"] = 0;//( ((int)$this->company()->tax/100) * (int)$data["trx_subtotal"]);
		$data["trx_service_tax"] = 0;//( ((int)$this->company()->service_tax/100) * (int)$data["trx_subtotal"]);
		$data["trx_total"] = $total; //$data["trx_subtotal"] + ($data["trx_subtotal"] * ($this->company()->tax/100)) + ( $data["trx_subtotal"] * ($this->company()->service_tax/100));
		$data["trx_payment_type"] = 1;
		$data["trx_invoice"] = $this->getInvoiceNo($data["trx_code"]);

		$this->db->insert('transaction_h', $data);
		$trxId = $this->db->insert_id();
		
		//insert detail and set booth name and delegation
		$this->db->insert_batch('transaction_d', $detail);
		//update and set booth name and delegation
		$this->db->insert_batch('stock', $stock);
		//insert booth name and delegation
		for ($i=0; $i < count($post['booth_product_id']); $i++) { 
			# code...
			if($post['booth_product_amount'][$i] > 0) {
				
				if($post['booth_product_is_package'][$i] == 1)
				{
					
					$amountBooth = $this->getproductPackageAmount($post['booth_product_id'][$i]) * $post['booth_product_amount'][$i];
					
					if($amountBooth > 0 )
					{
						for ($x=0; $x < $amountBooth; $x++) { 
							//set table booth and delegate
							$boothName['product_id'] = $post['booth_product_id'][$i];
							$boothName['transaction_id'] = $trxId;
							$boothName['ref_seller_id'] =$profile['seller_id'];
							$boothName['ref_user_id'] =$this->session->userdata('uid');
							$boothName['ref_trans'] = $data["trx_code"];
							$boothName['is_packed'] = 1;

							$this->db->insert('booth_name', $boothName);
	
							$delegate['transaction_id'] = $trxId;
							$delegate['ref_trans'] = $data["trx_code"];
							$delegate['ref_seller_id'] =$profile['seller_id'];
							$delegate['is_packed'] =1;

							$this->db->insert('booth_delegation', $delegate);
	
						}
					}

				} else {
					for ($x=0; $x < $post['booth_product_amount'][$i]; $x++) { 
						//set table booth and delegate
						$boothName['product_id'] = $post['booth_product_id'][$i];
						$boothName['transaction_id'] = $trxId;
						$boothName['ref_seller_id'] =$profile['seller_id'];
						$boothName['ref_user_id'] =$this->session->userdata('uid');
						$boothName['ref_trans'] = $data["trx_code"];
						$this->db->insert('booth_name', $boothName);

						$delegate['transaction_id'] = $trxId;
						$delegate['ref_trans'] = $data["trx_code"];
						$delegate['ref_seller_id'] =$profile['seller_id'];
						$this->db->insert('booth_delegation', $delegate);

					}
					
				}
				


			}
		}
			//send invoice
			$this->load->library('my_libraries');
			$email['invoiceNo'] = $data["trx_invoice"];
			$email['seller_name'] = $profile["seller_name"];
			$email['seller_phone'] = $profile["seller_phone"];
			$email['email'] = $profile["seller_email"];
			$email['total'] = $total;
			$email['date'] = date('Y-m-d');
			$email['dueDate'] = date('Y-m-d',strtotime($email['date'].'+10 days'));
			$email['res'] = $detailEmail;//$this->db->get_where('v_product_detail',array('trx_code'=>$data["trx_code"]))->result_array();
			
			
			$this->my_libraries->sellerInvoice($email);

			SiteHelpers::alert('success'," Order has been saved succesfuly, Please Create Payment !");

		redirect('Transaction',301);
		
	}

	public function getproductPackageAmount($productId)
	{
	
		$query = "SELECT * FROM product_packet_amount where parent_product =".$productId." ";

		return $this->db->query($query)->row()->amount_psa;
	
	}

	public function create()
	{
		
		$post= $this->input->post();
		$this->load->library('my_libraries');
		$recaptcha = $this->input->post('g-recaptcha-response');
        if (!empty($recaptcha)) {
            $response = $this->recaptcha->verifyResponse($recaptcha);
            if (isset($response['success']) and $response['success'] === true) {
				$sellerId = $this->insertTblSeller($post);
				//$userId = $this->insertTblUser($sellerId,$post);
				
				$msg = "<h1>Registration Successful</h1>";
				$msg.= "<p>Your registration successfully sent.</p>";
				$msg.= "<p>Thank you very much for your participation for BBTF 2020.</p>";
				$msg.= "<p>Please <b><a href='".base_url()."user/login'>login</a></b> to complete your order </p>";
				$msg.= "<p>Should you have any questions or concerns, please feel free to contact us.</p>";
				$msg.= "<br>";
				$msg.= "<br>";
				$msg.= "<p>Thank you for your attention. Sincerely yours</p>";
				$msg.= "<br>";
				$msg.= "<p>Committee of BBTF ".date('Y')."</p>";
		
				$this->session->set_flashdata('success_register',$msg);
		
				redirect('',301);
            } else {
				SiteHelpers::alert('error'," upss!!");
				redirect('seller-registration.html');
			}
		}
		

		
	}

	public function createTNC($data)
	{
		//$this->load->library('M_pdf');
		$path = base_url()."files/pdf/";
		$data['year'] = $this->getYearEvent();
		$data['date'] = $this->getDateEvent();
		
		//$filename = "tnc_".$data['seller_name'].".pdf";
		$filename = "tnc_".str_replace(' ', '_', $data['seller_name']).".pdf";
		$fullpath = $path.$filename;
		$html = $this->load->view('layouts/tnc/tnc_seller', $data,true);
		$mpdf = new mPDF();
		$mpdf->WriteHTML($html);
		$mpdf->Output("./files/pdf/".$filename, "F");
		
		unset($mpdf); // this is the magic
        unset($html); // this is the magic
		return $filename;
	}

	public function createInvoice($data)
	{
		
		$path = base_url()."files/pdf/";
		//$filename = "inv_".$data['seller_name'].".pdf";
		$filename = "inv_".str_replace(' ', '_', $data['seller_name']).".pdf";
		$fullpath = $path.$filename;
		$html = $this->load->view('layouts/email/seller_invoice_pdf', $data,true);
		$mpdf = new mPDF();
		$mpdf->WriteHTML($html);
		$mpdf->Output("./files/pdf/".$filename, "F");

		unset($mpdf); // this is the magic
        unset($html); // this is the magic
		return $filename;
	}



	public function update()
	{
		
		if(!$this->session->userdata('logged_in')) redirect('login.html',301);   
		$post= $this->input->post();
		$data["seller_name"] = $post["seller_name"];
		$data["seller_address"] = $post["seller_address"];
		$data["seller_city"] = $post["seller_city"];
		$data["seller_country_code"] = $post["seller_country_code"];
		$data["seller_email"] = $post["email"];
		$data["seller_phone"] = $post["seller_phone"];
		$data["seller_fax"] = $post["seller_fax"];
		$data["seller_target_market"] = json_encode($post["seller_target_market"]);
		$data["seller_company_profile"] =$post["seller_company_profile"];
		$data["seller_website"] = $post["seller_website"];
		$data["category_id"] = $post["category_id"];
		$data["full_delegate_name"] = $post["full_delegate_name"];
		$data["co_delegate_name"] = $post["co_delegate_name"];
		$data["registration_person"] = $post["registration_person"];
		$data["job_title"] = $post["job_title"];
		$data["seller_province"] = $post["seller_province"];
		$this->db->where('seller_id', $post['seller_id']);
		$this->db->update('seller', $data);

		if($post['password'] != "")
		{
			$user['password'] = md5($post['password']);
			$this->db->where('id', $post['user_id']);
			$this->db->update('tb_users', $user);
		}
	
		SiteHelpers::alert('success'," Data has been saved succesfuly !");
		redirect('Seller',301);
	}



	public function Registration(){

		$this->load->library('recaptcha');
		
		$this->data['recaptcha_html'] = $this->recaptcha->getWidget();
		$this->data['recaptcha_script'] = $this->recaptcha->getScriptTag();

		
		$country = $this->db->get('apps_countries')->result_array();
		$category = $this->db->get_where('category_seller',array('is_active'=>1))->result_array();
		$target_market = $this->db->get_where('general_settings',array('gs_name'=>'seller_target_market'))->result_array();
		$terms = $this->db->get_where('general_settings',array('gs_code'=>'seller_terms_condition'))->row_array();

		$this->db->where('category_id', 1);
		$this->db->where('product_id !=', 15);

		$this->db->where('total_stock >', 0);
		$this->db->order_by('product_id', 'asc');

		$product = $this->Stocks->getList()->result();
		//$this->debug($product);
		
		$this->data['products'] = $product;

		$this->data['country'] = $country;
		$this->data['category'] = $category;
		$this->data['target_market'] = $target_market;
		$this->data['terms'] = $terms;

		$this->data['company'] = $this->db->get('company')->row_array();
		$this->data['content'] = $this->load->view('frontend/seller-registration',$this->data,true);    
    	$this->load->view('layouts/registration/main',$this->data);
	}

	

	public function check_email_exist($email = "" ) {
		$email = trim($this->input->get('email'));
		$this->db->where('email', $email);
		$this->db->where('active', 1);
		$this->db->where('group_id', 5);
		
		$row = $this->db->get('tb_users')->num_rows();
		

		if($row == 1){
			//$this->form_validation->set_message('check_email_exist', 'The {field} has been exist !');
            echo json_encode(array('status'=> true));

		} else {
			echo json_encode(array('status'=> false));
		}

	}

	public function check_email_exist_sub($email = "" ) {
		$email = trim($this->input->get('email'));
		$this->db->where('email_login', $email);		
		$row = $this->db->get('seller_sub')->num_rows();
		

		if($row == 1){
			//$this->form_validation->set_message('check_email_exist', 'The {field} has been exist !');
            echo json_encode(array('status'=> true));

		} else {
			echo json_encode(array('status'=> false));
		}

	}

	public function insertTblUser($sellerId = null,$post = array () ){

		$post = $this->input->post();

		$data["email"] = $post["email"];
		$data["password"] = md5($post["password"]);
		$data["username"] = $post["seller_name"];
		$data["first_name"] = $post["seller_name"];
		$data["active"] = 1;
		$data["created_at"] = date('Y-m-d');
		$data["group_id"] = 5;
		$data["ref_user"] = (int)$sellerId;

		$this->db->insert('tb_users', $data);
		
		$id =  $this->db->insert_id();
		
		return $id;

	}

	public function insertTblSeller($post = array () ){

		$data["seller_name"] = $post["seller_name"];
		$data["seller_address"] = $post["seller_address"];
		$data["seller_city"] = $post["seller_city"];
		$data["seller_country_code"] = $post["seller_country_code"];
		$data["seller_email"] = $post["email"];
		$data["seller_phone"] = $post["seller_phone"];
		$data["seller_fax"] = $post["seller_fax"];
		$data["seller_target_market"] = json_encode($post["seller_target_market"]);
		$data["seller_company_profile"] =$post["seller_company_profile"];
		$data["seller_website"] = $post["seller_website"];
		$data["category_id"] = $post["category_id"];
		$data["is_active"] = 1;
		$data["dtm_crt"] = date('Y-m-d H:i:s');

		$data["is_read_terms_condition"] = $post["is_read_terms_condition"];
		$data["full_delegate_name"] = $post["full_delegate_name"];
		$data["co_delegate_name"] = $post["co_delegate_name"];
		$data["registration_person"] = $post["registration_person"];
		$data["job_title"] = $post["job_title"];
		$data["seller_province"] = $post["seller_province"];


		$this->db->insert('seller', $data);
		
		$id =  $this->db->insert_id();

		$userId = $this->insertTblUser($id,$data);
		
		//InsertOrder
		$this->InsertOrder($id,$userId);
	

	}

	public function insertTblTrx($userId = "",$post = array () ){

		$data["trx_code"] = $this->trx_code("TRX");;
		$data["trx_datetime"] =  date('Y-m-d H:i:s');
		$data["trx_subtotal"] = 0;//$this->_setSubTotal($post);
		$data["trx_status"] = 0;//$post["country"];
		$data["user_id"] = $userId;
		$data["trx_date"] = date('Y-m-d H:i:s');
		$data["trx_type"] =SELLER_REGISTRATION;
		$data["dtm_crt"] =date('Y-m-d H:i:s');
		$data["trx_tax"] = 0;//( ((int)$this->company()->tax/100) * (int)$data["trx_subtotal"]);
		$data["trx_service_tax"] = 0;//( ((int)$this->company()->service_tax/100) * (int)$data["trx_subtotal"]);
		$data["trx_total"] = $post['trx_total']; //$data["trx_subtotal"] + ($data["trx_subtotal"] * ($this->company()->tax/100)) + ( $data["trx_subtotal"] * ($this->company()->service_tax/100));
		$data["trx_payment_type"] = 1;
		$data["trx_invoice"] = "";

		$this->db->insert('transaction_h', $data);
		$id =  $this->db->insert_id();

		//$this->_insertTrxDetail($data["trx_code"],$post);
		
		return $id;

	}
	

	
	public function downloadLoc($trxCode)
	{
		# code...
		$data = $this->db->get_where('v_product_detail',array("trx_code"=>$trxCode))->result_array();
		
		$html = $this->load->view('layouts/loc',array("data"=>$data),true);

		echo $html;
		exit;
		$this->debug($html);
	}

	public function listing($id)
	{
		$profile = $this->db->get_where('seller',array('seller_id'=>$id))->row_array();
	
		$val = "";
		if($profile['is_listing'] == 1) {$val = 0;}
		if($profile['is_listing'] == 0 || $profile['is_listing'] == "" ) {$val = 1;}
		$update['is_listing'] = $val;
		// /$this->debug($update);
		$this->db->where('seller_id', $id);
		$this->db->update('seller', $update);

		$this->db->where('ref_seller_id', $id);
		$this->db->update('seller_sub', $update);
		

		SiteHelpers::alert('success'," Listing has been succesfuly !");
		redirect('Memberships/Seller',301);

	}

	public function ResetPassword()
	{
		$post = $this->input->post();
		//
		$id = $post['buyerId'];
		$newPassword = $post['newPassword'];
		$profile = $this->db->get_where('seller_sub',array('id'=>$id))->row_array();
		
		$update['password_login'] = md5($newPassword);
		$update['password_view'] = $newPassword;
		//$this->debug($update);
		$this->db->where('id', $profile['id']);
		$this->db->update('seller_sub', $update);

		$this->load->library('my_libraries');
		$email['seller_name'] = $profile['seller_name'];
		$email['email'] = $profile['seller_email'];
		$email['password'] = $newPassword;
		$this->my_libraries->resetPasswordSeller($email);

		SiteHelpers::alert('success'," Reset Passwod has been succesfuly !");
		redirect('Seller/boothDetail/'.$post['trxCode'],301);

	}

	public function resendInv($trxCode)
	{
		$trx = $this->db->get_where('transaction_h',array('trx_code'=>$trxCode))->row_array();
		$trxdetail = $this->db->get_where('v_product_detail',array('trx_code'=>$trxCode))->result_array();
		$user = $this->db->get_where('tb_users',array("id"=>$trx['user_id']))->row_array();
		$profile = $this->db->get_where('seller',array("seller_id"=>$user['ref_user']))->row_array();
		
		//send invoice
		$this->load->library('my_libraries');
		$email['invoiceNo'] = $trx["trx_invoice"];
		$email['seller_name'] = $profile["seller_name"];
		$email['seller_phone'] = $profile["seller_phone"];
		$email['email'] = $profile["seller_email"];
		$email['total'] = $trx["trx_total"];
		$email['date'] = date('Y-m-d');
		$email['dueDate'] = date('Y-m-d',strtotime($email['date'].'+10 days'));
		$email['res'] = $trxdetail;//$this->db->get_where('v_product_detail',array('trx_code'=>$data["trx_code"]))->result_array();
		$this->my_libraries->sellerInvoice($email);
		
		$update['is_send_inv'] = (int)$trx['is_send_inv'] + 1;
		$this->db->where('trx_code', $trxCode);
		$this->db->update('transaction_h', $update);
		
		SiteHelpers::alert('success'," Resend succesfuly !");
		redirect('Admin/Transactions',301);

	}

	public function updateBoothNUmber()
	{
		if(!$this->session->userdata('logged_in')) redirect('login.html',301);
		$post =$this->input->post();
		$profile = $this->db->get_where('seller_sub',array("id"=>$post['id']))->row();


		$fileName = "";
		if(isset($_FILES["image"]["name"]))
		{
			//$this->debug(count($this->input->post($_FILES["image"])));
			$config['upload_path'] = './files/booth_layouts/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']  = '1024';
			
			if(count($this->input->post($_FILES["image"])) > 3){
					//unlink exist data
					
					if($profile->booth_number_img != "" || $profile->booth_number_img != null){
						$image = $config['upload_path'].$profile->booth_number_img;
						unlink($image);
					}
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if (!$this->upload->do_upload('image')){
						$error = array('error' => $this->upload->display_errors());
						
						//validation image has error
						SiteHelpers::alert('error',$this->upload->display_errors());
						redirect('Seller/boothDetail/'.$post['trxCode']);
						} else {
							$data = $this->upload->data();
							$fileName = $data["file_name"];
							$profile->booth_number_img = $fileName;
							
						}
			}
			
		}

		$object['booth_number'] = $post['boothNumber'];
		$object['booth_number_img'] = $fileName;
		

		$this->db->where('id', $post['id']);
		$this->db->update('seller_sub', $object);

		SiteHelpers::alert('success'," Resend succesfuly !");
		redirect('Seller/boothDetail/'.$post['trxCode'],301);

	}

	public function getProfilePsa($id){
		$this->db->where('id', $id);
		$res = $this->db->get('list_seller_sub');
		$targetMarket = json_decode($res->row()->seller_target_market);
		$data = $res->row_array();
		$data['seller_target_market'] = implode(" , ",$targetMarket);
		//$data['nature_of_business'] = $this->getNatureOfBusiness($res->row()->nature_of_business);
		$category = $this->db->get_where('category_seller',array('is_active'=>1))->result_array();

		$this->data['category'] = $category;
		$this->data['profile'] = $data;
		$html = $this->load->view('seller/profile', $this->data, true);
		
		echo $html;
	}

	


}

/* End of file Controllername.php */
