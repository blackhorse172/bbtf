<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Psa extends MY_Controller {
	var $userGroup;

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);
		$this->load->model('Psa_model','psa');
		$this->load->model('Psa_schedule_model','psaSchedule');
		$this->userGroup = $this->session->userdata('gid');
		
	}

	public function index()
	{
		$psaDate = $this->psa->getPeriode();
		if($psaDate == 0)
		{
			redirect("Errors?title=PSA&message=PSA OUT DATE !");
		}
		$psaDay = $psaDate == 1 ? "Periode 1" : "Periode 2";
		$request = $this->psa->getListRequest();
		

		$this->data['title'] = "PSA ".$psaDay;
		$this->data['remainingSlot'] = $this->psa->getRemainingSlot($psaDate);
		$this->data['defaultSlot'] = $this->psa->getDefaultAmount();
		$this->data['systemSlot'] = $this->psa->getSystemAmount();
		$this->data['scheduleData'] = json_encode($this->psaSchedule->getSchedule());

		$this->data['dataConfirmList'] = json_encode($this->psa->getConfirmList());
		
		$this->data['data'] = json_encode($this->psa->getUserList());
		$this->data['countRequest'] = $request->num_rows();
		$this->data['countConfirm'] = count($this->psa->getConfirmList());


		if($this->session->userdata('gid') == 4){
			$this->data['content'] = $this->load->view('psa/index_buyer',$this->data,true);    
		} else if($this->session->userdata('gid') == 0)
		{
			$this->data['content'] = $this->load->view('psa/index_seller',$this->data,true);    
		}
		
    	$this->load->view('layouts/main',$this->data);
		
	}

	public function information()
	{
		$this->data['content'] = $this->load->view('psa/information',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function saveReqPsa()
	{
		# code...
		$data = $this->_setDataArray(PSA_STATUS_REQUEST);
		//$this->debug($data);
		$insert = $this->psa->insertReqPsa($data);
		$ret['status'] = false;
		if($insert > 0)
		{
			$ret['status'] = 1;
		}

		echo json_encode($ret);
	}

	private function _setDataArray($status)
	{
		# code...
		$post = $this->input->post();
		$data = [];
		//$this->debug($post);
		
		foreach ($post['user'] as $key => $value) {

			if((int)$this->userGroup == 4)
			{
				$data[$key]['seller_id'] = $value['id'];
				$data[$key]['buyer_id'] =  $this->session->userdata('ref_id');

			} else if((int)$this->userGroup == 0)
			{
				$data[$key]['seller_id'] = $this->session->userdata('ref_id');
				$data[$key]['buyer_id'] = $value['id'];
			}

			$data[$key]['time'] = date('H:i:s');
			$data[$key]['date'] = date('Y-m-d');
			$data[$key]['status'] = $status;
			$data[$key]['dtm_crt'] = date('Y-m-d H:i:s');
			$data[$key]['year'] = date('Y');
			$data[$key]['periode'] = $this->psa->getPeriode();
		}
		

		return $data;
	}

	public function ConfirmPsa()
	{
		$id = $this->input->post('user');
		
		 $update = $this->psa->updateStatusPsa($id);
		// $this->debug($update);
		$ret['status'] = 0;
		if($update > 0)
		{
			$ret['status'] = 1;
		}

		if($ret['status'] === 1 ) {
			//create schedule
			$psaData =  $this->psa->getRowData($id);
			$data['seller_sub_id'] = $psaData->seller_id;
			$data['buyer_id'] = $psaData->buyer_id;
			$data['schedule_id'] = $psaData->psa_schedule_id;
			$data['periode'] = $psaData->periode;
			$data['year'] = $psaData->year;
			$data['dtm_crt'] = date('Y-m-d H:i:s');
			//create psa schedule
			$create = $this->psaSchedule->create($data);

		}


		echo json_encode($ret);
	}


}

/* End of file Psa.php */

?>
