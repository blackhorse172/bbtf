<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Delegations extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);
	}
	

	public function index()
	{
		$id = $this->session->userdata('bid');
		
		$profile = $this->db->get_where('seller_sub',array('id'=>$id))->row_array(); //get_where('ref_trans');
		//$this->debug($profile);
			$target_market = $this->db->get_where('general_settings',array('gs_name'=>'seller_target_market'))->result_array();
			$country = $this->db->get('apps_countries')->result_array();
			$category = $this->db->get_where('category_seller',array('is_active'=>1))->result_array();

			$this->data['country'] = $country;
			$this->data['category'] = $category;
			$this->data['target_market'] = $target_market;
			$this->data['profile'] = $profile;

		//$this->data['data'] = $data;
		$this->data['trxCode'] = $profile['ref_trans'];
		$this->data['content'] = $this->load->view('delegation/create_booth',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function boothNumber()
	{
		$id = $this->session->userdata('bid');
		
		$profile = $this->db->get_where('seller_sub',array('id'=>$id))->row_array(); //get_where('ref_trans');
		//$this->debug($profile);
			$target_market = $this->db->get_where('general_settings',array('gs_name'=>'seller_target_market'))->result_array();
			$country = $this->db->get('apps_countries')->result_array();
			$category = $this->db->get_where('category_seller',array('is_active'=>1))->result_array();

			$this->data['country'] = $country;
			$this->data['category'] = $category;
			$this->data['target_market'] = $target_market;
			$this->data['profile'] = $profile;

		//$this->data['data'] = $data;
		$this->data['trxCode'] = $profile['ref_trans'];
		$this->data['content'] = $this->load->view('delegation/booth_number',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function updateBoothName()
	{
		# code...
		$post = $this->input->post();
		for ($i=0; $i < count($post['booth_id']); $i++) { 
			# code...
			$object['booth_name'] = $post['booth_name'][$i];
			$this->db->where('id', $post['booth_id'][$i]);
			$this->db->update('booth_name', $object);
		}
		SiteHelpers::alert('success'," Booth Name has been saved succesfuly !");

		redirect('Delegations',301);
	}

	public function updateData()
	{
		# code...
		$post = $this->input->post();
		$post['seller_target_market'] =json_encode($post['seller_target_market']);
		$id = $this->session->userdata('bid');
			$this->db->where('id', $id);
			$this->db->update('seller_sub', $post);
		
		SiteHelpers::alert('success'," Seller Profile has been saved succesfuly !");

		redirect('Delegations',301);
	}

}

/* End of file Delegations.php */

?>
