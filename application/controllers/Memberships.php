<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Memberships extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);

		$this->load->model('Memberships');
		$this->load->model('MembershipCategories');
		$this->load->model('GeneralSettings');
		//Do your magic here
	}

	private function getYearEvent()
	{
		return $this->GeneralSettings->getYearEvent();
	}

	private function getDateEvent()
	{
		return $this->GeneralSettings->getDateEvent();
	}
	

	public function index()
	{
		$data = $this->Memberships->join();
		
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('membership/index',$this->data,true);    
		$this->load->view('layouts/main',$this->data);
		
	}


	public function Seller($id= null)
	{
		if($id != null) {
			$profile = $this->db->get_where('seller_list',array('seller_id'=>$id))->row_array();
			$where['is_active'] = 1;
			$where['user_id'] = $this->session->userdata('uid');
			$target_market = $this->db->get_where('general_settings',array('gs_name'=>'seller_target_market'))->result_array();
			$country = $this->db->get('apps_countries')->result_array();
			$category = $this->db->get_where('category_seller',array('is_active'=>1))->result_array();

			$this->data['country'] = $country;
			$this->data['category'] = $category;
			$this->data['target_market'] = $target_market;
			$this->data['profile'] = $profile;
			$this->data['content'] = $this->load->view('membership/seller_profile',$this->data,true);
		} else {
			$this->db->order_by('seller_id', 'desc');
			$data = $this->db->get_where('seller_list')->result_array();
			//$this->debug($data);
			$this->data['data'] = json_encode($data);
			$this->data['content'] = $this->load->view('membership/seller_index',$this->data,true);
		}
		    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Media($id= null)
	{
		if($id != null) {
			$profile = $this->db->get_where('media_register',array('id'=>$id))->row_array();
			$where['is_active'] = 1;
			$where['user_id'] = $this->session->userdata('uid');
			$target_market = $this->db->get_where('general_settings',array('gs_name'=>'seller_target_market'))->result_array();
			$country = $this->db->get('apps_countries')->result_array();
			$category = $this->db->get_where('category_seller',array('is_active'=>1))->result_array();

			$this->data['country'] = $country;
			$this->data['category'] = $category;
			$this->data['target_market'] = $target_market;
			$this->data['profile'] = $profile;
			$this->data['content'] = $this->load->view('membership/media_profile',$this->data,true);
		} else {
			$data = $this->db->get_where('media_register')->result_array();
		
			$this->data['data'] = json_encode($data);
			$this->data['content'] = $this->load->view('membership/media_index',$this->data,true);
		}
		    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Buyer($id = null)
	{
		if($id != null) {
			$profile = $this->db->get_where('buyer_list',array('buyer_id'=>$id))->row_array();
			$where['is_active'] = 1;
			$where['user_id'] = $this->session->userdata('uid');
			$target_market = $this->db->get_where('general_settings',array('gs_name'=>'seller_target_market'))->result_array();
			$country = $this->db->get('apps_countries')->result_array();
			$hotel = $this->db->get_where('buyer_hotel',array('is_active'=>1,'year'=>date('Y'),'stock >'=>0))->result_array();
		
			$this->data['country'] = $country;
			$this->data['target_market'] = $target_market;
			$this->data['profile'] = $profile;
			$this->data['hotels'] = $hotel;

			$flight = $this->db->get_where('buyer_flight',array("buyer_id"=>$profile['buyer_id']))->row_array();
			$arrivalDate = $this->db->get_where('general_settings',array("gs_name"=>"arrival_dates"))->result_array();
			$departDate = $this->db->get_where('general_settings',array("gs_name"=>"departure_dates"))->result_array();

			$this->data['flight'] = $flight;
			$this->data['arrivalDate'] = $arrivalDate;
			$this->data['departDate'] = $departDate;

			//$this->debug($profile);
			$this->data['content'] = $this->load->view('membership/buyer_profile',$this->data,true);
		} else {
			$this->db->order_by('buyer_id', 'desc');
			$data = $this->db->get('buyer_list')->result_array();
			
			$this->data['data'] = json_encode($data);
			$this->data['content'] = $this->load->view('membership/buyer_index',$this->data,true);
		}
    	$this->load->view('layouts/main',$this->data);
	}

	public function TradeBuyer($id = null)
	{
		if($id != null) {
			$profile = $this->db->get_where('trade_buyer',array('id'=>$id))->row_array();
			$where['is_active'] = 1;
			$where['user_id'] = $this->session->userdata('uid');
			$target_market = $this->db->get_where('general_settings',array('gs_name'=>'seller_target_market'))->result_array();
			$country = $this->db->get('apps_countries')->result_array();
			//$category = $this->db->get_where('category_seller',array('is_active'=>1))->result_array();

			$this->data['country'] = $country;
			//$this->data['category'] = $category;
			$this->data['target_market'] = $target_market;
			$this->data['profile'] = $profile;
			$this->data['fee'] = $this->db->get_where('general_settings',array('gs_code'=>'trade_buyer_fee'))->row()->gs_value;;
			$this->data['days'] = $this->db->get_where('general_settings',array('gs_code'=>'number_days_tradebuyer'))->row()->gs_value;
			//$this->debug($profile);
			$this->data['content'] = $this->load->view('membership/trade_buyer_profile',$this->data,true);
		} else {
			$this->db->order_by('id', 'desc');
			$data = $this->db->get('tradebuyer_list')->result_array();
		
			$this->data['data'] = json_encode($data);
			$this->data['content'] = $this->load->view('membership/trade_buyer_index',$this->data,true);
		}
    	$this->load->view('layouts/main',$this->data);
	}

	public function saveBuyer(){
		$post = $this->input->post();
		$buyer = $this->db->get_where('buyer',array('buyer_id'=>$post['buyer_id']))->row_array();
		

		$data["company_name"] = $post["company_name"];
		$data["buyer_name"] = $post["first_name"];
		//$data["is_read_terms_condition"] = $post["is_read_terms_condition"];
		$data["first_name"] = $post["first_name"];
		$data["last_name"] = $post["last_name"];
		$data["gender"] = $post["gender"];
		$data["job_title"] = $post["job_title"];

		$data["buyer_city"] = $post["buyer_city"];
		$data["buyer_country_code"] = $post["buyer_country_code"];
		$data["buyer_email"] = $post["email"];
		$data["buyer_phone"] = $post["buyer_phone"];
		
		$data["nature_of_business"] = $post["nature_of_business"];//json_encode($post["target_market"]);
		$data["buyer_company_profile"] =$post["buyer_company_profile"];
		$data["buyer_website"] = $post["buyer_website"];
		$data["is_active"] = 1;
		$data["dtm_upd"] = date('Y-m-d H:i:s');
		
		if($buyer['buyer_class'] == null || $buyer['buyer_class'] == '')
		{
			$data["buyer_class"] = $post['buyer_class'];
		} else {
			$data["buyer_class"] = $buyer['buyer_class'];
		}

		if(isset($post['buyer_class']) &&  $post['buyer_class'] != "")
		{
			//create tmp doku
			$this->load->model('Dokus');
			$this->load->library('my_libraries');
			$full = $this->db->get_where('product',array('category_id'=>2,'product_name'=>'FULLY HOSTED'))->row_array();
			$partial = $this->db->get_where('product',array('category_id'=>2,'product_name'=>'PARTIALLY HOSTED'))->row_array();
			$charge = $this->my_libraries->getChargeCreditCard();

			$email['email'] = $data["buyer_email"];
			$email['buyer_class'] = $post['buyer_class'];
			$email['buyer_name'] = $data["first_name"];
			$email['first_name'] = $data["first_name"];
			$email['last_name'] = $data["last_name"];

			$email['company_name'] = $data["company_name"];
			$email['fee'] = 0;
			$email['buyer_type'] = '';
			
			if($data['buyer_class']== 1)
			{
				$fee = $full["product_price"] + ($full["product_price"] * ($charge/100));
				$doku = $this->Dokus->createTransactionTmp(array('fee'=>$fee,'buyer_id'=>$post['buyer_id'],"user_type"=>"BUYER"));
				$attachment = $this->createTNC($post,1);
				
				$email['gender'] = $buyer['gender'];
				$email['buyer_type'] = "Fully Hosted";
				$email['invoice_date'] = date('Y-m-d');
				$email['invoice_id'] = $doku['transidmerchant'];
				$email['invoice_due_date'] = date('Y-m-d',strtotime($email['invoice_date']."+7 days"));
				$email['fee'] = number_format($full["product_price"],2);
				$email['decline_url'] = base_url()."Buyer/declineClass/".$doku['transidmerchant'];
				$email['approve_url'] = base_url()."Doku/paymentRequest/".$doku['transidmerchant'];
				$this->my_libraries->buyerSendClass($email,$attachment);
			}

			if($data['buyer_class']== 2)
			{
				$fee = $partial["product_price"] + ($partial["product_price"] * ($charge/100));
				$doku = $this->Dokus->createTransactionTmp(array('fee'=>$fee,'buyer_id'=>$post['buyer_id'],"user_type"=>"BUYER"));
				$attachment = $this->createTNC($post,2);

				$email['gender'] = $data['gender'];
				$email['buyer_type'] = "Partially Hosted";
				$email['invoice_date'] = date('Y-m-d');
				$email['invoice_id'] = $doku['transidmerchant'];
				$email['invoice_due_date'] = date('Y-m-d',strtotime($email['invoice_date']."+7 days"));
				$email['fee'] = number_format($partial["product_price"],2);
				$email['decline_url'] = base_url()."Buyer/declineClass/".$doku['transidmerchant'];
				$email['approve_url'] = base_url()."Doku/paymentRequest/".$doku['transidmerchant'];
				$this->my_libraries->buyerSendClass($email,$attachment);
			}

			if($data['buyer_class']== 6)
			{
				
				$email['gender'] =  $post["gender"];
				$this->my_libraries->buyerAsSeller($email);
			}

			if($data['buyer_class']== 7)
			{
				$email['gender'] =  $post["gender"];
				$this->my_libraries->buyerAsTradeBuyer($email);
			}

			//$this->debug($email);
			
		}


		if($post['password'] != "")
		{
				$user['password'] = md5($post['password']);
				$this->db->where('id', $post['user_id']);
				$this->db->update('tb_users', $user);
		}

		if(isset($post['buyer_hotel']) &&  $post['buyer_hotel'] != "")
		{
			$hotel = $this->db->get_where('buyer_hotel',array('id'=>$post['buyer_hotel'],'year'=>date('Y')))->row_array();
			$stockHotel['buyer_id'] = $buyer['buyer_id'];
			$stockHotel['buyer_hotel_id'] = $hotel['id'];
			$stockHotel['amount'] = 1;
			$stockHotel['year'] = date('Y');
			$stockHotel['dtm_crt'] = date('Y-m-d H:i:s');
			$this->db->insert('buyer_hotel_stock', $stockHotel);
			$hotelUpd['stock'] = (int)$hotel['stock'] - 1;
			$this->db->where('id', $post['buyer_hotel']);
			$this->db->update('buyer_hotel', $hotelUpd);

			$data['buyer_hotel'] = $post['buyer_hotel'];
		}

		$this->db->where('buyer_id', $post['buyer_id']);
		$this->db->update('buyer', $data);

		
		
		//$info["buyer_id"] = $id;
		$info["do_you_selling_bali_and_beyond_product"] = $post["do_you_selling_bali_and_beyond_product"];
		$info["please_state_you_local_partner_in_bali_indonesia"] = $post["please_state_you_local_partner_in_bali_indonesia"];
		$info["member_id"] = $post["member_id"];
		$info["since_when_you_promoted_indonesia_destination"] = $post["since_when_you_promoted_indonesia_destination"];
		$info["is_this_your_first_time_attending_on_bbtf"] = $post["is_this_your_first_time_attending_on_bbtf"];
		$info["how_did_you_know_about_bbtf"] = $post["how_did_you_know_about_bbtf"];
		//$info["buyer_fax"] = $post["fax"];
		$info["indicated_main_destination"] = $post["indicated_main_destination"];//json_encode($post["target_market"]);
		$info["which_products_are_you_interested_in"] = json_encode($post["which_products_are_you_interested_in"]); 
		$info["do_you_sell_any_package_in_south_east_asia"] = $post["do_you_sell_any_package_in_south_east_asia"];
		$info["biggest_partner"] = $post["biggest_partner"];
		$info["potential_group"] = $post["potential_group"];
		$info["explore_bbtf"] = $post["explore_bbtf"];

		//$this->debug($info);

		$this->db->where('buyer_id', $post['buyer_id']);
		$this->db->update('buyer_additional_info', $info);
		
	
		SiteHelpers::alert('success'," Data has been saved succesfuly !");
		redirect('Memberships/Buyer',301);
	}

	public function createTNC($data,$class)
	{
		$this->load->library('M_pdf');
		$data['year'] = $this->getYearEvent();
		$data['date'] = $this->getDateEvent();

		$path = base_url()."files/pdf/";
		$filename = "tnc_".str_replace(' ', '_', $data['company_name']).".pdf";
		$fullpath = $path.$filename;

		if($class == 1){
			$html = $this->load->view('layouts/tnc/tnc_buyer_full', $data,true);
		}

		if($class == 2){
			$html = $this->load->view('layouts/tnc/tnc_buyer_partial', $data,true);
		}
		
		$this->m_pdf->pdf->WriteHTML($html);
		$this->m_pdf->pdf->Output("./files/pdf/".$filename, "F");

		return $filename;
	}


	public function saveTradeBuyer(){
		$post = $this->input->post();
		//$trade = $this->db->get_where('trade_buyer',array('id'=>$post['buyer_id']))->row_array();
		$post["is_active"] = 1;
		$post["dtm_crt"] = date('Y-m-d H:i:s');
		//$this->debug($data);
		$this->db->where('id', $post['id']);
		$this->db->update('trade_buyer', $post);
	
			
		SiteHelpers::alert('success'," Data has been saved succesfuly !");
		redirect('Memberships/TradeBuyer',301);
	}

	public function Create()
	{
		
		$group = $this->MembershipCategories->get_list();
		$this->data['isEdit'] = false;
		$this->data['group'] = $group;
		$this->data['content'] = $this->load->view('membership/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function update($userType)
	{
		$post= $this->input->post();

		if($userType == "seller"){

			//$this->debug($post);
			
			$data["seller_name"] = $post["seller_name"];
			$data["seller_address"] = $post["seller_address"];
			$data["seller_city"] = $post["seller_city"];
			$data["seller_country_code"] = $post["seller_country_code"];
			$data["seller_email"] = $post["email"];
			$data["seller_phone"] = $post["seller_phone"];
			$data["seller_fax"] = $post["seller_fax"];
			$data["seller_target_market"] = json_encode($post["seller_target_market"]);
			$data["seller_company_profile"] =$post["seller_company_profile"];
			$data["seller_website"] = $post["seller_website"];
			$data["category_id"] = $post["category_id"];
			$data["full_delegate_name"] = $post["full_delegate_name"];
			$data["co_delegate_name"] = $post["co_delegate_name"];
			$data["registration_person"] = $post["registration_person"];
			$data["job_title"] = $post["job_title"];
			$data["seller_province"] = $post["seller_province"];
			$this->db->where('seller_id', $post['seller_id']);
			$this->db->update('seller', $data);
	
			if($post['password'] != "")
			{
				$user['password'] = md5($post['password']);
				$this->db->where('id', $post['user_id']);
				$this->db->update('tb_users', $user);
			}
		
			SiteHelpers::alert('success'," Data has been saved succesfuly !");
			redirect('Memberships/Seller',301);
		} else if($userType == "buyer")
		{

		}
		else if($userType == "media")
		{

			if($post['media_class'] != "") {
					// $this->load->library('my_libraries');
					// $email['email'] = $post['email'];
					// $email['type'] = $post['media_class'];
					// $email['full_name'] = $post['full_name'];
					// $this->my_libraries->MediaClass($email);
					
			}

			$this->db->where('id', $post['id']);
			$this->db->update('media_register', $post);
			SiteHelpers::alert('success'," Data has been saved succesfuly !");
			redirect('Memberships/Media',301);
		}
		

		// }
		
		// $group = $this->MembershipCategories->get_list();
		// $this->data['isEdit'] = false;
		// $this->data['group'] = $group;
		// $this->data['content'] = $this->load->view('membership/create',$this->data,true);    
    	// $this->load->view('layouts/main',$this->data);
	}
	

	public function edit($id)
	{
		$data = $this->Memberships->get($id);
		if($data == '')
		{
			redirect('error/notFound');
		}
		//$this->debug($data);
		$group = $this->MembershipCategories->get_list();
		$this->data['isEdit'] = true;
		$this->data['group'] = $group;
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('membership/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function save()
	{
		//$this->debug($this->input->post());
		$membership = $this->Memberships->new_row();

		$membership->membership_name = $this->input->post('name');
		$membership->membership_fee = (int)str_replace('.','',$this->input->post('fee')) ;	
		$membership->category_id = $this->input->post('category_id');
		$membership->dtm_crt = date('Y-m-d H:i:s');
		$membership->usr_crt = $this->session->userdata('userName');

		$membership->active = 1;	

		$id = $membership->save();
		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Memberships',301);
	}

	public function saveEdit()
	{
		$id = $this->input->post('id');
		$membership = $this->Memberships->get($id);
		//$this->debug($this->input->post());
		
		$membership->membership_name = $this->input->post('name');
		$membership->membership_fee = (int)str_replace('.','',$this->input->post('fee')) ;	
		$membership->category_id = $this->input->post('category_id');
		$membership->dtm_upd = date('Y-m-d H:i:s');
		$membership->usr_upd = $this->session->userdata('userName');

		$id = $membership->save();
		SiteHelpers::alert('success'," Data has Edited succesfuly !");

		redirect('Memberships',301);
	}

	public function getExistEmail()
	{
		$email = $this->input->post('email');
		
		$res = false;
		$this->db->where('email', $email);
		$this->db->from('tb_membership');
		$q = $this->db->get();
		
		if($q->num_rows() > 0) $res = true;

		echo json_encode($res);
		
	}

	public function nonActive($id)
	{
		$membership = $this->Memberships->get($id);

		if($membership->active == 1){
			$membership->active = 0;
 		} else {
			$membership->active = 1;
		 }

		 $membership->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Memberships',301);
	}

	public function nonActiveSeller($id)
	{
		$membership = $this->db->get_where('seller',array('seller_id'=>$id))->row();
		 //$this->Memberships->get($id);

		if($membership->is_active == 1){
			$membership->is_active = 0;
 		} else {
			$membership->is_active = 1;
		 }


		 $this->db->where('seller_id', $membership->seller_id);
		 $this->db->update('seller', $membership);
		 

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Memberships/Seller',301);
	}

	public function nonActiveSellerMembership($id)
	{
		$membership = $this->db->get_where('seller',array('seller_id'=>$id))->row();
		 //$this->Memberships->get($id);
		if($membership->is_membership == 1){
			$membership->is_membership = 0;
 		} else {
			$membership->is_membership = 1;
		 }

		 $this->db->where('seller_id', $membership->seller_id);
		 $this->db->update('seller', $membership);

		 SiteHelpers::alert('success'," Data has been Change succesfuly !");

		redirect('Memberships/Seller',301);
	}

	public function nonActiveBuyer($id)
	{
		$membership = $this->db->get_where('tb_users',array('id'=>$id))->row();

		if($membership->active == 1){
			$membership->active = 0;
 		} else {
			$membership->active = 1;
		 }

		 $membership->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Memberships/Buyer',301);
	}

	

	public function categoryNonActive($id)
	{
		$memberships = $this->MembershipCategories->get($id);

		if($memberships->is_active == '1'){
			$memberships->is_active = '0';
 		} else {
			$memberships->is_active = '1';
		 }

		 $memberships->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Memberships/category',301);
	}

	public function category()
	{
		$data = $this->MembershipCategories->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('membership/category',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function createCategory()
	{
		$this->data['isEdit'] = false;
		
		$this->data['content'] = $this->load->view('membership/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function editCategory($id)
	{
		$this->data['isEdit'] = true;
		$data = $this->MembershipCategories->get($id);
		//$this->debug($data);
		if($data == null){
			SiteHelpers::alert('warning'," Data not found !");

			redirect('Memberships/category',301);
		}
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('membership/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function saveCategory()
	{
		$id = $this->input->post('id');
		
		if($this->input->post('isEdit') == '') {
			$category = $this->MembershipCategories->new_row();
			$msg = " Data has been Create succesfuly !";
		} else {
			$category = $this->MembershipCategories->get($id);
			$msg = " Data has been Create succesfuly !";
		}
		$category->category_name = $this->input->post('name');
		$category->category_desc =  $this->input->post('desc');
		$category->is_active =  '1';

		$id = $category->save();	
		SiteHelpers::alert('success',$msg);
		redirect('Memberships/category',301);
	}

}

/* End of file memberships.php */
