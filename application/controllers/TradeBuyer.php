<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TradeBuyer extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		   
		$this->load->library('form_validation');
		$this->data['pageTitle'] = "Trade Buyer Registration";
		$this->load->model('Stocks');
		$this->load->model('Products');
		//$this->load->library('Emails_libraries');
		$this->load->model('Dokus');
		
	}
	

	public function index()
	{
		if(!$this->session->userdata('logged_in')) redirect('login.html',301);   
		$where['is_active'] = 1;
		$where['user_id'] = $this->session->userdata('uid');
		$target_market = $this->db->get_where('general_settings',array('gs_name'=>'seller_target_market'))->result_array();
		$country = $this->db->get('apps_countries')->result_array();
		$category = $this->db->get_where('category_seller',array('is_active'=>1))->result_array();
		

		$this->data['country'] = $country;
		$this->data['category'] = $category;
		$this->data['target_market'] = $target_market;
		//$this->data['profile'] = $this->db->get_where('media_register',$where)->row_array();
		$this->data['content'] = $this->load->view('Media/dashboard',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Order()
	{
		if(!$this->session->userdata('logged_in')) redirect('login.html',301);   
		$where['is_active'] = 1;
		$where['user_id'] = $this->session->userdata('uid');
		$profile =  $this->db->get_where('seller_list',$where)->row_array();
		$where_data['is_active'] = 1;
		$where_data['user_id'] = $this->session->userdata('uid');
		$data = $this->db->get_where('transaction_d_tmp',$where_data)->row_array();
		
		$this->db->where('category_id', 1);
		$product = $this->Stocks->getList()->result();

		$this->data['data'] = json_encode($data);
		$this->data['booth'] = json_encode($product);

		$this->data['profile'] = $profile;
		$this->data['content'] = $this->load->view('Media/order',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function InsertOrder()
	{
		if(!$this->session->userdata('logged_in')) redirect('login.html',301);   
		//object
		$where['is_active'] = 1;
		$where['user_id'] = $this->session->userdata('uid');
		$profile =  $this->db->get_where('seller_list',$where)->row_array();


		$post = json_decode($this->input->post('values'));

		$this->db->where('product_id', $post->product_id);
		$product = $this->Stocks->getList()->row();
		$price = $product->product_price;
		if($profile['is_membership'] == '1' && $product->discount_member_type != '2')
		{
			if($product->price_after_discount_member != null || $product->price_after_discount_member != "" )
			{
				$price = $product->price_after_discount_member;
			}
		} else if(($profile['is_membership'] == '0' || $profile['is_membership'] == "") && $product->discount_type != '2') {
			
			if($product->price_after_discount != null || $product->price_after_discount != "" )
			{
				$price = $product->price_after_discount;
			}
		}
		$data['product_id'] = $product->id;
		$data['price'] = $price;
		$data['delegate_name'] = $post->delegate_name;
		$data['booth_name'] = $post->booth_name;
		$data['amount'] =1;
		$data['trx_code'] = 'TMP'.date('YmdHisu');
		$data['user_id'] = $this->session->userdata('uid');
		$data['is_active'] =1;

		$this->db->insert('transaction_d_tmp', $data);
		
		$tmp_where['is_active'] = 1;
		$tmp_where['user_id'] = $this->session->userdata('uid');
		$tmp = $this->db->get('transaction_d_tmp',$tmp_where)->result_array();
		
		echo json_encode($tmp);
		
	}

	public function DeleteOrder()
	{
		if(!$this->session->userdata('logged_in')) redirect('login.html',301);   
		$post = $this->input->post('key');
		//$this->debug($post['trx_code']);
		$this->db->where('id', $post['id']);

		$this->db->delete('transaction_d_tmp');
	
		
		
		echo json_encode(array('status'=> 200));
		
	}


	public function CreateOrder()
	{
		if(!$this->session->userdata('logged_in')) redirect('login.html',301);   
		$where['is_active'] = 1;
		$where['user_id'] = $this->session->userdata('uid');
		$profile =  $this->db->get_where('seller_list',$where)->row_array();

		$tmp_where['is_active'] = 1;
		$tmp_where['user_id'] = $this->session->userdata('uid');
		$tmp = $this->db->get('transaction_d_tmp',$tmp_where)->result_array();
		$total = 0;

		if(count($tmp) > 0){
			foreach ($tmp as $key => $value) {
				# code...
				$total = $total + $value['price'];
				//$data['is_active'] =1;
			}

		$data["trx_code"] = $this->trx_code("TRX");;
		$data["trx_datetime"] =  date('Y-m-d H:i:s');
		$data["trx_subtotal"] = $total;//$this->_setSubTotal($post);
		$data["trx_status"] = 0;//$post["country"];
		$data["user_id"] = $this->session->userdata('uid');
		$data["trx_date"] = date('Y-m-d H:i:s');
		$data["trx_type"] =SELLER_REGISTRATION;
		$data["dtm_crt"] =date('Y-m-d H:i:s');
		$data["trx_tax"] = 0;//( ((int)$this->company()->tax/100) * (int)$data["trx_subtotal"]);
		$data["trx_service_tax"] = 0;//( ((int)$this->company()->service_tax/100) * (int)$data["trx_subtotal"]);
		$data["trx_total"] = $total; //$data["trx_subtotal"] + ($data["trx_subtotal"] * ($this->company()->tax/100)) + ( $data["trx_subtotal"] * ($this->company()->service_tax/100));
		$data["trx_payment_type"] = 1;
		$data["trx_invoice"] = "";

		$this->db->insert('transaction_h', $data);
		$id =  $this->db->insert_id();

			foreach ($tmp as $key => $value) {
				# code...
				$detail['product_id'] = $value['product_id'];
				$detail['price'] =  $value['price'];
				$detail['delegate_name'] = $value['delegate_name'];
				$detail['booth_name'] = $value['booth_name'];
				$detail['amount'] =1;
				$detail['trx_code'] = $data["trx_code"];

				$this->db->insert('transaction_d', $detail);
				

				$stock['amount'] = -1;
				$stock['product_id'] = $value['product_id'];
				$stock['company_name'] = '-';
				$stock['ref_trans'] =  $data["trx_code"];
				$stock['type'] =  'TRANS';
				$stock['dtm_crt'] =  date('Y-m-d');

				$this->db->insert('stock', $stock);
				
				//$data['user_id'] = $this->session->userdata('uid');
				//$data['is_active'] =1;
			}
			$this->db->where('user_id', $this->session->userdata('uid'));
			$this->db->delete('transaction_d_tmp');

			//send invoice
			$this->load->library('my_libraries');
			$email['invoiceNo'] = $data["trx_code"];
			$email['seller_name'] = $profile["seller_name"];
			$email['seller_phone'] = $profile["seller_phone"];
			$email['email'] = $profile["seller_mail"];
			$email['total'] = $total;
			$email['date'] = date('Y-m-d');
			$email['dueDate'] = date('Y-m-d',strtotime($email['date'],'+7 days'));
			$this->my_libraries->sellerInvoice($email);

			SiteHelpers::alert('success'," Order has been saved succesfuly, Please Create Payment !");

		redirect('Transaction',301);
		} else {
		SiteHelpers::alert('warning'," Order has empty !");

		redirect('Seller/Order',301);
		}

		
			
		
	}

	public function create()
	{
		$this->load->library('my_libraries');
		
		$post= $this->input->post();

		$recaptcha = $this->input->post('g-recaptcha-response');
        if (!empty($recaptcha)) {
            $response = $this->recaptcha->verifyResponse($recaptcha);
            if (isset($response['success']) and $response['success'] === true) {
				

				unset($post['g-recaptcha-response']);
				
				$this->my_libraries->sendTradeBuyerData($post);

				$sellerId = $this->insertTblSeller($post);

				$msg = "<h1>Registration Successful</h1>";
				$msg.= "<p>Your registration successfully sent.</p>";
				$msg.= "<p>Thank you very much for your participation for BBTF 2020.</p>";
				$msg.= "<p>Please kindly contact us if you have not received Trade Buyer Confirmation within 3 working days.</p>";
				$msg.= "<br>";
				$msg.= "<p>Should you have any questions or concerns, please feel free to contact us.</p>";
				$msg.= "<br>";
				$msg.= "<br>";
				$msg.= "<p>Sincerely yours,</p";
				$msg.= "<p>Committee of BBTF ".date('Y')."</p>";

				$this->session->set_flashdata('success_register',$msg);
					
				redirect('',301);
            } else {
				SiteHelpers::alert('error'," upss!!");
				redirect('seller-registration.html');
			}
		}
		
	}

	public function update()
	{
		
		if(!$this->session->userdata('logged_in')) redirect('login.html',301);   
		$post= $this->input->post();
		$data["seller_name"] = $post["seller_name"];
		$data["seller_address"] = $post["seller_address"];
		$data["seller_city"] = $post["seller_city"];
		$data["seller_country_code"] = $post["seller_country_code"];
		$data["seller_email"] = $post["email"];
		$data["seller_phone"] = $post["seller_phone"];
		$data["seller_fax"] = $post["seller_fax"];
		$data["seller_target_market"] = json_encode($post["seller_target_market"]);
		$data["seller_company_profile"] =$post["seller_company_profile"];
		$data["seller_website"] = $post["seller_website"];
		$data["category_id"] = $post["category_id"];
		$data["full_delegate_name"] = $post["full_delegate_name"];
		$data["co_delegate_name"] = $post["co_delegate_name"];
		$data["registration_person"] = $post["registration_person"];
		$data["job_title"] = $post["job_title"];
		$data["seller_province"] = $post["seller_province"];
		$this->db->where('seller_id', $post['seller_id']);
		$this->db->update('seller', $data);

		if($post['password'] != "")
		{
			$user['password'] = md5($post['password']);
			$this->db->where('id', $post['user_id']);
			$this->db->update('tb_users', $user);
		}
	
		SiteHelpers::alert('success'," Data has been saved succesfuly !");
		redirect('Seller',301);
	}



	public function Registration(){

		$this->load->library('recaptcha');
		
		$this->data['recaptcha_html'] = $this->recaptcha->getWidget();
		$this->data['recaptcha_script'] = $this->recaptcha->getScriptTag();
		
		$country = $this->db->get('apps_countries')->result_array();
		$category = $this->db->get_where('category_seller',array('is_active'=>1))->result_array();
		$target_market = $this->db->get_where('general_settings',array('gs_name'=>'seller_target_market'))->result_array();
		$terms = $this->db->get_where('general_settings',array('gs_code'=>'trade_buyer_tnc'))->row_array();

		$this->data['country'] = $country;
		$this->data['category'] = $category;
		$this->data['target_market'] = $target_market;
		$this->data['terms'] = $terms;
		$this->data['fee'] = $this->db->get_where('general_settings',array('gs_code'=>'trade_buyer_fee'))->row()->gs_value;;
		$this->data['days'] = $this->db->get_where('general_settings',array('gs_code'=>'number_days_tradebuyer'))->row()->gs_value;

		$this->data['company'] = $this->db->get('company')->row_array();
		$this->data['content'] = $this->load->view('frontend/trade-buyer-registration',$this->data,true);    
    	$this->load->view('layouts/registration/main',$this->data);
	}

	

	public function check_email_exist($email = "" ) {
		$email = trim($this->input->get('email'));
		$this->db->where('email', $email);
		
		$row = $this->db->get('trade_buyer')->num_rows();
		

		if($row == 1){
			//$this->form_validation->set_message('check_email_exist', 'The {field} has been exist !');
            echo json_encode(array('status'=> true));

		} else {
			echo json_encode(array('status'=> false));
		}

	}

	public function insertTblUser($sellerId = null,$post = array () ){

		$data["email"] = $post["email"];
		$data["password"] = md5($post["password"]);
		$data["username"] = $post["seller_name"];
		$data["first_name"] = $post["seller_name"];
		$data["active"] = 1;
		$data["created_at"] = date('Y-m-d');
		$data["group_id"] = 5;
		$data["ref_user"] = (int)$sellerId;

		$this->db->insert('tb_users', $data);
		
		$id =  $this->db->insert_id();
		
		return $id;

	}

	public function insertTblSeller($data = array () ){

		$data["is_active"] = 1;
		$data["dtm_crt"] = date('Y-m-d H:i:s');
		$data['company_name'] = strtoupper($data['company_name']);
		//$this->debug($data);		
		$this->db->insert('trade_buyer', $data);
		
		$id =  $this->db->insert_id();
		
		return $id;

	}

	public function confirm($id)
	{
		# code...
		$profile = $this->db->get_where('trade_buyer',array('id'=>$id))->row_array();

		if($profile == null)
		{
			redirect('',301);
		}

		$fee = $this->db->get_where('general_settings',array('gs_code'=>'trade_buyer_fee'))->row()->gs_value;
		$newFee = $fee + (4/100 * $fee);
		$days = $this->db->get_where('general_settings',array('gs_code'=>'number_days_tradebuyer'))->row()->gs_value;
		$doku = $this->Dokus->createTransactionTmpBuyer(array('fee'=>$newFee,'buyer_id'=>$profile['id'],"user_type"=>"TRADE_BUYER"));
		$email['decline_url'] = base_url()."TradeBuyer/declineClass/".$doku['transidmerchant'];
		$email['approve_url'] = base_url()."Doku/paymentRequest/".$doku['transidmerchant'];
		$attachment = $this->createTNCTradeBuyer($profile);
		//$this->debug($attachment);
		$this->load->library('my_libraries');
		$email['email'] = $profile['company_email'];
		$email['full_name'] = $profile['full_name'];
		$email['buyer_type'] = "FULL HOSTED";
		$email['company_name'] = $profile['company_name'];
		$email['fee'] = $fee + (4/100 * $fee);
		$email['invoice_date'] = date('Y-m-d');
		$email['invoice_due_date'] = date('Y-m-d',strtotime($email['invoice_date']."+7 days"));

		$this->my_libraries->tradeBuyerInvoice($email,$attachment);

		$profile['membership_verify'] = 1;

		$this->db->where('id', $profile['id']);
		$this->db->update('trade_buyer', $profile);
		

		SiteHelpers::alert('success'," Confirm save succesfuly !");
		redirect('Memberships/TradeBuyer',301);

	}

	public function createTNCTradeBuyer($data)
	{
		$this->load->library('M_pdf');
		$path = base_url()."files/pdf/";
		$filename = "tnc_".str_replace(' ', '_', $data['company_name']).".pdf";
		$fullpath = $path.$filename;
		
		$html = $this->load->view('layouts/tnc/tnc_tradebuyer', $data,true);

		
		$this->m_pdf->pdf->WriteHTML($html);
		$this->m_pdf->pdf->Output("./files/pdf/".$filename, "F");

		return $filename;
	}

	public function insertTblTrx($userId,$fee ){

		$data["trx_code"] = $this->trx_code("TRX");
		$data["trx_datetime"] =  date('Y-m-d H:i:s');
		$data["trx_subtotal"] = 0;//$this->_setSubTotal($post);
		$data["trx_status"] = 1;//$post["country"];
		$data["user_id"] = $userId;
		$data["trx_date"] = date('Y-m-d H:i:s');
		$data["trx_type"] ='TRADE_BUYER_REGISTRATION';
		$data["dtm_crt"] =date('Y-m-d H:i:s');
		$data["trx_tax"] = 0;//( ((int)$this->company()->tax/100) * (int)$data["trx_subtotal"]);
		$data["trx_service_tax"] = 0;//( ((int)$this->company()->service_tax/100) * (int)$data["trx_subtotal"]);
		$data["trx_total"] = $fee; //$data["trx_subtotal"] + ($data["trx_subtotal"] * ($this->company()->tax/100)) + ( $data["trx_subtotal"] * ($this->company()->service_tax/100));
		$data["trx_payment_type"] = 1;
		$data["trx_invoice"] = "";

		$this->db->insert('transaction_register_trade_buyer', $data);
		$id =  $this->db->insert_id();

		//$this->_insertTrxDetail($data["trx_code"],$post);
		
		return $id;

	}
	private function _insertTrxDetail($trxCode,$post="")
	{
		
		for ($i=0; $i < count($post['product_id']); $i++) { 
			# code...
			if($post['amount'][$i] != "" || $post['amount'][$i] != null)
			{
				$where = array(
					'product_id'  => $post["product_id"][$i]
				  );
			  
				  $row = $this->db->get_where('v_product_stock',$where)->row();

				$data["trx_code"] = $trxCode;
				$data["product_id"] = $post["product_id"][$i];
				$data["amount"] = $post["amount"][$i];
				$price = $row->product_price;
				$discountPrice = 0;
				$dsc = 0; 
			  if( $row->discount_date != null && (strtotime($row->discount_date) >= strtotime(date('Y-m-d')))){
				  $price = $row->price_after_discount;
				  $discountPrice = ($row->discount/100) * $row->product_price;
				  $dsc = $row->discount;
			  }

				$data["price"] = $price;
				$data['discount'] = $dsc;
				$data['discount_price'] = $discountPrice;


				//$this->db->insert('transaction_d', $data);

				//insert_stock
				$stock['amount'] = $data['amount'] * -1;
				$stock['product_id'] = $data['product_id'];
				$stock['company_name'] = '-';
				$stock['ref_trans'] =  $trxCode;
				$stock['type'] =  'TRANS';
				$stock['dtm_crt'] =  date('Y-m-d');

				$this->db->insert('stock', $stock);
				
			}
		}

	}

	private function _setSubTotal($post="")
	{
		$subTotal = 0;
		$reg = array();
		for ($i=0; $i < count($post['product_id']); $i++) { 
			# code...
			if($post['amount'][$i] != "" || $post['amount'][$i] != null)
			{
				$reg[$i]["product_id"] = $post["product_id"][$i];
				$reg[$i]["amount"] = $post["amount"][$i];

				array_values($reg);
			}
		}

		foreach ($reg as $i => $v) {
			# code...
			$where = array(
				'product_id'  => $v["product_id"]
			  );
		  
			  $row = $this->db->get_where('v_product_stock',$where)->row();
			  //$this->debug($row);
			  $price = $row->product_price;
			  if( $row->discount_date != null && (strtotime($row->discount_date) >= strtotime(date('Y-m-d')))){
				  $price = $row->price_after_discount;
			  }
			  
				$subTotal = $subTotal + ($price * (int)$v["amount"]);
		}

			return $subTotal;
	}

	public function insertTblTrxMembership($userId = "",$post = array () ){


		$membership = $this->db->get_where('membership',array('membership_id'=>(int)$post["membership_id"]))->row();
		$data["trx_code"] = $this->trx_code("MEM");;
		$data["trx_datetime"] =  date('Y-m-d H:i:s');
		$data["trx_subtotal"] =  $membership->membership_fee;//$this->_setSubTotal($post);
		$data["trx_status"] = 0;//$post["country"];
		$data["user_id"] = $userId;
		$data["trx_date"] = date('Y-m-d H:i:s');
		$data["trx_type"] =MEMBERSHIP_ORDER;
		$data["dtm_crt"] =date('Y-m-d H:i:s');
		$data["trx_tax"] = 0;//( ((int)$this->company()->tax/100) * (int)$data["trx_subtotal"]);
		$data["trx_service_tax"] =0; //( ((int)$this->company()->service_tax/100) * (int)$data["trx_subtotal"]);
		$data["trx_total"] = $membership->membership_fee; //$this->_setSubTotal($post);//$data["trx_subtotal"] + ($data["trx_subtotal"] * ($this->company()->tax/100)) + ( $data["trx_subtotal"] * ($this->company()->service_tax/100));
		$data["trx_payment_type"] = $post["paymentType"];
		$data["trx_invoice"] = "";

		$this->db->insert('transaction_h', $data);
		$id =  $this->db->insert_id();

		$this->_insertTrxDetailMembership($data["trx_code"],$post);
		
		return $id;

	}

	private function _insertTrxDetailMembership($trxCode,$post="")
	{
			$membership = $this->db->get_where('membership',array('membership_id'=>(int)$post["membership_id"]))->row();
			
				$data["trx_code"] = $trxCode;
				$data["product_id"] = $post["membership_id"];
				$data["amount"] = 1;
				$data["price"] = $membership->membership_fee;
				$data['discount'] = 0;
				$data['discount_price'] = 0;


				$this->db->insert('transaction_d', $data);

	}

	public function declineClass($invoiceNo)
	{
		# code...
		$dokuTmp = $this->db->get_where('doku_tmp',array('transidmerchant'=>$invoiceNo))->row_array();
		$profile = $this->db->get_where('trade_buyer',array('id'=>$dokuTmp['ref_user_id']))->row_array();
		$data['trxstatus'] = "DECLINE";
		$this->db->where('transidmerchant', $invoiceNo);
		$this->db->update('doku_tmp', $data);

		$profileBuyer['membership_verify'] = 2;
		$this->db->where('id', $profile['id']);
		$this->db->update('trade_buyer', $profileBuyer);

		//$this->load->library('my_libraries');

		$msg = "<h1>You Have Decline Status</h1>";
		$msg.= "<p>Thank you very much for your participation for BBTF 2020.</p>";
		$msg.= "<p>Should you have any questions or concerns, please feel free to contact us.</p>";
		$msg.= "<br>";
		$msg.= "<br>";
		$msg.= "<p>Thank you for your attention. Sincerely yours</p>";
		$msg.= "<br>";
		$msg.= "<p>BBTF Committe</p>";

		$this->session->set_flashdata('success_register',$msg);
		redirect('',301);
		
	}

	public function delete($id)
	{
		# code...
		
		$this->db->where('id', $id);
		$this->db->delete('trade_buyer');

		SiteHelpers::alert('success'," Data has been saved succesfuly !");
		redirect('Memberships/TradeBuyer',301);

		
	}

	public function setIgnoreStatus($id)
	{
		$profile = $this->db->get_where('trade_buyer',array('buyer_id'=>$id))->row_array();
		$user = $this->db->get_where('tb_users',array('ref_user'=>$profile['buyer_id']))->row_array();
		
		$update['membership_verify'] = 3;
		///$this->debug($update);
		$this->db->where('id', $id);
		$this->db->update('trade_buyer', $update);
		
		SiteHelpers::alert('success'," Listing has been succesfuly !");
		redirect('Memberships/TradeBuyer',301);

	}

	public function resendInv($id)
	{
		# code...
		$profile = $this->db->get_where('trade_buyer',array('id'=>$id))->row_array();

		if($profile == null)
		{
			redirect('',301);
		}

		$fee = $this->db->get_where('general_settings',array('gs_code'=>'trade_buyer_fee'))->row()->gs_value;
		$newFee = $fee + (4/100 * $fee);
		$days = $this->db->get_where('general_settings',array('gs_code'=>'number_days_tradebuyer'))->row()->gs_value;
		$doku = $this->db->get_where('doku_tmp',array('ref_user_id'=>$id , 'user_type'=>'TRADE_BUYER'))->row_array();
		//$this->debug($doku);
		//$this->Dokus->createTransactionTmpBuyer(array('fee'=>$newFee,'buyer_id'=>$profile['id'],"user_type"=>"TRADE_BUYER"));
		$email['decline_url'] = base_url()."TradeBuyer/declineClass/".$doku['transidmerchant'];
		$email['approve_url'] = base_url()."Doku/paymentRequest/".$doku['transidmerchant'];
		$attachment = $this->createTNCTradeBuyer($profile);
		//$this->debug($attachment);
		$this->load->library('my_libraries');
		$email['email'] = $profile['company_email'];
		$email['buyer_type'] = "FULL HOSTED";
		$email['company_name'] = $profile['company_name'];
		$email['fee'] = $fee + (4/100 * $fee);
		$email['invoice_date'] = date('Y-m-d');
		$email['invoice_due_date'] = date('Y-m-d',strtotime($email['invoice_date']."+7 days"));

		$this->my_libraries->tradeBuyerInvoice($email,$attachment);

		// $profile['membership_verify'] = 1;

		// $this->db->where('id', $profile['id']);
		// $this->db->update('trade_buyer', $profile);
		
		SiteHelpers::alert('success'," Resend succesfuly !");
		redirect('Memberships/TradeBuyer',301);

	}



}

/* End of file Controllername.php */
