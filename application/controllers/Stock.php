<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);

		$this->load->model('Stocks');
		$this->load->model('Products');
		
	}
	

	public function index()
	{	
		$this->db->order_by('category_id', 'asc');
	
		$data = $this->Stocks->getList()->result();
		
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('stock/index',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function create()
	{
		$product = $this->Products->get_list();
		$boothProduct = $this->Products->getBoothProduct();
		
		$this->data['product'] = $product;
		$this->data['boothProduct'] = $boothProduct;

		$this->data['isEdit'] = false;

		$this->data['content'] = $this->load->view('stock/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function save()
	{
		$data = $this->input->post();

		if((int)$data['is_package'] != 1){

			$insert["product_id"] = $data['product_id'];
			$insert["amount"] = $this->replaceToNumber($this->input->post('amount'));
			$insert["type"] = 'STOCK';
			$insert["usr_crt"] = $this->session->userdata('userName');
			$insert["dtm_crt"] = date('Y-m-d H:i:s');
			
			
			$this->db->insert('stock', $insert);
		} else {
			$product["product_id"] = $data['product_id'];
			$product["amount"] = $this->replaceToNumber($this->input->post('amount'));
			$product["type"] = 'STOCK';
			$product["usr_crt"] = $this->session->userdata('userName');
			$product["dtm_crt"] = date('Y-m-d H:i:s');
			$product["note"] = $data['note'];
			$product["company_name"] = $data['company_name'];

			$this->db->insert('stock', $product);
			$this->db->where('parent_product', $data['product_id']);
			$this->db->delete('product_packet_amount');

			for ($i=0; $i < count($data['booth_product_id']); $i++) { 
				# code...
				if($data['booth_product_amount'][$i] > 0) {
					
					$packageStock['parent_product'] = $data['product_id'];
					$packageStock['product_id'] = $data['booth_product_id'][$i];
					$packageStock['amount'] = $data['booth_product_amount'][$i];
					$packageStock['amount_psa'] = $data['amountPSA'];


					$this->db->insert('product_packet_amount', $packageStock);

					//update stock package
					$updateStock['product_id'] = $packageStock['product_id'];
					$updateStock['amount'] = $packageStock['amount'] * -1;
					$updateStock["type"] = 'ADJ_PACKAGE_STOCK';
					$updateStock["usr_crt"] = $this->session->userdata('userName');
					$updateStock["dtm_crt"] = date('Y-m-d H:i:s');
					
					$this->db->insert('stock', $updateStock);
					
				}

				if($data['product_id'] == 15) {
					$packageStock['parent_product'] = $data['product_id'];
					$packageStock['product_id'] = $data['booth_product_id'][$i];
					$packageStock['amount'] = $data['booth_product_amount'][$i];
					$packageStock['amount_psa'] = $data['amountPSA'];


					$this->db->insert('product_packet_amount', $packageStock);
				}
			}
		}
		
		
		SiteHelpers::alert('success'," Data has been saved succesfuly !");
		redirect('Stock',301);
	}

	public function getBooth()
	{
		$this->db->where('category_id', 1);
		$product = $this->Products->get_list();

		echo json_encode($product);
	}

}

/* End of file Stock.php */
