<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);
		$this->load->model('Products');
		$this->load->model('ProductCategories');
		
	}
	

	public function index()
	{
		$this->db->order_by('id', 'desc');
		$data = $this->Products->join();
		//$this->debug($data);
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('product/index',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function create()
	{
		$parent = $this->Products->where("parent",0)->where("category_id",1)->get_list();
		$this->data['category'] = $this->ProductCategories->isActive()->get_list();
		$this->data['isEdit'] = false;
		$this->data['parent'] = $parent;
		$this->data['content'] = $this->load->view('product/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function edit($id)
	{
		$data = $this->Products->get($id);
		
		$parent = $this->Products->where("parent",0)->where("category_id",1)->get_list();
		$this->data['category'] = $this->ProductCategories->isActive()->get_list();
		$this->data['isEdit'] = true;
		$this->data['data'] = $data;
		$this->data['parent'] = $parent;
		$this->data['content'] = $this->load->view('product/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Save()
	{
		
		//$this->debug($this->input->post());
		$fileName = null;
		if($_FILES["image"]["name"] != "")
		{
			//$this->debug($_FILES);
			$config['upload_path'] = './files/product/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				
				//validation image has error
				SiteHelpers::alert('error',$this->upload->display_errors());
				redirect('Products/Create');
				} else {
					$data = $this->upload->data();
					$fileName = $data["file_name"];
				}
		}
		
		$product = $this->Products->new_row();

		$product->product_name = $this->input->post('name');
		$product->product_desc = $this->input->post('content');	
		$product->category_id = $this->input->post('category_id');
		$product->is_package = $this->input->post('is_package');
		$product->product_price = (int)str_replace('.','',$this->input->post('price'));	
		$product->dtm_crt = date('Y-m-d H:i:s');
		$product->usr_crt = $this->session->userdata('userName');
		$product->active = 1;	
		$product->image = $fileName;
		$product->discount = $this->input->post('discount');
		$product->discount_date = $this->input->post('discount_date') == "" ? null : $this->input->post('discount_date');


		if((int)$product->category_id == 2) $product->parent = $this->input->post('parent');

		//$this->debug($product);
		$id = $product->save();
		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Products',301);
	}

	public function saveEdit()
	{
		//$this->debug($this->input->post());
		$fileName = '';
		$id = $this->input->post('id');
		$product = $this->Products->get($id);
		
		if($_FILES["image"]["name"] != "")
		{
			//$this->debug(count($this->input->post($_FILES["image"])));
			$config['upload_path'] = './files/product/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';
			
			if(count($this->input->post($_FILES["image"])) > 3){
					//unlink exist data
					if($product->image != '' || $product->image != null){
						$image = $config['upload_path'].$product->image;
						unlink($image);
					}
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if (!$this->upload->do_upload('image')){
						$error = array('error' => $this->upload->display_errors());
						
						//validation image has error
						SiteHelpers::alert('error',$this->upload->display_errors());
						redirect('Product/Create');
						} else {
							$data = $this->upload->data();
							$fileName = $data["file_name"];
							$product->image = $fileName;

						}
			}
			
		}
		
		$product->product_name = $this->input->post('name');
		$product->product_desc = $this->input->post('content');	
		$product->category_id = $this->input->post('category_id');
		$product->is_package = $this->input->post('is_package');

		$product->product_price = (int)str_replace('.','',$this->input->post('price'));
		$product->dtm_crt = date('Y-m-d H:i:s');
		$product->usr_crt = $this->session->userdata('userName');
		//$product->active = 1;
		$product->discount_type = $this->input->post('discount_type');
		$product->discount_value = (int)str_replace('.','',$this->input->post('discount_value'));
		$product->discount = $this->input->post('discount');
		$product->discount_date = $this->input->post('discount_date') == "" ? null : $this->input->post('discount_date');

		$product->discount_member_type = $this->input->post('discount_member_type');
		$product->discount_member_value = (int)str_replace('.','',$this->input->post('discount_member_value'));
		$product->discount_member = $this->input->post('discount_member');
		$product->discount_member_date = $this->input->post('discount_member_date') == "" ? null : $this->input->post('discount_member_date');

		if((int)$product->category_id == 2) $product->parent = $this->input->post('parent');

		
		$id = $product->save();
		SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Products',301);
	}

	public function nonActive($id)
	{
		$product = $this->Products->get($id);

		if($product->active == 1){
			$product->active = 0;
 		} else {
			$product->active = 1;
		 }

		 $product->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Products',301);
	}

	public function categoryNonActive($id)
	{
		$product = $this->ProductCategories->get($id);

		if($product->enable == '1'){
			$product->enable = '0';
 		} else {
			$product->enable = '1';
		 }

		 $product->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Product/category',301);
	}

	public function category()
	{
		$data = $this->ProductCategories->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('product/category',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function createCategory()
	{
		$this->data['isEdit'] = false;
		
		$this->data['content'] = $this->load->view('product/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function editCategory($id)
	{
		$this->data['isEdit'] = true;
		$data = $this->ProductCategories->get($id);
		//$this->debug($data);
		if($data == null){
			SiteHelpers::alert('warning'," Data not found !");

			redirect('Product/category',301);
		}
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('product/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function saveCategory()
	{
		$id = $this->input->post('id');
		
		if($this->input->post('isEdit') == '') {
			$category = $this->ProductCategories->new_row();
			$msg = " Data has been Create succesfuly !";
		} else {
			$category = $this->ProductCategories->get($id);
			$msg = " Data has been Create succesfuly !";
		}
		$category->category_name = $this->input->post('name');
		$category->category_desc =  $this->input->post('desc');
		$category->is_active =  1;

		$id = $category->save();	
		SiteHelpers::alert('success',$msg);
		redirect('Products/category',301);
	}

	public function uploadImage()	{
		if(isset($_FILES["image"]["name"]))
		{
			
			$config['upload_path'] = './files/product/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			//$this->debug($this->upload->do_upload("image"));
			if (!$this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				echo json_encode($error);
			}
			else{
				
				$data = $this->upload->data();
					//Compress Image
					$config['image_library']='gd2';
					$config['source_image']='./assets/images/'.$data['file_name'];
					$config['create_thumb']= FALSE;
					$config['maintain_ratio']= TRUE;
					$config['quality']= '60%';
					$config['width']= 800;
					$config['height']= 800;
					$config['new_image']= './files/product/'.$data['file_name'];
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
					echo base_url().'files/product/'.$data['file_name'];
			}
		}
		
	}

	public function deleteImage(){
		$src = $this->input->post('src');
		$file_name = str_replace(base_url(), '', $src);
		
		if(unlink($file_name)){
			echo 'File Delete Successfully';
		}
	}

	public function getParent()
	{
		$data = $this->Products->where("parent",0)->get_list();

		echo json_encode($data);
		# code...
		
	}


	public function isPackage(){
		$bool = false;

		$id = $this->input->post('id');

		$product = $this->Products->get($id);

		if($product->is_package == "1")
		{
			$bool = true;
		}

		echo json_encode($bool);

	}



}

/* End of file Product.php */
