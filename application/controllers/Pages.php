<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);
		$this->load->model('Pages');
		//$this->load->model('PageCategories');
		
	}
	

	public function index()
	{
		$data = $this->Pages->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('pages/index',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function create()
	{
		$data = array(
			'id' => 1,
			'is_active' => 1,
			'title' => "Test Pages",
			'is_publish' => 1,
			'value' => "<p> hello world </p>",
			'image' => "assets/metronic/app/media/img/page/blog1.jpg",
		);
		//$this->data['category'] = $this->PageCategories->isActive()->get_list();
		$this->data['isEdit'] = false;
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('pages/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function edit($id)
	{
		$data = $this->Pages->get($id);
		$filename = "application/views/frontend/".$data->filename.".php";
				if(file_exists($filename))
				{
					$data->content = file_get_contents($filename);
				} else {
					$data->content = '';
				} 	

		$this->data['isEdit'] = true;
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('pages/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Save()
	{
		
		$pages = $this->Pages->new_row();

		if(isset($_FILES["image"]["name"]))
		{
			//$this->debug($_FILES);
			$config['upload_path'] = './files/sub_header/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				
				//validation image has error
				SiteHelpers::alert('error',$this->upload->display_errors());
				redirect('Pages/Create');
				} else {
					$data = $this->upload->data();
					$fileName = $data["file_name"];
					
				}
		}


		$pages->title = $this->input->post('title');
		$pages->alias = strtolower(str_replace(' ','-',$this->input->post('title')));
		$pages->filename = strtolower($pages->alias);
		$pages->meta_desc = $this->input->post('meta_desc');
		$pages->meta_keyword = $this->input->post('meta_keyword');
		$pages->created = date('Y-m-d H:i:s');
		$pages->status = 1;
		$pages->sub_image = $fileName;

		$this->setFileName($pages->filename,$this->input->post('content'));
		$id = $pages->save();
		$this->createRouters();

		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Pages',301);
	}

	private function setFileName($name = null, $content = '')
	{
		if($name != null){
			
			$filename = "application/views/frontend/".$name.".php";
					
			$fp=fopen($filename,"w+"); 				
			fwrite($fp,$content); 
			fclose($fp);
		}
	}

	public function saveEdit()
	{
		$id = $this->input->post('id');
		$pages = $this->Pages->get($id);
		
		if(strtolower($pages->title) != strtolower($this->input->post('title')))
		{
			$filename = "./application/views/frontend/".$pages->filename.".php";
			if(file_exists($filename) && $pages->filename !='')
			{
				unlink($filename);
			}
						
		}

		if(isset($_FILES["image"]["name"]))
		{
			//$this->debug(count($this->input->post($_FILES["image"])));
			$config['upload_path'] = './files/sub_header/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';
			
			if(count($this->input->post($_FILES["image"])) > 3){
					//unlink exist data
					if($pages->sub_image != '' || $pages->sub_image != null){
						$image = $config['upload_path'].$pages->sub_image;
						unlink($image);
					}
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if (!$this->upload->do_upload('image')){
						$error = array('error' => $this->upload->display_errors());
						
						//validation image has error
						SiteHelpers::alert('error',$this->upload->display_errors());
						redirect('Pages/Create');
						} else {
							$data = $this->upload->data();
							$fileName = $data["file_name"];
							$pages->sub_image = $fileName;

						}
			}
			
		}

		
		$pages->title = $this->input->post('title');
		$pages->alias = strtolower(str_replace(' ','-',$this->input->post('title')));
		$pages->filename = strtolower($pages->alias);
		$pages->updated = date('Y-m-d H:i:s');
		$pages->meta_desc = $this->input->post('meta_desc');
		$pages->meta_keyword = $this->input->post('meta_keyword');
		$pages->status = 1;
		$this->setFileName($pages->filename,$this->input->post('content'));		

		$id = $pages->save();
		$this->createRouters();
		SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Pages',301);
	}

	public function nonActive($id)
	{
		$pages = $this->Pages->get($id);

		if($pages->status == 1){
			$pages->status = 0;
 		} else {
			$pages->status = 1;
		 }

		 $pages->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Pages',301);
	}

	public function categoryNonActive($id)
	{
		$pages = $this->PageCategories->get($id);

		if($pages->enable == '1'){
			$pages->enable = '0';
 		} else {
			$pages->enable = '1';
		 }

		 $pages->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Pages/category',301);
	}

	// public function category()
	// {
	// 	$data = $this->PageCategories->get_list();
	// 	$this->data['data'] = json_encode($data);
	// 	$this->data['content'] = $this->load->view('pages/category',$this->data,true);    
    // 	$this->load->view('layouts/main',$this->data);
	// }

	// public function createCategory()
	// {
	// 	$this->data['isEdit'] = false;
		
	// 	$this->data['content'] = $this->load->view('pages/category_create',$this->data,true);    
    // 	$this->load->view('layouts/main',$this->data);
	// }

	// public function editCategory($id)
	// {
	// 	$this->data['isEdit'] = true;
	// 	$data = $this->PageCategories->get($id);
	// 	//$this->debug($data);
	// 	if($data == null){
	// 		SiteHelpers::alert('warning'," Data not found !");

	// 		redirect('Pages/category',301);
	// 	}
	// 	$this->data['data'] = $data;
	// 	$this->data['content'] = $this->load->view('pages/category_create',$this->data,true);    
    // 	$this->load->view('layouts/main',$this->data);
	// }

	// public function saveCategory()
	// {
	// 	$id = $this->input->post('id');
		
	// 	if($this->input->post('isEdit') == '') {
	// 		$category = $this->PageCategories->new_row();
	// 		$msg = " Data has been Create succesfuly !";
	// 	} else {
	// 		$category = $this->PageCategories->get($id);
	// 		$msg = " Data has been Create succesfuly !";
	// 	}
	// 	$category->name = $this->input->post('name');
	// 	$category->slug =  str_replace(' ','-',$this->input->post('name'));
	// 	$category->enable =  '1';

	// 	$id = $category->save();	
	// 	SiteHelpers::alert('success',$msg);
	// 	redirect('Pages/category',301);
	// }

	public function uploadImage()	{
		if(isset($_FILES["image"]["name"]))
		{
			
			$config['upload_path'] = './files/page/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '1024';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			//$this->debug($this->upload->do_upload("image"));
			if (!$this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				echo json_encode($error);
			}
			else{
				
				$data = $this->upload->data();
					//Compress Image
					$config['image_library']='gd2';
					$config['source_image']='./assets/images/'.$data['file_name'];
					$config['create_thumb']= FALSE;
					$config['maintain_ratio']= TRUE;
					$config['quality']= '60%';
					$config['width']= 800;
					$config['height']= 800;
					$config['new_image']= './files/page/'.$data['file_name'];
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
					echo base_url().'files/page/'.$data['file_name'];
			}
		}
		
	}

	public function deleteImage(){
		$src = $this->input->post('src');
		$file_name = str_replace(base_url(), '', $src);
		
		if(unlink($file_name)){
			echo 'File Delete Successfully';
		}
	}

	function createRouters()
	{
		$rows =$this->db->get('tb_pages')->result();
		$val  =	"<?php \n";
		foreach($rows as $row)
		{
			$val .= '$route["' . $row->alias . '.html"] = "page/index/' . $row->alias . '";' ."\n";	
			$val .= '$route["' . $row->alias . '.html/(:any)"] = "page/index/' . $row->alias . '/%1";' ."\n";	
		}
		$val .= 	"?>";
		$filename = 'application/config/pageroutes.php';
		$fp=fopen($filename,"w+"); 
		fwrite($fp,$val); 
		fclose($fp);	
		return true;	
		
	}		



}

/* End of file Pages.php */
