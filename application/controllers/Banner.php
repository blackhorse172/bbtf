<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends MY_Controller {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if(!$this->session->userdata('logged_in')) redirect('user/login',301);      
		$this->load->model('Banners');
		$this->load->model('BannerCategories');

		
	}
	

	public function index()
	{
		$this->load->helper('inflector');
		//$this->debug(plural('gallery'));
		$data = $this->Banners->join();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('banner/index',$this->data,true);   
    	$this->load->view('layouts/main',$this->data);
	}

	public function Create()
	{
		$this->data['isEdit'] = false;
		$this->data['category'] = $this->BannerCategories->isActive()->get_list();
		$this->data['content'] = $this->load->view('banner/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function edit($id)
	{
		$data = $this->Banners->get($id);
		$this->data['category'] = $this->BannerCategories->isActive()->get_list();
		$this->data['isEdit'] = true;
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('banner/create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function Save()
	{
		$fileName = '';
		//var_dump((int)$this->input->post('status'));
		//$this->debug($this->input->post('status'));
		if(isset($_FILES["image"]["name"]))
		{
			//$this->debug($_FILES);
			$config['upload_path'] = './files/banner/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '12004';
		
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if (!$this->upload->do_upload('image')){
				$error = array('error' => $this->upload->display_errors());
				
				//validation image has error
				SiteHelpers::alert('error',$this->upload->display_errors());
				redirect('Banner/Create');
				} else {
					$data = $this->upload->data();
					$fileName = $data["file_name"];
				}
		}
		
		$Banner = $this->Banners->new_row();

		$Banner->category_id = $this->input->post('category_id');
		$Banner->title = $this->input->post('title');
		$Banner->descr = $this->input->post('desc');	
		$Banner->dtm_crt = date('Y-m-d H:i:s');
		$Banner->usr_crt = $this->session->userdata('userName');
		$Banner->status = (int)$this->input->post('status');	
		$Banner->end_show = $this->input->post('end_show');

		$Banner->image = $fileName;	
		//$this->debug($Banner);
		$id = $Banner->save();
		SiteHelpers::alert('success'," Data has been saved succesfuly !");

		redirect('Banner',301);
	}

	public function saveEdit()
	{
		$fileName = '';
		$id = $this->input->post('id');
		$Banner = $this->Banners->get($id);
		
		if(isset($_FILES["image"]["name"]))
		{
			$config['upload_path'] = './files/banner/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '10024';
			
			if(count($this->input->post($_FILES["image"])) > 3){
			
					//unlink exist data
					if($Banner->image != '' || $Banner->image != null){
						$image = $config['upload_path'].$Banner->image;
						unlink($image);
					}
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if (!$this->upload->do_upload('image')){
						$error = array('error' => $this->upload->display_errors());
						
						//validation image has error
						SiteHelpers::alert('error',$this->upload->display_errors());
						redirect('banner/create');
						} else {
							$data = $this->upload->data();
							$fileName = $data["file_name"];
							$Banner->image = $fileName;

						}
			}
			
		}
		$Banner->category_id = $this->input->post('category_id');
		$Banner->title = $this->input->post('title');
		$Banner->descr = $this->input->post('desc');	
		$Banner->dtm_crt = date('Y-m-d H:i:s');
		$Banner->usr_crt = $this->session->userdata('userName');
		$Banner->status = (int)$this->input->post('status');	
		$Banner->end_show = $this->input->post('end_show');
		$id = $Banner->save();
		SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Banner',301);
	}

	public function nonActive($id)
	{
		$Banners = $this->Banners->get($id);
		
		if($Banners->status == 1){
			$Banners->status = 0;
 		} else {
			$Banners->status = 1;
		 }

		 $Banners->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Banner',301);
	}

	public function categoryNonActive($id)
	{
		$banner = $this->BannerCategories->get($id);

		if($banner->is_active == '1'){
			$banner->is_active = '0';
 		} else {
			$banner->is_active = '1';
		 }

		 $banner->save();

		 SiteHelpers::alert('success'," Data has been Edit succesfuly !");

		redirect('Banner/category',301);
	}

	public function category()
	{
		$data = $this->BannerCategories->get_list();
		$this->data['data'] = json_encode($data);
		$this->data['content'] = $this->load->view('banner/category',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function createCategory()
	{
		$this->data['isEdit'] = false;
		
		$this->data['content'] = $this->load->view('banner/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function editCategory($id)
	{
		$this->data['isEdit'] = true;
		$data = $this->BannerCategories->get($id);
		//$this->debug($data);
		if($data == null){
			SiteHelpers::alert('warning'," Data not found !");

			redirect('Banner/category',301);
		}
		$this->data['data'] = $data;
		$this->data['content'] = $this->load->view('banner/category_create',$this->data,true);    
    	$this->load->view('layouts/main',$this->data);
	}

	public function saveCategory()
	{
		$id = $this->input->post('id');
		
		if($this->input->post('isEdit') == '') {
			$category = $this->BannerCategories->new_row();
			$msg = " Data has been Create succesfuly !";
		} else {
			$category = $this->BannerCategories->get($id);
			$msg = " Data has been Create succesfuly !";
		}
		$category->category_name = $this->input->post('name');
		$category->category_desc =  $this->input->post('desc');
		$category->is_active =  '1';

		$id = $category->save();	
		SiteHelpers::alert('success',$msg);
		redirect('Banner/category',301);
	}


	public function delete($id)
	{
		$banner = $this->Banners->get($id);
		$config['upload_path'] = './files/banner/';
		$image = $config['upload_path'].$banner->image;
		unlink($image);

		$this->db->where('banner_id', $id);
		$this->db->delete('banner');

		SiteHelpers::alert('success',"Delete Success");
		redirect('Banner',301);
		
	}

}

/* End of file banner.php */
