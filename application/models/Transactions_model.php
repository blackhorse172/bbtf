<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions_model extends MY_Model {

	public function __construct()
	{
		// If you use standard naming convention, this code can be omitted.
		$this->table ='transaction_h';
		$this->id_field = 'id';
		$this->row_type = 'trx_object';
	
		parent::__construct();
	}

	public function join()
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('tb_users', $this->table.'.user_id = tb_users.id');
		$this->db->join('seller', 'seller.seller_id = tb_users.ref_user');

		$res = $this->db->get();
		
		return $res->result();
		
		
	}

	public function joinWithUserId($id)
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('tb_users', $this->table.'.user_id = tb_users.id');
		$this->db->join('seller', 'seller.seller_id = tb_users.ref_user');
		$this->db->where('user_id', $id);
		
		$res = $this->db->get();
		
		return $res->result();
		
		
	}

	public function getListWithTrxCode($trxCode)
	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('transaction_d', $this->table.'.trx_code = transaction_d.trx_code');
		$this->db->join('product', 'product.id = transaction_d.product_id');
		$this->db->where($this->table.".trx_code", $trxCode);
		
		$res = $this->db->get();
		
		return $res->result();
		
		
	}

	public function joinTradeBuyer()
	{
		$this->db->select('*');
		$this->db->from('transaction_register_trade_buyer');
		$this->db->join('trade_buyer', 'transaction_register_trade_buyer.user_id = trade_buyer.id');

		$res = $this->db->get();
		
		return $res->result();
		
		
	}
}


	class trx_object extends Model_object {
	
	}
	

/* End of file ModelName.php */
 