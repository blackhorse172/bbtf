<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class psa_schedule_model extends CI_Model {

	var $tableSeller;
	var $tableBuyer;
	var $data;
	var $dateNow;
	var $userGroup;
	var $vSchecdule;
	var $vSellerList;
	var $id;
	var $uid;
	var $table;


	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->tableSeller = "psa_seller";
		$this->tableBuyer = "psa_buyer";

		$this->dateNow = date('Y-m-d');
		$this->data = [];
		$this->userGroup = $this->session->userdata('gid');
		$this->vSchecdule = 'v_psa_schedule';
		$this->table = "psa_schedule";
		$this->id = $this->session->userdata('ref_id');
	}

	//@data = array table psa
	public function create($data)
	{
		# code
		
		$ret = $this->db->insert($this->table, $data);
		
		return $ret;
	}

	/**
	 *  function getSchedule
	 *
	 *  
	 *
	 * @param int $var Description
	 * @return array_result()
	 * @throws conditon
	 **/
	public function getSchedule()
	{
		# code
		if((int)$this->userGroup == 4)
		{
			$this->db->where('buyer_id', $this->id);
			$tableSendRequest = $this->tableBuyer;
		} else if((int)$this->userGroup == 0)
		{
			$this->db->where('seller_sub_id', $this->id);
		}

		$ret = $this->db->get($this->vSchecdule);
		
		return $ret->result_array();
	}



}

/* End of file Psa_model.php */

?>
