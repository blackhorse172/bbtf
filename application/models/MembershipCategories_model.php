<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MembershipCategories_model extends MY_Model {

	public function __construct()
	{
		// If you use standard naming convention, this code can be omitted.
		$this->table ='category_membership';
		$this->id_field = 'category_id';
		$this->row_type = 'membershipCategories_object';
		/*$this->table = 'cars';
		$this->id_field = 'id';
		$this->row_type = 'Car_object';*/
		parent::__construct();
	}

	public function isActive(){
		$this->db->where('is_active',1);
		return $this;
	}

	public function noActive(){
		$this->db->where('is_active',0);
		return $this;
	}
	

}

class membershipCategories_object extends Model_object {
	
}

/* End of file MembershipCategories_model.php */
