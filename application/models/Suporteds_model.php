<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Suporteds_model extends MY_Model {

	public function __construct()
	{
		// If you use standard naming convention, this code can be omitted.
		$this->table ='suported';
		$this->id_field = 'id';
		$this->row_type = 'Suported_object';
		/*$this->table = 'cars';
		$this->id_field = 'id';
		$this->row_type = 'Car_object';*/
		parent::__construct();
	}

	public function isActive(){
		$this->db->where('status',1);
		return $this;
	}

	public function noActive(){
		$this->db->where('status',0);
		return $this;
	}

	// public function join(){
	// 	$this->db->select('*');
	// 	$this->db->from($this->table);
	// 	$this->db->join('category_banner', $this->table.'.category_id = category_banner.category_id', 'left');
	// 	$query = $this->db->get();

	// 	return $query->result();
	// }
}


	class Suported_object extends Model_object {
	
	}
	

/* End of file ModelName.php */
 