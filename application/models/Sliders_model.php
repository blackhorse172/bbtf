<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sliders_model extends MY_Model {

	public function __construct()
	{
		// If you use standard naming convention, this code can be omitted.
		$this->table ='slider';
		$this->id_field = 'slider_id';
		$this->row_type = 'Slider_object';
		
		parent::__construct();
	}

	public function isActive(){
		$this->db->where('status',1);
		return $this;
	}

	public function noActive(){
		$this->db->where('status',0);
		return $this;
	}

	public function join(){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('category_slider', $this->table.'.category_id = category_slider.category_id', 'left');
		$query = $this->db->get();

		return $query->result();
	}

	
}


	class Slider_object extends Model_object {
	
	}
	

/* End of file ModelName.php */
 