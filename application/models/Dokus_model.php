<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dokus_model extends CI_Model {

	
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->table = "doku";
		$this->load->config('doku');
		$this->load->library('my_libraries');
		
	}

	public function createTransactionTmp($post)
	{
		$config['mallId'] = $this->config->item('mallId');
		$config['sharedKey'] = $this->config->item('sharedKey');
		$config['invoice'] = $this->incrementalHash();
		$params = array('amount' => $post['fee'].'.00','invoice' => $config['invoice'], 'currency' => '360');
		$words = sha1($params['amount'].$config['mallId'].$config['sharedKey'].$config['invoice']);

		$data['transidmerchant'] = $config['invoice'];
		$data['totalamount'] = $post['fee'];
		$data['words'] = $words;
		$data['payment_channel'] = 15;
		$data['ref_user_id'] = $post['buyer_id'];
		$data['user_type'] = $post['user_type'];


		$this->db->insert('doku_tmp', $data);
		return $data;
		
	}


	public function createTransactionTmpBuyer($post)
	{
		$config['mallId'] = $this->config->item('mallId');
		$config['sharedKey'] = $this->config->item('sharedKey');
		$config['invoice'] = $this->incrementalHash();
		$params = array('amount' => $post['fee'].'.00','invoice' => $config['invoice'], 'currency' => '360');
		$words = sha1($params['amount'].$config['mallId'].$config['sharedKey'].$config['invoice']);

		$data['transidmerchant'] = $config['invoice'];
		$data['totalamount'] = $post['fee'];
		$data['words'] = $words;
		$data['payment_channel'] = 15;
		$data['ref_user_id'] = $post['buyer_id'];
		$data['user_type'] = $post['user_type'];


		$this->db->insert('doku_tmp', $data);

		return $data;
		
	}


	private function incrementalHash($length=10){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function testReturn()
	{
		return "TEst";
	}

	public function getList()
	{
		return $this->db->get($this->table)->result_array();
	}
	

}

/* End of file Dokus_model.php */
