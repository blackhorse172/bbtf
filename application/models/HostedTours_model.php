<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class HostedTours_model extends MY_Model {

	public function __construct()
	{
		// If you use standard naming convention, this code can be omitted.
		$this->table = 'hosted_tour_request';
		$this->id_field = 'id';
		$this->row_type = 'hosted_tour_request_object';
		parent::__construct();
	}

	public function join(){
		
		$this->db->select('hosted_tour_request.*,product.product_name,product.id as product_id,tb_users.id as user_id,tb_users.first_name,tb_users.ref_user,tb_groups.name as group_name');
		$this->db->from($this->table);
		$this->db->join('product', $this->table.".product_id = product.id");
		$this->db->join('tb_users', $this->table.".user_id = tb_users.id");
		$this->db->join('tb_groups',"tb_users.group_id = tb_groups.group_id");
		

		$res = $this->db->get();
		return $res->result();
	}

	public function joinById(){
		
		$this->db->select('hosted_tour_request.*,product.product_name,product.id as product_id,tb_users.first_name,tb_groups.name as group_name');
		$this->db->from($this->table);
		$this->db->join('product', $this->table.".product_id = product.id");
		$this->db->join('tb_users', $this->table.".user_id = tb_users.id");
		$this->db->join('tb_groups',"tb_users.group_id = tb_groups.group_id");
		$this->db->where('user_id', $this->session->userdata('uid'));
		$this->db->where('hosted_tour_request.status', 1);
		

		$res = $this->db->get();
		return $res->result();
	}

	// public function noActive(){
	// 	$this->db->where('enable','0');
	// 	return $this;
	// }
}


	class hosted_tour_request_object extends Model_object {
	
	}
	

/* End of file ModelName.php */
 