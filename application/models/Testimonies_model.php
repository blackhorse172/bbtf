<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonies_model extends MY_Model {

	public function __construct()
	{
		// If you use standard naming convention, this code can be omitted.
		$this->table ='testimony';
		$this->row_type= 'Testimonies_object';
		$this->id_field = 'id';
		/*$this->table = 'cars';
		$this->id_field = 'id';
		$this->row_type = 'Car_object';*/
		parent::__construct();
	}

	public function hasPublish(){
		$this->db->where('status','publish');
		return $this;
	}

	public function noPublish(){
		$this->db->where('status','unpublish');
		return $this;
	}

	public function getRandom($id = "",$limit)
	{
		if($id != "")
		{
			$query = "SELECT * FROM testimony where id != ".$id." AND is_active=1 and show_home=1
			ORDER BY RAND()
			LIMIT ".$limit." ";
		} else {
			$query = "SELECT * FROM testimony where is_active=1 and show_home=1 ORDER BY RAND()
			LIMIT ".$limit." ";
		}
		

		$q = $this->db->query($query);
		return $q->result_array();
	}
}


	class Testimonies_object extends Model_object {
	
	}
	

/* End of file ModelName.php */
 