<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Memberships_model extends MY_Model {

	public function __construct()
	{
		// If you use standard naming convention, this code can be omitted.
		$this->table ='membership';
		$this->id_field = 'membership_id';
		$this->row_type = 'membership_object';
		/*$this->table = 'cars';
		$this->id_field = 'id';
		$this->row_type = 'Car_object';*/
		parent::__construct();
	}

	public function isActive(){
		$this->db->where('is_active',1);
		return $this;
	}

	public function noActive(){
		$this->db->where('is_active',0);
		return $this;
	}

	public function join(){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('category_membership', $this->table.'.category_id = category_membership.category_id', 'left');
		$query = $this->db->get();

		return $query->result();
	}
}


	class membership_object extends Model_object {
	
	}
	

/* End of file ModelName.php */
 