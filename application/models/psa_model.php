<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Psa_model extends CI_Model {

	var $tableSeller;
	var $tableBuyer;
	var $data;
	var $dateNow;
	var $userGroup;
	var $vBuyerList;
	var $vSellerList;
	var $id;
	var $uid;

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->tableSeller = "psa_seller";
		$this->tableBuyer = "psa_buyer";

		$this->dateNow = date('Y-m-d');
		$this->data = [];
		$this->userGroup = $this->session->userdata('gid');
		$this->vBuyerList = 'buyer_list';
		$this->vBuyerList = 'list_seller_sub';
		$this->id = $this->session->userdata('ref_id');

	}

	public function getPeriode()
	{
		# code...
		$this->data = 0;
		$where = "gs_code= 'psa_first_start_periode' OR gs_code= 'psa_first_end_periode'  AND CURDATE() >= gs_value AND CURDATE() <= gs_value";
		$this->db->where($where);
		$res = $this->db->get('general_settings');
		if($res->num_rows() > 0)
		{
			$this->data = 1;
		} else {
			$where = "gs_code= 'psa_second_start_periode' OR gs_code= 'psa_second_end_periode'  AND CURDATE() >= gs_value AND CURDATE() <= gs_value";
			$this->db->where($where);
			$res = $this->db->get('general_settings');
			if($res->num_rows() > 0)
			{
				$this->data = 2;
			} 
		}
		

		return $this->data;
		
	}

	public function information()
	{
		# code...
		$this->db->where('DATE(gs_value)', $this->dateNow);
		
		$res = $this->db->get('general_settings');
		if($res->num_rows() > 0)
		{
			$this->data = $res->row();
		} else {
			$this->data = 0;
		}
	}
	
	/**
	 *  function getSlotAppointment
	 *
	 *  function long description
	 *
	 * @param int $var Description
	 * @return int
	 * @throws conditon
	 **/
	public function getRemainingSlot(int $day = null)
	{
		# code...
		$tableSendRequest = '';
		$tableRequest = '';
		$periode = $day == null ? $this->getPeriode() : $day;
		
		if((int)$this->userGroup == 4)
		{
			$this->db->where('buyer_id', $this->id);
			$tableSendRequest = $this->tableBuyer;
		} else if((int)$this->userGroup == 0)
		{
			$this->db->where('seller_id', $this->id);
			$tableSendRequest = $this->tableSeller;
		}

		$this->db->where('status', PSA_STATUS_CONFIRM);
		$this->db->where('year', date('Y'));
		$this->db->where('periode', $periode);

		
		$sendRequest = $this->db->get($tableSendRequest);
		$default = $this->getDefaultAmount();

		if((int)$this->userGroup == 4)
		{
			$this->db->where('buyer_id', $this->id);
			$tableSendRequest = $this->tableSeller;
		} else if((int)$this->userGroup == 0)
		{
			$this->db->where('seller_id', $this->id);
			$tableSendRequest = $this->tableBuyer;
		}
		
		$confirmRequest = $this->db->get($tableSendRequest);

		$slot = $default - ($sendRequest->num_rows() + $confirmRequest->num_rows());

		return $slot;
		
	}

	/**
	 * undocumented function summary
	 *
	 * Undocumented function long description
	 *
	 * @param int $var Description
	 * @return int
	 * @throws conditon
	 **/
	public function getDefaultAmount()
	{
		# code...
		$this->db->where('gs_code', 'psa_participant_amount');
		$res = $this->db->get('general_settings');

		return $res->row()->gs_value;
	}

	/**
	 * undocumented function summary
	 *
	 * Undocumented function long description
	 *
	 * @param Type $var Description
	 * @return type
	 * @throws conditon
	 **/
	public function getSystemAmount()
	{
		# code...
		$this->db->where('gs_code', 'psa_pick_amount_sys');
		$res = $this->db->get('general_settings');
		
		$slot = (int)$this->getDefaultAmount() * ((int)$res->row()->gs_value /100);

		return round($slot);
	}

	public function getAdminAmount()
	{
		# code...
		$this->db->where('gs_code', 'psa_pick_amount_usr');
		$res = $this->db->get('general_settings');
		$slot = (int)$this->getDefaultAmount() * ((int)$res->row()->gs_value /100);

		return round($slot);
		
	}

	/**
	 * undocumented function summary
	 *
	 * Undocumented function long description
	 *
	 * @param Type $var Description
	 * @return object list
	 * @throws conditon
	 **/
	public function getUserList()
	{
		# code...
		$query = "";
		$periode = $this->getPeriode();
		if((int)$this->userGroup == 4)
		{
			$query = "call getPsaSellerList(".$this->id.",".$periode.")";
		} else if((int)$this->userGroup == 0){
			$query = "call getPsaBuyerList(".$this->id.",".$periode.")";
		}

		$query = $this->db->query($query);

		$res = $query->result(); 
	
		$query->next_result();
		$query->free_result();
		
		return $res;
	}

	/**
	 * undocumented function summary
	 *
	 * Undocumented function long description
	 *
	 * @param Type $var Description
	 * @return object
	 * @throws conditon
	 **/
	public function getListRequest()
	{
			# code...
			$tableSendRequest = '';
			$tableRequest = '';
			$periode = $this->getPeriode();
			//print_r($this->session->userdata('gid'));
			if((int)$this->userGroup == 4)
			{
				$this->db->where('buyer_id', $this->id);
				$tableSendRequest = $this->tableBuyer;
			} else if((int)$this->userGroup == 0)
			{
				$this->db->where('seller_id', $this->id);
				$tableSendRequest = $this->tableSeller;
			}
			$this->db->where('status', PSA_STATUS_REQUEST);
			$this->db->where('year', date('Y'));
			$this->db->where('periode', $periode);
	
			
			$sendRequest = $this->db->get($tableSendRequest);
			
			return $sendRequest;
	}
	

	public function getConfirmList()
	{
		# code...getReqPsaBuyerList
		# code...
		$query = "";
		$periode = $this->getPeriode();
		
		if((int)$this->userGroup == 4)
		{
			$query = "call getReqPsaSellerList(".$this->id.",".$periode.")";
		} else if((int)$this->userGroup == 0){
			$query = "call getReqPsaBuyerList(".$this->id.",".$periode.")";
		}
		
		$query = $this->db->query($query);
		
		$res = $query->result(); 
	
		$query->next_result();
		$query->free_result();
		
		return $res;
	}


	
	public function getRandomSchedule($data)
	{
			$periode = $this->getPeriode();
			$query = "
			SELECT t1.*
				FROM (
				SELECT id FROM psa_master_schedule
				WHERE
				time_type = 'APPOINTMENT'
				AND
				day = ".$periode."
				UNION ALL
				SELECT schedule_id FROM psa_schedule
				where psa_schedule.buyer_id = ".$data->buyer_id."
				UNION ALL
				SELECT schedule_id FROM psa_schedule
				where psa_schedule.seller_sub_id = ".$data->seller_id."
				) t1
				GROUP BY id
				HAVING count(*) = 1
				ORDER BY RAND()
				LIMIT 1;";
			$res = $this->db->query($query)->row();
			
			return $res;
		
	}


	public function checkScheduleUser($id = null)
	{
		# code...
		$periode = $this->getPeriode();
		$query = "SELECT * FROM psa_master_schedule
		where day = ".$periode."
		AND time_type = 'APPOINTMENT'
		ORDER BY RAND()
		LIMIT 1";
		$res = $this->db->query($query)->row();

		return $res;
	}

	public function updateStatusPsa($id)
	{
		# code...
		$table = "";
		
		if((int)$this->userGroup == 4)
		{
			$table = "psa_seller";
		

		} else if((int)$this->userGroup == 0){
			$table = "psa_buyer";
			
		}

		$periode = $this->getPeriode();
		$update = $this->getRowData($id);

		$schedule = $this->getRandomSchedule($update);
		
		if($schedule->id != NULL || $schedule->id != '' )
		{
			$update->status = PSA_STATUS_CONFIRM;
			$update->confirm_date = date('Y-m-d H:i:s');
			$update->psa_schedule_id = $schedule->id;
	
			$this->db->where('id', $update->id);
			$ret = $this->db->update($table, $update);
		} 
		
		
		return  $ret;
		

		
	}

	public function getRowData($id)
	{
		# code...
		$table = "";
		$periode = $this->getPeriode();
		if((int)$this->userGroup == 4)
		{
			$table = "psa_seller";
			$this->db->where('seller_id', $id);
			$this->db->where('buyer_id', $this->id);

		} else if((int)$this->userGroup == 0){
			$table = "psa_buyer";
			$this->db->where('seller_id', $this->id);
			$this->db->where('buyer_id', $id);
		}

		$this->db->where('periode', $periode);
		$this->db->where('year', date('Y'));

		$res = $this->db->get($table);
		
		return $res->row();
	}

	/**
	 * undocumented function summary
	 *
	 * Undocumented function long description
	 *
	 * @param data array
	 * @return int 
	 * @throws conditon
	 **/
	public function insertReqPsa($data)
	{
		# code...
		# code...
		$tableSendRequest = '';
		$tableRequest = '';
		
		//print_r($this->session->userdata('gid'));
		if((int)$this->userGroup == 4)
		{
			
			$tableSendRequest = $this->tableBuyer;
		} else if((int)$this->userGroup == 0)
		{
			$tableSendRequest = $this->tableSeller;
		}
		

		return $this->db->insert_batch($tableSendRequest, $data);
		
	}

}

/* End of file Psa_model.php */

?>
