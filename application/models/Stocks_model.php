<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stocks_model extends MY_Model {

	public function __construct()
	{
		// If you use standard naming convention, this code can be omitted.
		$this->table ='stock';
		$this->id_field = 'id';
		$this->row_type = 'stock_object';
		$this->view = 'v_product_stock';
		
		parent::__construct();
	}

	public function getList()
	{
		return $this->db->get($this->view);
		
	}
	

}

class stock_object extends Model_object {
	
}
/* End of file Stocks.php */
