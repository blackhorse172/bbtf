<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends MY_Model {

	public function __construct()
	{
		// If you use standard naming convention, this code can be omitted.
		$this->table ='product';
		$this->id_field = 'id';
		$this->row_type = 'product_object';
		
		parent::__construct();
	}

	public function isActive(){
		$this->db->where('active',1);
		return $this;
	}

	public function noActive(){
		$this->db->where('active',0);
		return $this;
	}

	public function join(){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('category_product', $this->table.'.category_id = category_product.category_id', 'left');
		$query = $this->db->get();

		return $query->result();
	}

	public function getBoothProduct(){
		$this->db->where('category_id', 1);
		$this->db->where('active',1);
		$this->db->where('total_stock >',0);
		$this->db->where("(is_package = 0 OR is_package is null)");
		return $this->db->get('v_product_stock')->result();
	}

	
}


	class product_object extends Model_object {
	
	}
	

/* End of file ModelName.php */
 