<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Schedules_model extends MY_Model {

	public function __construct()
	{
		// If you use standard naming convention, this code can be omitted.
		$this->table = 'meeting_schedule';
		$this->id_field = 'id';
		$this->row_type = 'meeting_schedule_object';
		parent::__construct();
	}


	public function save_data($data,$id = null)
	{
		if($id == null){
			$this->db->insert($this->table,$data);

			return $this->db->insert_id();
			
		} else{
			$this->db->update($this->table,$data);
			$this->db->where('id', $id);
			
			return true;
			
		}
	}

	// public function isActive(){
	// 	$this->db->where('enable','1');
	// 	return $this;
	// }

	// public function noActive(){
	// 	$this->db->where('enable','0');
	// 	return $this;
	// }
}


	class meeting_schedule_object extends Model_object {
	
	}
	

/* End of file ModelName.php */
 