<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payments_model extends MY_Model {

	public function __construct()
	{
		// If you use standard naming convention, this code can be omitted.
		$this->table ='payment';
		$this->id_field = 'id';
		$this->row_type = 'trx_object';
	
		parent::__construct();
	}
}


	class trx_object extends Model_object {
	
	}
	

/* End of file ModelName.php */
 