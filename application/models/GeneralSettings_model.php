<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class GeneralSettings_model extends CI_Model {

	var $table;
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->table = 'general_settings';
	}


	public function getYearEvent()
	{
		# code...
		return $this->db->where('gs_code', 'bbtf_year')->get($this->table)->row()->gs_value;
		
	}
	

	public function getDateEvent()
	{
		# code...
		return $this->db->where('gs_code', 'bbtf_date')->get($this->table)->row()->gs_value;
		
	}

}

/* End of file GeneralSettings_model.php */
