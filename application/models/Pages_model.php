<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pages_model extends MY_Model {

	public function __construct()
	{
		// If you use standard naming convention, this code can be omitted.
		$this->table ='tb_pages';
		$this->id_field = 'pageID';
		$this->row_type = 'page_object';

		/*$this->table = 'cars';
		$this->id_field = 'id';
		$this->row_type = 'Car_object';*/
		parent::__construct();
	}

	public function isActive(){
		$this->db->where('status',1);
		return $this;
	}

	public function noActive(){
		$this->db->where('status',0);
		return $this;
	}
}


	class page_object extends Model_object {
	
	}
	

/* End of file ModelName.php */
 