<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UserGroups_model extends MY_Model {

	public function __construct()
	{
		// If you use standard naming convention, this code can be omitted.
		$this->table ='tb_groups';
		$this->id_field = 'group_id';
		$this->row_type = 'UserGroups_object';
	
		parent::__construct();
	}
}


	class UserGroups_object extends Model_object {
	
	}
	

/* End of file ModelName.php */
 