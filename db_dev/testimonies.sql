/*
 Navicat Premium Data Transfer

 Source Server         : localhost_mysql
 Source Server Type    : MySQL
 Source Server Version : 100406
 Source Host           : localhost:3306
 Source Schema         : bbtf

 Target Server Type    : MySQL
 Target Server Version : 100406
 File Encoding         : 65001

 Date: 21/01/2020 01:41:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for media_register
-- ----------------------------
DROP TABLE IF EXISTS `media_register`;
CREATE TABLE `media_register`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `company_address` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `media_phone` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `gender` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `full_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `position` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `mobile` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `media_email` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `membership_id` int(11) NULL DEFAULT NULL,
  `company_profile` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `country_code` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `dtm_crt` datetime(0) NULL DEFAULT NULL,
  `dtm_upd` datetime(0) NULL DEFAULT NULL,
  `is_active` enum('1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '1',
  `usr_id` int(11) NOT NULL,
  `membership_verify` int(1) NULL DEFAULT 0,
  `media_class` int(10) NULL DEFAULT NULL,
  `city` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci;

-- ----------------------------
-- Records of media_register
-- ----------------------------
BEGIN;
INSERT INTO `media_register` VALUES (1, 'Testing', 'Jakarta', 'Jakarta', 'ID', 'reno@mail.com', '081298004000', NULL, NULL, 1, 'test', '', '2019-11-24 16:05:34', NULL, '1', 0, 0, NULL, NULL), (2, 'buyer1234', 'jakarta', 'jakarta', 'ID', 'buyer1@mail.com', '0838984357832', NULL, NULL, 0, 'test', 'test', '2020-01-09 11:59:49', NULL, '1', 0, 0, NULL, NULL), (4, 'The Raid', 'test', 'test', 'ID', 'buyer2@mail.com', '082112341234', NULL, NULL, NULL, 'teset', 'test.com', '2020-01-14 05:37:08', NULL, '', 0, 0, NULL, NULL), (5, 'test21313', NULL, 'safsdfsdf', 'AO', 'buyyer@mail.com', '12313', NULL, NULL, NULL, 'test21313', 'sdfsf', '2020-01-16 07:17:55', '2020-01-19 15:37:04', '1', 0, 1, 1, NULL), (6, 'Adi', NULL, 'Jakarta', 'ID', 'blackhorse172@gmail.com', '083898435858', NULL, NULL, NULL, 'TEST', 'test', '2020-01-20 00:37:50', NULL, '', 0, 0, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for testimony
-- ----------------------------
DROP TABLE IF EXISTS `testimony`;
CREATE TABLE `testimony`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_active` int(1) NULL DEFAULT NULL,
  `user_id` int(10) NULL DEFAULT NULL,
  `dtm_crt` datetime(0) NULL DEFAULT NULL,
  `show_home` int(1) NULL DEFAULT NULL,
  `company_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci;

-- ----------------------------
-- Records of testimony
-- ----------------------------
BEGIN;
INSERT INTO `testimony` VALUES (1, 'One stop shop....\r\n', 'IAN GARRIT', 'BBTF is one stop shop for me, I managed to meet new potential hotels, resorts,and tour operators for our clients during appointment session.', 1, NULL, NULL, 1, 'VENTURE HOLIDAY AUSTRALIA'), (2, 'Yeah amazing', 'JOHN', 'tes123', 1, 0, '2020-01-20 22:03:09', 1, 'TEST'), (3, 'Lots of inquiries....', 'JUDI ANGLIM', 'We received alot inquiries for a new hotels and destinations for our clients. Through BBTF we are able to answer their demands now.', 1, 0, '2020-01-21 01:14:36', 1, 'INFINITY HOLIDAY AUSTRALIA'), (4, 'A must visit event!', 'MARITZA MANTILA GONZALEZ', 'BBTF was a great event to attend, we discovered destinations beyond Bali and introduced them to our viewers in South America. A must visit!', 1, 0, '2020-01-21 01:15:33', 1, 'WWW.TRAVESIA.TV'), (5, 'Experience through post tours', 'STRATIS VOURSOUKIS', 'Not only that we are able to meet with new tour operators , hotels and destination at BBTF, but by experiencing these destination through post tours we can now further prmote them to our clients in Greece.', 1, 0, '2020-01-21 01:22:55', 1, ' ');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
