/*
 Navicat Premium Data Transfer

 Source Server         : localhost_mysql
 Source Server Type    : MySQL
 Source Server Version : 100406
 Source Host           : localhost:3306
 Source Schema         : bbtf

 Target Server Type    : MySQL
 Target Server Version : 100406
 File Encoding         : 65001

 Date: 22/01/2020 13:37:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for suported
-- ----------------------------
DROP TABLE IF EXISTS `suported`;
CREATE TABLE `suported`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `image` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `link` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `position` int(2) NULL DEFAULT NULL,
  `featured` tinyint(2) NULL DEFAULT NULL,
  `status` int(2) NULL DEFAULT NULL,
  `dtm_crt` datetime(0) NULL DEFAULT NULL,
  `dtm_upd` datetime(0) NULL DEFAULT NULL,
  `usr_crt` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `usr_upd` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 43 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci;

-- ----------------------------
-- Records of suported
-- ----------------------------
BEGIN;
INSERT INTO `suported` VALUES (42, 'test', 'asita_logo_transparan.png', 'test', 0, 0, 1, '2020-01-22 13:29:32', '0000-00-00 00:00:00', 'Adi', ''), (41, 'test', 'logo_wonderful_indonesia.png', 'test', 0, 0, 1, '2020-01-22 13:06:39', '0000-00-00 00:00:00', 'Adi', '');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
