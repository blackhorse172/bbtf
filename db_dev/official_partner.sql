/*
 Navicat Premium Data Transfer

 Source Server         : localhost_mysql
 Source Server Type    : MySQL
 Source Server Version : 100406
 Source Host           : localhost:3306
 Source Schema         : bbtf

 Target Server Type    : MySQL
 Target Server Version : 100406
 File Encoding         : 65001

 Date: 19/01/2020 19:17:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for official_partner
-- ----------------------------
DROP TABLE IF EXISTS `official_partner`;
CREATE TABLE `official_partner`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `price` int(11) NULL DEFAULT NULL,
  `location` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tour_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `desc` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `usr_crt` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `dtm_crt` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_active` int(1) NULL DEFAULT NULL,
  `image` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of official_partner
-- ----------------------------
INSERT INTO `official_partner` VALUES (1, 'TIKET GARDENS BY THE BAY AT SINGAPORE\r\n', 3850000, 'Singapore', 'ABC Tours & Travel Singapore\r\n', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum socii', 'ADIP', 'ADIP', 1, NULL);
INSERT INTO `official_partner` VALUES (2, 'TICKET TO BANGKOK', 1000000, 'Thailand', 'CDE BANGKOK TOURSS', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum socii', NULL, NULL, 1, NULL);
INSERT INTO `official_partner` VALUES (3, 'TIKET GARDENS BY THE BAY AT SINGAPORE\r\n', 3850000, 'Singapore', 'ABC Tours & Travel Singapore\r\n', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum socii', 'ADIP', 'ADIP', 1, NULL);
INSERT INTO `official_partner` VALUES (4, 'TICKET TO BANGKOK', 1000000, 'Thailand', 'CDE BANGKOK TOUR', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum socii', NULL, NULL, 0, NULL);
INSERT INTO `official_partner` VALUES (5, 'TIKET GARDENS BY THE BAY AT SINGAPORE\r\n', 3850000, 'Singapore', 'ABC Tours & Travel Singapore\r\n', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum socii', 'ADIP', 'ADIP', 1, NULL);
INSERT INTO `official_partner` VALUES (6, 'TICKET TO BANGKOK', 1000000, 'Thailand', 'CDE BANGKOK TOUR', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum socii', NULL, NULL, 0, NULL);

SET FOREIGN_KEY_CHECKS = 1;
