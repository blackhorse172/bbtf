/*
 Navicat Premium Data Transfer

 Source Server         : localhost_mysql
 Source Server Type    : MySQL
 Source Server Version : 100406
 Source Host           : localhost:3306
 Source Schema         : bbtf_stag

 Target Server Type    : MySQL
 Target Server Version : 100406
 File Encoding         : 65001

 Date: 08/03/2020 23:17:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- View structure for list_seller_sub
-- ----------------------------
DROP VIEW IF EXISTS `list_seller_sub`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `list_seller_sub` AS select `transaction_h`.`trx_code` AS `trx_code`,`transaction_h`.`trx_datetime` AS `trx_datetime`,`transaction_h`.`trx_subtotal` AS `trx_subtotal`,`transaction_h`.`trx_status` AS `trx_status`,`transaction_h`.`user_id` AS `user_id`,`transaction_h`.`trx_date` AS `trx_date`,`transaction_h`.`trx_type` AS `trx_type`,`transaction_h`.`usr_crt` AS `usr_crt`,`transaction_h`.`dtm_crt` AS `trxdate_crt`,`transaction_h`.`dtm_upd` AS `trxdate_upd`,`transaction_h`.`usr_upd` AS `usr_upd`,`transaction_h`.`trx_tax` AS `trx_tax`,`transaction_h`.`trx_service_tax` AS `trx_service_tax`,`transaction_h`.`trx_total` AS `trx_total`,`transaction_h`.`trx_payment_type` AS `trx_payment_type`,`transaction_h`.`trx_invoice` AS `trx_invoice`,`transaction_h`.`is_send_inv` AS `is_send_inv`,`seller_sub`.`id` AS `id`,`seller_sub`.`seller_name` AS `seller_name`,`seller_sub`.`seller_address` AS `seller_address`,`seller_sub`.`seller_city` AS `seller_city`,`seller_sub`.`seller_country_code` AS `seller_country_code`,`seller_sub`.`seller_email` AS `seller_email`,`seller_sub`.`seller_phone` AS `seller_phone`,`seller_sub`.`seller_fax` AS `seller_fax`,`seller_sub`.`seller_target_market` AS `seller_target_market`,`seller_sub`.`category_id` AS `category_id`,`seller_sub`.`seller_company_profile` AS `seller_company_profile`,`seller_sub`.`seller_website` AS `seller_website`,`seller_sub`.`dtm_crt` AS `dtm_crt`,`seller_sub`.`dtm_upd` AS `dtm_upd`,`seller_sub`.`is_active` AS `is_active`,`seller_sub`.`membership_id` AS `membership_id`,`seller_sub`.`is_membership` AS `is_membership`,`seller_sub`.`is_read_terms_condition` AS `is_read_terms_condition`,`seller_sub`.`full_delegate_name` AS `full_delegate_name`,`seller_sub`.`job_title` AS `job_title`,`seller_sub`.`registration_person` AS `registration_person`,`seller_sub`.`co_delegate_name` AS `co_delegate_name`,`seller_sub`.`seller_province` AS `seller_province`,`seller_sub`.`transaction_id` AS `transaction_id`,`seller_sub`.`is_packed` AS `is_packed`,`seller_sub`.`booth_name` AS `booth_name`,`seller_sub`.`delegation_name` AS `delegation_name`,`seller_sub`.`co_delegate` AS `co_delegate`,`seller_sub`.`full_delegate` AS `full_delegate`,`seller_sub`.`email_login` AS `email_login`,`seller_sub`.`password_login` AS `password_login`,`seller_sub`.`is_additional` AS `is_additional`,`seller_sub`.`ref_seller_id` AS `ref_seller_id`,`seller_sub`.`product_id` AS `product_id`,`seller_sub`.`ref_trans` AS `ref_trans`,`seller_sub`.`ref_user_id` AS `ref_user_id`,`seller_sub`.`booth_number` AS `booth_number`,`seller_sub`.`password_view` AS `password_view`,`seller_sub`.`booth_number_img` AS `booth_number_img`,`seller_sub`.`is_reset_password` AS `is_reset_password`,`transaction_h`.`id` AS `trx_id` from ((`transaction_h` join `tb_users` on((`transaction_h`.`user_id` = `tb_users`.`id`))) join `seller_sub` on((`tb_users`.`ref_user` = `seller_sub`.`ref_seller_id`))) ;

-- ----------------------------
-- View structure for seller_list
-- ----------------------------
DROP VIEW IF EXISTS `seller_list`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `seller_list` AS select `tb_users`.`group_id` AS `group_id`,`tb_users`.`id` AS `user_id`,`seller`.`seller_id` AS `seller_id`,`seller`.`seller_name` AS `seller_name`,`seller`.`seller_address` AS `seller_address`,`seller`.`seller_city` AS `seller_city`,`seller`.`seller_country_code` AS `seller_country_code`,`tb_users`.`active` AS `active`,`tb_users`.`last_login` AS `last_login`,`tb_users`.`login_attempt` AS `login_attempt`,`seller`.`seller_email` AS `seller_email`,`seller`.`seller_phone` AS `seller_phone`,`seller`.`seller_fax` AS `seller_fax`,`seller`.`seller_target_market` AS `seller_target_market`,`seller`.`category_id` AS `category_id`,`seller`.`seller_company_profile` AS `seller_company_profile`,`seller`.`seller_website` AS `seller_website`,`seller`.`is_active` AS `is_active`,`seller`.`membership_id` AS `membership_id`,`seller`.`is_membership` AS `is_membership`,`seller`.`is_read_terms_condition` AS `is_read_terms_condition`,`seller`.`full_delegate_name` AS `full_delegate_name`,`seller`.`job_title` AS `job_title`,`seller`.`registration_person` AS `registration_person`,`seller`.`co_delegate_name` AS `co_delegate_name`,`seller`.`seller_province` AS `seller_province`,`category_seller`.`category_name` AS `category_name`,`apps_countries`.`country_name` AS `country_name`,`tb_users`.`username` AS `username`,`tb_users`.`first_name` AS `first_name`,`tb_users`.`last_name` AS `last_name`,`seller`.`is_listing` AS `is_listing` from (((`seller` join `tb_users` on((`seller`.`seller_id` = `tb_users`.`ref_user`))) join `category_seller` on((`seller`.`category_id` = `category_seller`.`category_id`))) join `apps_countries` on((convert(`seller`.`seller_country_code` using utf8) = `apps_countries`.`country_code`))) where (`tb_users`.`group_id` = 5) ;

-- ----------------------------
-- View structure for v_booth_delegations
-- ----------------------------
DROP VIEW IF EXISTS `v_booth_delegations`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_booth_delegations` AS select `transaction_h`.`trx_code` AS `trx_code`,`booth_delegation`.`delegation_name` AS `delegation_name`,`booth_delegation`.`dtm_crt` AS `dtm_crt`,`booth_delegation`.`usr_crt` AS `usr_crt`,`booth_delegation`.`co_delegate` AS `co_delegate`,`booth_delegation`.`email` AS `email`,`booth_delegation`.`password` AS `password`,`booth_delegation`.`is_packed` AS `is_packed`,`booth_delegation`.`ref_seller_id` AS `ref_seller_id`,`booth_delegation`.`group_id` AS `group_id`,`booth_delegation`.`ref_trans` AS `ref_trans`,`seller`.`seller_name` AS `seller_name`,`seller`.`seller_city` AS `seller_city`,`seller`.`seller_email` AS `seller_email`,`seller`.`seller_phone` AS `seller_phone`,`seller`.`seller_address` AS `seller_address`,`booth_delegation`.`id` AS `delegation_id` from ((`booth_delegation` join `transaction_h` on((`transaction_h`.`id` = `booth_delegation`.`transaction_id`))) join `seller` on((`booth_delegation`.`ref_seller_id` = `seller`.`seller_id`))) where (`transaction_h`.`trx_status` = 2) ;

-- ----------------------------
-- View structure for v_booth_name
-- ----------------------------
DROP VIEW IF EXISTS `v_booth_name`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_booth_name` AS select `booth_name`.`booth_number` AS `booth_number`,`booth_name`.`booth_name` AS `booth_name`,`product`.`product_name` AS `product_name`,`booth_name`.`product_id` AS `product_id`,`product`.`product_price` AS `product_price`,`transaction_h`.`trx_code` AS `trx_code`,`transaction_h`.`trx_status` AS `trx_status`,`seller`.`seller_name` AS `seller_name`,`seller`.`seller_email` AS `seller_email`,`seller`.`seller_country_code` AS `seller_country_code`,`transaction_h`.`trx_datetime` AS `trx_datetime`,`booth_name`.`id` AS `booth_id`,`booth_name`.`delegation_name` AS `delegation_name`,`booth_name`.`co_delegate` AS `co_delegate`,`booth_name`.`email` AS `email`,`booth_name`.`password` AS `password`,`booth_name`.`is_packed` AS `is_packed` from (((`transaction_h` join `booth_name` on((`transaction_h`.`id` = `booth_name`.`transaction_id`))) join `product` on((`booth_name`.`product_id` = `product`.`id`))) join `seller` on((`booth_name`.`ref_seller_id` = `seller`.`seller_id`))) where (`transaction_h`.`trx_status` = 2) ;

-- ----------------------------
-- View structure for v_buyer_list
-- ----------------------------
DROP VIEW IF EXISTS `v_buyer_list`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_buyer_list` AS select `buyer`.`buyer_name` AS `buyer_name`,`buyer`.`buyer_address` AS `buyer_address`,`tb_users`.`id` AS `user_id`,`buyer`.`buyer_city` AS `buyer_city`,`apps_countries`.`country_name` AS `country_name` from (((`buyer` join `tb_users` on((`buyer`.`buyer_id` = `tb_users`.`ref_user`))) join `apps_countries` on((convert(`buyer`.`buyer_country_code` using utf8) = `apps_countries`.`country_code`))) join `transaction_h` on((`tb_users`.`id` = `transaction_h`.`user_id`))) group by `tb_users`.`id` ;

-- ----------------------------
-- View structure for v_product_detail
-- ----------------------------
DROP VIEW IF EXISTS `v_product_detail`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_product_detail` AS select `transaction_d`.`id` AS `id`,`transaction_d`.`trx_code` AS `trx_code`,`transaction_d`.`amount` AS `product_amount`,`transaction_d`.`price` AS `product_price`,`product`.`product_name` AS `product_name`,`product`.`product_desc` AS `product_desc`,`product`.`category_id` AS `category_id`,`transaction_d`.`delegate_name` AS `delegate_name`,`transaction_d`.`delegate_phone` AS `delegate_phone`,`transaction_d`.`delegate_email` AS `delegate_email`,`transaction_d`.`discount` AS `discount`,`transaction_d`.`discount_price` AS `discount_price`,`transaction_d`.`booth_name` AS `booth_name`,`transaction_d`.`booth_number` AS `booth_number` from (`transaction_d` join `product` on((`transaction_d`.`product_id` = `product`.`id`))) ;

-- ----------------------------
-- View structure for v_product_stock
-- ----------------------------
DROP VIEW IF EXISTS `v_product_stock`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_product_stock` AS select `stock`.`id` AS `id`,`product`.`id` AS `product_id`,`stock`.`note` AS `note`,`product`.`product_name` AS `product_name`,`stock`.`company_name` AS `company_name`,`product`.`product_price` AS `product_price`,`product`.`is_package` AS `is_package`,(case when (`product`.`discount_type` = 2) then `product`.`product_price` when (`product`.`discount_type` = 0) then (`product`.`product_price` - (`product`.`product_price` * (`product`.`discount` / 100))) else (`product`.`product_price` - `product`.`discount_value`) end) AS `price_after_discount`,(case when (`product`.`discount_member_type` = 2) then `product`.`product_price` when (`product`.`discount_member_type` = 0) then (`product`.`product_price` - (`product`.`product_price` * (`product`.`discount_member` / 100))) else (`product`.`product_price` - `product`.`discount_member_value`) end) AS `price_after_discount_member`,`product`.`active` AS `active`,`category_product`.`category_name` AS `category_name`,`category_product`.`category_id` AS `category_id`,sum(`stock`.`amount`) AS `total_stock`,`product`.`discount` AS `discount`,`product`.`discount_date` AS `discount_date`,`product`.`parent` AS `parent`,`product`.`discount_value` AS `discount_value`,`product`.`discount_member` AS `discount_member`,`product`.`discount_member_date` AS `discount_member_date`,`product`.`discount_member_value` AS `discount_member_value`,`product`.`discount_type` AS `discount_type`,`product`.`discount_member_type` AS `discount_member_type` from ((`product` left join `stock` on((`stock`.`product_id` = `product`.`id`))) join `category_product` on((`category_product`.`category_id` = `product`.`category_id`))) group by `product`.`id` ;

-- ----------------------------
-- View structure for v_seller_list
-- ----------------------------
DROP VIEW IF EXISTS `v_seller_list`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_seller_list` AS select `seller`.`seller_name` AS `seller_name`,`seller`.`seller_address` AS `seller_address`,`seller`.`seller_city` AS `seller_city`,`seller`.`is_active` AS `is_active`,`seller`.`category_id` AS `category_id`,`seller`.`membership_id` AS `membership_id`,`tb_users`.`id` AS `user_id`,`apps_countries`.`country_name` AS `country_name` from (((`seller` join `tb_users` on((`seller`.`seller_id` = `tb_users`.`ref_user`))) join `transaction_h` on((`tb_users`.`id` = `transaction_h`.`user_id`))) join `apps_countries` on((convert(`seller`.`seller_country_code` using utf8) = `apps_countries`.`country_code`))) group by `tb_users`.`id` ;

-- ----------------------------
-- View structure for v_seller_list_new
-- ----------------------------
DROP VIEW IF EXISTS `v_seller_list_new`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_seller_list_new` AS select `seller`.`seller_name` AS `seller_name`,`seller`.`seller_address` AS `seller_address`,`seller`.`seller_city` AS `seller_city`,`apps_countries`.`country_name` AS `country_name`,`tb_users`.`id` AS `user_id`,group_concat(`transaction_d`.`booth_name` separator ',') AS `booth_name`,group_concat(`transaction_d`.`booth_number` separator ',') AS `booth_number` from (((((`seller` join `tb_users` on((`seller`.`seller_id` = `tb_users`.`ref_user`))) join `category_seller` on((`seller`.`category_id` = `category_seller`.`category_id`))) join `apps_countries` on((convert(`seller`.`seller_country_code` using utf8) = `apps_countries`.`country_code`))) join `transaction_h` on((`tb_users`.`id` = `transaction_h`.`user_id`))) join `transaction_d` on((`transaction_h`.`trx_code` = `transaction_d`.`trx_code`))) where (year(`transaction_h`.`trx_date`) = year(curdate())) group by (`tb_users`.`id` and (`transaction_h`.`trx_status` = 2)) ;

SET FOREIGN_KEY_CHECKS = 1;
