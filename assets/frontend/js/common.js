 (function($){
  
  $.fn.extend({
              outerHTML: function( value ){
              
              // If there is no element in the jQuery object
              if(!this.length)
              return null;
              
              // Returns the value
              else if(value === undefined) {
              
              var element = (this.length) ? this[0] : this,
              result;
              
              // Return browser outerHTML (Most newer browsers support it)
              if(element.outerHTML)
              result = element.outerHTML;
              
              // Return it using the jQuery solution
              else
              result = $(document.createElement("div")).append($(element).clone()).html();
              
              // Trim the result
              if(typeof result === "string")
              result = $.trim(result);
              
              return result;
              } else if( $.isFunction(value) ) {
              // Deal with functions
              
              this.each(function(i){
                        var $this = $( this );
                        $this.outerHTML( value.call(this, i, $this.outerHTML()) );
                        });
              
              } else {
              // Replaces the content
              
              var $this = $(this),
              replacingElements = [],
              $value = $(value),
              $cloneValue;
              
              for(var x = 0; x < $this.length; x++) {
              
              // Clone the value for each element being replaced
              $cloneValue = $value.clone(true);
              
              // Use jQuery to replace the content
              $this.eq(x).replaceWith($cloneValue);
              
              // Add the replacing content to the collection
              for(var i = 0; i < $cloneValue.length; i++)
              replacingElements.push($cloneValue[i]);
              
              }
              
              // Return the replacing content if any
              return (replacingElements.length) ? $(replacingElements) : null;
              }
              }
              });
  
  })(jQuery);
var networkConnect = true;
function setNetworkConnect(){
    networkConnect = true;
}
function setNetworkDisconnect(){
    networkDisconnect = false;
}
function showTitle(text){
          var title = $(".p-title");
          title.css("color","");
          title.text(text);         
          title.addClass("fadeIn animated").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                                                  $(this).removeClass("fadeIn" + ' animated');

           });
          $(".loadingHidden").show();
          $(".loadingHidden").addClass("fadeIn animated").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                                                  $(this).removeClass("fadeIn" + ' animated');

           });

}
if (!cms24k) {
    var cms24k = {}
};
$.extend(true, cms24k, {
         
         
         replaceHtml : function(config)
         {
         var html  = $(config.content);
         var css   = config.css;
         var container = config.container;
         var e = jQuery.Event( "beforeLeave");
         $(document).trigger(e,[config]);
           if (document.offline){
          $(html).find("img").each(function(){
                                  var src = $(this).attr('src');
                                  if (src.indexOf('/') === 0){
                                  this.src = src.substring(1);
                                  }
                                  })
         $(html).find(".background").each(function(){
                                          var src = $(this).css('background-image').trim();
                                          
                                          if (src.indexOf('url("/' === 0)){
                                          var substring  = src.substring(6);
                                          var data = 'url("'+ substring;
                                          //alert(data);

                                            // $(this).css('background-image', "url('resources/front/template/assetsnaps/images/banner3.jpg');");
                                          }
                                          })
         };
         
         $(html).find(".loadingHidden").hide();
         if (config.replace){
         container.replaceWith(html);
         }else{
         container.html(html);
         }
         e = jQuery.Event("afterReplace" );
         $(document).trigger(e,[config]);
         console.log(config);
         if (config.top){
         try{
         $(container).scrollTop(config.top);
         $(document).scrollTop(config.docTop);
         }catch(err){
         console.log(err);
         }
         } else{
         $(document).scrollTop(0);
         $(container).scrollTop(0);
         }
          var title = $(".p-title");
          title.text(".").css("color","#fcd800");
          var x = 1;
         if (config.replace){
         $(html).addClass(css + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                                                 $(this).removeClass(css + ' animated');
                                                   if (x == 1){
                                                      x = 2;
                                                    showTitle(config.title);                                                  
                                                  }

                                                  
                                                 })
         }else{
         $(container).addClass(css + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                                                      $(this).removeClass(css + ' animated');
                                                                                                         if (x == 1){
                                                      x = 2;
                                                    showTitle(config.title);                                                  
                                                  }
                                                      });
         }
         },
         
         toDataUrl: function (src, callback) {
         var img = new Image();
         img.crossOrigin = 'Anonymous';
         img.onload = function() {
         var canvas = document.createElement('CANVAS');
         var ctx = canvas.getContext('2d');
         var dataURL;
         canvas.height = this.height;
         canvas.width = this.width;
         ctx.drawImage(this, 0, 0);
         dataURL = canvas.toDataURL("image/png");
         callback(dataURL);
         };
         img.src = src;
         if (img.complete || img.complete === undefined) {
         img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
         img.src = src;
         }
         },
         fbclient : function(token,success){
         $.ajax({
                type : "POST",
                url  : '/pub-facebook/token-login/',
                data : {
                token : token
                },
                dataType : "json",
                success : function(data) {
                location.href = success;
                },
                error: function(xhr, ajaxOptions, thrownError) {
                alert('facebook login failed');
                }
                });
         },
         urls : [],
         living : function() {
         return cms24k.isDef(typeof (k_edit));
         },
         searchSimple : true,
         normalAlert  : window.alert,
         isDef : function(value) {
         return value == "undefined";
         },
         
         
         //url,container
         loadNative : function(){
         document.nativeLoad(loadConfig.url ,function(content){
                             loadConfig.content = content;
                             cms24k.replaceHtml( content );
                             },function(){
                             cms24k.getHtml(loadConfig);
                             });
         
         },
         
         simpleLoad : function(url,container){
           var config = {
          container : container,
          css       : "fadeInRight", 
          url       : url,
          top       : 1,
          docTop    :  $(document).scrollTop(),
          title     : "",
          replace   : false,
          nohistory  : true,  
           };
           
           cms24k.getHtml(config);
         },
         
         
         
         
         // url,container
         loadCacheHtml : function( loadConfig){
         
         if (document.nativeLoad){
         document.nativeLoad(loadConfig.url, function(content){
                             loadConfig.content = content;
                             cms24k.replaceHtml(loadConfig);
                             } , function(){
                             cms24k.getHtml(loadConfig);
                             });
         
         }  else{
         
         var content  =   localStorage.getItem(loadConfig.url);
         if ( content === null ||  content === undefined ) {
         cms24k.getHtml(loadConfig);
         }else{
         loadConfig.content = content ;
         cms24k.replaceHtml(loadConfig);
         }
         }
         },
         
         backHtml: function(loadConfig,cache){
         var urls = cms24k.urls;
         if (urls.length  < 2){
         console.log("no cache return");
         return;
         }
         var  setting = urls[urls.length-2];
         loadConfig.currentUrl = urls[urls.length-1].url;
         loadConfig.top = setting.top;
         loadConfig.docTop = setting.docTop;
         loadConfig.url = setting.url
         loadConfig.title = setting.title
         
         
         if (cache){
         cms24k.loadCacheHtml(loadConfig);
         } else{
         //       loadConfig.url = urls[urls.length-2];
         cms24k.getHtml(loadConfig);
         }
         cms24k.urls.splice(urls.length-1,1);
         },
         loadHtml: function(loadConfig,cache){
         var urls = cms24k.urls;
         if (urls.length  > 0){
         urls[urls.length-1].top = $(loadConfig.container).scrollTop();
         urls[urls.length-1].docTop = $(document).scrollTop();
         urls[urls.length-1].title = $(".p-title").text();
         loadConfig.currentUrl = urls[urls.length-1].url;
         }
         if   (loadConfig.nohistory){
         cms24k.urls = [];
         }
         cms24k.urls.push(
                          {
                          url :loadConfig.url,
                          top : 0,
                          docTop : 0,
                          title  : loadConfig.title
                          }
                          );
         if (cache){
         cms24k.loadCacheHtml(loadConfig);
         
         }else{
         cms24k.getHtml(loadConfig);
         }
         },
         
         persisit: function(replaced  ,images, requesturl) {
         var total  = images.length;
         var count = 0;
         images.each(function(){
                     var this_ = $(this);
                     var url = $(this).attr("src");
                     cms24k.toDataUrl(url, function(base64Img) {
                                      this_.attr("src",base64Img);
                                      count =  count + 1;
                                      if (count == total){
                                      var x = replaced.outerHTML();
                                      document.nativeSave(requesturl,x);
                                      }
                                      });
                     })
         },
         
         getHtml : function (loadConfig) {
         var requesturl = loadConfig.url;
         if (!networkConnect){
         loadConfig.container.html(localStorage.getItem(loadConfig.url));
         try{
         // $(document).trigger("nativemsg","loadfromcache");
         }catch(err){
         
         }
         return;
         }
         $.ajax({
                timeout: 1000,
                url: requesturl,
                dataType: 'html',
                success: function(html,status,request) {
                var loadCache = false;  
                if ("jovx" == request.getResponseHeader("x-from")){
                  if (!html.trim()) {
                    loadCache = true;  // if content is empty , means not login logout
                  }else{
                        loadConfig.content = html;
                        cms24k.replaceHtml(loadConfig);
                        var z = loadConfig.container.parents(".modal:first");
                        if (z){
                          try{
                          $(z).modal('show');
                          }catch(e){
                            console.log(e);
                          }
                        }
                        if (document.nativeSave){
                          document.nativeSave(requesturl,html);
                        }
                        else{
                          localStorage.setItem(requesturl, html);
                        }
                       }
                  } else{
                      loadCache = true;
                    }
                if (loadCache){
                    var content
                    if (document.nativeLoad){
                    content  =  document.nativeLoad(requesturl,function(content){
                                                    loadConfig.content = content;
                                                    cms24k.replaceHtml(loadConfig);
                                                    },function(){
                                                        e = jQuery.Event("loadPageFailed" );
                                                        $(document).trigger(e,[loadConfig]);
                                                    });
                    } else{
                      var    content  =   localStorage.getItem(requesturl);
                      if (content){
                        loadConfig.content = loadConfig;
                        cms24k.replaceHtml(loadConfig);
                      }else{
                            e = jQuery.Event("loadPageFailed" );
                            $(document).trigger(e,[loadConfig]);
                      }
                    }
                } else{
                  // success load 
//                    e = jQuery.Event("sessionLost" );
//                    $(document).trigger(e,[loadConfig]);
                }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                $(document).trigger("nativemsg","request error , load from storage");
                var content
                if (document.nativeLoad){
                content  =  document.nativeLoad(requesturl,function(content){
                                                loadConfig.content = content;
                                                cms24k.replaceHtml(loadConfig);
                                                },function(){
                                                    e = jQuery.Event("loadPageFailed" );
                                                    $(document).trigger(e,[loadConfig]);
                                                });
                } else{
                var    content  =   localStorage.getItem(requesturl);
                 loadConfig.content = content;
                cms24k.replaceHtml(loadConfig);
                
                }
                
                }
                }
                )
         },
         process_success : function(data,form,callback) {
         if (data
             && data.state
             && (data.state
                 .indexOf("ERROR") != -1)) {
         alert(data.message);
         var x = $(form).find("img.captcha");
         if (x){
         x.attr('src','/captcha/?' + new Date().getTime());
         }
         if(!cms24k.isDef(typeof (grecaptcha))){
           try{
             grecaptcha.reset();
           }catch(e){
             console.log(e);
           }
         }
         } else {
         if (data.message){
         alert(data.message);
         }else{
         processForm(form,callback,data);
         if(!cms24k.isDef(typeof (grecaptcha))){
           try{
             grecaptcha.reset();
           }catch(e){
             console.log(e);
           }
         }
         var msg =form.attr("message")
         if (msg){
         if ("false"!=msg){
              alert(msg);
         }
         }else{
         alert('Your request has been processed successfully');
         }
         }
         }
         },
         templateEngine : function(html, options) {
         var re = /<%([^%>]+)?%>/g, reExp = /(^( )?(if|for|else|switch|case|break|{|}))(.*)?/g, code = 'var r=[];\n', cursor = 0;
         var add = function(line, js) {
          js? (code += line.match(reExp) ? line + '\n' : 'r.push(this.' + line + ');\n') :
         (code += line != '' ? 'r.push("' + line.replace(/"/g, '\\"') + '");\n' : '');
                                                         return add;
                                                         }
                                                         while(match = re.exec(html)) {
                                                         add(html.slice(cursor, match.index))(match[1], true);
                                                         cursor = match.index + match[0].length;
                                                         }
                                                         add(html.substr(cursor, html.length - cursor));
                                                         code += 'return r.join("");';
                                                         return new Function(code.replace(/[\r\t\n]/g, '')).apply(options);
                                                         }
                                                         });
          $.ajaxSetup({
                      
                      beforeSend: function(jqXHR, settings) {
                      settings.crossDomain =  true;
                      if (document.basic_url){
                      settings.url = document.basic_url+settings.url;
                      }
                      
                      if (document.request_for){
                      jqXHR.setRequestHeader('X-request-for', document.request_for);
                      }
                      },
                      cache: false ,
                      headers: {
                      "x-jovx-request":"ajax",
                      },
                      error: function (jqXHR, exception) {
                      if (exception === 'parsererror') {
                      alert('Requested JSON parse failed.');
                      } else if (exception === 'timeout') {
                      alert('Time out error.');
                      } else if (exception === 'abort') {
                        // alert('request aborted.');
                      }
                      },
                      statusCode: {
                      500: function (xhr) {
                      alert("Internal Server Error. \nPlease inform IT Support if it happends again.");
                      },
                      400: function (xhr) {
                      alert("Please verity data and try again");
                      },
                      401: function (xhr) {
                      alert("No permission to access that resource.");
                      }
                      }
                      
                      });
          
          function simple_submit(e,valid){
          if ($(this).attr("type")=='submit'){
          
          console.dir(e);
          
          if (e){
          e.preventDefault();
          }
          }
          
          
          
          var form =  $(this).parents("form:first");
          var action =  $(this).attr("action");
          if (!action){
          action  = form.attr('action');
          }
          if (true === valid){
          
          if (!validateForm(form)){
          alert("Please verify your data and try again.")
          return;
          }
          }
          
          var callback = $(this).attr("callback");
          var dt =  $(this).attr("datas");
          var enctype = $(form).attr("enctype");
          if( 'multipart/form-data' == enctype &&  !dt && typeof $.fn.ajaxSubmit == 'function'){
          var options = {
          dataType: 'json',
          success : function(data){
          cms24k.process_success(data,form,callback);
          }
          }
          $(form).ajaxSubmit(options);
          return
          }else{
          var formData;
          var forms = $(dt);
          if (dt){
          formData = $(dt).serialize();
          }else{
          formData =form.serialize()
          }
          $.ajax({
                 type : "POST",
                 url  : action,
                 data : formData,
                 dataType : "json",
                 success : function(data) {
                 cms24k.process_success(data,form,callback)
                 }
                 });
          }
          
                
          
          }
          
          function search(key,target) {
          if ('' != key) {
          if (target.substring(0,1) == '/'){
          target = target.substring(1);
          }
          if (cms24k.searchSimple){
          window.location = '/'+target + '/' + key+"/";
          }else{
          window.location = '/'+target + '/view/1/' + key;
          }
          
          }
          
          }
          function login(){
          
          var isForm = true;
          var form = $("#loginForm");
          if (form.length ==0){
          form = $(this).parents("form:first");
          }
          var data ;
          
          if($(form).get(0).tagName == 'DIV'){
          data  = $('#loginForm :input').serialize();
          }else{
          data  = form.serialize();
          }
          
          $
          .ajax({
                type : "POST",
                url : "/pub-member/login/",
                data : data,
                dataType : "json",
                success : function(data) {
                if (data
                    && data.state
                    && (data.state
                        .indexOf("ERROR") != -1)) {
                alert(data.message);
                } else {
                if (document.nativeLogin){
                document.nativeLogin(JSON.stringify(data.entity));
                }
                
                var reload =  (form).attr('reload');
                if (reload){
                location.reload();
                }else{
                try{
                
                //                alert(data.entity.id);
                //                internal.login(data.entity.id);
                //                return;
                }catch(e ){
                
                
                }
                
                if (typeof (loginURL) == 'undefined') {
                var redirectURL = (form).attr("url");
                if (redirectURL){
                window.location.href = redirectURL;
                }else{
                window.location.href = '/user-announce/';
                }
                
                } else {
                window.location.href = loginURL;
                }
                }
                }
                }
                });
          }
          function cleanFrom(form){
          form.find("input:not([type=checkbox],[type=button],[type=submit]),select,textarea").val("");
          form.find("input[type=checkbox]").prop('checked', false);
          }
          
          function showMessage(msg,form){
          var show="";
          if (form){
          var message = form.attr('message');
          if (message){
          show = message;
          }else{
          show = msg;
          }
          }else{
          show = msg;
          }
          alert(show);
          
          
          }
          
          function processForm(form,callback,data){
          var x =   $(form).find("img.captcha");
          if (x){
          x.attr('src','/captcha/?' + new Date().getTime());
          }
          
          if (form){
          var attr = form.attr('reload');
          if (attr){
          location.reload();
          return
          }
          
          attr = form.attr('redirect');
          if (attr){
          setTimeout(function(){
                     window.location.href= attr;
                     }, 2000);
          return
          }
          attr = form.attr('clean');
          if (attr){
          cleanFrom(form);
          }
          attr = form.attr('hide');
          if (attr){
          $(form).hide();
          $( "html,body").animate({ "scrollTop" : 0 }, 600);
          return;
          }}
          if (callback){
          attr = callback;
          }else{
          attr = form.attr("callback");
          }
          if (attr){
          var fn = window[attr];
          if(typeof fn === 'function') {
          if(!fn(form,this,data)){
          return;
          }
          }
          }
          }
          function isEmail(str){

          var reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;            
//          var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
          return reg.test(str);
          }
          function validateForm(form){
          if (form.data('bootstrapValidator')){
          var x = $(form).data('bootstrapValidator').validate();
          if(x.$invalidFields.length>0){
          return false
          }
          } else if (form.valid){
          var x = (form).valid();
          if (!x){
          return false;
          }
          }
          return true;
          }
          
          
          function reloadcapcha() {
          var url = '/captcha/?' + new Date().getTime();
          $('.captcha').attr('src', url);
          }
          $(document).ready(function(){
                            $("form").on("submit",function(e){
                                         e.preventDefault();
                                         var btn = $(this).find("button");
                                         // alert(btn.length);
                                         if (btn){
                                         
                                         if (  btn.hasClass("btnLogin")  ){
                                         login.apply(btn);
                                         }else{
                                         $(btn).trigger("click");
                                         }
                                         } else{
                                         alert("can not found button")
                                         }
                                         });
                            });
          
          $(document)
          .ready(
                 function() {
                 var  singapore = $('#memberCountry option:contains(Singapore)').attr("value");
                 $("#memberCountry").val(singapore);
                 $("#memberCountry").trigger("change");
                 $("#memberCountry").change(function() {
                                            var countryId = $(this).val();
                                            $.ajax({
                                                   url : "/pub-shopping-member/country-zone/"+countryId+"/list/",
                                                   type : "GET",
                                                   dataType : "json",
                                                   success : function(data){
                                                   if(data && data.state && (data.state.indexOf("ERROR") != -1)){
                                                   johnny.error(data.message);
                                                   return;
                                                   }else{
                                                   
                                                   var zone = $('#memberZone');
                                                   zone.empty();
                                                   $(data).each(function(step,value){
                                                                $('<option>').val(value.id).text(value.name).appendTo(zone);
                                                                });
                                                   var selectedId = $("#memberZone").attr("selectedid");
                                                   if (selectedId){
                                                   $("#memberZone").val(selectedId);
                                                   $("#memberZone").removeAttr("selectedid");
                                                   }
                                                   }
                                                   }});
                                            });
                 
                 
                 
                 
                 $("select.auto-country").each(function(step,value){
                                               if (countries){
                                               $(countries).each(function(s,v){
                                                                 
                                                                 $(value).append($('<option>', {
                                                                                   value: v.name,
                                                                                   text : v.name
                                                                                   }));
                                                                 
                                                                 
                                                                 });
                                               $(value).val("Singapore");
                                               
                                               }
                                               
                                               });
                 
                 $("select.auto-country-id").each(function(step,value){
                                                  if (countries){
                                                  var singaporeId;
                                                  $(countries).each(function(s,v){
                                                                    
                                                                    $(value).append($('<option>', {
                                                                                      value: v.id,
                                                                                      text : v.name
                                                                                      }));
                                                                    if (v.name == 'Singapore'){
                                                                    singaporeId = v.id;
                                                                    }
                                                                    });
                                                  if (singaporeId){
                                                  $(value).val(singaporeId);
                                                  }
                                                  
                                                  
                                                  }
                                                  
                                                  });
                 
                 $(document).delegate("textarea[maxlength][target]","keyup",function(){
                                      var words = $(this).val().length;
                                      var max = $(this).attr("maxlength");
                                      if (words > max) {
                                      $(this).val(val.value.substring(0, max));
                                      }else {
                                      $($(this).attr("target")).text((max-words)+ "characters left");
                                      }
                                      });
                 if (cms24k.living()) {
                   
                $(document).on("click",".open_target",function(e){
                  if (document.nativeOpen){
                    e.preventDefault()  
                    var link = $(this).attr("href");
                    document.nativeOpen(link);
                  }
                }); 
                   
                 if ($.datepicker) {
                 $(".datepicker").each(function(){
                                       var pattern = 'dd/mm/yy';
                                       var this_pattern = $(this).attr('pattern');
                                       var month = ("true" == $(this).attr('month'))
                                       var year  =  ("true" == $(this).attr('year'));
                                       if (this_pattern){
                                       pattern = this_pattern
                                       }
                                       $(this).datepicker({
                                                          dateFormat : pattern,
                                                          showOtherMonths : true,
                                                          changeMonth: month,
                                                          changeYear: year
                                                          });
                                       
                                       
                                       });
                 }
                 if ($.fn.datetimepicker){
                 $(".datetimepicker").each(function(){
                                           var pattern = 'dd/mm/yy';
                                           var this_pattern = $(this).attr('pattern');
                                           if (this_pattern){
                                           pattern = this_pattern
                                           }
                                           $(this).datetimepicker({
                                                                  dateFormat : pattern,
                                                                  showOtherMonths : true
                                                                  });
                                           });
                 } 
                 $('.newsletter').click(function(e){
                                        console.log(e);
                                        e.preventDefault();
                                        var email = $('#newsletter-email');
                                        var value = email.val();
                                        if (isEmail(value)){
                                        var submit = {
                                        url : '/pub-news-letter/subscribe/',
                                        type : "POST",
                                        dataType : "json",
                                        async: false,
                                        data  : {email : email.val()} ,
                                        success : function(data) {
                                        if (data && data.state && (data.state.indexOf("ERROR") != -1)) {
                                        alert(data.message);
                                        return;
                                        } else {
                                        email.val("");
                                        alert('You are subscribed!');
                                        }
                                        }
                                        }
                                        $.ajax(submit);
                                        }else{
                                        alert('Please enter a valid email');
                                        email.focus();
                                        }
                                        
                                        
                                        });
                 $('.searchBtn').each(function(index, value) {
                                      var input =$(this).siblings('input:text');
                                      var target = $(this).attr('target');
                                      if (!target){
                                      target = 'search';
                                      }
                                      if (input.length ==0){
                                      input = $($(this).attr("search-text"));
                                      } 
                                      input.keypress(function(event) {
                                                     
                                                     var keycode = (event.keyCode ? event.keyCode : event.which);
                                                     if (keycode == '13') {
                                                     search(input.val(),target);
                                                     }
                                                     
                                                     });
                                      $(this).click(function(e) {
                                                    e.preventDefault();
                                                    
                                                    search(input.val(),target);
                                                    
                                                    });
                                      }); 
                 
                 $('.reset-form').click(function(){
                                        var form = $(this).parents('form:first');
                                        cleanFrom(form);  
                                        });
                 
                 $('.commentbtn').click(function(){
                                        var form = $(this).parents('form:first');
                                        if(!validateForm(form)){
                                        return;
                                        }
                                        var postId = form.attr('postid');
                                        var submit = {
                                        url : '/pub-blog/'+postId+'/comment/',
                                        type : "POST",
                                        dataType : "json",
                                        async: false,
                                        data  : form.serialize() ,
                                        success : function(data) {
                                        if (data && data.state && (data.state.indexOf("ERROR") != -1)) {
                                        alert(data.message);
                                        return;
                                        } else {
                                        cleanFrom(form);
                                        showMessage('thanks for your common',form);
                                        }
                                        }
                                        }
                                        $.ajax(submit);
                                        
                                        });
                 
                 
                 $('.feedback').click(function() {
                                      
                                      
                                      
                                      var this_ = this;
                                      var form = $(this)
                                      .parents("form:first");
                                      if (!validateForm(form)){
                                      return;
                                      }
                                      
                                      var files = form.find("input[type = 'file']");
                                      var hasfile = files.length > 0;
                                      if (hasfile) {
                                      if (form.find('input[name="no_remove"]').length == 0){
                                      $('<input type="hidden" name="no_remove" value="true"/>').appendTo(form);                     
                                      }
                                      
                                      $io = $('<iframe />');
                                      $io.appendTo("body");
                                      $io.css({ position: 'absolute', top: '-1000px', left: '-1000px' });
                                      
                                      
                                      var picform = $('<form enctype="multipart/form-data" method="post" />');
                                      setTimeout(function(){
                                                 picform.appendTo($io.contents().find("body"));                 
                                                 
                                                 
                                                 
                                                 },150);
                                      
                                      $(files).each(
                                                    function(step, value) {
                                                    $(value).clone().removeAttr("name").insertBefore($(value));
                                                    $(value).attr("name","img-"+step);
                                                    $(value).appendTo(picform);
                                                    $("<input name='size'/>").val(files.length).appendTo(picform);
                                                    $("<input name='captcha'/>").val(form.find("input[name = 'captcha']").val()).appendTo(picform);
                                                    });                     
                                      }
                                      
                                      setTimeout(function(){
                                                 
                                                 var submit = {
                                                 url : '/pub-feed/feedback/',
                                                 type : "POST",
                                                 dataType : "json",
                                                 data : form.serialize(),
                                                 success : function(data) {
                                                 if (data
                                                     && data.state
                                                     && (data.state
                                                         .indexOf("ERROR") != -1)) {
                                                 alert(data.message);
                                                 processForm(form);
                                                 // reloadcapcha();
                                                 return;
                                                 } else {
                                                 if (hasfile){
                                                 picform.attr('action','/pub-feed/'+data.entity.id+'/image/');
                                                 $io.bind('load', function(){
                                                          alert('thanks for your enquiry');
                                                          processForm(form);
                                                          });
                                                 picform.submit();
                                                 }else{
                                                 reloadcapcha();
                                                 processForm(form);
                                                 alert('thanks for your enquiry');
                                                 
                                                 }
                                                 
                                                 
                                                 
                                                 }
                                                 }}
                                                 
                                                 $.ajax(submit);
                                                 
                                                 
                                                 },200);
                                      
                                      
                                      
                                      
                                      
                                      });
                 
                 
                 
                 
                 
                 
                 $(document).delegate('input:checkbox[value=true],input:checkbox[value=false]','click',
                                      function() {
                                      var val = $(this).val();
                                      
                                      if ($(this).is(':checked')){
                                      $(this).val("true");
                                      var name = $(this).attr("name");
                                      var xx= $(this).siblings("[name="+name+"]")
                                      xx.remove();
                                      
                                      } else{
                                      $(this).val("false");
                                      var hidden =  $("<input>",{
                                                      type :'hidden',
                                                      name : $(this).attr("name")
                                                      }).val("false");
                                      hidden.insertAfter($(this));
                                      }
                                      });
                 
                 
                 
                 
                 
                 $(document).delegate(".simple-submit","click",function(e){
                                      simple_submit.apply(this,[e,true]);
                                      });
                 
                 
                 $('#confirm[type=button] ,.resetpwd[type=button]').click(function(){
                                                                          if ($('#new').val()!= $('#pwd').val()){
                                                                          alert('password not same');
                                                                          return
                                                                          }
                                                                          var token =$("#token").val();
                                                                          
                                                                          var submit = {
                                                                          url : '/pub-member/confirm/'+token+'/',
                                                                          type : "POST",
                                                                          dataType : "json",
                                                                          data  : {'password': $('#pwd').val()} ,
                                                                          success : function(data) {
                                                                          if (data && data.state && (data.state.indexOf("ERROR") != -1)) {
                                                                          alert(data.message);
                                                                          return;
                                                                          } else {
                                                                          alert('reset password successfully');
                                                                          windows.location.href= '/';
                                                                          }
                                                                          }
                                                                          };
                                                                          $.ajax(submit);
                                                                          })
                 
                 $(document).delegate('.btnLogin','click',  function(e) {
                                      e.preventDefault();
                                      login.apply(this);
                                      });
                 $('.forget')
                 .click(
                        function() {
                        if (typeof (check) === 'function') {
                        var l = check();
                        if (!l) {
                        return;
                        }
                        }
                        var form = $("#forgetForm");
                        if (form.length ==0){
                        
                        form = $(this).parents("form:first");
                        }
                        if (form.valid){
                        if (!form.valid()){
                        return;
                        
                        }
                        }
                        
                        $
                        .ajax({
                              type : "POST",
                              url : "/pub-member/forget/",
                              data : form
                              .serialize(),
                              dataType : "json",
                              success : function(data) {
                              if (data
                                  && data.state
                                  && (data.state
                                      .indexOf("ERROR") != -1)) {
                              alert(data.message);
                              } else {
                              alert('An email already sent to your email address for reset password.');
                              }
                              }
                              });
                        });
                 
                 $(document).delegate('.logout','click',function() {
                                      $
                                      .ajax({
                                            type : "POST",
                                            url : "/pub-member/logout/",
                                            data : $("#loginForm")
                                            .serialize(),
                                            dataType : "json",
                                            success : function(data) {
                                            
                                            if (data  && data.state   && (data.state
                                                                                .indexOf("ERROR") != -1)) {
                                            alert(data.message);
                                            } else {
                                            
                                            if (document.nativeLogout){
                                            window.location.href =    document.nativeLogout();                      
                                            }else{
                                            window.location.href = '/';
                                            }
                                            }
                                            }
                                            });
                                      });
                 
                 
                 
                 
                 $('.formBtn')
                 .click(
                        function() {
                        var this_ = this;
                        
                        var form = $(".cms_form");
                        if (form.length ==0){
                        
                        form = $(this).parents("form:first");
                        }
                        
                        var submit = {
                        url : '/pub-feed/feedback/',
                        type : "POST",
                        dataType : "json",
                        data : form.serialize(),
                        success : function(data) {
                        if (data
                            && data.state
                            && (data.state
                                .indexOf("ERROR") != -1)) {
                        alert(data.message);
                        reloadcapcha();
                        return;
                        } else {
                        reloadcapcha();
                        alert('thanks for your enquiry');
                        $(this_).attr(
                                      'disabled',
                                      'disabled');
                        }
                        }
                        };
                        $.ajax(submit);
                        
                        });
                 
                 $(document).delegate('.captcha','click',function() {
                                      var url = '/captcha/?' + new Date().getTime();
                                      $(this).attr('src', url);
                                      })
                 
                 $('input.input_radio:text').focus(function(val) {
                                                   var container = $(this).siblings().last();
                                                   var radio = $(container).find("input:radio");
                                                   var name = radio.attr('name');
                                                   name = (name.replace(".", "\\."));
                                                   var $radios = $('input:radio[name=' + name + ']');
                                                   $($radios).each(function(index, val) {
                                                                   var v = $(val).attr('value');
                                                                   if (v == 'Other') {
                                                                   $(val).prop('checked', true);
                                                                   } else {
                                                                   $(val).prop('checked', false);
                                                                   }
                                                                   });
                                                   
                                                   });
                 }
                 });