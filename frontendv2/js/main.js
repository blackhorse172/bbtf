/* Thanks to CSS Tricks for pointing out this bit of jQuery
http://css-tricks.com/equal-height-blocks-in-rows/
It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */

equalheight = function(container){

	var currentTallest = 0,
		 currentRowStart = 0,
		 rowDivs = new Array(),
		 $el,
		 topPosition = 0;
	 $(container).each(function() {
	
	   $el = $(this);
	   $($el).height('auto')
	   topPostion = $el.position().top;
	
	   if (currentRowStart != topPostion) {
		 for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
		   rowDivs[currentDiv].height(currentTallest);
		 }
		 rowDivs.length = 0; // empty the array
		 currentRowStart = topPostion;
		 currentTallest = $el.height();
		 rowDivs.push($el);
	   } else {
		 rowDivs.push($el);
		 currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
	  }
	   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
		 rowDivs[currentDiv].height(currentTallest);
	   }
	 });
	}
	
	if( $(window).width() > 767 ) {
		$(window).load(function() {
		  equalheight('.same-height>div');
		});
		$(window).resize(function(){
		  equalheight('.same-heights>div');
		});
	}

	
"use strict";
var lastScroll = 0;

//check for browser os
var isMobile = false;
var isiPhoneiPad = false;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    isMobile = true;
}

if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
    isiPhoneiPad = true;
}

function SetMegamenuPosition() {
    if ($(window).width() > 991) {
        setTimeout(function () {
            var totalHeight = $('nav.navbar').outerHeight();
            $('.mega-menu').css({top: totalHeight});
            if ($('.navbar-brand-top').length === 0)
                $('.dropdown.simple-dropdown > .dropdown-menu').css({top: totalHeight});
        }, 200);
    } else {
        $('.mega-menu').css('top', '');
        $('.dropdown.simple-dropdown > .dropdown-menu').css('top', '');
    }
}

function pad(d) {
    return (d < 10) ? '0' + d.toString() : d.toString();
}

function isIE() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
    {
        return true;
    } else  // If another browser, return 0
    {
        return false;
    }
}

//page title space
function setPageTitleSpace() {
    if ($('.navbar').hasClass('navbar-top') || $('nav').hasClass('navbar-fixed-top')) {
        if ($('.top-space').length > 0) {
            var top_space_height = $('.navbar').outerHeight();
            if ($('.top-header-area').length > 0) {
                top_space_height = top_space_height + $('.top-header-area').outerHeight();
            }
            $('.top-space').css('margin-top', top_space_height + "px");
        }
    }
}



/*  smoothscroll */
$('a.scroll-more').click(function(event) {
	event.preventDefault();
	var target = $(this).prop('hash');
	if( $(window).width() > 768 ) {
		$('html, body').animate({
			scrollTop: $(target).offset().top
		}, 500);
	}
	else {
		$('html, body').animate({
			scrollTop: $(target).offset().top
		}, 500);
	}
});

/*  Multilevel */
if( $(window).width() < 769 ) {
$(".dropdown-menu > li.trigger > a").on("click",function(e){
	var current=$(this).next();
	var grandparent=$(this).parent().parent();
	grandparent.find(".sub-menu:visible").not(current).hide();
	current.toggle();
	e.stopPropagation();
	$(".dropdown-menu > li.trigger > a").removeClass();
	$(this).addClass("open");
});
$(".dropdown-menu > li:not(.trigger) > a").on("click",function(){
	var root=$(this).closest('.dropdown');
	root.find('.sub-menu:visible').hide();
	
});
}

/*  Inview */
if( $(window).width() > 769 ) {
$(function(){
 	$('[data-class-in]').each(function(){
		$(this).inview({ });
	});
});
}

/*==============================================================
     portfolio filter
     ==============================================================*/
	 var $portfolio_filter = $('.portfolio-grid');
	 $portfolio_filter.imagesLoaded(function () {
		 $portfolio_filter.isotope({
			 layoutMode: 'masonry',
			 itemSelector: '.grid-item',
			 percentPosition: true,
			 masonry: {
				 columnWidth: '.grid-sizer'
			 }
		 });
		 $portfolio_filter.isotope();
	 });
	 var $grid_selectors = $('.portfolio-filter > li > a');
	 $grid_selectors.on('click', function () {
		 $grid_selectors.parent().removeClass('active');
		 $(this).parent().addClass('active');
		 var selector = $(this).attr('data-filter');
		 $portfolio_filter.find('.grid-item').removeClass('animated').css("visibility", ""); // avoid problem to filter after sorting
		 $portfolio_filter.find('.grid-item').each(function () {
			 /* remove perticular element from WOW array when you don't want animation on element after DOM lead */
			 
			 $(this).css("-webkit-animation", "none");
			 $(this).css("-moz-animation", "none");
			 $(this).css("-ms-animation", "none");
			 $(this).css("animation", "none");
		 });
		 $portfolio_filter.isotope({filter: selector});
		 return false;
	 });
	 $(window).resize(function () {
		 if (!isMobile && !isiPhoneiPad) {
			 $portfolio_filter.imagesLoaded(function () {
				 setTimeout(function () {
					 $portfolio_filter.find('.grid-item').removeClass('wow').removeClass('animated'); // avoid problem to filter after window resize
					 $portfolio_filter.isotope('layout');
				 }, 300);
			 });
		 }
	 });
	 var $blog_filter = $('.blog-grid');
	 $blog_filter.imagesLoaded(function () {
		 $blog_filter.isotope({
			 layoutMode: 'masonry',
			 itemSelector: '.grid-item',
			 percentPosition: true,
			 masonry: {
				 columnWidth: '.grid-sizer'
			 }
		 });
	 });
	 $(window).resize(function () {
		 setTimeout(function () {
			 $blog_filter.find('.grid-item').removeClass('wow').removeClass('animated'); // avoid problem to filter after window resize
			 $blog_filter.isotope('layout');
		 }, 300);
	 });
 
	 /*==============================================================
     lightbox gallery
     ==============================================================*/
	 $('.lightbox-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-fade',
        fixedContentPos: true,
        closeBtnInside: false,
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        }
    });
    /* for group gallery */
    var lightboxgallerygroups = {};
    $('.lightbox-group-gallery-item').each(function () {
        var id = $(this).attr('data-group');
        if (!lightboxgallerygroups[id]) {
            lightboxgallerygroups[id] = [];
        }
        lightboxgallerygroups[id].push(this);
    });
    $.each(lightboxgallerygroups, function () {
        $(this).magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            closeBtnInside: false,
            gallery: {enabled: true}
        });
    });

    $('.lightbox-portfolio').magnificPopup({
        delegate: '.gallery-link',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-fade',
        fixedContentPos: true,
        closeBtnInside: false,
        gallery: {
            enabled: true,
            navigateByImgClick: false,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        }
    });
    /*==============================================================
     single image lightbox - zoom animation
     ==============================================================*/
    $('.single-image-lightbox').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        fixedContentPos: true,
        closeBtnInside: false,
        mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
        image: {
            verticalFit: true
        },
        zoom: {
            enabled: true,
            duration: 300 // don't foget to change the duration also in CSS
        }
    });
    /*==============================================================
     zoom gallery
     ==============================================================*/
    $('.zoom-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        fixedContentPos: true,
        closeBtnInside: false,
        image: {
            verticalFit: true,
            titleSrc: function (item) {
                return item.el.attr('title');
            }
        },
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function (element) {
                return element.find('img');
            }
        }
    });
    /*==============================================================*/
    //Modal popup - START CODE
    /*==============================================================*/
    $('.modal-popup').magnificPopup({
        type: 'inline',
        preloader: false,
        // modal: true,
        blackbg: true,
        callbacks: {
            open: function () {
                $('html').css('margin-right', 0);
            }
        }
    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

    /*==============================================================*/
    //Modal popup - zoom animation - START CODE
    /*==============================================================*/
    $('.popup-with-zoom-anim').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        blackbg: true,
        mainClass: 'my-mfp-zoom-in'
    });

    $('.popup-with-move-anim').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        blackbg: true,
        mainClass: 'my-mfp-slide-bottom'
    });
/*==============================================================
     ajax magnific popup for onepage portfolio
     ==============================================================*/
	 $('.ajax-popup').magnificPopup({
        type: 'ajax',
        alignTop: true,
        fixedContentPos: true,
        overflowY: 'scroll', // as we know that popup content is tall we set scroll overflow by default to avoid jump
        callbacks: {
            open: function () {
                $('.navbar .collapse').removeClass('show');
                $('.navbar a.dropdown-toggle').addClass('collapsed');
            }
        }
    });

    $(document).on('ready', function() {
        $(".lazy").slick({
            lazyLoad: 'ondemand', // ondemand progressive anticipated
            infinite: true,
            dots: true,
            arrows: false,
            autoplay: true,
            pauseOnFocus: true,
        customPaging: function(slick,index) {
            return '<a>' + (index + 1) + '</a>';
        }
          });
          $(".regular").slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            arrows: true,
            responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 400,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }]
          });
          $(".testimony").slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            arrows: true,
            responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 400,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }]
          });
          $(".comm").slick({
            infinite: true,
            autoplay: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 400,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }]
          });
        });